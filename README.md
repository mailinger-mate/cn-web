# Connect - WebClient

## Development

### Requirements

Below you can find list of packages and services you need to have to build and run your copy of WebClient application.

#### Packages

##### PHP backend:

* PHP (>=5.5.9) with extensions:
    * mbstring
    * intl
    * opcache
    * mcrypt
    * pdo
    * mysql
    * pdo_mysql
* Composer

##### JS, CSS frontend:

* sass
* compass
* bower

#### Services

##### Database

For database application use MySQL. By default it try to connect to db at:

* host: **db.dev**
* user: **cn**
* password: **cn**
* schema: **cn**

You can change this in ``app/config/parameters.yml``

##### UMS

To provide users for authorization WebClient uses external service [User Management Service](https://git.int.x-formation.com/projects/UMS/repos/ums-core/browse) accessible at: `http://ums.dev:8081`

You can change this in ``app/config/parameters.yml``

##### Core

[Core](https://git.int.x-formation.com/projects/CN/repos/cn-core/browse) is a backend to manage Connections and Accounts. WebClient use `http://core.dev:8082` endpoint to access Core.

You can change this in ``app/config/parameters.yml``

#### Build

First download dependencies

```bash
$ composer install
$ bower install
```

now you can build

```bash
$ bin/phing build
```

#### Run

Before first run and after any change to database you have to run doctrine migrations to make your shcema up to date. If you do not have database yet you can create one with

```bash
$ app/console doctrine:database:create
```

and migrate it

```bash
$ app/console doctrine:migrations:migrate
```

you are ready to run application using PHP built-in server

```bash
# app/console server:start localhost:8080
```

to use port **80** you need to have root privileges.

#### Run with Docker

For your convince there is docker configuration you can use to run WebClient. Using it you do not have to worry about database or UMS service. Make sure you have latest version of **docker-engine** and **docker-compose**.

Build containers

```bash
$ docker-compose build
```

###### First run

Because of common problem with slow starting services in docker (https://github.com/docker-library/mysql/issues/81) run databases first

```bash
$ docker-compose up -d ums-db cn-core-db db
```

it needs a few seconds to initialize db for the first time. Start Core and UMS

```bash
$ docker-compose up -d ums-web cn-core
```

rebuild and migrate database

```bash
$ docker-compose run web bin/phing rebuild
$ docker-compose run web app/console doctrine:migrations:migrate
```

start WebClient

```bash
$ docker-compose up -d web
```

>**Note**: You need to run commands dependent on cache through container. Just prepend what you usually use with *docker-compose run web*. For example ``docker-compose run web bin/phing watch``

###### Continuous run
To start and stop environment you can use respectively ``docker-compose start`` and ``docker-compose stop``

##### Pitfalls using docker

1. everything created in docker is owned by *root* and you need gain sufficient privileges to remove **logs** or **cache** from your host.
1. there might be a problem with phing test target because different paths in cache

## Testing

### JS - unitests

It's running JS unitests using Karma on listed browsers:

* PhantomJS
* Firefox

Test results are stored per browser in:

```bash
/path/to/cn-web/tests/results/js/unit/{browser}/karma-output.xml
```

Test coverage html report (if enabled in karma config) is stored per browser in:

```bash
/path/to/cn-web/tests/results/js/unit/coverage/{browser}
```

#### Run tests using localhost libraries

##### Requirements

* apt libraries:
    * nodejs
    * nodejs-legacy
    * npm
    * libfreetype6
    * libfreetype6-dev
    * libfontconfig1
    * libfontconfig1-dev
    * fonts-ipafont-gothic
    * xfonts-100dpi
    * xfonts-75dpi
    * xfonts-cyrillic
    * xfonts-scalable
    * ttf-ubuntu-font-family
    * firefox

* nmp libraries:
    * jasmine-core
    * jasmine
    * phantomjs
    * karma
    * karma-coverage
    * karma-phantomjs-launcher
    * karma-firefox-launcher
    * karma-jasmine
    * karma-qunit
    * karma-junit-reporter
    * mocks

##### Run tests

```bash
bin/phing test:js:unit
```

#### Run tests using docker container

##### Requirements

* install docker engine: https://docs.docker.com/engine/installation/ubuntulinux/

##### Run tests

```bash
docker pull docker.int.x-formation.com:443/karma-(phantomjs|firefox)
docker run -v /path/to/cn-web:/path/to/cn-web -w /path/to/cn-web/tests/js/unit docker.int.x-formation.com:443/karma-(phantomjs|firefox) karma start --single-run
```

If you want to use continuous test running while files changes, do not use ```--single-run``` flag.
To cancel such a run, you will have to stop the container with:

```bash
docker stop {{container_id}}
```