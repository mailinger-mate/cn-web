<?php

namespace Connect\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160112122650 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            /** @lang MySQL */
            'CREATE TABLE `sessions` (
              `sess_id`       VARBINARY(128)   NOT NULL PRIMARY KEY,
              `sess_data`     BLOB             NOT NULL,
              `sess_time`     INTEGER UNSIGNED NOT NULL,
              `sess_lifetime` MEDIUMINT        NOT NULL
            )
              COLLATE utf8_bin, ENGINE = InnoDB;'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE `sessions`');
    }
}
