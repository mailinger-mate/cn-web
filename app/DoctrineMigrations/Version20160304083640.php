<?php

namespace Connect\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160304083640 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(/** @lang MySQL */
            'CREATE TABLE post (
              id           INT AUTO_INCREMENT NOT NULL,
              title        VARCHAR(255)       NOT NULL,
              intro        LONGTEXT DEFAULT NULL,
              content      LONGTEXT           NOT NULL,
              author       VARCHAR(255)       NOT NULL,
              publish_date DATETIME           NOT NULL,
              slug         VARCHAR(255)       NOT NULL,
              created      DATETIME           NOT NULL,
              updated      DATETIME           NOT NULL,
              UNIQUE INDEX slug_value_unique_idx (slug),
              PRIMARY KEY (id)
            )
              DEFAULT CHARACTER SET utf8
              COLLATE utf8_unicode_ci
              ENGINE = InnoDB'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE post');
    }
}
