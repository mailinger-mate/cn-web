<?php

return array(
    'company'  => array(
        'name' => 'X-Formation',
    ),
    'product'  => array(
        'name' => 'Connect',
        'title'   => "Connect | X-Formation"
    ),
    'button'   => array(
        'register'       => 'Register',
        'login'          => 'Sign In',
        'continue'       => 'Continue',
        'back'           => 'Back',
        'reset_password' => 'Reset Password',
        'learn_more'     => 'Learn more'
    ),
    'terms' => array(
        'support' => array(
            'url'  => 'mailto:support@x-formation.com?subject=Connect',
            'text' => 'Support'
        ),
        'privacy' => array(
            'url'  => 'https://www.x-formation.com/company/privacy-statement',
            'text' => 'Privacy Statement'
        ),
        'terms' => array(
            'url'  => 'https://www.x-formation.com/company/terms-of-service',
            'text' => 'Terms of Service'
        ),
        'documentation' => array(
            'url'  => 'https://docs.x-formation.com/display/CN/Connect+Documentation',
            'text' => 'Documentation'
        ),
        'feedback' => array(
            'url'  => 'https://feedback.x-formation.com/list/36372-general/?category=20532',
            'text' => 'Feedback'
        ),
        'about' => array(
            'url'  => 'https://www.x-formation.com/company/about-us',
            'text' => 'About Us'
        )
    ),
    'common' => array(
        'and' => 'and'
    ),
    'error' => array(
        'generic' => array(
            'code'  => 'OH!',
            'title' => 'An Error Occurred',
            'text'  => 'Something went wrong and we will fix it as soon as possible. If you keep receiving this message, please contact us immediately.',
            'link'  => 'Return to Connect'
        ),
        '404' => array(
            'code'  => '404',
            'title' => 'Page Not Found',
            'text'  => 'Sorry, but the page you are looking for has not been found. Make sure the requested URL is correct, then hit the refresh button on your browser or return to the main page.',
            'link'  => 'Return to Connect'
        )
    ),
    'meta' => array(
        'keywords'    => 'connect, business apps, saas, automation, integrations, syncing',
        'description' => 'Connect your apps and services to automate your workflow and boost your productivity.'
    )
);
