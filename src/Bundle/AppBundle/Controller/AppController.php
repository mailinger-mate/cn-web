<?php

namespace Connect\Bundle\AppBundle\Controller;

use Connect\Bundle\AppBundle\Feedback\TokenFactory;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class AppController
 */
class AppController
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var TokenStorageInterface
     */
    private $coreTokenStorage;
    /**
     * @var string
     */
    private $coreApiUrl;
    /**
     * @var string
     */
    private $coreRpcUrl;
    /**
     * @var string
     */
    private $feedbackHost;
    /**
     * @var string
     */
    private $feedbackForum;
    /**
     * @var string
     */
    private $feedbackCategory;
    /**
     * @var TokenFactory
     */
    private $feedbackTokenFactory;

    /**
     * @param EngineInterface $templating
     * @param TokenStorageInterface $coreTokenStorage
     * @param TokenFactory $feedbackTokenFactory
     * @param string $coreApiUrl
     * @param string $coreRpcUrl
     * @param string $feedbackHost
     * @param string $feedbackForum
     * @param string $feedbackCategory
     * @param $stripePublishableKey
     */
    function __construct(
        EngineInterface $templating,
        TokenStorageInterface $coreTokenStorage,
        TokenFactory $feedbackTokenFactory,
        $coreApiUrl,
        $coreRpcUrl,
        $feedbackHost,
        $feedbackForum,
        $feedbackCategory,
        $stripePublishableKey
    ) {
        $this->templating = $templating;
        $this->coreTokenStorage = $coreTokenStorage;
        $this->feedbackTokenFactory = $feedbackTokenFactory;
        $this->coreApiUrl = $coreApiUrl;
        $this->coreRpcUrl = $coreRpcUrl;
        $this->feedbackHost = $feedbackHost;
        $this->feedbackForum = $feedbackForum;
        $this->feedbackCategory = $feedbackCategory;
        $this->stripePublishableKey = $stripePublishableKey;
    }

    /**
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function showAction(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('ums_session')) {
            $this->coreTokenStorage->setToken(null);
            $session->invalidate();
            throw new AuthenticationException();
        }

        /** @var UMSUser $user */
        $user = $this->coreTokenStorage->getToken()->getUser();
        $contact = $user->getPrimaryContact();

        $umsSessionId = $session->get('ums_session');
        $jsConfig = [
            'user'  =>  [
                'id'        =>  $user->getId(),
                'name'      =>  $user->getName(),
                'role'      =>  $user->getBaseRole(),
                'vendor'    =>  $user->getVendor()->getName(),
                'email'     =>  [
                    'id'        => $contact->getId(),
                    'address'   => $contact->getValue(),
                    'verified'  => $contact->isVerified()
                ],
                'core'  =>  [
                    'token'     => $umsSessionId
                ],
                'feedback'  =>  [
                    'token'     =>  $this->feedbackTokenFactory->create($user)
                ]
            ],
            'core'  =>  [
                'api_url'   => $this->coreApiUrl,
                'rpc_url'   => $this->coreRpcUrl
            ],
            'feedback'  =>  [
                'host'      =>  $this->feedbackHost,
                'forum'     =>  $this->feedbackForum,
                'category'  =>  $this->feedbackCategory
            ],
            'stripe' => [
                'publishable_key' => $this->stripePublishableKey
            ]
        ];

        return $this->templating->renderResponse('ConnectAppBundle:App:show.html.twig', ['js_config' => $jsConfig]);
    }
}
