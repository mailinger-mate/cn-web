<?php

namespace Connect\Bundle\AppBundle\Controller;

use Connect\Bundle\AppBundle\Repository\CoreBillingRepository;
use Connect\Bundle\AppBundle\Routing\AppUrlGenerator;
use GuzzleHttp\Exception\ClientException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BillingController
 */
class BillingController
{
    /**
     * @var CoreBillingRepository
     */
    private $coreBillingRepository;
    /**
     * @var AppUrlGenerator
     */
    private $appUrlGenerator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CoreBillingRepository $coreBillingRepository
     * @param AppUrlGenerator $appUrlGenerator
     * @param LoggerInterface $logger
     */
    function __construct(
        CoreBillingRepository $coreBillingRepository,
        AppUrlGenerator $appUrlGenerator,
        LoggerInterface $logger
    ) {
        $this->coreBillingRepository = $coreBillingRepository;
        $this->appUrlGenerator = $appUrlGenerator;
        $this->logger = $logger;
    }
    
    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param int $invoiceId
     * @return Response
     */
    public function getInvoiceAction($invoiceId)
    {
        try {
            $invoice = $this->coreBillingRepository->find($invoiceId);
            $content = $invoice->getPdfContent();

            $response = new Response($content);
            $response->headers->add([
                'Content-Type'          => 'application/pdf',
                'Cache-Control'         => 'public',
                'Content-Disposition'   => sprintf('inline; filename="%s"', $invoice->getPdfName()),
                'Content-Length'        => strlen($content)
            ]);

            return $response;
        }
        catch (ClientException $ex) {
            if ($ex->getResponse()->getStatusCode() !== 404) {
                $this->logger->error($ex);
            }

            throw new NotFoundHttpException();
        }
    }
}