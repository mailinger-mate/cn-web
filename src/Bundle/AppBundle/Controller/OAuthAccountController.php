<?php

namespace Connect\Bundle\AppBundle\Controller;

use Connect\Bundle\AppBundle\Form\Type\TypeFactory;
use Connect\Bundle\AppBundle\OAuth\App\AppException;
use Connect\Bundle\AppBundle\OAuth\App\AppInterface;
use Connect\Bundle\AppBundle\Repository\CoreAccountRepository;
use Connect\Bundle\AppBundle\Routing\AppUrlGenerator;
use Connect\Bundle\UMSSecurityBundle\Controller\JsonController;
use Connect\Bundle\UMSSecurityBundle\Form\MessageBuilder;
use Connect\Domain\Account;
use Exception;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LogicException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OAuthAccountController
 */
class OAuthAccountController extends JsonController
{
    /**
     * @var CoreAccountRepository
     */
    private $coreAccountRepository;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var MessageBuilder
     */
    private $messageBuilder;
    /**
     * @var AppUrlGenerator
     */
    private $appUrlGenerator;
    /**
     * @var JsonEncoder
     */
    private $jsonEncoder;

    /**
     * @param FormFactoryInterface $formFactory
     * @param CoreAccountRepository $coreAccountRepository
     * @param MessageBuilder $messageBuilder
     * @param AppUrlGenerator $appUrlGenerator
     * @param JsonEncoder $jsonEncoder
     * @param TranslatorInterface $translator
     */
    function __construct(
        FormFactoryInterface $formFactory,
        CoreAccountRepository $coreAccountRepository,
        MessageBuilder $messageBuilder,
        AppUrlGenerator $appUrlGenerator,
        JsonEncoder $jsonEncoder,
        TranslatorInterface $translator
    ) {
        parent::__construct($translator);
        $this->coreAccountRepository = $coreAccountRepository;
        $this->formFactory = $formFactory;
        $this->messageBuilder = $messageBuilder;
        $this->appUrlGenerator = $appUrlGenerator;
        $this->jsonEncoder = $jsonEncoder;
    }

    /**
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     *
     * @param Request $request
     * @param AppInterface $app
     * @param int|null $accountId
     * @return RedirectResponse
     */
    public function ajaxAuthorizeAction(Request $request, AppInterface $app, $accountId = null)
    {
        try {
            $account = $this->getAccountInstance($app, $accountId);

            $form = $this->formFactory->create(
                TypeFactory::create($app->getType())
            );

            $form->submit(
                $this->jsonEncoder->decode($request->getContent(), JsonEncoder::FORMAT)
            );

            if ($form->isValid()) {
                $app->populateAccount($account, $form->getData());
                $app->initializeCallback($account);
                $request->getSession()->set('app_oauth_account', $account);
                $request->getSession()->set('app_return_url', $request->get('return_url', 'account/list'));

                return $this->jsonResponse(true, ['auth_url' => $app->getAuthorizationUrl($account)]);
            } else {
                return $this->jsonResponse(false, $this->messageBuilder->build($form));
            }
        } catch (Exception $e) {
            return $this->jsonUnknownError();
        }
    }

    /**
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     *
     * @param Request $request
     * @param AppInterface $app
     * @return RedirectResponse
     */
    public function callbackAction(Request $request, AppInterface $app)
    {
        $session = $request->getSession();

        $returnUrl = $this->getReturnUrlFromSession($session);
        $account = $this->getAccountFromSession($session);

        if ($account->getType() !== $app->getType()) {
            throw new LogicException('Account and app types mismatch');
        }

        $isAccountNew = $account->getId() === null;

        try {
            $app->validateCallback($request, $account);
        }
        catch (AppException $ex) {
            return new RedirectResponse(
                $this->appUrlGenerator->generate($returnUrl, [
                    'auth' => 'error'
                ])
            );
        }

        $app->handleCallback($request, $account);

        $isAccountNew ?
            $this->coreAccountRepository->add($account) :
            $this->coreAccountRepository->update($account);

        return new RedirectResponse(
            $this->appUrlGenerator->generate($returnUrl, [
                'auth' => 'success',
                'account'   => $account->getId()
            ])
        );
    }

    /**
     * @param SessionInterface $session
     * @return Account
     * @throws LogicException
     */
    private function getAccountFromSession(SessionInterface $session)
    {
        if (!$session->has('app_oauth_account')) {
            throw new LogicException('Account was not saved in session');
        }

        /** @var Account $account */
        $account = $session->get('app_oauth_account');
        if (!$account instanceof Account) {
            throw new LogicException('Account in session must be instance of Account');
        }

        $session->remove('app_oauth_account');

        return $account;
    }

    /**
     * @param AppInterface $app
     * @param int | null $accountId
     * @return Account
     */
    private function getAccountInstance(AppInterface $app, $accountId)
    {
        if ($accountId === null) {
            $account = new Account();
            $account->setType($app->getType());
        } else {
            $account = $this->coreAccountRepository->find($accountId);
            if ($account->getType() !== $app->getType()) {
                throw new LogicException('Account and app types mismatch');
            }
        }

        return $account;
    }

    /**
     * @param SessionInterface $session
     * @return string
     */
    private function getReturnUrlFromSession(SessionInterface $session)
    {
        $returnUrl = $session->get('app_return_url', 'account/list');

        $session->remove('app_return_url');

        return $returnUrl;
    }
}
