<?php

namespace Connect\Bundle\AppBundle\Controller;

use Connect\Bundle\UMSSecurityBundle\Client\Facade;
use Connect\Bundle\UMSSecurityBundle\Controller\JsonController;
use Connect\Bundle\UMSSecurityBundle\Form\MessageBuilder;
use Connect\Bundle\UMSSecurityBundle\Form\Type\ProfileType;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\AjaxErrorHandler;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\Exception\InvalidFormatException;
use Connect\Bundle\UMSSecurityBundle\Mail\Message\Factory;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Contact;
use Exception;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Swift_Mailer;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserController
 */
class UserController extends JsonController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var UMSUserRepository
     */
    private $userRepository;
    /**
     * @var MessageBuilder
     */
    private $messageBuilder;
    /**
     * @var Facade
     */
    private $UMSClient;
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Factory
     */
    private $messageFactory;
    /**
     * @var AjaxErrorHandler
     */
    private $profileFormErrorHandler;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param FormFactoryInterface $formFactory
     * @param UMSUserRepository $userRepository
     * @param MessageBuilder $messageBuilder
     * @param TranslatorInterface $translator
     * @param Facade $UMSClient
     * @param Swift_Mailer $mailer
     * @param Factory $messageFactory
     * @param AjaxErrorHandler $profileFormErrorHandler
     */
    function __construct(
        TokenStorageInterface $tokenStorage,
        FormFactoryInterface $formFactory,
        UMSUserRepository $userRepository,
        MessageBuilder $messageBuilder,
        TranslatorInterface $translator,
        Facade $UMSClient,
        Swift_Mailer $mailer,
        Factory $messageFactory,
        AjaxErrorHandler $profileFormErrorHandler
    ) {
        parent::__construct($translator);
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
        $this->messageBuilder = $messageBuilder;
        $this->UMSClient = $UMSClient;
        $this->mailer = $mailer;
        $this->messageFactory = $messageFactory;
        $this->profileFormErrorHandler = $profileFormErrorHandler;
    }

    /**
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxEditAction(Request $request)
    {
        /** @var UMSUser $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $result = [];

        try {
            $form = $this->formFactory->create(new ProfileType());
            $form->submit(
                (new JsonEncoder())->decode($request->getContent(), JsonEncoder::FORMAT)
            );

            if ($form->isSubmitted()) {
                $result = $this->UMSClient->editUserProfile(
                    $user,
                    $request->getSession()->get('ums_session'),
                    $form->getData()
                );

                if (is_array($result)) {
                    $this->profileFormErrorHandler->handle($result, $form);
                } else {
                    $user = $this->userRepository->find($user->getId());
                    if (is_string($result)) {

                        $this->mailer->send(
                            $this->messageFactory
                                ->getContactConfirmMessage($result, $user->getPrimaryContact())
                        );
                    }
                    $this->tokenStorage->getToken()->setUser($user);

                    return $this->jsonResponse();
                }

            }
        } catch (InvalidFormatException $exception) {
            return $this->jsonResponse(false, $result);
        } catch (Exception $exception) {
            return $this->jsonUnknownError();
        }

        return $this->jsonResponse(false, $this->messageBuilder->build($form));

    }
}
