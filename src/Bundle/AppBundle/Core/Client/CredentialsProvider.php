<?php

namespace Connect\Bundle\AppBundle\Core\Client;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
* Class CredentialsProvider
 */
class CredentialsProvider
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @return string
     */
    public function getCoreTokenValue()
    {
        if (!$this->session->has('ums_session')) {
            throw new AccessDeniedException('Missing UMS session id.');
        };

        return $this->session->get('ums_session');
    }
}