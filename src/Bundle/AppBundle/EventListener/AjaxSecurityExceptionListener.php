<?php

namespace Connect\Bundle\AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AjaxSecurityExceptionListener
 */
class AjaxSecurityExceptionListener
{
    /**
     * @var JsonEncoder
     */
    private $jsonEncoder;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param JsonEncoder $jsonEncoder
     * @param TranslatorInterface $translator
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(JsonEncoder $jsonEncoder, TranslatorInterface $translator, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->jsonEncoder = $jsonEncoder;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Handles kernel AccessDeniedException:
     *
     * If ajax request failed on expired session or denied access,
     * than 200 OK response is returned with error message.
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();

        if (($request->isXmlHttpRequest() || $request->getContentType() === 'json') &&
            $event->getException() instanceof AccessDeniedException
        ) {
            if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
                $message = 'message.error.not_found';
                $statusCode = Response::HTTP_NOT_FOUND;
            }
            else {
                $message = 'message.error.session_expired';
                $statusCode = Response::HTTP_UNAUTHORIZED;
            }

            $event->setResponse(new Response(
                $this->jsonEncoder->encode(
                    ['success' => false, 'message' => $this->translator->trans($message)],
                    JsonEncoder::FORMAT
                ),
                $statusCode,
                array(
                    'X-Status-Code' => $statusCode
                )
            ));
        }
    }
}