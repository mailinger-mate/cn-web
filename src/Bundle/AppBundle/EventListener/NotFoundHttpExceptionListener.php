<?php

namespace Connect\Bundle\AppBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class NotFoundHttpExceptionListener
 */
class NotFoundHttpExceptionListener
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var KernelInterface
     */
    private $kernel;
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var JsonEncoder
     */
    private $jsonEncoder;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param LoggerInterface $logger
     * @param KernelInterface $kernel
     * @param EngineInterface $templating
     * @param JsonEncoder $jsonEncoder
     * @param TranslatorInterface $translator
     */
    public function __construct(
        LoggerInterface $logger,
        KernelInterface $kernel,
        EngineInterface $templating,
        JsonEncoder $jsonEncoder,
        TranslatorInterface $translator
    ) {
        $this->logger = $logger;
        $this->kernel = $kernel;
        $this->templating = $templating;
        $this->jsonEncoder = $jsonEncoder;
        $this->translator = $translator;
    }

    /**
     * Handles NotFoundHttpException exception in prod environment
     *
     * Logs exception message with INFO level (which overrides default ERROR in ExceptionListener).
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException && $this->kernel->getEnvironment() === 'prod') {
            $this->logger->info($exception->getMessage());

            $request = $event->getRequest();
            if (($request->isXmlHttpRequest() || $request->getContentType() === 'json')) {
                $content = $this->jsonEncoder->encode(
                    ['success' => false, 'message' => $this->translator->trans('message.error.not_found')],
                    JsonEncoder::FORMAT
                );
            }
            else {
                $content = $this->templating->render('@Twig/Exception/error404.html.twig');
            }

            $event->setResponse(new Response(
                $content,
                Response::HTTP_NOT_FOUND,
                ['X-Status-Code' => Response::HTTP_NOT_FOUND]
            ));
        }
    }
}