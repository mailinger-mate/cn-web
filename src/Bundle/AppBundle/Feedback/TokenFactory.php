<?php

namespace Connect\Bundle\AppBundle\Feedback;

use Connect\Domain\User;

/**
 * Class TokenFactory
 */
class TokenFactory
{
    const HASH_LENGTH = 16;
    
    /**
     * @var string
     */
    private $feedbackApiKey;

    /**
     * @var string
     */
    private $feedbackProjectKey;

    /**
     * @param string $feedbackApiKey
     * @param string $feedbackProjectKey
     */
    public function __construct($feedbackApiKey, $feedbackProjectKey)
    {
        $this->feedbackApiKey = $feedbackApiKey;
        $this->feedbackProjectKey = $feedbackProjectKey;
    }

    /**
     * Algorithm implementation originally based on instructions in:
     * https://xformation.userecho.com/settings/features/sso/ and
     * https://xformation.userecho.com/settings/features/sso/php/
     *
     * @param User $user
     * @return string
     */
    public function create(User $user)
    {
        $contact = $user->getPrimaryContact();
        $requestJson = json_encode([
            'guid'              =>  'connect_user_' . $user->getId(),
            'expires_date'      =>  gmdate("Y-m-d H:i:s", time()+(86400)),
            'display_name'      =>  $user->getName(),
            'email'             =>  $contact->getValue(),
            'verified_email'    =>  $contact->isVerified(),
        ]);

        $initializationVector = $this->generateRandomString(self::HASH_LENGTH);

        for ($i = 0; $i < self::HASH_LENGTH; $i++)
            $requestJson[$i] = $requestJson[$i] ^ $initializationVector[$i];

        $pad = self::HASH_LENGTH - (strlen($requestJson) % self::HASH_LENGTH);
        $requestJson = $requestJson . str_repeat(chr($pad), $pad);

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128,'','cbc','');
        $keyHash = $this->generateKeyHash(self::HASH_LENGTH);

        mcrypt_generic_init($cipher, $keyHash, $initializationVector);
        $encryptedBytes = mcrypt_generic($cipher,$requestJson);
        mcrypt_generic_deinit($cipher);

        return urlencode(base64_encode($encryptedBytes));
    }

    /**
     * @param $length
     * @return string
     */
    private function generateRandomString($length)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * @param $length
     * @return string
     */
    private function generateKeyHash($length)
    {
        return substr(hash('sha1', $this->feedbackApiKey . $this->feedbackProjectKey, true), 0, $length);
    }
}
