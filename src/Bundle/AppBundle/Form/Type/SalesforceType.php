<?php

namespace Connect\Bundle\AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

/**
 * Class SalesforceType
 */
class SalesforceType extends OAuthAccountType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('login_endpoint', 'text', [
            'constraints' => [
                new NotBlank(),
                new Url(),
                new Length(['max' => 255])
            ]
        ]);
    }
}
