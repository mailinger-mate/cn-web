<?php

namespace Connect\Bundle\AppBundle\Form\Type;

use Connect\Bundle\AppBundle\OAuth\App\Type;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Symfony\Component\Form\AbstractType;

/**
 * Class Factory
 */
class TypeFactory
{
    /**
     * @param $appType
     * @return AbstractType
     */
    public static function create($appType)
    {
        switch($appType) {
            case Type::TWITTER:
            case Type::SLACK:
                return new OAuthAccountType();

            case Type::SALESFORCE:
                return new SalesforceType();

            default:
                throw new InvalidArgumentException('Not supported app type: ' . $appType);
        }
    }
}