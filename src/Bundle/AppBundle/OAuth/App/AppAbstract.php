<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use Connect\Domain\Account;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AppAbstract
 */
abstract class AppAbstract implements AppInterface
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Account $account
     * @param array $accountData
     */
    public function populateAccount(Account $account, array $accountData)
    {
        if (!array_key_exists('name', $accountData)) {
            throw new InvalidArgumentException('Missing required field in account data: name');
        }

        $account->setName($accountData['name']);
    }

    /**
     * @param Account $account
     */
    public function initializeCallback(Account $account)
    {
    }

    /**
     * @return string
     */
    protected function getCallbackUrl()
    {
        return $this->router->generate(
            'app_oauth_callback',
            ['app' => $this->getType()],
            Router::ABSOLUTE_URL
        );
    }
}