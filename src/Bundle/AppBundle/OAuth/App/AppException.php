<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use Exception;

/**
 * Class AppException
 */
class AppException extends Exception {}