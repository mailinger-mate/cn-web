<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use Connect\Domain\Account;
use LogicException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface AppInterface
 */
interface AppInterface
{
    /**
     * Returns type of app
     *
     * @return string
     */
    public function getType();

    /**
     * Populates account instance with values provided in accountData
     * (eg. use data from form)
     *
     * @param Account $account
     * @param array $accountData
     */
    public function populateAccount(Account $account, array $accountData);

    /**
     * Runs additional actions for account required before redirecting to external app
     * (eg. gather oauth request token for account)
     *
     * @param Account $account
     */
    public function initializeCallback(Account $account);

    /**
     * Returns authorization url, that user will be redirected to authorize in app
     *
     * @param Account $account
     * @return string
     */
    public function getAuthorizationUrl(Account $account);

    /**
     * Validates callback request received from external app after authorization
     * (eg. checks if callback request contains proper tokens)
     *
     * @param Request $callbackRequest
     * @param Account $account
     * @throws AppException
     * @throws LogicException
     */
    public function validateCallback(Request $callbackRequest, Account $account);

    /**
     * Handles callback request received from external app
     * (eg. populates account with received tokens)
     *
     * @param Request $callbackRequest
     * @param Account $account
     */
    public function handleCallback(Request $callbackRequest, Account $account);
}