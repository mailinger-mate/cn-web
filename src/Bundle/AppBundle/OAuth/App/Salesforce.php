<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use Connect\Domain\Account;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use GuzzleHttp\Client;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Zend\Uri\Uri;

/**
 * Class Twitter
 */
class Salesforce extends AppAbstract
{
    /**
     * @var Client
     */
    private $httpClient;
    /**
     * @var string
     */
    private $clientId;
    /**
     * @var string
     */
    private $clientSecret;
    /**
     * @var JsonEncoder
     */
    private $jsonEncoder;

    /**
     * @param RouterInterface $router
     * @param JsonEncoder $jsonEncoder
     * @param Client $httpClient
     * @param $clientId
     * @param $clientSecret
     */
    function __construct(
        RouterInterface $router,
        JsonEncoder $jsonEncoder,
        Client $httpClient,
        $clientId,
        $clientSecret
    ) {
        parent::__construct($router);

        $this->httpClient = $httpClient;
        $this->jsonEncoder = $jsonEncoder;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return Type::SALESFORCE;
    }

    /**
     * @inheritdoc
     */
    public function getAuthorizationUrl(Account $account)
    {
        $loginEndpoint = $account->getConfig('login_endpoint');
        if (empty($loginEndpoint)) {
            throw new InvalidArgumentException('Login endpoint cannot be empty');
        }

        $uri = new Uri($loginEndpoint);
        $uri->setPath('/services/oauth2/authorize');
        $uri->setQuery([
            'response_type' => 'code',
            'client_id'     => $this->clientId,
            'redirect_uri'  => $this->getCallbackUrl(),
            'prompt'        => 'consent'
        ]);

        return $uri->toString();
    }

    /**
     * @param Account $account
     * @param array $accountData
     */
    public function populateAccount(Account $account, array $accountData)
    {
        parent::populateAccount($account, $accountData);

        if (!array_key_exists('login_endpoint', $accountData)) {
            throw new InvalidArgumentException('Missing required field in account data: login_endpoint');
        }

        $account->setConfig([
            'login_endpoint' => $accountData['login_endpoint']
        ]);
    }

    /**
     * @param Request $callbackRequest
     * @param Account $account
     * @throws AppException
     * @throws LogicException
     */
    public function validateCallback(Request $callbackRequest, Account $account)
    {
        $error = $callbackRequest->get('error');
        if ($error === 'access_denied') {
            throw new AppException('Salesforce denied access in callback request');
        }

        if ($error !== null) {
            throw new AppException('Salesforce error returned in callback request: ' . $error);
        }

        if (empty($callbackRequest->get('code'))) {
            throw new LogicException('Code callback param cannot be empty');
        }
    }

    /**
     * @param Request $callbackRequest
     * @param Account $account
     */
    public function handleCallback(Request $callbackRequest, Account $account)
    {
        $uri = new Uri($account->getConfig('login_endpoint'));
        $uri->setPath('/services/oauth2/token');

        $authResponse = $this->httpClient->post($uri->toString(), [
            'form_params'   => [
                'grant_type'    => 'authorization_code',
                'client_id'     => $this->clientId,
                'client_secret' => $this->clientSecret,
                'redirect_uri'  => $this->getCallbackUrl(),
                'code'          => $callbackRequest->get('code'),
            ]
        ]);

        $authResponseData = $this->jsonEncoder->decode(
            $authResponse->getBody()->getContents(),
            JsonEncoder::FORMAT
        );

        $account->setConfig([
            'login_endpoint'    =>  $account->getConfig('login_endpoint'),
            'refresh_token'     =>  $authResponseData['refresh_token']
        ]);
    }
}