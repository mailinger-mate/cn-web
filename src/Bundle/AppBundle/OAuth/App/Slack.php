<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use Bramdevries\Oauth\Client\Provider\Slack as SlackClient;
use Connect\Domain\Account;
use LogicException;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class Slack
 */
class Slack extends AppAbstract implements AppInterface
{
    /**
     * @var SlackClient
     */
    private $slackConnection;

    /**
     * @param RouterInterface $router
     * @param SlackClient $slackConnection
     */
    function __construct(RouterInterface $router, SlackClient $slackConnection)
    {
        parent::__construct($router);

        $this->setRedirectUri($slackConnection, $this->getCallbackUrl());
        $this->slackConnection = $slackConnection;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return Type::SLACK;
    }

    /**
     * @param Account $account
     */
    public function initializeCallback(Account $account)
    {
        $account->setConfig([
            'state' =>  $this->slackConnection
                ->getRandomFactory()
                ->getMediumStrengthGenerator()
                ->generateString(32)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getAuthorizationUrl(Account $account)
    {
        return $this->slackConnection->getAuthorizationUrl([
            'state' => $account->getConfig('state'),
            'scope' => [
                'channels:read',
                'users:read',
                'im:write',
                'chat:write:user',
                'chat:write:bot',
                'files:write:user',
            ]
        ]);
    }

    /**
     * @param Request $callbackRequest
     * @param Account $account
     * @throws AppException
     * @throws LogicException
     */
    public function validateCallback(Request $callbackRequest, Account $account)
    {
        if (!empty($callbackRequest->get('error'))) {
            throw new AppException('Slack denied access in callback request');
        }

        $callbackState = $callbackRequest->get('state');
        if (empty($callbackState)) {
            throw new LogicException('Callback OAuth state cannot be empty');
        }

        if ($callbackState !== $account->getConfig('state')) {
            throw new LogicException('Request states mismatch');
        }

        if (empty($callbackRequest->get('code'))) {
            throw new LogicException('OAuth authorization code cannot be empty');
        }
    }

    /**
     * @param Request $callbackRequest
     * @param Account $account
     */
    public function handleCallback(Request $callbackRequest, Account $account)
    {
        $code = $callbackRequest->get('code');
        if (empty($code)) {
            throw new LogicException('OAuth authorization code cannot be empty');
        }

        $account->setConfig([
            'access_token'  => $this->slackConnection
                ->getAccessToken('authorization_code', ['code' => $code])
                ->getToken()
        ]);
    }

    /**
     * Bypass for lack of setter for redirectUri property in used library.
     * It's originally injected only through constructor, which makes it hard to use as service.
     *
     * @param SlackClient $slackConnection
     * @param $redirectUri
     */
    private function setRedirectUri(SlackClient $slackConnection, $redirectUri)
    {
        $reflectionProperty = (new ReflectionClass(SlackClient::class))->getProperty('redirectUri');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($slackConnection, $redirectUri);
    }
}