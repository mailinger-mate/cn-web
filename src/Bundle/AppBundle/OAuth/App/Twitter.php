<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use Abraham\TwitterOAuth\TwitterOAuth;
use Connect\Domain\Account;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class Twitter
 */
class Twitter extends AppAbstract implements AppInterface
{
    /**
     * @var TwitterOAuth
     */
    private $twitterConnection;

    /**
     * @param RouterInterface $router
     * @param TwitterOAuth $twitterConnection
     */
    function __construct(RouterInterface $router, TwitterOAuth $twitterConnection)
    {
        parent::__construct($router);

        $this->twitterConnection = $twitterConnection;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return Type::TWITTER;
    }

    /**
     * @inheritdoc
     */
    public function getAuthorizationUrl(Account $account)
    {
        return $this->twitterConnection->url(
            'oauth/authorize',
            ['oauth_token' => $account->getConfig('request_token')]
        );
    }

    /**
     * @param Account $account
     */
    public function initializeCallback(Account $account)
    {
        $requestToken = $this->twitterConnection->oauth(
            'oauth/request_token',
            [
                'oauth_callback' => $this->getCallbackUrl()
            ]
        );

        $account->setConfig([
            'request_token'        =>  $requestToken['oauth_token'],
            'request_token_secret' =>  $requestToken['oauth_token_secret']
        ]);
    }

    /**
     * @param Request $callbackRequest
     * @param Account $account
     * @throws AppException
     * @throws LogicException
     */
    public function validateCallback(Request $callbackRequest, Account $account)
    {
        $deniedToken = $callbackRequest->get('denied');
        if (!empty($deniedToken)) {
            throw new AppException('Twitter denied access in callback request');
        }

        $callbackRequestToken = $callbackRequest->get('oauth_token');
        if (empty($callbackRequestToken)) {
            throw new LogicException('Callback OAuth request token cannot be empty');
        }

        if ($callbackRequestToken !== $account->getConfig('request_token')) {
            throw new LogicException('Request tokens mismatch');
        }

        if (empty($callbackRequest->get('oauth_verifier'))) {
            throw new LogicException('OAuth verifier token cannot be empty');
        }
    }

    /**
     * @param Request $callbackRequest
     * @param Account $account
     */
    public function handleCallback(Request $callbackRequest, Account $account)
    {
        $verifierToken = $callbackRequest->get('oauth_verifier');
        if (empty($verifierToken)) {
            throw new LogicException('OAuth verifier token cannot be empty');
        }

        $this->twitterConnection->setOauthToken(
            $account->getConfig('request_token'),
            $account->getConfig('request_token_secret')
        );

        $accessToken = $this->twitterConnection
            ->oauth('oauth/access_token', ['oauth_verifier' => $verifierToken]);

        $account->setConfig([
            'access_token'        =>  $accessToken['oauth_token'],
            'access_token_secret' =>  $accessToken['oauth_token_secret']
        ]);
    }
}