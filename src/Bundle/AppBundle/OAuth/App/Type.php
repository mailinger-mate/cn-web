<?php

namespace Connect\Bundle\AppBundle\OAuth\App;

use ReflectionClass;

/**
 * Class Type
 */
class Type
{
    const TWITTER       = 'twitter';
    const SALESFORCE    = 'salesforce';
    const SLACK         = 'slack';

    /**
     * @param string $appType
     * @return bool
     */
    public static function isSupported($appType)
    {
        return in_array($appType, (new ReflectionClass(static::class))->getConstants());
    }
}