<?php

namespace Connect\Bundle\AppBundle\Repository;

use Connect\Domain\Account;
use Connect\Domain\AccountFactory;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class CoreAccountRepository
 */
class CoreAccountRepository extends CoreRepositoryAbstract
{
    /**
     * @param Account $account
     * @return bool
     */
    public function add(Account $account)
    {
        $response = $this->post(
            $account,
            'accounts',
            $this->getJsonHeaders(),
            SerializationContext::create()->setGroups(['add'])
        );

        if ($response->getStatusCode() === Response::HTTP_OK) {
            $account->setId(
                $this->jsonToArray(
                    $response->getBody()
                )['id']
            );

            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function update(Account $account)
    {
        return $this->patch(
            $account,
            sprintf('accounts/%s', $account->getId()),
            $this->getJsonHeaders(),
            SerializationContext::create()->setGroups(['edit'])
        );
    }

    /**
     * @param int $id
     * @return Account
     */
    public function find($id)
    {
        return (new AccountFactory())->createFromArray(
            $this->jsonToArray(
                $this->get(
                    sprintf('accounts/%s', $id),
                    $this->getJsonHeaders()
                )->getBody()
            )
        );
    }

    /**
     * @param $json
     * @return array
     */
    private function jsonToArray($json) {
        return $this->getSerializer()
            ->deserialize(
                $json,
                'array',
                JsonEncoder::FORMAT
            );
    }

    /**
     * @return array
     */
    private function getJsonHeaders()
    {
        return [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json'
        ];
    }
}
