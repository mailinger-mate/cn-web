<?php

namespace Connect\Bundle\AppBundle\Repository;

use Connect\Domain\Invoice;
use LogicException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class CoreBillingRepository
 */
class CoreBillingRepository extends CoreRepositoryAbstract
{
    /**
     * @param int $id
     * @return Invoice
     */
    public function find($id)
    {
        $coreResponse = $this->get(
            sprintf('billings/%s', $id),
            ['Accept' => 'application/pdf']
        );

        return (new Invoice())
            ->setId($id)
            ->setPdfName($this->getFilename($coreResponse))
            ->setPdfContent($coreResponse->getBody());
    }

    /**
     * @param ResponseInterface $response
     * @return string
     */
    private function getFilename(ResponseInterface $response)
    {
        if (!$response->hasHeader("Content-Disposition")) {
            throw new LogicException("Missing required Content-Disposition header");
        }

        $header = $response->getHeader('Content-Disposition')[0];

        preg_match('/filename="?([\w. ]+[\w]+)"?/', $header, $output);

        if (count($output) !== 2) {
            throw new LogicException("Not supported Content-Disposition received: " . $header);
        }

        return $output[1];
    }
}