<?php

namespace Connect\Bundle\AppBundle\Repository;

use Connect\Bundle\AppBundle\Core\Client\CredentialsProvider;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class CoreRepositoryAbstract
 */
abstract class CoreRepositoryAbstract
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var CredentialsProvider
     */
    private $credentialsProvider;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param ClientInterface $client
     * @param CredentialsProvider $credentialsProvider
     * @param SerializerInterface $serializer
     */
    function __construct(ClientInterface $client, CredentialsProvider $credentialsProvider, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->credentialsProvider = $credentialsProvider;
        $this->serializer = $serializer;
    }

    /**
     * @return SerializerInterface
     */
    protected function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @param mixed $data
     * @param string $uri
     * @param array $headers
     * @param SerializationContext $serializationContext
     * @return ResponseInterface
     */
    protected function post($data, $uri, array $headers, SerializationContext $serializationContext = null)
    {
        $data = $this->serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            $serializationContext
        );

        return $this->client->request(
            'POST',
            $uri,
            [
                'body'    => $data,
                'headers' => $this->buildHeaders($headers)
            ]
        );
    }

    /**
     * @param mixed $data
     * @param string $uri
     * @param array $headers
     * @param SerializationContext $serializationContext
     * @return bool
     */
    protected function patch($data, $uri, array $headers, SerializationContext $serializationContext = null)
    {
        $data = $this->serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            $serializationContext
        );

        $response = $this->client->request(
            'PATCH',
            $uri,
            [
                'body'    => $data,
                'headers' => $this->buildHeaders($headers)
            ]
        );

        return $response->getStatusCode() === Response::HTTP_NO_CONTENT;
    }

    /**
     * @param $uri
     * @param array $headers
     * @return ResponseInterface
     */
    protected function get($uri, array $headers)
    {
        $response = $this->client->request(
            'GET',
            $uri,
            [
                'headers' => $this->buildHeaders($headers)
            ]
        );

        return $response;
    }

    /**
     * @param array $headers
     * @return array
     */
    private function buildHeaders(array $headers)
    {
        $headers['Authorization'] = 'Bearer ' . $this->credentialsProvider->getCoreTokenValue();

        return $headers;
    }
}
