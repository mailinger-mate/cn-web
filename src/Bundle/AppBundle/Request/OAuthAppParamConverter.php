<?php

namespace Connect\Bundle\AppBundle\Request;

use Connect\Bundle\AppBundle\OAuth\App\AppInterface;
use Connect\Bundle\AppBundle\OAuth\App\Twitter;
use Connect\Bundle\AppBundle\OAuth\App\Type;
use InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OAuthAppParamConverter
 */
class OAuthAppParamConverter implements ParamConverterInterface
{
    /**
     * @var ContainerInterface
     */
    private $serviceContainer;

    /**
     * @param ContainerInterface $serviceContainer
     */
    function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();

        if (!$request->attributes->has($param)) {
            return false;
        }

        $appType = $request->attributes->get($param);
        if (!Type::isSupported($appType)) {
            throw new NotFoundHttpException('Not supported app type: ' . $appType);
        }

        $request->attributes->set($param, $this->getApp($appType));

        return true;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration)
    {
        return AppInterface::class === $configuration->getClass();
    }

    /**
     * @param string $appType
     * @return AppInterface
     */
    private function getApp($appType)
    {
        switch($appType)
        {
            case Type::SALESFORCE:
                return $this->serviceContainer->get('oauth_app.salesforce');

            case Type::SLACK:
                return $this->serviceContainer->get('oauth_app.slack');

            case Type::TWITTER:
                return $this->serviceContainer->get('oauth_app.twitter');

            default:
                throw new InvalidArgumentException('Not supported app type: ' . $appType);
        }
    }
}