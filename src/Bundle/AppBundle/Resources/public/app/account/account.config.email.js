/*global angular*/

/*property
    config, email, host, imap, module, name, port, provider, set, smtp
*/

angular.module("cn.config.email", [
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            email: {
                provider: [
                    {
                        name: "custom",
                        imap: {
                            host: ""
                        },
                        smtp: {
                            host: "",
                            port: ""
                        }
                    },
                    {
                        name: "gmail",
                        imap: {
                            host: "imap.gmail.com"
                        },
                        smtp: {
                            host: "smtp.gmail.com",
                            port: 587
                        }
                    },
                    {
                        name: "outlook",
                        imap: {
                            host: "imap-mail.outlook.com"
                        },
                        smtp: {
                            host: "smtp-mail.outlook.com",
                            port: 587
                        }
                    },
                    {
                        name: "hotmail",
                        imap: {
                            host: "imap-mail.outlook.com"
                        },
                        smtp: {
                            host: "smtp-mail.outlook.com",
                            port: 587
                        }
                    },
                    {
                        name: "yahoo",
                        imap: {
                            host: "imap.mail.yahoo.com"
                        },
                        smtp: {
                            host: "smtp.mail.yahoo.com",
                            port: 587
                        }
                    },
                    {
                        name: "icloud",
                        imap: {
                            host: "imap.mail.me.com"
                        },
                        smtp: {
                            host: "smtp.mail.me.com",
                            port: 587
                        }
                    },
                    {
                        name: "aol",
                        imap: {
                            host: "imap.aol.com"
                        },
                        smtp: {
                            host: "smtp.aol.com",
                            port: 587
                        }
                    },
                    {
                        name: "zoho",
                        imap: {
                            host: "imap.zoho.com"
                        },
                        smtp: {
                            host: "smtp.zoho.com",
                            port: 587
                        }
                    },
                    {
                        name: "gmx",
                        imap: {
                            host: "imap.gmx.com"
                        },
                        smtp: {
                            host: "mail.gmx.com",
                            port: 587
                        }
                    },
                    {
                        name: "yandex",
                        imap: {
                            host: "imap.yandex.com"
                        },
                        smtp: {
                            host: "smtp.yandex.com",
                            port: 465
                        }
                    }
                ]
            }
        });
    });

