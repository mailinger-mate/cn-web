/*global angular*/

/*property
    $data, $label, $track, account, config, dynamic, enabled, find, help,
    hipchat, imap, input, jira, label, model, module, name, oauth, options,
    pattern, placeholder, prefix, production, required, rss, salesforce,
    sandbox, set, slack, smtp, string, translate, twitter, type, url, value, webhook

*/

angular.module("cn.account.config", [
    "cn.config",
    "cn.config.email"
])
    .config(function (cfgProvider) {

        "use strict";

        var providers = cfgProvider.find("email.provider");

        cfgProvider.set({
            account: {
                oauth: {
                    twitter: {
                        url: "/oauth/twitter/"
                    },
                    salesforce: {
                        url: "/oauth/salesforce/",
                        config: ["login_endpoint"]
                    },
                    slack: {
                        url: "/oauth/slack/"
                    }
                },
                config: {
                    hipchat: [
                        {
                            model: "token",
                            type: "password",
                            placeholder: true
                        }
                    ],
                    imap: [
                        {
                            model: "form.config.email_provider",
                            label: "email_provider",
                            type: "select",
                            name: "email_provider",
                            options: providers,
                            dynamic: {
                                options: {
                                    $label: "p as 'account.item.select.email_provider.' + p.name | translate",
                                    $data: "p",
                                    $track: "p.name"
                                }
                            },
                            value: providers[0],
                            help: true,
                            prefix: {
                                config: false,
                                model: false
                            }
                        },
                        {
                            model: "host",
                            type: "text",
                            placeholder: "host_imap"
                        },
                        {
                            model: "email",
                            label: "user_name",
                            type: "text",
                            placeholder: "email"
                        },
                        {
                            model: "password",
                            type: "password"
                        }
                    ],
                    jira: [
                        {
                            model: "server",
                            type: "url",
                            label: "url_jira",
                            placeholder: "url_jira",
                            help: "url_jira"
                        },
                        {
                            model: "username",
                            type: "text",
                            label: "user_name",
                            placeholder: "user_name"
                        },
                        {
                            model: "password",
                            type: "password"
                        }
                    ],
                    rss: [
                        {
                            model: "feed",
                            type: "url",
                            placeholder: true,
                            help: true
                        }
                    ],
                    salesforce: [
                        {
                            model: "login_endpoint",
                            type: "select",
                            options: {
                                sandbox: "https://test.salesforce.com",
                                production: "https://login.salesforce.com"
                            },
                            value: "https://login.salesforce.com",
                            required: true,
                            help: true
                        }
                    ],
                    slack: [],
                    smtp: [
                        {
                            model: "form.config.email_provider",
                            label: "email_provider",
                            type: "select",
                            name: "email_provider",
                            options: providers,
                            dynamic: {
                                options: {
                                    $label: "p as 'account.item.select.email_provider.' + p.name | translate",
                                    $data: "p",
                                    $track: "p.name"
                                }
                            },
                            value: providers[0],
                            help: true,
                            prefix: {
                                config: false,
                                model: false
                            }
                        },
                        {
                            model: "host",
                            type: "text",
                            placeholder: "host_smtp"
                        },
                        {
                            model: "port",
                            type: "number",
                            placeholder: "port_smtp",
                            help: "port_smtp",
                            string: true
                        },
                        {
                            model: "user_name",
                            type: "text",
                            placeholder: "email"
                        },
                        {
                            model: "password",
                            type: "password"
                        }
                    ],
                    twitter: [
                    ],
                    webhook: [
                        {
                            model: "method",
                            type: "select",
                            options: ["OPTIONS", "GET", "HEAD", "POST", "PUT", "PATCH", "DELETE", "TRACE", "CONNECT"],
                            value: "POST",
                            translate: false,
                            help: true
                        },
                        {
                            model: "url",
                            type: "url",
                            placeholder: true
                        },
                        {
                            model: "auth_type",
                            type: "select",
                            options: ["none", "basic", "bearer"],
                            value: "none"
                        },
                        {
                            model: "auth_token",
                            type: "password",
                            enabled: "config.auth_type=='bearer'"
                        },
                        {
                            model: "auth_username",
                            type: "text",
                            enabled: "config.auth_type=='basic'"
                        },
                        {
                            model: "auth_password",
                            type: "password",
                            enabled: "config.auth_type=='basic'"
                        },
                        {
                            model: "success_codes",
                            type: "tags",
                            placeholder: "success_codes",
                            help: true,
                            input: {
                                placeholder: "success_codes",
                                pattern: "http_code"
                            },
                            value: [200, 204]
                        }
                    ]
                }
            }
        });
    });
