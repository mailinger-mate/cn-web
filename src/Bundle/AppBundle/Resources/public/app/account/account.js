/*global angular*/
/*property
    abstract, account, accounts, addClass, addPart, attr, auth, button,
    capitalizeString, config, connection, controller, controllerProvider, css,
    data, destination, directive, get, go, icon, id, length, level, link, load,
    loadPlugin, module, name, notify, pageTitle, prefix, redirect, replace,
    resolve, restrict, return, state, status, template, templateUrl, then,
    title, type, unknown, url, push
*/

angular.module("cn.account", [
    "oc.lazyLoad",
    "pascalprecht.translate",
    "cn.core",
    "cn.action",
    "cn.account.list",
    "cn.account.config",
    "cn.account.item"
])
    .config(function ($translatePartialLoaderProvider, $stateProvider) {

        "use strict";

        $translatePartialLoaderProvider.addPart('account/account');

        $stateProvider
            .state("account", {
                url: "/account",
                template: "<div ui-view autoscroll='false'></div>",
                redirect: "account.list"
            })

            .state("account.none", {
                url: "/none",
                templateUrl: "html/account/none.html",
                data: {
                    pageTitle: "account.none.pageTitle"
                }
            })

            .state("account.list", {
                url: "/list",
                resolve: {
                    accounts: function ($state, $q, action) {
                        return $q(function (resolve, reject) {
                            return action.account.get().then(function (accounts) {
                                if (accounts && accounts.length) {
                                    resolve(accounts);
                                } else {
                                    reject($state.go("account.none"));
                                }
                            }, reject);
                        });
                    },
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load("datatables");
                    }
                },
                controller: "cnAccountList",
                templateUrl: "html/account/list.html",
                data: {
                    pageTitle: "nav.account.list"
                }
            })

            .state("account.item", {
                url: "/item?auth&connection&destination",
                resolve: {
                    auth: function ($stateParams, notification) {

                        if ($stateParams.auth) {
                            notification.push({
                                level: $stateParams.auth,
                                prefix: "account",
                                name: "auth"
                            });
                        }

                        return true;
                    }
                },
                controller: "cnAccountItem",
                templateUrl: "html/account/item.html",
                abstract: true
            })
            .state("account.item.add", {
                url: "/add/:type?account",
                resolve: {
                    return: function ($state, $stateParams, $q, $timeout) {

                        return $q(function (resolve, reject) {

                            if ($stateParams.account && $stateParams.connection) {

                                $timeout(function () {
                                    if ($stateParams.connection === "add") {
                                        reject($state.go("connection.item.add.source", {
                                            type: $stateParams.type,
                                            account: $stateParams.account
                                        }));

                                    } else {
                                        reject($state.go("connection.item.edit.destination.add", {
                                            type: $stateParams.type,
                                            account: $stateParams.account,
                                            connection: $stateParams.connection
                                        }));
                                    }
                                });

                            } else {
                                resolve(false);
                            }
                        });
                    },
                    account: function ($state, $stateParams, $q, $timeout) {

                        return $q(function (resolve, reject) {

                            if ($stateParams.account && !$stateParams.connection && !$stateParams.destination) {

                                $timeout(function () {
                                    reject($state.go("account.item.edit", {
                                        account: $stateParams.account
                                    }));
                                });

                            } else {
                                resolve(false);
                            }
                        });
                    }
                },
                controllerProvider: function ($stateParams, helper, cfg) {

                    var type = $stateParams.type,
                        prefix = "cnAccountItem";

                    if (cfg.account.config[type]) {
                        return prefix + helper.capitalizeString(type);
                    }
                    return prefix + "Default";
                },
                templateUrl: "html/account/item.form.html",
                data: {
                    pageTitle: "account.item.title.add"
                }
            })
            .state("account.item.edit", {
                url: "/edit/:account",
                resolve: {
                    account: function ($stateParams, action) {
                        return action.account.get({id: $stateParams.account});
                    },
                    return: function ($state, $stateParams, $q, $timeout, account) {

                        return $q(function (resolve, reject) {

                            if ($stateParams.auth === "success" && ($stateParams.connection || $stateParams.destination)) {

                                $timeout(function () {

                                    if ($stateParams.destination) {

                                        if ($stateParams.destination === "add") {
                                            reject($state.go("connection.item.edit.destination.add", {
                                                type: account.type,
                                                account: $stateParams.account,
                                                connection: $stateParams.connection
                                            }));
                                        } else {
                                            reject($state.go("connection.item.edit.destination.edit", {
                                                connection: $stateParams.connection,
                                                destination: $stateParams.destination
                                            }));
                                        }

                                    } else if ($stateParams.connection) {

                                        if ($stateParams.connection === "add") {
                                            reject($state.go("connection.item.add.source", {
                                                type: account.type,
                                                account: $stateParams.account
                                            }));
                                        } else {
                                            reject($state.go("connection.item.edit.source.edit", {
                                                connection: $stateParams.connection
                                            }));
                                        }
                                    }
                                });

                            } else {
                                resolve(false);
                            }
                        });
                    }
                },
                controllerProvider: function (helper, account) {
                    return "cnAccountItem" + helper.capitalizeString(account.type);
                },
                templateUrl: "html/account/item.form.html",
                data: {
                    pageTitle: "account.item.title.edit"
                }
            });
    })
    .directive("cnAccountType", function ($filter, cfg) {

        "use strict";

        return {
            restrict: "E",
            template: "<i ng-attr-title='{{ title }}'>",
            replace: true,
            link: function (scope, element, attrs) {

                var type = attrs.type || "unknown",
                    css = cfg.css;

                scope.title = $filter("translate")("account.item.select.type." + type);

                element.addClass("icon-box " + type + " " + (css.icon[type] || css.icon.unknown));
            }
        };
    })
    .directive("cnAccountStatus", function ($translate, cfg) {

        "use strict";

        return {
            restrict: "E",
            template: "<i>",
            replace: true,
            link: function (element, attrs) {

                var status = attrs.status || "warning",
                    css = cfg.css;

                $translate("account.status." + status).then(function (txt) {
                    element.attr("title", txt);
                });

                element.addClass(css.button.status[status] + " " + css.icon.status[status]);
            }
        };
    });
