/*global angular*/

/*property
    account, controller, initAccount, module, type
*/

angular.module("cn.account.item.hipchat", [])
    .controller("cnAccountItemHipchat", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "hipchat",
            account: account
        });
    });
