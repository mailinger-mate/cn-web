/*global angular*/

/*property
    account, controller, initAccount, module, type
*/


angular.module("cn.account.item.jira", [])
    .controller("cnAccountItemJira", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "jira",
            account: account
        });
    });
