/*global angular, window*/
/*property
    $current, $error, $setDirty, $valid, $watch, account, action, auth,
    authAccount, auth_url, back, button, call, class, config, confirm, connect,
    connectAccount, connection, controller, ctrl, current, data, delete,
    deleteAccount, destination, email_provider, error, errors, field, forEach,
    form, formAccount, formConfig, go, goBack, hasOwnProperty, history, href,
    id, initAccount, keys, label, location, mode, model, module, name, notify,
    oauth, options, params, placeholder, prefix, push, required, reset,
    resource, return_url, run, save, saveAccount, show, substr, success,
    translate, type, update, validateForm, view, wait
*/

angular.module("cn.account.item", [
    "cn.core",
    "cn.account.config",
    "cn.account.item.hipchat",
    "cn.account.item.imap",
    "cn.account.item.jira",
    "cn.account.item.rss",
    "cn.account.item.smtp",
    "cn.account.item.slack",
    "cn.account.item.salesforce",
    "cn.account.item.twitter",
    "cn.account.item.webhook"
])
    .controller("cnAccountItem", function ($window, $scope, $state, $stateParams, $filter, cfg, action, core) {

        "use strict";

        $scope.data = {};

        $scope.view = {
            mode: "add"
        };

        $scope.form = {
            action: {},
            config: {
                field: {
                    name: {
                        placeholder: true,
                        required: true
                    },
                    type: {
                        model: "form.config.type",
                        type: "select",
                        options: Object.keys(cfg.account.config),
                        required: true,
                        placeholder: true,
                        prefix: {
                            model: false
                        }
                    }
                },
                prefix: {
                    translate: "account.item"
                }
            },
            error: {}
        };

        $scope.initAccount = function (config) {

            $scope.form.field = {
                config: {}
            };

            $scope.form.config.email_provider = {};

            $scope.view.mode = "add";

            delete $scope.data.account;
            delete $scope.form.config.oauth;
            delete $scope.form.config.type;
            delete $scope.form.config.field.config;

            if (!config) {
                return;
            }

            $scope.form.button = {
                account: [{
                    label: "save",
                    type: "submit",
                    action: "saveAccount",
                    class: "btn-primary"
                }],
                config: [{
                    label: config.account
                        ? "reconnect"
                        : "connect",
                    type: "submit",
                    action: "connectAccount",
                    class: "btn-primary"
                }]
            };

            if (config.type) {

                $scope.form.config.type = config.type;
                $scope.form.config.field.config = cfg.account.config[config.type];

                if (cfg.account.oauth.hasOwnProperty(config.type)) {

                    $scope.form.config.oauth = cfg.account.oauth[config.type];
                    $scope.form.button.config = [{
                        label: config.account
                            ? "reauthorize"
                            : "authorize",
                        type: "button",
                        action: "authAccount",
                        class: "btn-primary",
                        wait: true
                    }];
                }
            }

            if (config.account) {

                $scope.data.account = config.account;

                $scope.view.mode = "edit";

                $scope.form.field = {
                    name: config.account.name,
                    config: config.account.config
                };

                $scope.form.button.account.push({
                    label: "delete",
                    type: "button",
                    action: "deleteAccount",
                    confirm: true,
                    class: "btn-warning"
                });

            } else {

                $scope.form.field.name = $filter('translate')('account.item.select.type.' + config.type);
                $scope.form.config.field.name.reset = true;
            }

            $scope.form.button.account.push({
                label: "back",
                type: "link",
                action: "goBack",
                class: "btn-default"
            });
        };

        $scope.saveAccount = function ($scope) {

            var call;

            if ($scope.validateForm($scope.formAccount).$valid) {

                if ($scope.view.mode === "edit") {

                    call = core.account.update({id: $scope.data.account.id}, {
                        name: $scope.form.field.name
                    });

                } else {

                    call = core.account.save({
                        type: $scope.form.config.type,
                        name: $scope.form.field.name,
                        config: $scope.form.field.config
                    });
                }

                return action.run({
                    notify: {
                        show: true,
                        translate: {
                            name: $scope.form.field.name
                        }
                    },
                    resource: "account",
                    name: $scope.view.mode,
                    call: call,
                    success: function (account) {

                        delete $scope.form.error.field;

                        if ($scope.form.action.hasOwnProperty("success")) {

                            $scope.form.action.success(account);

                        } else {

                            if ($stateParams.destination) {

                                if ($stateParams.destination === "add") {
                                    $state.go("connection.item.edit.destination.add", {
                                        connection: $stateParams.connection,
                                        type: $scope.form.config.type,
                                        account: account.id || $scope.data.account.id
                                    });

                                } else {
                                    $state.go("connection.item.edit.destination.edit", {
                                        connection: $stateParams.connection,
                                        destination: $stateParams.destination
                                    });
                                }

                            } else if ($stateParams.connection) {

                                if ($stateParams.connection === "add") {
                                    $state.go("connection.item.add.source", {
                                        type: $scope.form.config.type,
                                        account: account.id || $scope.data.account.id
                                    });

                                } else {
                                    $state.go("connection.item.edit.source.edit", {
                                        connection: $stateParams.connection
                                    });
                                }

                            } else if ($scope.view.mode === "add") {

                                $state.go("account.item.edit", {
                                    account: account.id
                                });
                            }
                        }

                        return account;
                    },
                    error: function (error) {

                        $scope.form.error.field = error.data
                            ? error.data.errors
                            : {};
                    }
                });

            } else {
                return false;
            }
        };

        $scope.connectAccount = function ($scope) {

            if ($scope.data.account) {

                return action.account.connect({
                    name: $scope.form.field.name,
                    id: $scope.data.account.id,
                    config: $scope.form.field.config,
                    success: function () {

                        if ($stateParams.destination) {

                            if ($stateParams.destination === "add") {
                                $state.go("connection.item.edit.destination.add", {
                                    connection: $stateParams.connection,
                                    type: $scope.form.config.type,
                                    account: $scope.data.account.id
                                });

                            } else {
                                $state.go("connection.item.edit.destination.edit", {
                                    connection: $stateParams.connection,
                                    destination: $stateParams.destination
                                });
                            }

                        } else if ($stateParams.connection) {

                            if ($stateParams.connection === "add") {
                                $state.go("connection.item.add.source", {
                                    type: $scope.form.config.type,
                                    account: $scope.data.account.id
                                });

                            } else {
                                $state.go("connection.item.edit.source.edit", {
                                    connection: $stateParams.connection
                                });
                            }

                        }
                    }
                });

            } else {

                if ($scope.validateForm($scope.formConfig).$valid) {
                    return $scope.saveAccount($scope);
                } else {
                    return false;
                }
            }
        };

        $scope.authAccount = function ($scope) {

            var data = {},
                id;

            if ($scope.validateForm($scope.formAccount).$valid && $scope.validateForm($scope.formConfig).$valid) {

                if ($scope.form.config.oauth.config) {
                    angular.forEach($scope.form.config.oauth.config, function (key) {
                        data[key] = $scope.form.field.config[key];
                    });
                }

                if ($scope.data.account) {
                    id = $scope.data.account.id;
                }

                return action.account.auth({
                    id: id,
                    type: $scope.form.config.type,
                    name: $scope.form.field.name,
                    data: data,
                    params: {
                        return_url: $state.href($state.current.name, {
                            auth: undefined
                        }).substr(2)
                    },
                    success: function (data) {
                        window.location.href = data.auth_url;
                    }
                });

            } else {
                return false;
            }
        };

        $scope.deleteAccount = function () {

            return action.account.delete({
                name: $scope.form.field.name,
                id: $scope.data.account.id,
                success: function (data) {
                    $state.go("account.list");
                    return data;
                }
            });
        };

        $scope.validateForm = function (form) {

            if (!form) {
                form = $scope.form.ctrl;
            }

            angular.forEach(form.$error.required, function (field) {
                field.$setDirty();
            });

            return form;
        };

        $scope.goBack = function () {
            $window.history.back();
        };

        $scope.$watch("form.config.type", function (type, before) {
            if (type && type !== before) {
                $state.go($state.$current.name, {type: type});
            }
        });

    })
    .controller("cnAccountItemDefault", function ($scope) {

        "use strict";

        $scope.initAccount();
    });
