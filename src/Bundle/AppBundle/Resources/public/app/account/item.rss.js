/*global angular*/

/*property
    account, controller, initAccount, module, type
*/

angular.module("cn.account.item.rss", [])
    .controller("cnAccountItemRss", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "rss",
            account: account
        });
    });
