/*global angular*/

/*property
    account, controller, initAccount, module, type
*/

angular.module("cn.account.item.salesforce", [])
    .controller("cnAccountItemSalesforce", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "salesforce",
            account: account
        });
    });
