/*global angular*/
/*property
    account, controller, initAccount, module, type
*/

angular.module("cn.account.item.slack", [])
    .controller("cnAccountItemSlack", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "slack",
            account: account
        });
    });
