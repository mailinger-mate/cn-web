/*global angular*/

/*property
    $dirty, $pristine, $setPristine, $watch, account, config, controller, ctrl,
    email, email_provider, field, filter, form, host, initAccount, module,
    name, port, provider, smtp, type
*/

angular.module("cn.account.item.smtp", [])
    .controller("cnAccountItemSmtp", function ($scope, cfg, account) {

        "use strict";

        $scope.initAccount({
            type: "smtp",
            account: account
        });

        $scope.$watch("form.config.email_provider", function (provider) {

            if (provider && provider.smtp) {

                if ($scope.form.ctrl.host.$pristine || ($scope.form.ctrl.host.$dirty && provider.name !== "custom")) {
                    $scope.form.field.config.host = provider.smtp.host;
                    $scope.form.field.config.port = provider.smtp.port;
                }

                $scope.form.ctrl.host.$setPristine();
            }
        }, true);

        $scope.$watch("form.field.config.host", function (host) {

            var provider;

            if (host) {

                provider = cfg.email.provider.filter(function (provider) {
                    if (provider.smtp.host === host) {
                        return provider;
                    }
                })[0];

            }

            if (!provider) {

                provider = cfg.email.provider[0];
            }

            $scope.form.config.email_provider = provider;
        });
    });
