/*global angular*/

/*property
    account, controller, initAccount, module, type
*/

angular.module("cn.account.item.twitter", [])
    .controller("cnAccountItemTwitter", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "twitter",
            account: account
        });
    });
