/*global angular*/

/*property
    account, controller, initAccount, module, type
*/

angular.module("cn.account.item.webhook", [])
    .controller("cnAccountItemWebhook", function ($scope, account) {

        "use strict";

        $scope.initAccount({
            type: "webhook",
            account: account
        });
    });
