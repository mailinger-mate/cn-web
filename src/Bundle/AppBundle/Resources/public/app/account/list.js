/*global angular*/

/*property
    accounts, config, connection, controller, dtColumnDefs, dtOptions, keys,
    module, newOptions, orderable, searchable, source, targets, type,
    withOption
*/

angular.module("cn.account.list", [
    "cn.connection.config"
])
    .controller("cnAccountList", function ($scope, DTOptionsBuilder, cfg, accounts) {

        "use strict";

        $scope.accounts = accounts;
        $scope.type = {
            source: Object.keys(cfg.connection.config.source)
        };

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption("order", [[0, "desc"]])
            .withOption('responsive', true);

        $scope.dtColumnDefs = [
            {
                targets: 'no-sort',
                orderable: false
            },
            {
                targets: [0],
                searchable: false
            }
        ];
    });
