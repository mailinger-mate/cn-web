/*global $ angular Stripe*/
/*property
    $broadcast, $promise, account, accountInfo, add, auth, billing, call, card,
    card_token, config, connect, connection, contact, createToken, data,
    delete, edit, email, error, extend, field, forEach, get, id, info, invoice,
    isArray, join, length, level, message, method, module, name, notify,
    params, post, prefix, property, query, resource, run, service, show,
    status, statusText, success, text, then, translate, trypay, type, update,
    url, user, vendor
*/

angular.module("cn.action", [
    "cn.config",
    "cn.core",
    "cn.notification"
])
    .service("action", function ($rootScope, $http, $q, core) {

        "use strict";

        var service = {};

        function notify(config) {
            $rootScope.$broadcast('action', config);
        }

        service.run = function (config) {

            var call = config.call;

            function success(data) {
                if (config.notify && config.notify.show !== "error") {
                    notify({
                        level: "success",
                        prefix: config.resource,
                        name: config.name,
                        translate: config.notify.translate
                    });
                }

                if (config.success) {
                    return config.success(data);
                }

                return data;
            }

            function error(error) {

                if (error && error.status !== 401 && config.notify && config.notify.show !== "success") {
                    notify({
                        level: "error",
                        prefix: config.resource,
                        name: config.name,
                        translate: config.notify.translate,
                        text: error.data
                            ? typeof error.data.message === "string"
                                ? error.data.message
                                : false
                            : error.statusText
                    });
                }

                if (config.error) {
                    return config.error(error);
                }
                return false;
            }

            if (call.$promise) {

                call.$promise.then(success, error);
                return call.$promise;

            } else {

                return config.call.then(
                    function (response) {
                        if (response && response.data && response.data.success) {
                            return success(config.property
                                ? response.data[config.property] || response.data
                                : response.data);
                        } else {
                            return error(response);
                        }
                    },
                    error
                );
            }
        };

        service.connection = {
            get: function (config) {

                var name = "list",
                    call;

                if (config && config.id) {
                    call = core.connection.get({id: config.id});
                    name = "get";
                } else {
                    call = core.connection.query();
                }

                return service.run($.extend(config, {
                    notify: {show: "error"},
                    resource: "connection",
                    name: name,
                    call: call
                }));
            }
        };
        service.account = {
            delete: function (config) {

                return service.run({
                    notify: {
                        show: true,
                        translate: {
                            name: config.name
                        }
                    },
                    resource: "account",
                    name: "delete",
                    call: core.account.delete({
                        id: config.id
                    }),
                    success: config.success
                });
            },
            info: function (config) {

                var query = {
                    id: config.id
                };

                if (angular.isArray(config.field)) {
                    query.field = config.field[0];

                    if (config.field.length > 1) {
                        angular.forEach(config.field, function (field, key) {
                            query["field" + (key + 1)] = field;
                        });
                    }
                } else {
                    query.field = config.field;
                }

                return service.run({
                    notify: {
                        show: "error",
                        translate: {
                            field: angular.isArray(config.field)
                                ? config.field.join(".")
                                : config.field
                        }
                    },
                    resource: "account",
                    name: "getInfo",
                    call: core.accountInfo.query(query),
                    success: config.success
                });
            },
            get: function (config) {

                var name = "list",
                    call;

                if (config && config.id) {
                    call = core.account.get({id: config.id});
                    name = "get";
                } else {
                    call = core.account.query();
                }
                return service.run($.extend(config, {
                    notify: {show: "error"},
                    resource: "account",
                    name: name,
                    call: call
                }));
            },
            connect: function (config) {

                return service.run({
                    notify: {
                        show: true,
                        translate: {
                            name: config.name
                        }
                    },
                    resource: "account",
                    name: "reconnect",
                    call: core.account.update({id: config.id}, {
                        config: config.config
                    }),
                    success: config.success || function () {
                        return true;
                    }
                });
            },
            auth: function (config) {

                var name = "auth",
                    url = "/oauth/" + config.type + "/";

                if (config.id) {
                    url = url + config.id + "/ajaxReAuthorize";
                    name = "reauth";
                } else {
                    url = url + "ajaxAuthorize";
                }

                if (config.name) {
                    config.data.name = config.name;
                }

                return service.run({
                    notify: {
                        show: "error",
                        translate: {
                            name: config.name
                        }
                    },
                    resource: "account",
                    name: name,
                    call: $http({
                        method: "POST",
                        url: url,
                        params: config.params,
                        data: config.data
                    }),
                    success: config.success
                });
            }
        };

        service.user = {
            get: function (config) {

                return service.run({
                    notify: {
                        show: "error"
                    },
                    resource: "admin.user",
                    name: "get",
                    property: "users",
                    call: $http($.extend(config, {
                        method: "GET",
                        url: "/user/list",
                        data: ""
                    }))
                });
            },
            update: function (data, config) {

                return service.run({
                    notify: {
                        show: true
                    },
                    resource: "settings",
                    name: config.name || "updateProfile",
                    call: $http({
                        method: "POST",
                        url: "/user/edit",
                        data: data
                    }),
                    success: config.success,
                    error: config.error
                });
            }
        };

        service.contact = {
            email: function (config) {
                return service.run({
                    notify: true,
                    resource: "settings",
                    name: "sendEmailVerification",
                    call: $http.get("/contact/verify/" + config.id),
                    success: config.success,
                    error: config.error
                });
            }
        };

        service.vendor = {
            invoice: {
                get: function () {
                    return service.run({
                        notify: false,
                        resource: "billing",
                        name: "get",
                        call: core.billing.query()
                    });
                }
            },
            card: {
                get: function (config) {
                    return service.run(angular.extend(config || {}, {
                        notify: false,
                        resource: "admin.billing.card",
                        name: "get",
                        property: "card",
                        call: $http.get("/vendor/card")
                    }));
                },
                add: function (config) {

                    return $q(function (resolve, reject) {

                        Stripe.createToken(config.card, function (status, data) {

                            if (!data.error) {

                                service.run({
                                    notify: true,
                                    resource: "admin.billing.card",
                                    name: "add",
                                    call: $http.post("/vendor/card/add", {
                                        card_token: data.id
                                    }),
                                    success: function () {
                                        var response = {
                                                status: status,
                                                data: angular.extend(data, {
                                                    success: true
                                                })
                                            },
                                            success;

                                        if (config.success) {
                                            success = config.success(response);
                                        }

                                        resolve(success || response);
                                    },
                                    error: function () {
                                        reject({
                                            status: status,
                                            data: data
                                        });
                                    }
                                });

                            } else {
                                reject({
                                    status: status,
                                    data: data
                                });
                            }
                        });
                    });
                },
                delete: function (config) {
                    return service.run(angular.extend(config || {}, {
                        notify: true,
                        resource: "admin.billing.card",
                        name: "delete",
                        call: $http.get("/vendor/card/delete")
                    }));
                }
            },
            billing: {
                get: function () {
                    return service.run({
                        notify: false,
                        resource: "admin.billing",
                        name: "get",
                        call: $http.get("/vendor"),
                        property: "vendor"
                    });
                },
                edit: function (config) {
                    return service.run({
                        notify: {
                            show: true,
                            translate: {
                                name: config.name
                            }
                        },
                        resource: "admin.billing",
                        name: "edit",
                        call: $http.post("/vendor/edit", config.data),
                        success: config.success,
                        error: config.error
                    });
                },
                trypay: function (config) {
                    return service.run(angular.extend(config || {}, {
                        resource: "vendors",
                        name: "get",
                        call: core.trypay.get()
                    }));
                }
            },
            get: function (config) {
                return service.run(angular.extend(config || {}, {
                    notify: false,
                    resource: "vendors",
                    name: "get",
                    call: core.vendor.get()
                }));
            }
        };

        return service;
    });
