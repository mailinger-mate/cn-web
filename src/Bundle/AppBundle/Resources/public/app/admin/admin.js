/*global angular*/
/*property
    abstract, addPart, config, controller, data, filter, get, id, load,
    loadPlugin, module, only, pageTitle, permissions, redirect, resolve, state,
    template, templateUrl, url, user, users
*/

angular.module("cn.admin", [
    "permission",
    "oc.lazyLoad",
    "cn.core",
    "cn.user.list",
    "cn.user.item",
    "cn.billing"
])
    .config(function ($translatePartialLoaderProvider, $stateProvider) {

        "use strict";

        $translatePartialLoaderProvider.addPart('admin/admin');

        $stateProvider
            .state("admin", {
                url: "/admin",
                template: '<div ui-view autoscroll="false"></div>',
                redirect: "admin.user.list",
                data: {
                    permissions: {
                        only: ['admin']
                    }
                }
            })
            .state("admin.user", {
                url: "/user",
                templateUrl: "html/admin/user.html",
                redirect: "admin.user.list"
            })
            .state("admin.user.list", {
                url: "/list",
                resolve: {
                    users: function (action) {
                        return action.user.get();
                    },
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load("datatables");
                    }
                },
                controller: "cnUserList",
                templateUrl: "html/admin/user.list.html",
                data: {
                    pageTitle: "nav.admin"
                }
            })
            .state("admin.user.item", {
                url: "/item",
                template: '<div ui-view autoscroll="false"></div>',
                abstract: true
            })
            .state("admin.user.item.add", {
                url: "/add",
                resolve: {
                    user: function () {
                        return false;
                    }
                },
                controller: "cnUserItem",
                templateUrl: "html/admin/user.item.html",
                data: {
                    pageTitle: "nav.user.add"
                }
            })
            .state("admin.user.item.edit", {
                url: "/edit/{user:int}",
                resolve: {
                    users: function (action) {
                        return action.user.get();
                    },
                    user: function ($stateParams, users) {
                        return users.filter(function (user) {
                            if (user.id === $stateParams.user) {
                                return user;
                            }
                        })[0];
                    }
                },
                controller: "cnUserItem",
                templateUrl: "html/admin/user.item.html",
                data: {
                    pageTitle: "nav.user.edit"
                }
            });
    });
