/*global angular, Stripe*/
/*property
    $commitViewValue, $error, $render, $setDirty, $setPristine, $setViewValue,
    $valid, $watch, analyze, Discover, JCB, MasterCard, Visa, action, add, addClass,
    amex, attr, billing, brand, button, card, class, cnCardLastFour, config,
    controller, ctrl, cvc, delete, deleteCard, dinersclub, directive, discover,
    element, empty, error, exp_month, exp_year, expiration_month,
    expiration_year, expiry, field, focus, forEach, form, formCard, four,
    has_card, help, icon, jcb, label, lastFour, link, mastercard, mode, module,
    month, name, number, parseExpiry, placeholder, prefix, publishable_key,
    require, required, reset, resetFour, restrict, saveCard, scope, set,
    setPublishableKey, stripe, success, translate, type, validateForm, vendor,
    view, visa, year
*/

angular.module("cn.billing.card", [
    "angularPayments",
    "cn.config",
    "cn.action"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            billing: {
                card: {
                    brand: {
                        Visa: "visa",
                        "American Express": "amex",
                        MasterCard: "mastercard",
                        Discover: "discover",
                        JCB: "jcb",
                        "Diners Club": "dinersclub"
                    },
                    icon: {
                        visa: "fa-cc-visa",
                        mastercard: "fa-cc-mastercard",
                        amex: "fa-cc-amex",
                        discover: "fa-cc-discover",
                        dinersclub: "fa-cc-diners-club",
                        jcb: "fa-cc-jcb"
                    }
                }
            }
        });
    })
    .directive("cncardlastfour", function () {

        "use strict";

        return {
            restrict: "A",
            require: "ngModel",
            scope: {
                lastFour: "=cncardlastfour"
            },
            link: function ($scope, $element, ignore, $ctrl) {

                $element.addClass("input-card");

                $scope.$watch("lastFour", function (lastFour) {

                    var viewValue = lastFour
                        ? "**** **** **** " + lastFour
                        : "";

                    $ctrl.$setViewValue(viewValue);
                    $ctrl.$commitViewValue();
                    $ctrl.$setPristine();
                    $ctrl.$render();
                });
            }
        };
    })
    .controller("cnBillingCard", function ($scope, $timeout, Common, cfg, action, Card) {

        "use strict";

        function initCard(card) {

            if (card) {

                $scope.view.mode = "edit";

                $scope.card = card;

                $scope.form.field = {
                    expiry: card.expiration_month + " / " + card.expiration_year
                };

                $timeout(function () {
                    $scope.form.config.type = cfg.billing.card.brand[card.brand];
                });
            }
        }

        Stripe.setPublishableKey(cfg.stripe.publishable_key);

        $scope.view = {
            mode: "add"
        };

        $scope.form = {
            config: {
                field: {
                    number: {
                        analyze: false,
                        required: true,
                        placeholder: true,
                        reset: true,
                        empty: true,
                        attr: {
                            "payments-validate": "card",
                            "payments-format": "card",
                            "payments-type-model": "form.config.type",
                            cnCardLastFour: "card.last_four_digits",
                            "ng-click": "view.action.edit = true"
                        }
                    },
                    expiry: {
                        analyze: false,
                        required: true,
                        placeholder: true,
                        reset: true,
                        attr: {
                            "payments-validate": "expiry",
                            "payments-format": "expiry",
                            "ng-click": "view.action.edit = true"
                        }
                    },
                    name: {
                        analyze: false,
                        placeholder: true,
                        required: true
                    },
                    cvc: {
                        analyze: false,
                        required: true,
                        placeholder: true,
                        help: true,
                        attr: {
                            "payments-validate": "cvc",
                            "payments-format": "cvc",
                            "payments-type-model": "type"
                        }
                    }
                },
                prefix: {
                    translate: "admin.billing.card"
                }
            },
            field: {},
            error: {}
        };

        $scope.form.button = {
            card: [
                {
                    label: "save",
                    type: "submit",
                    action: "saveCard",
                    class: "btn-primary"
                },
                {
                    label: "delete",
                    type: "submit",
                    action: "deleteCard",
                    class: "btn-warning",
                    attr: {
                        "ng-show": "view.mode === 'edit'"
                    }
                }
            ]
        };

        $scope.icon = cfg.billing.card.icon;

        $scope.resetFour = function () {
            $scope.form.config.four = false;
            angular.element("#cardNumber").focus();
        };

        $scope.validateForm = function (form) {

            if (!form) {
                form = $scope.form.ctrl;
            }

            angular.forEach(form.$error.required, function (field) {
                field.$setDirty();
            });

            return form;
        };

        $scope.saveCard = function ($scope) {

            var card, expiry;

            if ($scope.validateForm($scope.formCard).$valid) {

                expiry = Common.parseExpiry($scope.form.field.expiry);
                card = $scope.form.field;

                return action.vendor.card.add({
                    card: {
                        number: card.number,
                        name: card.name,
                        exp_month: expiry.month,
                        exp_year: expiry.year,
                        cvc: card.cvc
                    },
                    success: function () {
                        $scope.view.mode = "edit";
                        cfg.billing.card.has_card = true;
                    }
                });
            }
        };

        $scope.deleteCard = function () {

            if ($scope.view.mode === "edit") {

                return action.vendor.card.delete({
                    success: function () {
                        $scope.form.field = {};
                        $scope.view.mode = "add";
                        $scope.formCard.$setPristine();
                        delete $scope.card;
                        cfg.billing.card.has_card = false;
                        return true;
                    }
                });

            } else {
                return false;
            }
        };

        initCard(Card);

    });
