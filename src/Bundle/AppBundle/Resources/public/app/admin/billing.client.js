/*global $ angular*/
/*property
    $emit, $error, $label, $setDirty, $valid, attr, billing, billingForm,
    billing_name, city, codeToCountry, config, controller, countries, country,
    countryInEU, ctrl, data, dynamic, edit, enabled, error, field, forEach,
    form, go, help, indexOf, message, mode, model, module, name, note, options,
    placeholder, prefix, required, saveBilling, state, street, success,
    toUpperCase, translate, type, validateForm, vat, vendor, view
*/

angular.module("cn.billing.client", [
    "iso-3166-country-codes",
    "ui.router",
    "cn.action"
])
    .controller('cnBillingClient', function ($scope, $state, Client, action, ISO3166) {

        "use strict";

        $scope.countryInEU = function () {

            var eu = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'],
                country = $scope.form.field.country;

            return country && eu.indexOf(country.toUpperCase()) >= 0;
        };

        $scope.validateForm = function (form) {

            if (!form) {
                form = $scope.form.ctrl;
            }
            angular.forEach(form.$error.required, function (field) {
                field.$setDirty();
            });

            return form;
        };

        $scope.saveBilling = function ($scope) {

            if ($scope.validateForm($scope.billingForm).$valid) {
                return action.vendor.billing.edit({
                    name: "edit",
                    data: $scope.form.field,
                    success: function (data) {
                        if ($scope.view.mode === "add") {
                            $scope.$emit("billing.client.add");
                            $state.go("admin.billing.card");
                        }
                        return data;
                    },
                    error: function (error) {

                        $scope.form.error.field = error.data
                            ? error.data.message
                            : {};
                    }
                });
            }
        };

        $scope.countries = [ISO3166.codeToCountry];

        $scope.view = {
            mode: Client.billing_name
                ? "edit"
                : "add"
        };

        $scope.form = {
            config: {
                field: {
                    billing_name: {
                        attr: {
                            "ng-maxlength": 255
                        },
                        model: 'billing_name',
                        required: true,
                        placeholder: true
                    },

                    country: {
                        attr: {
                        },
                        type: "select",
                        options: $scope.countries[0],
                        dynamic: {
                            options: {
                                $label: "key as value"
                            }
                        },
                        model: 'country',
                        placeholder: true,
                        translate: false,
                        required: true
                    },

                    street: {
                        attr: {
                            "ng-maxlength": 255
                        },
                        model: 'street',
                        required: true,
                        enabled: "country"
                    },

                    city: {
                        attr: {
                            "ng-maxlength": 255
                        },
                        model: 'city',
                        required: true,
                        enabled: "country"
                    },

                    state: {
                        attr: {
                            "ng-maxlength": 255
                        },
                        model: 'state',
                        enabled: "country"
                    },

                    vat: {
                        attr: {
                            "ng-pattern": '/^(?=^.{0,20}$)(([A-Z]{2})([A-Z0-9]{2})(([A-Z0-9]+( [A-Z0-9]+)?){0,3}([A-Z0-9]{1}))?)$/'
                        },
                        model: 'vat_number',
                        placeholder: true,
                        help: true,
                        enabled: "countryInEU()",
                        prefix: {
                            enabled: false
                        }
                    },

                    note: {
                        type: 'textarea',
                        attr: {
                            "ng-maxlength": 255
                        },
                        model: 'note',
                        placeholder: true
                    }
                },
                prefix: {
                    translate: 'admin.billing.client'
                }
            },

            field: Client,
            error: {}
        };
    });
