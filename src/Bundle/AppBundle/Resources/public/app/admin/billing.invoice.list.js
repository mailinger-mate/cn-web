/*global $ angular*/
/*property
    addClass, config, connections, controller, css, current, directive,
    disabled, dtColumnDefs, dtOptions, end_date, error, failure, filter,
    free_of_charge, invoice, link, module, newOptions, orderable, paid, price,
    processing, push, replace, restrict, searchable, set, start_date, status,
    targets, template, title, toFixed, trial, visible, withOption
*/


angular.module("cn.billing.invoice", [
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            css: {
                invoice: {
                    failure: 'icon-box status-warn fa fa-warning',
                    processing: 'icon-box status-info fa fa-gears',
                    paid: 'icon-box status-info fa fa-check',
                    error: 'icon-box status-error fa fa-warning',
                    trial: 'icon-box status-info fa fa-gift',
                    disabled: 'icon-box status-disabled fa fa-times',
                    current: 'icon-box status-info fa fa-repeat',
                    free_of_charge: 'icon-box status-info fa fa-gift'
                }
            }
        });
    })
    .controller('cnBillingInvoice', function ($scope, DTOptionsBuilder, Invoice) {
        "use strict";
        $scope.invoice = Invoice;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption("order", [[0, "desc"]])
            .withOption('responsive', true);

        $scope.dtColumnDefs = [
            {
                targets: 'no-sort',
                orderable: false
            },
            {
                targets: [0],
                visible: false,
                searchable: true
            }
        ];
    })
    .directive("changeStatus", function ($filter, cfg) {

        "use strict";

        return {
            restrict: "E",
            template: "<i ng-attr-title='{{ title }}'>",
            replace: true,
            link: function (scope, element, attrs) {
                var status = attrs.status;
                scope.title = $filter("translate")("admin.billing.invoice.status." + status);
                element.addClass(cfg.css.invoice[status]);
            }
        };
    })
    .filter('centToDollar', function () {
        "use strict";
        return function (input) {
            var cents = (input / 100);
            return (cents % 1 === 0)
                ? '$' + cents
                : '$' + cents.toFixed(2);
        };
    })
    .filter('timeZone', function DateFilter($filter) {
        "use strict";
        return function (text) {

            if (text) {
                var tempdate = new Date(text.replace(/-/g, "/"));
                return $filter('date')(tempdate, "yyyy-MM-dd");
            } else {
                return '---';
            }
        };
    });
