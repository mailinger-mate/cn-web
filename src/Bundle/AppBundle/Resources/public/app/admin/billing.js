/*global angular, Stripe*/
/*property
    $broadcast, $on, Card, Client, Invoice, Stripe, billing, billing_name,
    buttonTryPay, card, client, config, controller, data, disabled, error,
    files, get, go, has_card, ico, invoice, isClient, load, loadPlugin, log,
    module, modules, name, nav, notify, pageTitle, redirect, resolve, show,
    sref, state, success, templateUrl, translate, tryPay, trypay, url, vendor,
    vendorBlocked
*/

angular.module("cn.billing", [
    "ui.router",
    "oc.lazyLoad",
    "cn.action",
    "cn.billing.invoice",
    "cn.billing.client",
    "cn.billing.card"
])
    .config(function ($ocLazyLoadProvider, $stateProvider) {

        "use strict";

        $ocLazyLoadProvider.config({
            modules: [{
                name: "stripe",
                files: ["js!https://js.stripe.com/v2/"]
            }]
        });

        $stateProvider.state("admin.billing", {
            url: "/billing",
            templateUrl: "html/admin/billing.html",
            controller: "cnBilling",
            redirect: "admin.billing.invoice",
            resolve: {
                Client: function (action) {
                    return action.vendor.billing.get();
                }
            }
        })
            .state("admin.billing.invoice", {
                url: "/invoice",
                resolve: {
                    Invoice: function (action) {
                        return action.vendor.invoice.get();
                    },
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load("datatables");
                    }
                },
                controller: "cnBillingInvoice",
                templateUrl: "html/admin/billing.invoice.list.html",
                data: {
                    pageTitle: "nav.billing.invoice"
                }
            })
            .state("admin.billing.client", {
                url: "/client",
                controller: "cnBillingClient",
                templateUrl: "html/admin/billing.client.html"
            })
            .state("admin.billing.card", {
                url: "/card",
                resolve: {
                    Stripe: function ($ocLazyLoad) {
                        return $ocLazyLoad.load("stripe");
                    },
                    Card: function (action) {
                        return action.vendor.card.get();
                    }
                },
                controller: "cnBillingCard",
                templateUrl: "html/admin/billing.card.html"
            });
    })
    .controller("cnBilling", function ($scope, Client, action, $rootScope, cfg, $state) {

        "use strict";
        $scope.client = !!Client.billing_name;

        $scope.nav = [
            {
                sref: "admin.billing.invoice",
                name: "admin.billing.nav.invoice",
                ico: "fa fa-file-text"
            },
            {
                sref: "admin.billing.client",
                name: "admin.billing.nav.client",
                ico: "fa fa-building"
            },
            {
                sref: "admin.billing.card",
                name: "admin.billing.nav.card",
                disabled: "!isClient()",
                ico: "fa fa-credit-card-alt"
            }
        ];

        $scope.$on("billing.client.add", function () {
            $scope.client = true;
        });

        $scope.buttonTryPay = $rootScope.vendorBlocked;

        $scope.isClient = function () {
            return $scope.client;
        };

        $scope.tryPay = function () {
            if ($scope.buttonTryPay) {
                if (cfg.billing.card.has_card) {
                    return action.vendor.billing.trypay({
                        notify: {
                            show: true,
                            translate: {}
                        },
                        success: function (data) {
                            $rootScope.$broadcast("vendorBlocked", false);
                            return (data);
                        },
                        error: function (error) {
                            return (error);
                        }
                    });
                }
                if ($scope.client) {
                    $state.go('admin.billing.card');
                } else {
                    $state.go('admin.billing.client');
                }
            }
        };
    });