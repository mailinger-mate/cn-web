/*global angular*/

/*property
    $error, $setDirty, $valid, action, button, call, class, config, confirm,
    contact, controller, ctrl, data, deleteUser, email, error, errors, field,
    forEach, form, get, go, id, label, message, mode, module, name, notify,
    options, placeholder, post, prefix, push, required, resource, role, roles,
    run, saveUser, show, success, translate, type, user, value, verified, view
*/
angular.module("cn.user.item", [
    "cn.core"
])
    .controller("cnUserItem", function ($scope, $state, $http, action, user) {

        "use strict";

        function initUser(config) {

            if (config.user) {
                $scope.view.mode = "edit";
                $scope.form.field = config.user;
                $scope.form.button.push({
                    label: "delete",
                    type: "button",
                    action: "deleteUser",
                    confirm: true,
                    class: "btn-warning"
                });
            }
        }

        $scope.view = {
            mode: "add"
        };

        $scope.form = {
            config: {
                field: {
                    name: {
                        placeholder: true,
                        required: true
                    },
                    email: {
                        type: "email",
                        placeholder: true,
                        required: true

                    },
                    verified: {
                        type: "select",
                        options: [true, false]
                    },
                    role: {
                        type: "select",
                        value: "user",
                        options: ["user", "admin"],
                        required: true
                    }
                },
                prefix: {
                    translate: "admin.user.item"
                }
            },
            error: {}
        };

        $scope.form.button = [{
            label: "save",
            type: "submit",
            action: "saveUser",
            class: "btn-primary"
        }];

        $scope.saveUser = function ($scope) {

            var url = "user/add";

            angular.forEach($scope.form.ctrl.$error.required, function (field) {
                field.$setDirty();
            });

            if ($scope.form.ctrl.$valid) {

                if (user) {
                    url = "user/edit/" + user.id;
                }

                return action.run({
                    notify: {
                        show: true,
                        translate: {
                            name: $scope.form.field.name
                        }
                    },
                    resource: "admin.user",
                    name: $scope.view.mode,
                    call: $http.post(url, {
                        name: $scope.form.field.name,
                        role: $scope.form.field.role,
                        contact: $scope.form.field.contact
                    }),
                    success: function (data) {

                        delete $scope.form.error.field;
                        $state.go("admin.user.list");
                        return data;
                    },
                    error: function (error) {

                        $scope.form.error.field = error.data
                            ? error.data.message
                            : {};
                    }
                });
            }
            return false;
        };

        $scope.deleteUser = function ($scope) {

            return action.run({
                notify: {
                    show: true,
                    translate: {
                        name: $scope.form.field.name
                    }
                },
                resource: "admin.user",
                name: "delete",
                call: $http.get('user/delete/' + user.id),
                success: function (data) {

                    delete $scope.form.error.field;
                    $state.go("admin.user.list");
                    return data;
                },
                error: function (error) {

                    $scope.form.error.field = error.data
                        ? error.data.errors
                        : {};
                }
            });
        };

        initUser({
            user: user
        });
    });
