/*global angular*/
angular.module("cn.user.list", [
    "cn.core"
])
    .controller("cnUserList", function ($scope, DTOptionsBuilder, users, cfg) {

        "use strict";

        $scope.users = users;

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption("order", [[0, "desc"]])
            .withOption('responsive', true);

        $scope.dtColumnDefs = [
            {
                targets: 'no-sort',
                orderable: false
            },
            {
                targets: [0],
                visible: false,
                searchable: false
            }
        ];

        $scope.checkContact = function (contact) {
            if (contact === cfg.user.email.address) {
                return true;
            } else {
                return false;
            }
        };
    });
