/*global angular*/
/*property
    $broadcast, $get, $on, CN, FS, addPart, address, billing, blocked, card,
    config, controller, data, defineManyRoles, directive, dismiss, dismissAll,
    displayName, email, error, files, get, getItem, go, has_card, home, id,
    identify, module, modules, name, nav, open, otherwise, pageTitle, path,
    preferred, preferredLanguage, preventDefault, priority, redirect,
    remaining_trial_days, role, run, setItem, showOnce, size, state, success,
    templateUrl, terminal, text, then, translate, trialdays, url, user, vendor,
    vendorName_str, windowClass, defaultPrevented
*/

angular.module("cn", [
    "inspinia",
    "ui.router",
    "permission",
    "oc.lazyLoad",
    "ui.bootstrap",
    "pascalprecht.translate",
    "ui.gravatar",
    "cn.config",
    "cn.config.style",
    "cn.init",
    "cn.session",
    "cn.helper",
    "cn.input",
    "cn.button",
    "cn.tooltip",
    "cn.translate",
    "cn.action",
    "cn.core",
    "cn.nav",
    "cn.error",
    "cn.connection",
    "cn.account",
    "cn.admin",
    "cn.tutorial",
    "cn.settings",
    "cn.feedback"
])
    .config(function ($urlRouterProvider, $stateProvider, $translateProvider, $translatePartialLoaderProvider, $ocLazyLoadProvider, cfgProvider) {

        "use strict";

        var cfg = cfgProvider.$get();

        $translateProvider.preferredLanguage(cfg.translate.preferred);
        $translatePartialLoaderProvider.addPart("app");

        $urlRouterProvider.otherwise(cfg.nav.error.path);

        $stateProvider.state("root", {
            url: "",
            redirect: cfg.nav.home.state
        });

        $ocLazyLoadProvider.config({
            modules: [
                {
                    name: "datatables",
                    files: ["js/plugin/angular-datatables.js", "css/plugin/angular-datatables.css"]
                }
            ]
        });
    })
    .run(function ($rootScope, $state, $window, Permission, cfg, action, $modal, $modalStack) {

        "use strict";

        Permission.defineManyRoles(["admin", "user"], function (ignore, roleName) {
            return cfg.user.role === roleName;
        });

        $rootScope.$on('$stateChangeStart', function (event, to, params) {
            if (to.redirect) {
                event.preventDefault();
                $state.go(to.redirect, params);
            }
        });

        $rootScope.$on("$stateChangeError", function (event) {
            if (event.defaultPrevented === false) {
                $state.go("error.400");
            }
        });

        var makeModal = function (config) {
            if (config && !localStorage.getItem(config.templateUrl)) {
                $modal.open({
                    templateUrl: '/html/form/' + config.templateUrl,
                    size: 'lg',
                    windowClass: 'modal-window',
                    controller: function ($scope) {
                        $scope.role = cfg.user.role;
                        $scope.trialdays = config.trialdays;
                        $scope.dismiss = function (redirect) {
                            $modalStack.dismissAll();
                            if (redirect) {
                                $state.go('admin.billing.invoice');
                            }
                        };
                    }
                });

                if (config.showOnce !== false) {
                    localStorage.setItem(config.templateUrl, true);
                }
            }
        };

        action.vendor.get({
            success: function (vendor) {
                var config;
                cfg.billing.card.has_card = vendor.has_card;
                if (vendor.blocked) {
                    $rootScope.$broadcast("vendorBlocked", true);
                    config = {templateUrl: 'modal.blocked.html', showOnce: false};
                    makeModal(config);
                } else if (!vendor.has_card) {
                    if (vendor.remaining_trial_days <= 7 && vendor.remaining_trial_days > 1) {
                        config = {templateUrl: 'modal.endtrial.html', showOnce: true, trialdays: vendor.remaining_trial_days};
                        makeModal(config);
                    }
                }
            }
        });

        if ($window.FS !== undefined) {
            $window.FS.identify('connect_' + cfg.user.id, {
                displayName: cfg.user.name,
                email: cfg.user.email.address,
                vendorName_str: cfg.user.vendor
            });
        }
    })
    .controller('AppCtrl', function ($scope, cfg) {

        "use strict";

        $scope.CN = cfg;
    })
    .directive('pageTitle', function ($rootScope, $translate) {

        "use strict";

        return {
            priority: 1,
            terminal: true,
            controller: function ($element) {

                $rootScope.$on('$stateChangeSuccess', function (ignore, state) {

                    $translate('product.name').then(function (pageTitle) {
                        if (state.data && state.data.pageTitle) {
                            $translate(state.data.pageTitle).then(function (stateTitle) {
                                $element.text(stateTitle + " - " + pageTitle);
                            });
                        } else {
                            $element.text(pageTitle);
                        }
                    });
                });
            }
        };
    });
