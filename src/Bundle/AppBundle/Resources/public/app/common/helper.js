/*global $ angular*/

angular.module("cn.helper", [])
    .service("helper", function () {

        "use strict";

        var service = {};

        service.capitalizeString = function (string) {

            if (string) {
                return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
            } else {
                return;
            }
        };

        service.processURL = function (url) {

            var a = $("<a>").attr("href", url)[0],
                host = a.hostname.split("."),
                path = a.pathname.substr(0, a.pathname.lastIndexOf('.')) || a.pathname,
                slice = 0,
                name;

            if (host.length === 1 || !isNaN(host[host.length - 1])) {

                name = a.hostname;

            } else {

                if (host[0] === "www") {
                    slice = 1;
                }
                name = host.slice(slice, host.length - 1).reverse().join(" ");
            }

            return {
                host: a.hostname,
                name: service.capitalizeString(name),
                path: path.substr(1).replace(/\W+/g, " ").replace(/[^A-Za-z\s.]/g, "").replace(/\s\s+/g, " ").replace(/\s\s*$/, "")
            };
        };

        return service;
    });
