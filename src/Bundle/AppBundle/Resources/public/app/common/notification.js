/*global $ angular*/
/*property
    $on, classes, config, container, css, extend, getElementById, join, level,
    maximumOpen, message, module, name, notification, prefix, push, run,
    service, split, startTop, templateUrl, text, then, translate
*/

angular.module("cn.notification", [
    "cn.config",
    "pascalprecht.translate",
    "cgNotify"
])
    .service("notification", function ($translate, cfg, notify) {

        "use strict";

        var service = {
            push: function (config) {

                var string;

                function push(text) {
                    if (notify) {
                        notify($.extend({
                            message: text,
                            classes: cfg.css.notification[config.level || "info"] + " cg-notify-message-right cn-notify-message"
                        }, config));
                    }
                }

                if (!config) {
                    return false;
                }

                if (typeof config === "string") {
                    string = config.split("/");
                    config = {
                        level: string[0],
                        prefix: string[1],
                        name: string[2]
                    };
                }

                if (config.text) {

                    push(config.text);

                } else {

                    config.text = [config.prefix, "notification", config.name, config.level];
                    $translate(config.text.join("."), config.translate).then(function (text) {
                        push(text);
                    });
                }
            }

        };

        notify.config({
            startTop: 90,
            maximumOpen: 1,
            templateUrl: "notify.html",
            container: document.getElementById("page-wrapper")
        });

        return service;
    })
    .run(function ($rootScope, notification) {

        "use strict";

        $rootScope.$on("action", function (ignore, config) {
            notification.push(config);
        });
    });
