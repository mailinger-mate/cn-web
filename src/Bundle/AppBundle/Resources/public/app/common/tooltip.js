/*global $ angular*/
/*property
    $$state, active, addClass, base, cancel, config, css, focus, module, on,
    preventDefault, removeClass, run, set, target, tooltip, trigger, value
*/

angular.module('cn.tooltip', [
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            css: {
                tooltip: {
                    base: "cn-tooltip",
                    active: "cn-tooltip-active"
                }
            }
        });
    })
    .run(function ($timeout, cfg) {

        "use strict";

        var timeout;

        $("body").on("touchstart", "[title]", function (event) {

            var element = $(event.target);

            event.preventDefault();

            timeout = $timeout(function () {
                element.addClass(cfg.css.tooltip.base);
                $timeout(function () {
                    element.addClass(cfg.css.tooltip.active);
                });
                return true;
            }, 400);

        }).on("touchend", "[title]", function (event) {

            var element = $(event.target);

            $timeout(function () {
                element.removeClass(cfg.css.tooltip.active);
                $timeout(function () {
                    element.removeClass(cfg.css.tooltip.base);
                }, 400);
            }, 800);

            if (!timeout.$$state.value) {
                element.trigger("click").focus();
            }
            $timeout.cancel(timeout);
        });

    });
