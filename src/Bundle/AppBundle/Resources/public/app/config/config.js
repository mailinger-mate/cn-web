/*global $ angular*/
/*property
    $get, error, extend, find, home, module, namespace, nav, path, preferred,
    provider, reduce, set, split, state, translate
*/

angular.module('cn.config', [])
    .provider('cfg', function () {

        "use strict";

        var config = {
            translate: {
                namespace: "cn",
                preferred: "en"
            },
            nav: {
                home: {
                    state: "connection.list"
                },
                error: {
                    path: "error/404"
                }
            }
        };

        return {
            set: function (settings) {
                $.extend(true, config, settings);
            },
            $get: function () {
                return config;
            },
            find: function (key) {
                return key.split(".").reduce(function (object, key) {
                    return object[key] || object;
                }, config);
            }
        };
    });
