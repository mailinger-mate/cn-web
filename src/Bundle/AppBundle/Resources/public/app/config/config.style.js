/*global angular*/

angular.module("cn.config.style", [
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            css: {
                icon: {
                    unknown: "fa fa-question",
                    imap: "fa fa-envelope",
                    rss: "fa fa-rss",
                    slack: "fa fa-slack",
                    smtp: "fa fa-envelope",
                    hipchat: "icon-connect icon-connect-hipchat",
                    twitter: "fa fa-twitter",
                    webhook: "icon-connect icon-connect-webhook",
                    salesforce: "icon-connect icon-connect-salesforce",
                    jira: "icon-connect icon-connect-jira",
                    status: {
                        warn: "fa fa-warning",
                        error: "fa fa-warning",
                        unknown: "fa fa-question",
                        info: "fa fa-check",
                        disabled: "fa fa-times"
                    },
                    state: {
                        enabled: "fa fa-check",
                        disabled: "fa fa-times"
                    },
                    load: "fa fa-circle-o-notch fa-spin",
                    done: "fa fa-check",
                    error: "fa fa-warning",
                    help: "fa fa-question",
                    alert: "fa fa-exclamation"
                },
                button: {
                    base: "btn",
                    status: {
                        warning: "btn btn-warning"
                    },
                    primary: "btn-primary",
                    danger: "btn-danger",
                    default: "btn-default",
                    white: "btn-white",
                    group: "btn-group",
                    switch: "btn-switch"
                },
                input: {
                    group: "input-group",
                    button: "input-group-btn",
                    control: "form-control",
                    text: "control-text",
                    switch: "switch-input"
                },
                form: {
                    group: "form-group",
                    label: "control-label",
                    input: "control-input"
                },
                text: {
                    muted: "text-muted",
                    danger: "text-danger"
                },
                color: {
                    on: "#1ab394",
                    off: "#d3d3d3",
                    init: "#79d2c0",
                    warning: "#f7a54a",
                    error: "#ed5565"
                },
                field: {
                    invalid: "has-error"
                },
                notification: {
                    success: "alert-info",
                    error: "alert-danger",
                    info: "alert-success"
                },
                analyze: {
                    hidden: "analyze-hidden"
                }
            },
            animate: {
                hold: 1000
            }
        });
    });
