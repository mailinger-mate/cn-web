/*global angular*/

/*property
    $data, $expression, $filter, $label, $listener, $property, $source,
    attachment, binary, body, comment, concat, config, connection, content,
    data, description, destination, dynamic, editor, enabled, expression,
    field, form, help, hipchat, html, imap, info, input, issue, issue_key,
    jira, label, listener, local, max, meta, min, mode, model, module, name,
    object, operator, options, payload, placeholder, prefix, property, query,
    ref, required, rss, salesforce, select, set, slack, smtp, source, subject,
    summary, template, text, toggle, translate, twitter, type, value, voter,
    watcher, webhook
*/

angular.module("cn.connection.config", [
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        var config = {};

        config.query = {
            operator: ["equals", "contains", "matches"]
        };

        config.placeholder = ["current_date", "current_time"];

        config.ref = {
            rss: {
                dynamic: {
                    name: "tag"
                },
                options: [
                    {name: "title"},
                    {name: "author"},
                    {name: "summary"},
                    {name: "link"},
                    {name: "published"},
                    {name: "updated"},
                    {name: "id"}
                ]
            },
            imap: {
                options: ["from", "to", "subject", "body"]
            },
            twitter: {
                options: ["id", "created_at", "favorite_count", "favorited", "lang", "place", "retweet_count", "retweeted", "source", "text", "truncated", "hashtags", "urls", "user_id", "user_created_at", "user_description", "user_favourites_count", "user_followers_count", "user_following", "user_friends_count", "user_lang", "user_listed_count", "user_location", "user_name", "user_statuses_count", "user_time_zone", "user_url"]
            },
            salesforce: {
                dynamic: {
                    field: "config.fields"
                }
            },
            jira: {
                options: ["issue_id", "issue_key ", "issue_summary", "issue_description", "issue_project_key", "issue_project_name", "issue_assignee_name", "issue_assignee_display_name", "issue_creator_name", "issue_creator_display_name", "issue_reporter_name", "issue_reporter_display_name", "issue_type_name", "issue_priority_name", "issue_resolution_name", "issue_status_name", "issue_resolution_date", "issue_created_date", "issue_updated_date", "issue_viewed_date"],
                object: {
                    issue: ["issue_votes_count", 'issue_watches_count'],
                    comment: ["comment_id", "comment_body", "comment_author_name", "comment_author_display_name", "comment_updated_name", "comment_updated_display_name", "comment_created_date", "comment_updated_date"],
                    watcher: ["watcher_name", "watcher_display_name"],
                    voter: ["voter_name", "voter_display_name"],
                    attachment: ["attachment_id", "attachment_filename", "attachment_mime_type", "attachment_size", "attachment_content_url", "attachment_author_name", "attachment_author_display_name", "attachment_created_date"]
                },
                dynamic: {
                    field: "config.object",
                    local: function (object) {

                        var options = config.ref.jira.options;

                        if (object && config.ref.jira.object[object]) {
                            options = config.ref.jira.object[object].concat(options);
                        }

                        return options;
                    }
                }
            }
        };

        cfgProvider.set({
            connection: {
                config: {
                    source: {
                        imap: [
                            {
                                model: "interval",
                                type: "interval",
                                placeholder: true,
                                help: true,
                                info: true,
                                min: 10,
                                max: 2592000,
                                value: 600,
                                prefix: {
                                    form: false
                                }
                            },
                            {
                                model: "mailbox",
                                type: "select",
                                options: [],
                                dynamic: {
                                    name: "mailbox",
                                    options: {
                                        $label: "folder.fullName as folder.fullName",
                                        $data: "folder",
                                        $source: "field.options"
                                    }
                                },
                                translate: false,
                                placeholder: true,
                                help: true,
                                required: true
                            },
                            {
                                model: "query",
                                type: "query",
                                placeholder: "query_imap",
                                help: true,
                                required: false,
                                query: {
                                    field: {
                                        options: config.ref.imap.options
                                    },
                                    operator: {
                                        options: config.query.operator
                                    }
                                }
                            }
                        ],
                        rss: [
                            {
                                model: "interval",
                                type: "interval",
                                placeholder: true,
                                help: true,
                                min: 10,
                                value: 3600,
                                prefix: {
                                    form: false
                                }
                            },
                            {
                                model: "query",
                                type: "query",
                                placeholder: "query_rss",
                                help: true,
                                required: false,
                                query: {
                                    field: {
                                        options: config.ref.rss.options,
                                        dynamic: {
                                            options: {
                                                $property: "name",
                                                $filter: "orderBy:'name'"
                                            },
                                            name: "tag"
                                        }
                                    },
                                    operator: {
                                        options: config.query.operator
                                    }
                                }
                            }
                        ],
                        twitter: [
                            {
                                model: "interval",
                                type: "interval",
                                placeholder: true,
                                help: true,
                                min: 1,
                                value: 600,
                                prefix: {
                                    form: false
                                }
                            },
                            {
                                model: "query",
                                type: "query",
                                placeholder: "query_twitter",
                                help: true,
                                required: false,
                                query: {
                                    field: {
                                        options: config.ref.twitter.options
                                    },
                                    operator: {
                                        options: config.query.operator
                                    }
                                }
                            }
                        ],
                        salesforce: [
                            {
                                model: "interval",
                                type: "interval",
                                placeholder: true,
                                help: true,
                                min: 30,
                                value: 600,
                                prefix: {
                                    form: false
                                }
                            },
                            {
                                model: "trigger_action",
                                type: "select",
                                options: ["create", "update", "delete"],
                                placeholder: "salesforce_trigger",
                                label: "salesforce_trigger",
                                help: "salesforce_trigger",
                                required: true
                            },
                            {
                                model: "object",
                                type: "select",
                                options: [],
                                dynamic: {
                                    name: "sobject",
                                    options: {
                                        $label: "object.name as object.label",
                                        $data: "object",
                                        $source: "field.options",
                                        $filter: "orderBy:'label'"
                                    }
                                },
                                translate: false,
                                placeholder: "salesforce_object",
                                label: "salesforce_object",
                                help: "salesforce_object",
                                required: true
                            },
                            {
                                model: "fields",
                                type: "tags",
                                input: {
                                    type: "select",
                                    options: [],
                                    dynamic: {
                                        options: {
                                            $label: "field.name as field.label",
                                            $data: "field",
                                            $filter: "orderBy:'label'"
                                        },
                                        name: {
                                            expression: "config.object",
                                            listener: function (object) {
                                                return ["sobject", object];
                                            }
                                        }
                                    },
                                    translate: false
                                },
                                label: "salesforce_fields",
                                help: "salesforce_fields",
                                placeholder: "salesforce_fields",
                                required: true
                            },
                            {
                                model: "query",
                                type: "query",
                                placeholder: "query_salesforce",
                                help: true,
                                required: false,
                                query: {
                                    field: {
                                        options: [],
                                        translate: false,
                                        dynamic: {
                                            options: {
                                                $label: "f.name as f.label",
                                                $data: "f",
                                                $filter: "orderBy:'label'"
                                            },
                                            name: {
                                                expression: "config.object",
                                                listener: function (object) {
                                                    return ["sobject", object];
                                                }
                                            }
                                        }
                                    },
                                    operator: {
                                        options: config.query.operator
                                    },
                                    data: {
                                        options: [],
                                        property: "value",
                                        dynamic: {
                                            options: {
                                                $label: "o.label as o.value",
                                                $data: "o",
                                                $source: "rule.field.picklistValues",
                                                $filter: "orderBy:'label'"
                                            }
                                        },
                                        input: {
                                            text: "rule.field.type != 'picklist'",
                                            select: "rule.field.type == 'picklist'"
                                        }
                                    }
                                }
                            }
                        ],
                        jira: [
                            {
                                model: "interval",
                                type: "interval",
                                placeholder: true,
                                help: true,
                                min: 60,
                                value: 600,
                                prefix: {
                                    form: false
                                }
                            },
                            {
                                model: "object",
                                type: "select",
                                options: ["issue", "comment", "watcher", "voter", "attachment"],
                                placeholder: "jira_object",
                                label: "jira_object",
                                help: "jira_object",
                                required: true
                            },
                            {
                                model: "trigger",
                                type: "select",
                                options: ["created", "updated"],
                                placeholder: "jira_trigger",
                                label: "jira_trigger",
                                help: "jira_trigger",
                                required: true
                            },
                            {
                                model: "issue_key",
                                enabled: "config.object!='issue'",
                                required: true,
                                placeholder: true,
                                help: true
                            },
                            {
                                model: "query",
                                type: "query",
                                enabled: "config.object!='issue'",
                                placeholder: "query_jira",
                                help: true,
                                required: false,
                                query: {
                                    field: {
                                        options: [],
                                        dynamic: {
                                            options: {
                                                $expression: "config.object",
                                                $listener: config.ref.jira.dynamic.local
                                            }
                                        }
                                    },
                                    operator: {
                                        options: config.query.operator
                                    }
                                }
                            },
                            {
                                model: "jql",
                                enabled: "config.object=='issue'",
                                placeholder: true,
                                required: true,
                                help: true
                            }
                        ]
                    },
                    destination: {
                        hipchat: [
                            {
                                model: false,
                                type: "hipchat",
                                label: "hipchat_to"
                            }
                        ],
                        jira: [
                            {
                                model: "action",
                                type: "select",
                                label: "jira_action",
                                placeholder: "jira_action",
                                options: ["create", "update"],
                                help: "jira_action",
                                required: true
                            },
                            {
                                model: "project_key",
                                type: "select",
                                options: [],
                                help: true,
                                placeholder: true,
                                required: true,
                                enabled: "config.action.value=='create'",
                                dynamic: {
                                    name: "project",
                                    options: {
                                        $label: "p.key as p.label",
                                        $data: "p",
                                        $source: "field.options",
                                        $filter: "orderBy:'label'"
                                    }
                                }
                            },
                            {
                                model: "issue_type",
                                type: "select",
                                options: [],
                                help: true,
                                placeholder: true,
                                required: true,
                                enabled: "config.action.value=='create'",
                                dynamic: {
                                    options: {
                                        $label: "t.key as t.label",
                                        $data: "t",
                                        $source: "field.options",
                                        $filter: "orderBy:'label'"
                                    },
                                    name: {
                                        expression: "config.project_key.value",
                                        listener: function (project_key) {
                                            return ["project", project_key];
                                        }
                                    }
                                }
                            }
                        ],
                        slack: [
                            {
                                model: false,
                                type: "slack",
                                label: "slack_to"
                            }
                        ],
                        smtp: [
                            {
                                model: "recipients",
                                type: "tags",
                                required: true,
                                help: true,
                                ref: true,
                                input: {
                                    placeholder: "recipients"
                                }
                            },
                            {
                                model: "recipients_cc",
                                type: "tags",
                                placeholder: true,
                                help: true,
                                ref: true,
                                input: {
                                    placeholder: "recipients_cc"
                                }
                            },
                            {
                                model: "recipients_bcc",
                                type: "tags",
                                required: "config.action.value=='create'",
                                placeholder: true,
                                ref: true,
                                input: {
                                    placeholder: "recipients_bcc"
                                }
                            }
                        ],
                        webhook: []
                    }
                },
                template: {
                    destination: {
                        hipchat: {
                            content: {
                                value: {
                                    type: "editor",
                                    placeholder: true,
                                    required: true,
                                    ref: true,
                                    toggle: "config.content.meta.message_format",
                                    editor: "hipchat"
                                },
                                meta: [
                                    {
                                        model: "notify",
                                        type: "switch",
                                        binary: true,
                                        value: true,
                                        help: true,
                                        translate: "notify"
                                    },
                                    {
                                        model: "color",
                                        type: "select",
                                        options: ["yellow", "red", "green", "purple", "gray", "random"],
                                        value: "yellow",
                                        help: true,
                                        translate: "color",
                                        enabled: "config.room_id.value"
                                    },
                                    {
                                        model: "message_format",
                                        type: "select",
                                        options: ["text", "html"],
                                        value: "text",
                                        help: true,
                                        translate: "message_format"
                                    }
                                ]
                            }
                        },
                        jira: {
                            issue_key: {
                                value: {
                                    enabled: "config.action.value=='update'",
                                    required: true,
                                    placeholder: true,
                                    help: true,
                                    ref: true
                                }
                            },
                            summary: {
                                value: {
                                    placeholder: true,
                                    required: "config.action.value=='create'",
                                    ref: true
                                }
                            },
                            description: {
                                value: {
                                    type: "textarea",
                                    required: "config.action.value=='create'",
                                    ref: true,
                                    placeholder: true
                                }
                            },
                            comment: {
                                value: {
                                    type: "textarea",
                                    ref: true,
                                    placeholder: true
                                }
                            }
                        },
                        slack: {
                            content: {
                                value: {
                                    type: "textarea",
                                    placeholder: true,
                                    required: true,
                                    ref: true
                                },
                                meta: [
                                    {
                                        model: "as_bot",
                                        type: "switch",
                                        binary: false,
                                        value: true,
                                        help: true,
                                        translate: "as_bot"
                                    },
                                    {
                                        model: "bot_name",
                                        value: "Connectbot",
                                        help: true,
                                        placeholder: true,
                                        enabled: "config.content.meta.as_bot"
                                    },
                                    {
                                        model: "link_names",
                                        type: "switch",
                                        binary: false,
                                        value: true,
                                        help: true,
                                        translate: "link_names"
                                    }
                                ]
                            }
                        },
                        smtp: {
                            subject: {
                                value: {
                                    type: "text",
                                    placeholder: true,
                                    required: true,
                                    ref: true
                                }
                            },
                            body: {
                                value: {
                                    type: "editor",
                                    placeholder: true,
                                    required: true,
                                    ref: true,
                                    toggle: {
                                        model: "config.body.meta.format",
                                        mode: {
                                            "text/html": "html",
                                            "text/plain": "text"
                                        }
                                    }
                                },
                                meta: [
                                    {
                                        model: "format",
                                        type: "select",
                                        options: {text: "text/plain", html: "text/html"},
                                        value: "text/plain",
                                        help: true,
                                        translate: "message_format"
                                    }
                                ]
                            }
                        },
                        webhook: {
                            query: {
                                value: {
                                    type: "text",
                                    placeholder: true,
                                    ref: true
                                }
                            },
                            payload: {
                                value: {
                                    type: "textarea",
                                    placeholder: true,
                                    ref: true
                                },
                                meta: [
                                    {
                                        model: "content_type",
                                        type: "select",
                                        options: ["text/plain", "text/xml", "application/json", "application/x-www-form-urlencoded"],
                                        value: "text/plain",
                                        help: true,
                                        translate: false
                                    }
                                ]
                            }
                        }
                    }
                },
                ref: config.ref,
                placeholder: config.placeholder,
                attachment: {
                    hipchat: {},
                    jira: {},
                    slack: {},
                    smtp: {}
                }
            }
        });
    });
