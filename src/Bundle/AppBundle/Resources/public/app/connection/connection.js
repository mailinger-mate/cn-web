/*global angular*/
/*property
    $eval, $watch, _embedded, abstract, account, accounts, addClass, addPart,
    alert, attr, capitalizeString, cnEnabled, cnStatus, cnStatusGlobal,
    cnTitle, code_group, config, connection, connections, controller,
    controllerProvider, css, data, destination, destinations, directive,
    enabled, filter, form, get, getStatusClass, global_code, go, icon, id,
    length, level, link, load, loadPlugin, module, pageTitle, redirect,
    removeClass, replace, resolve, restrict, scope, setStatusClass, setTitle,
    source, state, status, template, templateUrl, then, title, toLowerCase,
    toggleClass, type, url, views
*/


angular.module("cn.connection", [
    "oc.lazyLoad",
    "cn.connection.config",
    "cn.connection.list",
    "cn.connection.item"
])
    .config(function ($translatePartialLoaderProvider, $stateProvider) {

        "use strict";

        $translatePartialLoaderProvider.addPart('account/account');
        $translatePartialLoaderProvider.addPart('connection/connection');

        $stateProvider
            .state("connection", {
                url: "/connection",
                template: '<div ui-view autoscroll="false"></div>',
                redirect: "connection.list"
            })

            .state("connection.none", {
                url: "/none",
                templateUrl: "html/connection/none.html",
                data: {
                    pageTitle: "connection.none.pageTitle"
                }
            })
            .state("connection.list", {
                url: "/list",
                resolve: {
                    connections: function ($state, $q, action) {
                        return $q(function (resolve, reject) {
                            return action.connection.get().then(function (connections) {
                                if (connections && connections.length) {
                                    resolve(connections);
                                } else {
                                    reject($state.go("connection.none"));
                                }
                            }, reject);
                        });
                    },
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load("datatables");
                    }
                },
                controller: "cnConnectionList",
                templateUrl: "html/connection/list.html",
                data: {
                    pageTitle: "nav.connection.list"
                }
            })

            .state("connection.item", {
                url: "/item",
                resolve: {
                    accounts: function (action) {
                        return action.account.get();
                    }
                },
                controller: "cnConnectionItem",
                templateUrl: "html/connection/item.html",
                abstract: true
            })
            .state("connection.item.add", {
                url: "/add",
                controller: "cnSourceItem",
                templateUrl: "html/connection/item.source.html",
                abstract: true,
                data: {
                    pageTitle: "nav.connection.add.source"
                }
            })
            .state("connection.item.add.source", {
                url: "/source/:type?account",
                data: {
                    pageTitle: "connection.item.title.source.add"
                },
                views: {
                    form: {
                        resolve: {
                            connection: function () {
                                return false;
                            },
                            account: function ($stateParams) {
                                if ($stateParams.account) {
                                    return {
                                        id: $stateParams.account
                                    };
                                } else {
                                    return false;
                                }
                            }
                        },
                        controllerProvider: function ($stateParams, helper, cfg) {

                            var type = $stateParams.type,
                                prefix = "cnSourceItem";
                            if (cfg.connection.config.source[type]) {
                                return prefix + helper.capitalizeString(type);
                            } else {
                                return prefix + "Default";
                            }
                        },
                        templateUrl: "html/connection/item.source.form.html"
                    }
                }
            })
            .state("connection.item.edit", {
                url: "/edit/:connection",
                template: '<div ui-view autoscroll="false"></div>',
                abstract: true
            })
            .state("connection.item.edit.source", {
                url: "/source",
                controller: "cnSourceItem",
                templateUrl: "html/connection/item.source.html",
                abstract: true
            })
            .state("connection.item.edit.source.edit", {
                url: "/edit",
                data: {
                    pageTitle: "connection.item.title.source.edit"
                },
                views: {
                    form: {
                        resolve: {
                            connection: function ($stateParams, action) {
                                return action.connection.get({id: $stateParams.connection});
                            },
                            account: function (connection) {
                                return connection._embedded.account;
                            }
                        },
                        controllerProvider: function (account, helper) {
                            return "cnSourceItem" + helper.capitalizeString(account.type);
                        },
                        templateUrl: "html/connection/item.source.form.html"
                    }
                }
            })

            .state("connection.item.edit.destination", {
                url: "/destination",
                controller: "cnDestinationItem",
                templateUrl: "html/connection/item.destination.html",
                abstract: true
            })
            .state("connection.item.edit.destination.add", {
                url: "/add/:type?account",
                data: {
                    pageTitle: "connection.item.title.destination.add"
                },
                views: {
                    form: {
                        resolve: {
                            connection: function ($stateParams, action) {
                                return action.connection.get({id: $stateParams.connection});
                            },
                            destination: function () {
                                return false;
                            },
                            account: function ($stateParams) {
                                if ($stateParams.account) {
                                    return {
                                        id: $stateParams.account
                                    };
                                } else {
                                    return false;
                                }
                            }
                        },
                        controllerProvider: function ($stateParams, helper, cfg) {

                            var type = $stateParams.type,
                                prefix = "cnDestinationItem";

                            if (cfg.connection.config.destination[type]) {
                                return prefix + helper.capitalizeString(type);
                            } else {
                                return prefix + "Default";
                            }
                        },
                        templateUrl: "html/connection/item.destination.form.html"
                    }
                }
            })
            .state("connection.item.edit.destination.edit", {
                url: "/edit/:destination",
                data: {
                    pageTitle: "connection.item.title.destination.edit"
                },
                views: {
                    form: {
                        resolve: {
                            connection: function ($stateParams, action) {
                                return action.connection.get({id: $stateParams.connection});
                            },
                            destination: function ($stateParams, connection) {

                                return connection._embedded.destinations.filter(function (destination) {
                                    return destination.id === parseInt($stateParams.destination);
                                })[0];
                            },
                            account: function (destination) {
                                return destination._embedded.account;
                            }
                        },
                        controllerProvider: function (account, helper) {
                            return "cnDestinationItem" + helper.capitalizeString(account.type);
                        },
                        templateUrl: "html/connection/item.destination.form.html"
                    }
                }
            });
    })
    .directive("cnIconApp", function ($filter, cfg) {

        "use strict";

        return {
            restrict: "E",
            replace: true,
            scope: true,
            template: "<i ng-attr-title='{{ title }}'><span class='txt'>{{ type }}</span></i>",

            link: function (scope, element, attrs) {

                var disabled = (scope.$eval(attrs.cnStatusGlobal).global_code === "G23");

                scope.getStatusClass = function (level) {
                    var status = "status-" + (level || "warning");
                    return status.toLowerCase();
                };

                scope.setTitle = function (title) {
                    scope.title = (title || attrs.cnTitle) + (scope.alert
                        ? " - " + scope.alert
                        : "");
                };
                var disabledClass = scope.getStatusClass("disabled");
                scope.type = attrs.account;

                element.addClass(attrs.account + " " + cfg.css.icon[attrs.account]);

                scope.$watch(attrs.cnStatus, function status(now, before) {

                    if (now) {
                        var enabled = scope.$eval(attrs.cnEnabled);
                        element.toggleClass(disabledClass, !enabled);
                        if (disabled) {
                            element.toggleClass(disabledClass);
                        } else if (enabled) {
                            var level = now.level.toLowerCase();

                            if (before) {
                                element.removeClass(scope.getStatusClass(before.level));
                                element.addClass(scope.getStatusClass(level));
                            }
                            scope.alert = level !== "info"
                                ? $filter("translate")("connection.status.config." + now.code_group)
                                : false;
                            scope.setTitle();
                        }
                    }
                });

                scope.$watch(attrs.cnEnabled, function enabled(now) {
                    if (now !== "undefined") {
                        element.toggleClass(scope.getStatusClass("disabled"), (!now || disabled));
                    }
                });

                scope.$watch(attrs.cnTitle, function title(now) {
                    if (now) {
                        scope.setTitle(now);
                    }
                });
            }
        };
    })
    .directive("cnIconEnabled", function ($filter, cfg) {

        "use strict";

        return {
            restrict: "E",
            replace: true,
            scope: true,
            template: "<i ng-attr-title='{{ title }}'>",
            link: function (scope, element, attrs) {
                var state = attrs.enabled === "true"
                    ? "enabled"
                    : "disabled";

                element.addClass("status-" + state + " " + cfg.css.icon.state[state]);
                scope.title = $filter("translate")("connection.state." + state);
            }
        };
    })
    .directive("cnIconStatus", function ($filter, cfg) {

        "use strict";

        return {
            restrict: "E",
            replace: true,
            scope: true,
            template: "<i ng-attr-title='{{ title }}'>",
            link: function (scope, element, attrs) {
                scope.setStatusClass = function (level) {
                    element.attr("class", "icon-box status-" + (level || "warn") + " " + (cfg.css.icon.status[level || "unknown"]));
                };

                scope.$watch(attrs.cnStatus, function (status) {

                    if (status) {
                        if (status.global_code === "G23") {
                            scope.setStatusClass("disabled");
                            scope.title = $filter("translate")("connection.status.global." + status.global_code);
                        } else {
                            var enabled = scope.$eval(attrs.cnEnabled);
                            scope.setStatusClass(enabled
                                ? status.level.toLowerCase()
                                : "disabled");
                            scope.title = $filter("translate")("connection.status.global." + (enabled
                                ? status.global_code
                                : "disabled"));
                        }
                    }
                });
            }
        };
    });
