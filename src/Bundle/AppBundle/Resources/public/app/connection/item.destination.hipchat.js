/*global angular*/

/*property
    account, connection, controller, destination, initDestination, module,
    type
*/

angular.module("cn.destination.hipchat", [])
    .controller("cnDestinationItemHipchat", function ($scope, connection, destination, account) {

        "use strict";

        $scope.initDestination({
            type: "hipchat",
            connection: connection,
            destination: destination,
            account: account
        });
    });
