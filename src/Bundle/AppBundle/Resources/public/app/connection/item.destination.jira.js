/*global angular*/
/*property
    $watch, account, allowed_values, concat, config, connection, controller,
    data, destination, dynamic, field, forEach, form, global, hasOwnProperty,
    id, indexOf, info, initDestination, input, issue_type, jira, key, keys,
    label, map, model, module, name, options, placeholder, project_key, push,
    required, success, template, translate, type, value
*/

angular.module("cn.destination.jira", [
    "cn.action"
])
    .controller("cnDestinationItemJira", function ($scope, $timeout, action, connection, destination, account, cfg) {

        "use strict";

        $scope.initDestination({
            type: "jira",
            connection: connection,
            destination: destination,
            account: account
        });

        function getConfigDynamic(issue_type) {

            if (issue_type) {

                action.account.info({
                    id: $scope.form.config.account.id,
                    field: ["project", $scope.form.field.config.project_key.value, issue_type],
                    success: function (data) {

                        var config = {
                            global: $scope.form.config.field.config.map(function (field) {
                                return field.model;
                            }),
                            dynamic: data
                        };

                        if ($scope.data.destination && $scope.data.destination.id !== destination.id) {
                            return;
                        }

                        $scope.form.config.field.dynamic = [];

                        $timeout(function () {

                            angular.forEach(config.dynamic, function (remote) {

                                var templates = cfg.connection.template.destination.jira,
                                    field;

                                if (templates) {
                                    config.global = config.global.concat(Object.keys(templates));
                                }

                                if (!templates.hasOwnProperty(remote.key)) {

                                    field = {
                                        name: remote.key,
                                        model: remote.key,
                                        required: remote.required || false,
                                        label: remote.label,
                                        translate: false
                                    };

                                    config.global.push(remote.key);

                                    if (remote.key.indexOf("customfield") < 0) {

                                        field.placeholder = remote.key;
                                    }

                                    if (remote.type === "array") {

                                        field.type = "tags";

                                        if (remote.allowed_values) {

                                            field.input = {
                                                type: "select",
                                                options: remote.allowed_values,
                                                translate: false,
                                                placeholder: field.placeholder
                                            };

                                        } else {

                                            field.input = {
                                                placeholder: field.placeholder
                                            };
                                        }

                                    } else if (remote.allowed_values) {

                                        field.type = "select";
                                        field.options = remote.allowed_values;
                                    }

                                    $scope.form.config.field.dynamic.push(field);
                                }
                            });

                            angular.forEach(Object.keys($scope.form.field.config), function (key) {

                                if (config.global.indexOf(key) < 0) {
                                    delete $scope.form.field.config[key];
                                }
                            });
                        });
                    }
                });
            }
        }

        function deleteConfigDynamic() {

            angular.forEach($scope.form.config.field.dynamic, function (field) {
                delete $scope.form.field.config[field.model];
            });

            delete $scope.form.config.field.dynamic;
        }

        $scope.$watch("form.field.config.action.value", function (action) {

            if (action === "update") {
                deleteConfigDynamic();
            }
        });

        $scope.$watch("form.field.config.project_key.value", function (project, before) {

            if (project && project !== before) {

                deleteConfigDynamic();

                if ($scope.form.field.config.issue_type) {
                    getConfigDynamic($scope.form.field.config.issue_type.value);
                }
            }
        });

        $scope.$watch("form.field.config.issue_type.value", getConfigDynamic);
    });
