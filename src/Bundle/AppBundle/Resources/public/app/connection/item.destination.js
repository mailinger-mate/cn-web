/*global angular, $*/

/*property
    $current, $setPristine, $valid, $watch, account, action, addAttachment,
    all, attachment, attachments, button, call, class, config, confirm,
    connection, controller, ctrl, data, delete, deleteAttachment,
    deleteDestination, destination, dynamic, error, errors, extend, field,
    forEach, form, getRefText, getRefTextRaw, go, hasOwnProperty, id, indexOf,
    initConnection, initDestination, initReference, keys, label, mode, module,
    name, notify, options, placeholder, push, ref, resource, run, save,
    saveDestination, search, searchRef, selectAttachment, show, splice,
    success, template, templates, text, translate, type, update, validateForm,
    value, view
*/

angular.module("cn.connection.item.destination", [
    "cn.action",
    "cn.core",
    "cn.connection.config",
    "cn.destination.hipchat",
    "cn.destination.jira",
    "cn.destination.slack",
    "cn.destination.smtp",
    "cn.destination.webhook"
])
    .controller("cnDestinationItem", function ($scope, $state, $filter, cfg, action, core) {

        "use strict";

        $scope.view.name = "destination";
        $scope.form.config.field.type.placeholder = "type_destination";

        $scope.selectAttachment = function (type, reset) {

            var allowed = Object.keys(cfg.connection.attachment),
                allow = allowed.indexOf(type) !== -1;

            $scope.form.config.attachments = allow;

            if (reset === true) {
                if (!allow) {
                    delete $scope.form.field.attachments;
                } else {
                    $scope.form.field.attachments = [];
                }
            }
        };

        $scope.addAttachment = function () {
            if (!$scope.form.field || !$scope.form.field.attachments) {
                $scope.form.field.attachments = [];
            }
            $scope.form.field.attachments.push({});
        };

        $scope.deleteAttachment = function (key) {
            $scope.form.field.attachments.splice(key, 1);
        };

        $scope.searchRef = function (term) {

            $scope.ref = [];

            angular.forEach($scope.data.ref.all, function (field) {

                if (typeof field === "string") {

                    field = {
                        name: field
                    };
                }

                if (!term || field.name.search(new RegExp(term, "i")) >= 0) {

                    $scope.ref.push(field);
                }
            });
        };

        $scope.getRefText = function (field) {
            return '%{' + field.name + '}';
        };

        $scope.getRefTextRaw = function (field) {
            return '%{' + field.name + '}';
        };

        function clearTemplates() {

            var data = $scope.form.field;

            angular.forEach(data.config, function (config, key) {

                if (!config.value) {
                    delete data.config[key];
                }
            });
        }

        $scope.initDestination = function (config) {

            $scope.view.mode = "add";

            $scope.form.config.field.type.options = Object.keys(cfg.connection.config.destination);
            $scope.form.ctrl.$setPristine();
            $scope.ref = [];

            delete $scope.form.field;

            delete $scope.form.config.type;
            delete $scope.form.config.account;

            delete $scope.form.config.field.config;
            delete $scope.form.config.field.dynamic;
            delete $scope.form.config.field.template;
            delete $scope.form.config.field.attachment;

            delete $scope.data.destination;
            $scope.form.button = [{
                label: "save",
                type: "submit",
                action: "saveDestination",
                class: "btn-primary"
            }];

            if (!config) {
                return;
            }

            if (config.type) {

                $scope.form.config.type = config.type;
                $scope.form.config.field.config = cfg.connection.config.destination[config.type];
                $scope.form.config.field.template = cfg.connection.template.destination[config.type];
                $scope.form.config.attachment = cfg.connection.attachment[config.type];
            }

            if (config.connection) {

                $scope.initConnection(config.connection);
            }

            if (config.account) {

                $scope.form.config.account = config.account;
            }

            if (config.destination) {

                $scope.view.mode = "edit";

                $scope.data.destination = config.destination;

                $scope.form.field = {
                    config: config.destination.config,
                    templates: config.destination.templates,
                    attachments: config.destination.attachments
                };

                $scope.form.button.push({
                    label: "delete",
                    type: "button",
                    action: "deleteDestination",
                    confirm: true,
                    class: "btn-warning"
                });
            }

            $scope.initReference();

            $scope.$watch("form.config.type", function (type, before) {
                if (type && type !== before) {
                    $state.go($state.$current.name, {
                        type: type,
                        account: undefined
                    });
                }
            });
        };

        $scope.saveDestination = function () {

            var call;

            $scope.validateForm();

            if ($scope.form.ctrl.$valid) {

                clearTemplates();

                if ($scope.view.mode === "edit") {

                    call = core.destination.update({
                        id: $scope.data.destination.id
                    }, $scope.form.field);

                } else {

                    call = core.destination.save($.extend($scope.form.field, {
                        account: $scope.form.config.account.id,
                        connection: $scope.data.connection.id
                    }));
                }

                return action.run({
                    notify: {
                        show: true,
                        translate: {
                            text: $filter("translate")("account.item.select.type." + $scope.form.config.type)
                        }
                    },
                    resource: "connection",
                    name: "destination." + $scope.view.mode,
                    call: call,
                    success: function (destination) {

                        delete $scope.form.error.field;

                        if ($scope.form.action.hasOwnProperty("success")) {

                            $scope.form.action.success(destination);

                        } else {

                            $state.go("connection.item.edit.destination.edit", {
                                connection: $scope.data.connection.id,
                                destination: destination.id || $scope.data.destination.id
                            });
                        }

                        return destination;
                    },
                    error: function (error) {

                        $scope.form.error.field = error.data
                            ? error.data.errors
                            : {};
                    }
                });

            } else {

                return false;
            }
        };

        $scope.deleteDestination = function () {

            return action.run({
                notify: {
                    show: true,
                    translate: {
                        text: $filter("translate")("account.item.select.type." + $scope.form.config.type)
                    }
                },
                resource: "connection",
                name: "destination.delete",
                call: core.destination.delete({
                    id: $scope.data.destination.id
                }),
                success: function (data) {
                    $state.go("connection.item.edit.source.edit", {
                        connection: $scope.data.connection.id
                    });
                    return data;
                }
            });
        };

    })
    .controller("cnDestinationItemDefault", function ($scope, $state, $stateParams, connection) {

        "use strict";

        $scope.initDestination({
            connection: connection
        });

        if ($stateParams.type) {
            $state.go($state.$current.name, {type: null});
        }
    });
