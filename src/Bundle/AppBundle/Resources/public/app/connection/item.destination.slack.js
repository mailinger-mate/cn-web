/*global angular*/

/*property
    $watch, account, attachmentDisable, attachments, connection,
    controller, destination, field, form, initDestination, module, type
*/


angular.module("cn.destination.slack", [])
    .controller("cnDestinationItemSlack", function ($scope, connection, destination, account) {

        "use strict";
        $scope.initDestination({
            type: "slack",
            connection: connection,
            destination: destination,
            account: account
        });

        $scope.$watch("form.field.config.content.meta.as_bot", function (type, before) {
            if (type && type !== before) {
                $scope.attachmentDisable = true;
                if ($scope.form.field.attachments) {
                    delete $scope.form.field.attachments;
                }
            } else {
                $scope.attachmentDisable = false;
            }
        }, true);

    });
