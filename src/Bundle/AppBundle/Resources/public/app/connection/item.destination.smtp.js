/*global angular*/

/*property
    account, connection, controller, destination, initDestination, module,
    type
*/

angular.module("cn.destination.smtp", [])
    .controller("cnDestinationItemSmtp", function ($scope, connection, destination, account) {

        "use strict";

        $scope.initDestination({
            type: "smtp",
            connection: connection,
            destination: destination,
            account: account
        });
    });
