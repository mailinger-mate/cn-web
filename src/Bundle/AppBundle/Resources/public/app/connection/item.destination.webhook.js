/*global angular*/

/*property
    account, connection, controller, destination, initDestination, module,
    type
*/

angular.module("cn.destination.webhook", [])
    .controller("cnDestinationItemWebhook", function ($scope, connection, destination, account) {

        "use strict";

        $scope.initDestination({
            type: "webhook",
            connection: connection,
            destination: destination,
            account: account
        });
    });
