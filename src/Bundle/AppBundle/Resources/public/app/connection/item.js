/*global angular*/

/*property
    $data, $error, $eval, $filter, $label, $setDirty, $source, $track, $watch,
    _embedded, account, accounts, action, all, concat, config, connection,
    controller, ctrl, data, destination, destinations, dynamic, enabled, error,
    extra, field, field_rules, forEach, form, id, info, initAccounts,
    initConnection, initReference, local, map, mode, model, module, name,
    options, placeholder, prefix, ref, required, resetRef, select,
    setReference, source, success, translate, type, validateForm, value, view
*/

angular.module("cn.connection.item", [
    "cn.action",
    "cn.connection.config",
    "cn.connection.item.source",
    "cn.connection.item.destination"
])
    .controller("cnConnectionItem", function ($scope, $filter, cfg, action, accounts) {

        "use strict";

        $scope.view = {
            mode: "add",
            action: {
                account: true
            }
        };

        $scope.data = {
            accounts: accounts
        };

        $scope.form = {
            action: {},
            config: {
                field: {
                    type: {
                        type: "select",
                        options: [],
                        required: true,
                        placeholder: true,
                        translate: "type",
                        prefix: {
                            model: false,
                            translate: "account.item"
                        }
                    },
                    account: {
                        type: "select",
                        options: {
                            $label: "a as a.name",
                            $data: "a",
                            $source: "data.accounts",
                            $filter: "filter: { type: form.config.type }",
                            $track: "a.id"
                        },
                        select: true,
                        required: true,
                        placeholder: true,
                        prefix: {
                            model: false
                        }
                    },
                    name: {
                        value: $filter('translate')('connection.name.new'),
                        placeholder: true,
                        required: true
                    },
                    enabled: {
                        type: "switch",
                        value: true,
                        required: true
                    }
                },
                prefix: {
                    translate: "connection.item"
                }
            },
            error: {}
        };

        $scope.initConnection = function (connection) {

            delete $scope.data.connection;
            delete $scope.data.destinations;
            delete $scope.data.source;

            if (connection) {

                $scope.data.connection = connection;

                if (connection._embedded) {

                    $scope.data.destinations = connection._embedded.destinations || [];
                    $scope.data.source = {
                        account: connection._embedded.account
                    };
                }
            }
        };

        $scope.initAccounts = function (accounts) {

            $scope.data.accounts = accounts;
        };

        $scope.initReference = function () {

            var field;

            function getReference(config) {

                config.ref = cfg.connection.ref[config.account.type];

                if (!config.ref) {
                    return;
                }

                if (config.ref.dynamic) {

                    if (config.ref.dynamic.name) {
                        action.account.info({
                            id: config.account.id,
                            field: config.ref.dynamic.name,
                            success: function (options) {
                                $scope.setReference({
                                    source: options,
                                    extra: config.extra
                                });
                            }
                        });
                    }

                    if (config.ref.dynamic.field) {

                        if ($scope.view.name === "destination") {

                            field = $scope.$eval("data.connection." + config.ref.dynamic.field);
                            config.ref.options = field;

                            if (config.ref.dynamic.local) {
                                config.ref.options = config.ref.dynamic.local(field);
                            }

                        } else {

                            $scope.$watch("form.field." + config.ref.dynamic.field, function (field) {

                                var options;

                                if (field) {

                                    options = field;

                                    if (config.ref.dynamic.local) {
                                        options = config.ref.dynamic.local(field);
                                    }

                                    $scope.setReference({
                                        source: options,
                                        extra: config.extra
                                    });
                                }
                            });
                        }
                    }
                }

                $scope.setReference({
                    source: config.ref.options,
                    extra: config.extra
                });

            }

            $scope.setReference = function (config) {

                $scope.data.ref = {
                    source: [],
                    extra: []
                };

                if (config.source) {

                    $scope.data.ref.source = config.source.map(function (option) {
                        if (typeof option === "string") {
                            return {name: option};
                        }
                        return option;
                    });
                }

                if (config.extra) {

                    $scope.data.ref.all = config.extra.concat($scope.data.ref.source);
                } else {

                    $scope.data.ref.all = $scope.data.ref.source;
                }

                $scope.data.ref.all = cfg.connection.placeholder.concat($scope.data.ref.all);

            };

            if ($scope.data.source) {

                getReference({
                    account: $scope.data.source.account,
                    extra: $scope.data.connection.field_rules,
                    destination: true
                });

            } else {

                $scope.$watch("form.config.account", function (account) {
                    if (account) {
                        getReference({
                            account: account
                        });
                    }
                }, true);
            }
        };

        $scope.resetRef = function () {
            delete $scope.ref;
        };

        $scope.validateForm = function () {

            angular.forEach($scope.form.ctrl.$error.required, function (field) {
                field.$setDirty();
            });
        };

    });
