/*global angular*/

/*property
    account, connection, controller, initSource, module, type
*/

angular.module("cn.source.imap", [])
    .controller("cnSourceItemImap", function ($scope, connection, account) {

        "use strict";

        $scope.initSource({
            type: "imap",
            connection: connection,
            account: account
        });
    });
