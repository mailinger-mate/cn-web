/*global angular*/

/*property
    account, connection, controller, initSource, module, type
*/

angular.module("cn.source.jira", [
    "cn.action"
])
    .controller("cnSourceItemJira", function ($scope, connection, account) {

        "use strict";

        $scope.initSource({
            type: "jira",
            connection: connection,
            account: account
        });
    });
