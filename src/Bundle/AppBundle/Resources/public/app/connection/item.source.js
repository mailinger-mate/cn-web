/*global angular, $*/

/*property
    $current, $pristine, $valid, $watch, account, action, addFieldRule, button,
    call, class, config, confirm, connection, controller, ctrl, data, delete,
    deleteConnection, deleteFieldRule, enabled, error, errors, extend, field,
    field_rules, form, go, hasOwnProperty, id, initConnection, initReference,
    initSource, interval, keys, label, mode, module, name, notify, options,
    placeholder, push, resource, run, save, saveSource, show, source, splice,
    success, text, then, translate, type, update, validateForm, view
*/

angular.module("cn.connection.item.source", [
    "cn.action",
    "cn.core",
    "cn.connection.config",
    "cn.source.imap",
    "cn.source.rss",
    "cn.source.twitter",
    "cn.source.salesforce",
    "cn.source.jira"
])
    .controller("cnSourceItem", function ($scope, $state, $translate, cfg, action, core) {

        "use strict";

        $scope.view.name = "source";
        $scope.form.config.field.type.placeholder = "type_source";

        delete $scope.form.field;

        $scope.initSource = function (config) {

            $scope.initConnection(config.connection);
            $scope.view.mode = "add";

            $scope.form.config.field.type.options = Object.keys(cfg.connection.config.source);

            delete $scope.form.config.type;
            delete $scope.form.config.account;
            delete $scope.form.config.field.config;

            if ($scope.form.field) {

                delete $scope.form.field.interval;
                delete $scope.form.field.config;
                delete $scope.form.field.field_rules;
            }

            $scope.form.button = [
                {
                    label: config.connection
                        ? "save"
                        : "continue",
                    type: "submit",
                    action: "saveSource",
                    class: "btn-primary"
                }
            ];

            if (!config) {
                return;
            }

            if (config.type) {

                $scope.form.config.type = config.type;
                $scope.form.config.field.config = cfg.connection.config.source[config.type];
            }

            if (config.account) {

                $scope.form.config.account = config.account;
            }

            if (config.connection) {

                $scope.view.mode = "edit";

                $scope.form.field = {
                    name: config.connection.name,
                    enabled: config.connection.enabled,
                    interval: config.connection.interval,
                    config: config.connection.config,
                    field_rules: config.connection.field_rules
                };

                $scope.form.button.push({
                    label: "delete",
                    type: "button",
                    action: "deleteConnection",
                    confirm: true,
                    class: "btn-warning"
                });
            }

            $scope.initReference();

            $scope.$watch("form.config.type", function (type, before) {
                if (type && type !== before) {
                    $state.go($state.$current.name, {
                        type: type,
                        account: undefined
                    });
                }
            });

            if ($scope.view.mode === "add") {
                $scope.$watch("form.config.account", function (account) {
                    if (account && $scope.form.config.type && $scope.form.ctrl.name.$pristine) {
                        $translate("connection.name.new", {text: account.name}).then(function (text) {
                            $scope.form.field.name = text;
                        });
                    }
                }, true);
            }
        };

        $scope.saveSource = function () {

            var call;

            $scope.validateForm();

            if ($scope.form.ctrl.$valid) {

                if ($scope.view.mode === "edit") {

                    call = core.connection.update({
                        id: $scope.data.connection.id
                    }, $scope.form.field);

                } else {

                    call = core.connection.save($.extend($scope.form.field, {
                        account: $scope.form.config.account.id
                    }));
                }

                return action.run({
                    notify: {
                        show: true,
                        translate: {
                            name: $scope.form.field.name
                        }
                    },
                    resource: "connection",
                    name: $scope.view.mode,
                    call: call,
                    success: function (connection) {

                        delete $scope.form.error.field;

                        if ($scope.form.action.hasOwnProperty("success")) {

                            $scope.form.action.success(connection);

                        } else if (connection.id) {

                            $state.go("connection.item.edit.destination.add", {
                                connection: connection.id
                            });
                        }

                        return connection;
                    },
                    error: function (error) {

                        $scope.form.error.field = error.data
                            ? error.data.errors
                            : {};
                    }
                });

            } else {

                return false;
            }
        };

        $scope.deleteConnection = function () {

            return action.run({
                notify: {
                    show: true,
                    translate: {
                        name: $scope.form.field.name
                    }
                },
                resource: "connection",
                name: "delete",
                call: core.connection.delete({
                    id: $scope.data.connection.id
                }),
                success: function (data) {
                    $state.go("connection.list");
                    return data;
                }
            });
        };

        // into separate directive

        $scope.addFieldRule = function () {
            if ($scope.form.field.field_rules) {
                $scope.form.field.field_rules.push({});
            } else {
                $scope.form.field.field_rules = [{}];
            }
        };

        $scope.deleteFieldRule = function (key) {
            $scope.form.field.field_rules.splice(key, 1);
        };
    })
    .controller("cnSourceItemDefault", function ($scope, $state, $stateParams, connection) {

        "use strict";

        $scope.initSource({
            connection: connection
        });

        if ($stateParams.type) {
            $state.go($state.$current.name, {type: null});
        }
    });
