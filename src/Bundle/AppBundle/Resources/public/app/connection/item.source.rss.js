/*global angular*/

/*property
    account, connection, controller, initSource, module, type
*/

angular.module("cn.source.rss", [])
    .controller("cnSourceItemRss", function ($scope, connection, account) {

        "use strict";

        $scope.initSource({
            type: "rss",
            connection: connection,
            account: account
        });
    });
