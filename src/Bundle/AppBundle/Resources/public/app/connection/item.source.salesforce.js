/*global angular*/

/*property
    account, connection, controller, initSource, module, type
*/

angular.module("cn.source.salesforce", [])
    .controller("cnSourceItemSalesforce", function ($scope, connection, account) {

        "use strict";

        $scope.initSource({
            type: "salesforce",
            connection: connection,
            account: account
        });
    });
