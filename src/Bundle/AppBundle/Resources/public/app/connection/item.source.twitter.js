/*global angular*/

/*property
    account, connection, controller, initSource, module, type
*/

angular.module("cn.source.twitter", [])
    .controller("cnSourceItemTwitter", function ($scope, connection, account) {

        "use strict";

        $scope.initSource({
            type: "twitter",
            connection: connection,
            account: account
        });
    });
