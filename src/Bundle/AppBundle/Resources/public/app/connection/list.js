/*global angular*/

/*property
    connections, controller, dtColumnDefs, dtOptions, module, newOptions,
    orderable, searchable, targets, withOption
*/

angular.module("cn.connection.list", [])
    .controller("cnConnectionList", function ($scope, DTOptionsBuilder, connections) {

        "use strict";

        $scope.connections = connections;

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption("order", [[0, "desc"]])
            .withOption('responsive', true);

        $scope.dtColumnDefs = [
            {
                targets: 'no-sort',
                orderable: false
            },
            {
                targets: [0],
                searchable: false
            }
        ];
    });
