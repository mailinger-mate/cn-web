/*global angular*/

angular.module("cn.core", [
    "ngResource"
])
    .factory("core", function ($http, $resource, cfg) {

        "use strict";

        var core = {},
            url = "/",
            token,
            rpc;

        if (cfg.core && cfg.core.api_url && cfg.core.rpc_url) {
            url = cfg.core.api_url;
            rpc = cfg.core.rpc_url;
        }

        if (cfg.user && cfg.user.core) {
            token = cfg.user.core.token;
        }

        $http.defaults.headers.common.Authorization = "Bearer " + token;
        $http.defaults.headers.common["Content-Type"] = "application/json";

        core.connection = $resource(url + "connections/:id", {id: "@id"}, {
            update: {
                method: "PATCH"
            }
        });

        core.account = $resource(url + "accounts/:id", {id: "@id"}, {
            update: {
                method: "PATCH"
            }
        });

        core.accountInfo = $resource(url + "accounts/:id/info/:field/:field2/:field3", {
            id: "@id",
            field: "@field",
            field2: "@field2",
            field3: "@field3"
        });

        core.destination = $resource(url + "destinations/:id", {id: "@id"}, {
            update: {
                method: "PATCH"
            }
        });

        core.vendor = $resource(url + "vendors");

        core.billing = $resource(url + "billings/:id", {id: "@id"}, {
            id: "@id"
        });

        core.trypay = $resource(rpc + 'trypay/', {
        });
        return core;
    });