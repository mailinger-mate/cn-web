/*global angular*/

angular.module("cn.error", [])
    .config(function ($translatePartialLoaderProvider, $stateProvider) {

        "use strict";

        $translatePartialLoaderProvider.addPart("error/error");

        $stateProvider.state("error", {
            url: "/error",
            redirect: "error.404",
            template: '<div ui-view autoscroll="false"></div>',
            data: {
                pageTitle: "error.self"
            }
        })
            .state("error.400", {
                url: "/400",
                controller: "cnError",
                templateUrl: "error.html"
            })
            .state("error.404", {
                url: "/404",
                controller: "cnError",
                templateUrl: "error.html"
            });
    })
    .controller("cnError", function ($scope, $state) {

        "use strict";

        $scope.error = $state.current.url.slice(1);
    });
