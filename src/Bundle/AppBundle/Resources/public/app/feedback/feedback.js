/*global window angular UE*/
/*property
    Popin, _ues, async, category, controller, createElement, feedback, forum,
    getElementsByTagName, host, insertBefore, location, module, params,
    parentNode, preload, protocol, show, src, sso_token, tab_show, token, type,
    user
*/

angular.module('cn.feedback', [])
    .controller('FeedbackCtrl', function ($scope, cfg) {

        "use strict";

        var _ue = document.createElement('script'),
            s = document.getElementsByTagName('script')[0];

        window._ues = {
            host: cfg.feedback.host,
            forum: cfg.feedback.forum,
            category: cfg.feedback.category,
            tab_show: false,
            params: {
                sso_token: cfg.user.feedback.token
            }
        };

        _ue.type = 'text/javascript';
        _ue.async = true;
        _ue.src = (document.location.protocol || "http:") + '//cdn.userecho.com/js/widget-1.4.gz.js';

        s.parentNode.insertBefore(_ue, s);

        $scope.show = function () {
            UE.Popin.show();
        };

        $scope.preload = function () {
            UE.Popin.preload();
        };
    });
