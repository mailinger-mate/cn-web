/*global $ angular*/
/*property
    $broadcast, $eval, $watch, action, addClass, append, appendTo, applyConfig,
    attr, base, button, charAt, class, click, config, configAction, configAttr,
    configClass, configClick, configConfirm, configDisabled, confirm,
    container, content, controller, css, danger, data, default, directive,
    disabled, done, error, extendConfig, find, forEach, group, help, href,
    html, icon, indexOf, insertAfter, insertBefore, keys, link, load, module,
    next, popover, popup, prop, remove, removeClass, replace, replaceInput,
    replaceWith, restrict, scope, slice, sref, success, template, terminate,
    then, toUpperCase, toggle, toggleClass, trigger, type, wait
*/


angular.module("cn.button", [])
    .directive("cnButton", function ($compile, $timeout, cfg, $rootScope, $translate) {

        "use strict";

        return {
            restrict: "E",
            replace: true,
            scope: true,
            template: function (element) {

                var template = $("<div>"),
                    button = $("<button>"),
                    txt = $("<span translate>"),
                    icon = $("<i>");

                button.appendTo(template);
                txt.html(element.html()).appendTo(button);
                icon.insertBefore(txt);

                return template.html();
            },
            controller: function ($scope, $element, $attrs) {
                var config = $scope.$eval($attrs.config) || {},
                    start = parseInt(config.confirm) || 1;
                $scope.config = config;
                $scope.click = start;
                $scope.action = function (action) {

                    var css = cfg.css.icon,
                        hold = 4000,
                        button = $element,
                        icon = $element.find("i"),
                        call;

                    function success() {
                        if (!config.wait) {

                            $timeout(function () {
                                icon.removeClass(css.load).removeClass(css.error).addClass(css.done);
                                $timeout(function () {
                                    icon.removeClass(css.done);
                                    if (config.terminate !== true) {
                                        button.prop("disabled", false);
                                    }
                                    if (config.remove) {
                                        $element.next().remove();
                                        $element.remove();
                                        $rootScope[config.remove] = false;
                                        $rootScope.$broadcast(config.remove, false);
                                    }
                                }, hold * 2);
                            }, hold);
                        }
                    }

                    function error() {

                        $timeout(function () {
                            icon.removeClass(css.load).addClass(css.error);
                            button.prop("disabled", false);
                        }, hold);
                    }

                    if ($scope[action]) {

                        call = $scope[action]($scope);
                        icon.addClass(css.load);
                        button.prop("disabled", true);

                        if (call) {

                            call.then(function (response) {
                                if (!response || (response.data && response.data.success === false)) {
                                    error();
                                } else {
                                    success();
                                }
                            }, error);

                        } else {
                            error();
                        }
                    }
                };

                $scope.confirm = function (action) {

                    if (action) {
                        if ($scope.click === 0) {
                            $scope.action(action);
                            $scope.click = start;
                        } else {
                            $scope.click -= 1;
                        }
                    } else {
                        $scope.click = start;
                    }
                };

                $scope.popup = function (translateKey) {
                    $translate(translateKey).then(function (txt) {
                        $element.next().popover({
                            html: true,
                            container: 'body',
                            trigger: 'focus',
                            content: function () {
                                return txt;
                            }
                        });
                    });
                };

                if ($attrs.help) {
                    $scope.popup($attrs.help);
                }
            },
            link: function (scope, element, attrs) {

                var template = element,
                    button = element,
                    config = scope.config;
                scope.extendConfig = function (config) {

                    var deny = ["config", "model", "name"];

                    angular.forEach(attrs, function (value, key) {

                        if (key[0] !== "$" && deny.indexOf(key) < 0 && value !== "") {

                            config[key] = value === "true"
                                ? true
                                : value === "false"
                                    ? false
                                    : value;
                        }
                    });
                };
                scope.extendConfig(config);
                scope.configAttr = function () {
                    button.attr(config.attr);
                };

                scope.configAction = function () {
                    button.attr("ng-click", "action('" + config.action + "')");
                };

                scope.configClick = function () {
                    button.attr("ng-click", config.click);
                };

                scope.configClass = function () {
                    button.addClass(cfg.css.button.base).addClass(config.class);
                };

                scope.configDisabled = function () {
                    button.attr("ng-disabled", config.disabled);
                };

                scope.configConfirm = function () {

                    var css = cfg.css.button,
                        txtAction,
                        txtConfirm,
                        cancel,
                        group;

                    button.attr("ng-click", "confirm('" + config.action + "')");

                    group = $("<span>").insertAfter(button).append(button);
                    template = group;

                    cancel = $("<button translate>")
                        .attr("ng-click", "confirm" + "(false)")
                        .addClass(css.base + " " + css.default)
                        .html("button.cancel")
                        .insertAfter(button);

                    txtAction = $("span", button);

                    txtConfirm = $("<span translate>")
                        .html("button.confirm")
                        .appendTo(button);

                    scope.$watch("click", function (click) {

                        var confirm = click === 0;

                        button.toggleClass(css.danger, confirm);
                        group.toggleClass(css.group, confirm);
                        cancel.toggle(confirm);
                        txtConfirm.toggle(confirm);
                        txtAction.toggle(!confirm);
                    });
                };

                scope.applyConfig = function (config) {

                    var fnc;

                    angular.forEach(Object.keys(config), function (key) {

                        fnc = scope["config" + key.charAt(0).toUpperCase() + key.slice(1)];
                        if (typeof fnc === "function") {
                            fnc();
                        }
                    });
                };

                scope.replaceInput = function (config) {

                    var replace;

                    if (config.type === "link") {

                        replace = $("<a>").html(button.html());

                        if (config.sref) {
                            replace.attr("ui-sref", config.sref);
                        }
                        if (config.href) {
                            replace.attr("href", config.href);
                        }
                    }

                    if (replace) {

                        if (attrs.config) {
                            replace.attr("config", attrs.config);
                        }

                        button = replace;
                        template = replace;

                        element.replaceWith(replace);
                    } else {

                        button.attr("type", config.type);
                    }
                };

                scope.replaceInput(config);
                scope.applyConfig(config);

                $compile(template)(scope);
            }
        };
    });
