/*global angular*/

angular.module("cn.form.config", [
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            input: {
                pattern: {
                    csnv: new RegExp(/^(?!,)(,?[1-5][\d][\d])+$/),
                    http_code: new RegExp("^([1-5][\\d][\\d])+$"),
                    password: new RegExp(/^.{6,}$/)
                },
                editor: {
                    default: "[['h1','h2','h3','p','quote'], ['addRef', 'bold', 'italics', 'underline', 'ul', 'ol', 'html', 'insertImage', 'insertLink', 'clear']]",
                    hipchat: "[['addRef', 'bold', 'italics', 'ul', 'ol', 'html', 'insertImage', 'insertLink', 'clear']]"
                },
                prefix: {
                    form: "form.field.",
                    button: ".button.",
                    field: ".field.",
                    select: ".select.",
                    error: "form.error.field.",
                    ref: "form.ref."
                },
                postfix: {
                    placeholder: ".placeholder",
                    help: ".help",
                    label: ".label",
                    info: ".info",
                    options: ".options"
                }
            }
        });
    });
