/*global $ angular*/

angular.module("cn.input.attachment", [
    "cn.form.config"
])
    .directive("cninputattachment", function () {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "/html/form/input.attachment.html",
            controller: function ($scope, $element) {

                $scope.link($element);

                $scope.input = {
                    config: {
                        field: {
                            transformation: {
                                type: "select",
                                model: $scope.model + ".transformation",
                                options: ["pdf"],
                                value: "pdf",
                                translate: false,
                                required: true,
                                prefix: {
                                    model: false
                                }
                            },
                            parameters: {
                                name: {
                                    model: $scope.model + ".parameters.name",
                                    placeholder: "attachment_filename",
                                    help: "attachment",
                                    required: true,
                                    prefix: {
                                        model: false
                                    },
                                    ref: true
                                },
                                template: {
                                    type: "textarea",
                                    model: $scope.model + ".parameters.template",
                                    placeholder: "attachment_template",
                                    required: true,
                                    prefix: {
                                        model: false
                                    },
                                    ref: true
                                }
                            }
                        }
                    }
                };
            }
        };
    });
