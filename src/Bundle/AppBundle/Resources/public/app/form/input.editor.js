/*global $ angular*/
/*property
    $editor, $eval, $parent, $watch, action, addMentio, assign, attr,
    changeModel, class, compile, config, controller, decorator, default,
    directive, displayElements, editor, element, focus, form, getTextAngularId,
    handler, hasClass, iconclass, input, link, log, mentio, mode, model,
    module, name, on, placeholder, pre, prefix, push, replace, restrict,
    retrieveEditor, scope, setup, stopImmediatePropagation, target,
    templateUrl, text, toggle, toolbar, value, wrapSelection
*/
/*jslint this:true */

angular.module("cn.input.editor", [
    "textAngular",
    "mentio"
])
    .config(['$provide', function ($provide) {

        'use strict';

        $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function (taRegisterTool, taOptions) {
            taRegisterTool('addRef', {
                class: "btn btn-white addRef",
                iconclass: "fa fa-plus icon-bold addRef",
                action: function () {
                    var editor = this.$editor();
                    editor.wrapSelection('insertHTML', '%');
                }
            });
            taOptions.toolbar[3].push('addRef');
            return taOptions;
        }]);
    }])

    .directive("cninputeditor", function (cfg, $document) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "/html/form/input.editor.html",
            controller: function ($scope, $element, $timeout, textAngularManager, $parse) {

                $scope.changeModel = function (value) {
                    $parse($scope.model).assign($scope, value);

                };

                $scope.addMentio = function () {
                    var textAreaMentio = angular.element('textarea[id="textAreaMentio"]');
                    if ($scope.editor.mode !== 'html') {
                        angular.element(textAreaMentio).focus();
                        $timeout(function () {
                            $scope.changeModel(textAreaMentio[0].value + '%');
                        });
                    }
                };

                $scope.handler.pre.toggle = function (toggle) {
                    var expression = typeof toggle === "string"
                        ? toggle
                        : toggle.model;

                    $scope.$watch(cfg.input.prefix.form + expression, function (now) {
                        $scope.editor.mode = typeof toggle === "string"
                            ? now
                            : toggle.mode[now];
                    });
                };

                $scope.link($("textarea", $element));
                $scope.compile($("textarea", $element));

                $scope.editor = {
                    text: $scope.$eval($scope.model) || "",
                    name: "editorName",
                    placeholder: $("textarea", $element).attr("placeholder"),
                    toolbar: cfg.input.editor[$scope.config.editor] || cfg.input.editor.default
                };

                $scope.setup = function (element) {
                    element.attr({
                        mentio: "mentio"
                    });
                };

                $scope.getTextAngularId = function () {
                    var editor = textAngularManager.retrieveEditor("editorName");
                    return editor.scope.displayElements.text.attr("id");
                };

                $document.on("click", function (event) {
                    if (angular.element(event.target).hasClass("addRef")) {
                        event.stopImmediatePropagation();
                    }
                });

                $scope.$watch($scope.model, function (data) {
                    if ($scope.editor.mode === 'text') {
                        $scope.editor.text = data;
                    }
                });

                function htmlToPlaintext(text) {
                    return text
                        ? String(text).replace(/<[^>]+>/gm, '')
                        : '';
                }

                $scope.$watch('editor.text', function (data) {
                    if ($scope.editor.mode === 'html') {
                        var value = htmlToPlaintext(data);
                        $scope.changeModel(value);
                    }
                });
            }
        };
    });