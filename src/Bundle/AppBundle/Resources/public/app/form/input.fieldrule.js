/*global angular*/
/*property
    $parsers, $setValidity, base, config, controller, ctrl, data, directive,
    extra, field, filter, form, help, input, key, length, link, model, module,
    name, options, pattern, placeholder, prefix, ref, replace, required,
    restrict, scope, templateUrl, type, unshift
*/

angular.module("cn.input.fieldrule", [
    "cn.form.config"
])
    .directive("cninputfieldrule", function () {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "/html/form/input.fieldrule.html",
            controller: function ($scope, $element) {

                $scope.link($element);

                $scope.input = {
                    config: {
                        field: {
                            base: {
                                type: "select",
                                model: $scope.model + ".base",
                                options: "ref.name as ref.name for ref in data.ref.source",
                                required: true,
                                placeholder: "fieldrule_base",
                                prefix: {
                                    model: false
                                }
                            },
                            name: {
                                model: $scope.model + ".name",
                                placeholder: "fieldrule_name",
                                help: "fieldrules",
                                required: true,
                                prefix: {
                                    model: false
                                }
                            },
                            pattern: {
                                type: "textarea",
                                model: $scope.model + ".pattern",
                                placeholder: "fieldrule_pattern",
                                required: true,
                                prefix: {
                                    model: false
                                }
                            }
                        }
                    }
                };
            },
            link: function (scope) {

                var ctrl = scope.form.ctrl["fieldrule_" + scope.key + "_name"];

                ctrl.$parsers.unshift(function (name) {

                    ctrl.$setValidity("exists", !scope.data.ref.extra.filter(function (field) {
                        if (field.name === name) {
                            return field;
                        }
                    }).length);

                    return name;
                });
            }
        };
    });
