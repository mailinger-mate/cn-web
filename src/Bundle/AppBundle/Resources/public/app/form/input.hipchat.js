/*global angular*/
/*property
    $eval, $watch, application, config, controller, deleteProperty, directive,
    dynamic, error, form, help, hipchat, input, link, model, module, name,
    options, placeholder, prefix, replace, required, restrict, room, scope,
    templateUrl, to, type, user, empty
*/

angular.module("cn.input.hipchat", [
    "cn.form.config"
])
    .directive("cninputhipchat", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "/html/form/input.hipchat.html",
            controller: function ($scope, $element) {

                $scope.link($element);

                $scope.hipchat = {
                    config: {
                        room: {
                            type: "select",
                            name: "hipchat_room",
                            model: "config.room_id.value",
                            options: [],
                            error: true,
                            placeholder: true,
                            required: "hipchat.config.to==='room_id'",
                            dynamic: {
                                options: "room.id.toString() as room.name for room in hipchat.config.room.options",
                                application: "hipchat",
                                name: "room"
                            },
                            prefix: {
                                required: false
                            }
                        },
                        user: {
                            type: "select",
                            name: "hipchat_user",
                            model: "config.user_id.value",
                            options: [],
                            error: true,
                            placeholder: true,
                            required: "hipchat.config.to==='user_id'",
                            help: "hipchat_to",
                            empty: true,
                            dynamic: {
                                options: "user.id.toString() as user.name for user in hipchat.config.user.options",
                                application: "hipchat",
                                name: "user"
                            },
                            prefix: {
                                required: false
                            }
                        },
                        to: $scope.$eval(cfg.input.prefix.form + "config.user_id")
                            ? "user_id"
                            : "room_id"
                    }
                };

                $scope.$watch("hipchat.config.to", function (now, before) {
                    if (now !== before) {
                        $scope.deleteProperty(cfg.input.prefix.form + "config." + before);
                    }
                });

            }
        };
    });
