/*global $ angular*/
/*property
    $eval, $watch, attr, compile, config, controller, directive, error,
    forEach, interval, link, max, min, model, module, number, options, period,
    prefix, replace, restrict, scope, setInterval, templateUrl, type, value
*/

angular.module("cn.input.interval", [
    "cn.form.config"
])
    .directive("cninputinterval", function () {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "/html/form/input.interval.html",
            controller: function ($scope, $element) {

                var model;

                function getQueryInterval(interval) {

                    var period = interval || 1,
                        number = 1;

                    if (interval > 0) {

                        angular.forEach($scope.interval.config.period.options, function (value) {
                            if (interval % value === 0) {
                                period = value;
                                number = interval / value;
                            }
                        });
                    }

                    $scope.interval.value = {
                        period: period,
                        number: number
                    };
                }

                function setQueryInterval() {

                    var interval = $scope.interval.value;
                    $scope.$eval(model + "=" + (interval.period || 1) * (interval.number || 1));
                }

                $scope.link($("input.interval", $element));

                model = $scope.model;

                $scope.interval = {
                    config: {
                        period: {
                            type: "select",
                            options: [-1, 1, 60, 3600, 86400, 604800],
                            error: false,
                            prefix: {
                                model: false
                            }
                        },
                        number: {
                            type: "number",
                            error: false,
                            prefix: {
                                model: false
                            }
                        },
                        min: {
                            "1": $scope.config.min || 10,
                            "60": 1,
                            "3600": 1,
                            "86400": 1,
                            "604800": 1
                        },
                        max: {
                            "1": 2592000,
                            "60": 43200,
                            "3600": 720,
                            "86400": 30,
                            "604800": 4
                        }
                    }
                };

                getQueryInterval($scope.$eval($scope.model));

                $scope.$watch("interval.value.period", function (period) {

                    var number, min, max;

                    if (period > -1) {

                        number = $scope.interval.value.number;
                        min = $scope.interval.config.min[period];
                        max = $scope.interval.config.max[period];

                        setQueryInterval();

                        $("input.number", $element).attr({
                            min: min,
                            max: max
                        });

                        if (!number || (period === 1 && number < $scope.config.min)) {
                            $scope.interval.value.number = min;
                        }
                        if (number > max) {
                            $scope.interval.value.number = max;
                        }
                    }

                    if (period === -1) {
                        delete $scope.interval.value.number;
                        setQueryInterval();
                    }
                });

                $scope.$watch("interval.value.number", function (now) {
                    if (now) {
                        setQueryInterval();
                    }
                });
            },
            link: function (scope, element) {
                scope.compile($("input.interval", element));
            }
        };
    });
