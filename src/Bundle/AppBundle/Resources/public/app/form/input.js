/*global $ angular*/
/*property
    $dirty, $eval, $invalid, $parent, $parsers, $pristine, $render, $setDirty,
    $setValidity, $setViewValue, $watch, addClass, alert, appendTo, attr, analyze, blur,
    button, class, click, compile, config, confirm, controller, css, ctrl,
    deleteProperty, directive, each, empty, enabled, error, expression, extend,
    field, forEach, form, getConfig, group, handler, hasClass, hasOwnProperty,
    help, hidden, hide, icon, indexOf, info, input, insertAfter, invalid, is, label,
    labeltranslate, length, link, listener, match, mentio, model, module, name,
    nextAll, on, options, parent, parents, pattern, placeholder, placement,
    popover, post, postfix, pre, prefix, prependTo, ref, removeAttr,
    removeClass, replace, required, reset, restrict, scope, split, stringify,
    substring, switch, template, text, title, toString, toggle, toggleClass,
    transclude, translate, trigger, type, unshift, value, watch
*/

angular.module("cn.input", [
    "cn.config",
    "cn.input.attachment",
    "cn.input.editor",
    "cn.input.interval",
    "cn.input.fieldrule",
    "cn.input.hipchat",
    "cn.input.number",
    "cn.input.query",
    "cn.input.select",
    "cn.form.input.slack",
    "cn.input.switch",
    "cn.input.tags",
    "cn.input.text",
    "cn.input.textarea"
])
    .config(function ($tooltipProvider) {

        "use strict";

        $tooltipProvider.options({
            placement: "left",
            trigger: "focus"
        });

    })
    .directive("cninputcontrol", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            transclude: true,
            scope: true,
            template: '<div class="form-group">' +
                    '<label class="control-label col-sm-2" translate></label>' +
                    '<div class="control-input col-sm-10">' +
                    '<div ng-transclude></div>' +
                    '<span class="control-text">' +
                    '<span class="info" ng-show="view.action.info" translate></span>' +
                    '<span class="error text-danger"></span>' +
                    '</span>' +
                    '</div>' +
                    '</div>',
            controller: function ($scope, $element) {

                var form = $scope[$element.parents("form").attr("name")];

                $scope.form.ctrl = form;

                $scope.$watch(function () {

                    var input = $(":input:not(:button)", $element),
                        invalid = false;

                    input.each(function (index) {

                        var element = input[index],
                            ctrl = form[element.name],
                            config = $(element).scope().config,
                            error = (config && config.model)
                                ? $scope.$eval(cfg.input.prefix.error + config.model)
                                : false;

                        if ((ctrl && ctrl.$dirty && ctrl.$invalid) || error) {
                            invalid = true;
                        }

                    });

                    return invalid;

                }, function (invalid) {
                    $element.toggleClass(cfg.css.field.invalid, invalid);
                });
            },
            link: function (scope, element, attrs) {
                var $label,
                    text = attrs.labeltranslate === "false"
                        ? attrs.label
                        : (((scope.form && scope.form.config && scope.form.config.prefix && scope.form.config.prefix.translate) || "")
                                + cfg.input.prefix.field + attrs.label + cfg.input.postfix.label);

                $label = $("label", element).text(text);

                if (attrs.labeltranslate === "false") {
                    $label.removeAttr("translate");
                }
            }
        };
    })
    .directive("cninput", function () {

        "use strict";

        return {
            restrict: "A",
            controller: function ($scope, $compile, cfg) {

                $scope.form = $scope.form || $scope.$parent.form || {};

                $scope.deleteProperty = function (key) {

                    var scope = $scope,
                        regex = new RegExp("[.'[\\]]+", "g"),
                        object = key.split(regex);

                    angular.forEach(object, function (property, index) {

                        if (scope && scope.hasOwnProperty(property)) {
                            if (index === object.length - 1) {
                                delete scope[property];
                            }
                            scope = scope[property];
                        }
                    });
                };

                $scope.getConfig = function (attrs) {

                    var reject = ["config", "class"],
                        evaluate = ["prefix"];

                    $scope.config = $.extend(true, {
                        model: undefined,
                        type: "text",
                        error: true,
                        translate: true,
                        prefix: {
                            translate: $scope.form.config.prefix.translate
                        }
                    }, $scope.$eval(attrs.config));

                    angular.forEach(attrs, function (value, key) {

                        if (key[0] !== "$" && key.substring(0, 2) !== "ng" && reject.indexOf(key) < 0 && value !== "") {

                            if (value === "true") {
                                value = true;
                            }

                            if (value === "false") {
                                value = false;
                            }

                            if (evaluate.indexOf(key) >= 0) {
                                value = $scope.$eval(value);
                            }

                            $scope.config[key] = value;
                        }
                    });
                };

                $scope.handler = {
                    pre: {
                        attr: function ($element, attr) {

                            if (typeof attr === "object") {
                                $element.attr(attr);
                            } else {
                                $element.attr(attr, attr);
                            }
                        },
                        analyze: function ($element, analyze) {
                            if (analyze === false) {
                                $element.addClass(cfg.css.analyze.hidden);
                            }
                        },
                        enabled: function ($element, enabled) {

                            var prefix = $scope.config.prefix.enabled !== false
                                    ? cfg.input.prefix.form
                                    : "",
                                disabled;

                            disabled = enabled === false
                                ? true
                                : enabled === true
                                    ? false
                                    : (enabled.indexOf("==") !== -1
                                        ? (prefix + enabled.replace("==", "!="))
                                        : enabled.indexOf("!=") !== -1
                                            ? (prefix + enabled.replace("!=", "=="))
                                            : ("!" + prefix + enabled));

                            $element.attr("ng-disabled", disabled);

                            if (enabled && $scope.config.hide !== false) {

                                $scope.$watch(disabled, function (disabled) {
                                    if (disabled) {
                                        $scope.deleteProperty($scope.model);
                                    }
                                    $element.parents("." + cfg.css.form.group).toggle(!disabled);
                                });
                            }
                        },
                        error: function ($element) {

                            var parent = $element.parents("div.form-group"),
                                error = $("span.error", parent).attr("ng-bind", cfg.input.prefix.error + $scope.config.model);

                            $scope.compile(error);
                        },
                        info: function ($element, info) {

                            var prefix = ($scope.config.prefix.translate || "") + (cfg.input.prefix.field || ""),
                                parent = $element.parents("div.form-group");


                            if (info === true) {
                                info = $scope.config.model;
                            }

                            $scope.compile($("span.info", parent).text(prefix + info + cfg.input.postfix.info));
                        },
                        help: function ($element, help) {

                            var prefix = ($scope.config.prefix.translate || "") + (cfg.input.prefix.field || ""),
                                $parent = $element.hasClass(cfg.css.button.group)
                                    ? $element
                                    : $element.parent("div." + cfg.css.input.group),
                                $wrap = $parent.length
                                    ? $parent
                                    : $element.parent("div").addClass(cfg.css.input.group),
                                $insert = $element.nextAll("." + cfg.css.input.button),
                                $help = $insert.length
                                    ? $("<span>").insertAfter($insert)
                                    : $("<span>").appendTo($wrap),
                                $button;

                            if (help === true) {
                                help = $scope.config.name;
                            }

                            $help.addClass(cfg.css.input.button).addClass("control-help");

                            $button = $("<button>").prependTo($help).attr({
                                type: "button",
                                title: "{{ 'button.help' | translate }}",
                                class: "input-help btn btn-white",
                                popover: "{{ '" + prefix + help + cfg.input.postfix.help + "' | translate }}"
                            });

                            if ($element.parent().hasClass(cfg.css.input.switch)) {
                                $button.attr("popover-placement", "top");
                            }

                            $("<i>").addClass("icon-help " + cfg.css.icon.help).appendTo($button);
                            $("<i>").addClass("icon-alert " + cfg.css.icon.alert).appendTo($button);

                            $scope.compile($button);
                        },
                        model: function ($element, model) {

                            $scope.model = model;

                            if ($scope.config.prefix !== false && $scope.config.prefix.model !== false) {
                                $scope.model = ($scope.config.prefix.model || cfg.input.prefix.form) + model;
                            }

                            $element.attr("ng-model", $scope.model);

                            $scope.$watch($scope.model, function (now, before) {

                                if ((now === undefined || now === "") && !$scope.config.empty) {
                                    $scope.deleteProperty($scope.model);
                                    $element.addClass("input-empty");
                                } else {
                                    $element.removeClass("input-empty");
                                }

                                if (now !== before) {
                                    $scope.deleteProperty(cfg.input.prefix.error + model);
                                }
                            });
                        },
                        name: function ($element, name) {
                            $element.attr("name", name);
                        },
                        pattern: function ($element, pattern) {

                            $element.attr("ng-pattern", cfg.input.pattern[pattern]);
                        },
                        placeholder: function ($element, placeholder) {

                            placeholder = $scope.config.prefix.translate + cfg.input.prefix.field + (placeholder === true
                                ? $scope.config.name
                                : placeholder) + cfg.input.postfix.placeholder;

                            if ($element.is("select")) {
                                $("<option value='' class='placeholder' disabled translate>" + placeholder + "</option>").appendTo($element);
                            } else {
                                $element.attr("placeholder", "{{ '" + placeholder + "' | translate }}");
                            }
                        },
                        ref: function ($element) {

                            $element.attr({
                                mentio: "mentio",
                                "mentio-trigger-char": "'%'",
                                "mentio-items": "ref",
                                "mentio-template-url": "ref-search",
                                "mentio-search": "searchRef(term)",
                                "mentio-select": "getRefText(item)"
                            });
                        },
                        required: function ($element, required) {

                            var enabled = $scope.config.enabled,
                                prefix = {
                                    required: $scope.config.prefix.required !== false
                                        ? cfg.input.prefix.form
                                        : "",
                                    enabled: $scope.config.prefix.enabled !== false
                                        ? cfg.input.prefix.field
                                        : ""
                                };

                            required = typeof required === "string"
                                ? prefix.required + required
                                : required === true;

                            required = enabled !== false
                                ? typeof enabled === "string"
                                    ? typeof required === "string"
                                        ? required + "&&" + prefix.enabled + enabled
                                        : required === true
                                            ? prefix.required + enabled
                                            : false
                                    : required
                                : false;

                            $element.attr("ng-required", required);
                        },
                        reset: function ($element) {

                            var form = $scope.form.ctrl;

                            function inputReset() {

                                var ctrl = form[$scope.config.name];

                                if (ctrl.$pristine) {
                                    $scope.deleteProperty($scope.model);
                                    ctrl.$setViewValue("");
                                    ctrl.$render();
                                }
                            }

                            function inputDirty() {

                                form[$scope.config.name].$setDirty();
                            }

                            $element.on({
                                click: inputReset,
                                blur: inputDirty
                            });
                        },
                        value: function (value) {

                            if (!$scope.$eval($scope.model)) {
                                $scope.$eval($scope.model + "=" + JSON.stringify(value));
                            }
                        },
                        watch: function (watch) {

                            angular.forEach(watch, function (watch) {

                                var exp = watch.expression;

                                if (typeof exp === "string") {
                                    exp = cfg.input.prefix.form + (exp === "model"
                                        ? $scope.config.model
                                        : exp);
                                }

                                $scope.$watch(exp, function (now, before) {
                                    watch.listener($scope, now, before);
                                });
                            });
                        }
                    },
                    post: {
                        confirm: function (confirm) {

                            var name = $scope.config.name,
                                ctrl = $scope.form.ctrl[name];

                            $scope.$watch(function () {
                                return $scope.$eval($scope.config.model + "===" + cfg.input.prefix.form + confirm);
                            }, function (equals) {
                                $scope.form.ctrl[name].$setValidity(name, equals);
                            });

                            ctrl.$parsers.unshift(function (value) {

                                ctrl.$setValidity(name, value === $scope.$eval(cfg.input.prefix.form + confirm));
                                return value;
                            });
                        }
                    }
                };

                $scope.link = function (element, link) {

                    if (!$scope.config.name) {
                        $scope.config.name = $scope.config.model;
                    }

                    angular.forEach($scope.config, function (config, key) {

                        var handler = $scope.handler[link || "pre"][key],
                            params;

                        if (handler && config !== undefined) {

                            if (typeof handler === "function") {

                                element.removeAttr(key);
                                params = handler.toString().match(/\(\s*([^)]+?)\s*\)/)[1].split(/\s*,\s*/);

                                if (params[0] === "$element") {
                                    handler(element, config);
                                } else {
                                    handler(config);
                                }
                            }
                        }
                    });

                    element.removeAttr("cninput" + $scope.config.type).removeAttr("config");
                };

                $scope.compile = function (element) {

                    $compile(element)($scope);
                };

            },
            link: function (scope, element, attrs) {

                scope.getConfig(attrs);
                element.attr("cninput" + scope.config.type, "").removeAttr("cninput");
                scope.compile(element);
            }
        };
    });
