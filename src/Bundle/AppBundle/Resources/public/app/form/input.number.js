/*global angular*/
/*property
    $formatters, $parsers, attr, compile, controller, ctrl, directive, form,
    handler, link, max, min, module, post, pre, push, replace, restrict, scope,
    string, template
*/

angular.module("cn.input.number", [
    "cn.form.config"
])
    .directive("cninputnumber", function () {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<input type="number" class="form-control">',
            controller: function ($scope) {

                $scope.handler.pre.max = function ($element, max) {
                    $element.attr("max", max);
                };

                $scope.handler.pre.min = function ($element, min) {
                    $element.attr("min", min);
                };

                $scope.handler.post.string = function ($element) {

                    var ctrl = $scope.form.ctrl[$element.attr("name")];

                    ctrl.$parsers.push(function (value) {
                        return '' + value;
                    });

                    ctrl.$formatters.push(function (value) {
                        return parseFloat(value, 10);
                    });
                };
            },
            link: function (scope, element) {

                scope.link(element);
                scope.compile(element);
                scope.link(element, "post");
            }
        };
    });
