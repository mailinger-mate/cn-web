/*global $ angular*/
/*property
    $eval, $index, $parent, $watch, addGroup, addRule, application, attr,
    compile, config, controller, data, dataSelect, directive, extend, field,
    forEach, form, group, handler, help, indexOf, input, length, link, logic,
    manual, match, model, module, operator, options, placeholder, pre, prefix,
    property, push, query, querygroup, removeGroup, removeRule, replace,
    required, resetData, resetQuery, restrict, rule, rules, scope, select,
    slice, splice, stringify, templateUrl, text, translate, type
*/

angular.module("cn.input.query", [
    "cn.form.config"
])
    .directive("cninputquery", function () {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "html/form/input.query.html",
            controller: function ($scope, $element) {

                var regex = {
                        query: new RegExp("^(\\(.*?\\)|.*?)(?: +(AND|OR) +(.*))?$"),
                        rule: new RegExp("^(.*?)\\s(.*?)\\s+'?(.*?)'?$")
                    },
                    input = $("input.query", $element),
                    model;

                $scope.link(input);

                model = $scope.model;

                $scope.application = $scope.config.application;

                $scope.query = {
                    config: $.extend(true, $scope.config.query, {
                        group: {
                            type: "querygroup",
                            application: $scope.config.application,
                            model: false
                        },
                        manual: false
                    })
                };

                $scope.data = {};

                function initBuilder() {

                    function parseQuery(string, data) {

                        var partial, group;

                        function parseQueryRule(string) {

                            var rule = string.match(regex.rule) || [];

                            return {
                                field: rule[1],
                                operator: rule[2],
                                data: rule[3]
                            };
                        }

                        data.group = data.group || {rules: string
                            ? []
                            : [{}]};

                        if (!string) {
                            return;
                        }

                        partial = string.match(regex.query);

                        if (partial[1][0] === "(") {
                            group = {group: {rules: []}};
                            data.group.rules.push(group);
                            parseQuery(partial[1].slice(1, -1), group);
                        } else {
                            data.group.rules.push(parseQueryRule(partial[1]));
                        }

                        if (partial[2]) {
                            data.group.logic = partial[2];
                            parseQuery(partial[3], data);
                        }
                    }

                    parseQuery($scope.$eval(model), $scope.data);
                }

                function resetBuilder() {

                    $scope.data = {};
                }

                $scope.resetQuery = function () {

                    delete $scope.form.field.config.query;
                    resetBuilder();
                    initBuilder();
                };

                function serializeQuery(group, parenthesis) {

                    var string = "";

                    if (!group || !group.rules) {
                        return;
                    }

                    angular.forEach(group.rules, function (rule, key) {

                        var property = {},
                            field = rule.field,
                            operator = rule.operator,
                            data = rule.data;

                        if ($scope.query.config.field) {
                            property.field = $scope.query.config.field.property;
                        }
                        if ($scope.query.config.data) {
                            property.data = $scope.query.config.data.property;
                        }

                        if (field && field[property.field]) {
                            field = field[property.field];
                        }

                        if (data) {
                            if (data[property.data]) {
                                data = data[property.data];
                            }
                            if (data.indexOf(" ") >= 0) {
                                data = (" '" + data + "'");
                            }
                        }

                        if (group.logic && key > 0) {
                            string += " " + group.logic + " ";
                        }

                        if (rule.group) {
                            string += serializeQuery(rule.group, true);
                        } else if (field || operator || data) {
                            string += (field || "") + " " + (operator || "") + " " + (data || "");
                        }
                    });

                    if (parenthesis && group.rules.length > 1) {
                        string = "(" + string + ")";
                    }

                    return string;
                }

                $scope.$watch("data", function (data) {
                    if (data && data.group) {
                        $scope.$eval(model + "=" + JSON.stringify(serializeQuery(data.group)));
                    }
                }, true);

                $scope.$watch("query.config.manual", function (now) {
                    if (now) {
                        resetBuilder();
                    } else {
                        initBuilder();
                    }
                });

            },
            link: function (scope, element) {

                scope.compile($("input.query", element));
            }
        };
    })
    .directive("cninputquerygroup", function () {

        "use strict";

        return {
            restrict: 'A',
            scope: true,
            templateUrl: "html/form/input.query.group.html",
            controller: function ($scope, $element) {

                var input = {};

                $scope.handler.pre.group = function (group) {
                    $scope.$watch(group, function (now) {
                        $scope.group = now;
                    });
                };

                $scope.link($element);

                if ($scope.query.config.data && $scope.query.config.data.input) {
                    input = {
                        text: $scope.query.config.data.input.text,
                        select: $scope.query.config.data.input.select
                    };
                }

                $scope.querygroup = {
                    config: {
                        logic: {
                            type: "select",
                            placeholder: "query_group",
                            options: ["AND", "OR"],
                            required: "group.rules.length > 1",
                            help: "query_group",
                            prefix: {
                                model: false,
                                required: false
                            }
                        },
                        field: $.extend(true, {
                            type: "select",
                            placeholder: "query_field",
                            translate: "ref." + $scope.application,
                            required: "rule.operator || rule.data || level > 1",
                            attr: {
                                "ng-change": "resetData(rule)"
                            },
                            prefix: {
                                model: false,
                                required: false
                            }
                        }, $scope.query.config.field),
                        operator: {
                            type: "select",
                            placeholder: "query_operator",
                            options: $scope.query.config.operator.options,
                            required: "rule.field || rule.data || level > 1",
                            prefix: {
                                model: false,
                                required: false
                            }
                        },
                        data: {
                            placeholder: "query_data",
                            help: "query_rule",
                            required: "(rule.field || rule.operator || level > 1)" + (input.text
                                ? ("&&" + input.text)
                                : ""),
                            attr: {
                                "ng-show": input.text || true
                            },
                            prefix: {
                                model: false,
                                required: false
                            }
                        },
                        dataSelect: $.extend(true, {
                            type: "select",
                            placeholder: "query_data",
                            required: "(rule.field || rule.operator)" + (input.select
                                ? ("&&" + input.select)
                                : ""),
                            attr: {
                                "ng-show": input.select || false
                            },
                            prefix: {
                                model: false,
                                required: false
                            }
                        }, $scope.query.config.data)
                    }
                };

                $scope.addRule = function () {
                    $scope.group.rules.push({});
                };

                $scope.removeRule = function (index) {
                    $scope.group.rules.splice(index, 1);
                };

                $scope.addGroup = function () {
                    $scope.group.rules.push({
                        group: {rules: [{}]}
                    });
                };

                $scope.removeGroup = function () {
                    if ($scope.$parent.group) {
                        $scope.$parent.group.rules.splice($scope.$parent.$index, 1);
                    }
                };

                $scope.resetData = function (rule) {
                    delete rule.data;
                };
            }
        };
    });
