/*global angular $*/
/*property
    $data, $eval, $exp, $expression, $filter, $label, $listener, $property,
    $select, $source, $track, $watch, account, attr, compile, config, control,
    controller, css, directive, dynamic, expression, field, form, handler, id,
    info, input, isArray, length, link, listener, model, module, multiple,
    name, options, placeholder, postfix, pre, prefix, ref, replace, restrict,
    scope, select, stringify, success, template, translate
*/

angular.module("cn.input.select", [
    "cn.form.config"
])
    .directive("cninputselect", function (cfg, action) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<select class="' + cfg.css.input.control + '"></select>',
            controller: function ($scope, $attrs) {

                var model,
                    input;

                $scope.handler.pre.options = function ($element, options) {

                    var translate = $scope.config.translate,
                        placeholder = $scope.config.placeholder,
                        exp = "",
                        prefix;

                    input = ($scope.config.dynamic && $scope.config.dynamic.options)
                        ? $scope.config.dynamic.options
                        : options;
                    model = $scope.$eval($scope.config.model);

                    if (typeof input === "object") {

                        prefix = ($scope.config.prefix.translate || "") + (cfg.input.prefix.select || "");

                        if (input.$select) {
                            exp += "key." + input.$select + " as ";
                        }

                        if (input.$label) {

                            exp += input.$label;

                        } else {

                            if (translate === false) {

                                exp += "key";

                            } else {

                                if (input.$property) {
                                    exp += "key." + input.$property + " as ";
                                }

                                exp += "'" + prefix;

                                if (typeof translate === "string") {
                                    exp += translate;
                                } else {
                                    if (typeof placeholder === "string") {
                                        exp += placeholder;
                                    } else {
                                        exp += $scope.config.name;
                                    }
                                }

                                exp += ".' + key";

                                if (input.$property) {
                                    exp += "." + input.$property;
                                }

                                exp += " | translate";

                            }
                        }

                        exp += " for ";

                        if (input.$data) {

                            exp += input.$data;

                        } else {

                            if (input.$property || $.isArray($scope.$eval(input.$source) || options)) {
                                exp += "key";
                            } else {
                                exp += "(key, value)";
                            }
                        }

                        if (!input.$source) {
                            input.$source = $attrs.config + cfg.input.postfix.options;
                        }

                        exp += " in " + input.$source;

                        if (input.$filter) {
                            exp += " | " + input.$filter;
                        }

                        if (input.$track) {
                            exp += " track by " + input.$track;
                        }

                        if (input.$exp && input.$listener) {
                            $scope.$watch(cfg.input.prefix.form + input.$expression, function (now) {
                                options = input.$listener(now);
                                $scope.$eval($attrs.config + cfg.input.postfix.options + "=" + JSON.stringify(options));
                            });
                        }
                    } else {
                        exp = input;
                    }

                    $element.attr({
                        "ng-options": exp
                    });
                };

                $scope.handler.pre.multiple = function ($element, multiple) {
                    $element.attr("multiple", multiple);
                };

                $scope.handler.pre.dynamic = function (dynamic) {

                    function getOptions() {

                        action.account.info({
                            id: $scope.config.account || $scope.form.config.account.id,
                            field: dynamic.name,
                            success: function (data) {
                                var handler = dynamic.handler;
                                data = handler
                                    ? handler(data, $scope.form.ref)
                                    : data;
                                $scope.$eval($attrs.config + cfg.input.postfix.options + "=" + JSON.stringify(data));
                            }
                        });
                    }

                    $scope.$watch("form.config.account", function (account) {

                        var name = dynamic.name;

                        if (account && account.id) {
                            if (name) {
                                if (name.length) {
                                    getOptions();
                                } else {
                                    $scope.$watch(cfg.input.prefix.form + name.expression, function (now) {
                                        if (now) {
                                            dynamic.name = name.listener(now);
                                            getOptions();
                                        }
                                    });
                                }
                            }
                        }
                    }, true);
                };

                $scope.handler.pre.select = function ($element, select) {

                    var index = 0,
                        filter = input.$filter
                            ? " | " + input.$filter
                            : "";

                    if (!model) {
                        if (typeof select === "number") {
                            index = select;
                        }
                        $element.attr("ng-init", $scope.config.model + " = (" + input.$source + filter + ")[" + index + "]");
                    }
                };
            },
            link: function (scope, element) {
                scope.link(element);
                scope.compile(element);
            }
        };
    });
