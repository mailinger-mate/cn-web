/*global angular*/
/*property
    $eval, $watch, application, base, button, channel, config, controller, css,
    deleteProperty, directive, dynamic, error, form, group, help, input, link,
    model, module, name, options, placeholder, prefix, replace, required,
    restrict, scope, slack, templateUrl, to, type, user, white, empty
*/

angular.module("cn.form.input.slack", [
    "cn.form.config"
])
    .directive("cninputslack", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            templateUrl: "/html/form/input.slack.html",
            controller: function ($scope, $element) {

                $scope.link($element);

                $scope.slack = {
                    config: {
                        channel: {
                            type: "select",
                            name: "slack_channel",
                            model: "config.channel_id.value",
                            options: [],
                            error: true,
                            placeholder: true,
                            required: "slack.config.to==='channel_id'",
                            dynamic: {
                                options: "channel.id as channel.name for channel in slack.config.channel.options",
                                application: "slack",
                                name: "channel"
                            },
                            prefix: {
                                required: false
                            }
                        },
                        user: {
                            type: "select",
                            name: "slack_user",
                            model: "config.user_id.value",
                            options: [],
                            error: true,
                            placeholder: true,
                            required: "slack.config.to==='user_id'",
                            help: "slack_to",
                            empty: true,
                            dynamic: {
                                options: "user.id as user.name for user in slack.config.user.options",
                                application: "slack",
                                name: "user"
                            },
                            prefix: {
                                required: false
                            }
                        },
                        to: $scope.$eval(cfg.input.prefix.form + "config.user_id")
                            ? "user_id"
                            : "channel_id"
                    }
                };

                $scope.$watch("slack.config.to", function (now, before) {
                    if (now !== before) {
                        $scope.deleteProperty(cfg.input.prefix.form + "config." + before);
                    }
                });

            }
        };
    });
