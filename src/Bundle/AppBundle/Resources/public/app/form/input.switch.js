/*global $ angular*/
/*property
    $eval, appendTo, attr, class, compile, controller, css, directive, input,
    link, model, module, off, on, replace, restrict, scope, switch, template
*/

angular.module("cn.input.switch", [
    "uiSwitch"
])
    .directive("cninputswitch", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<div class="' + cfg.css.input.switch + '"></div>',
            controller: function ($scope, $element) {

                var input,
                    model;

                input = $("<switch>").attr({
                    on: "{{ 'connection.item.button.status.on' | translate }}",
                    off: "{{ 'connection.item.button.status.off' | translate }}",
                    class: "green"
                }).appendTo($element);

                $scope.link(input);

                model = $scope.$eval($scope.model);

                $scope.$eval($scope.model + "=" + !!(model === true || model === "true"));

                $scope.compile(input);

            }
        };
    });
