/*global $ angular*/
/*property
    $eval, $setValidity, $watch, attr, compile, config, controller,
    deleteProperty, directive, extend, handler, indexOf, input, keyCode,
    length, link, log, model, module, name, onKeyDown, parents, pattern, pre,
    prefix, preventDefault, push, remove, replace, require, required, restrict,
    scope, shiftKey, splice, stopImmediatePropagation, stringify, tags,
    template, test, type, updateField
*/

angular.module("cn.input.tags", [
    "cn.form.config"
])
    .directive("cninputtags", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            require: 'ngModel',
            replace: true,
            scope: true,
            template: '<div class="cn-tags">' +
                    '<span class="tag-item"  ng-repeat="(key, tag) in tags.model">' +
                    '<input name="{{ \'tagInput_\' + $index }}" type="text" id="{{ $index }}" ' +
                    'ng-model="tag" ng-blur="updateField(tag, $index)" class="input tag-value form-control"' +
                    '></input>' +
                    '<button class="btn btn-white" type="button" ng-click="remove(key)"><i class="fa fa-times"></i></button>' +
                    '</span>' +
                    '<div cninput class="input" config="tags.config.input"></div>' +
                    '<input type="hidden" class="model">' +
                    '</div>',
            controller: function ($scope, $element) {
                var model, pattern, form;
                form = $scope[$element.parents("form").attr("name")];
                delete $scope.handler.pre.pattern;
                $scope.config.input = $scope.config.input || {};

                $scope.link($("input", $element));
                $scope.compile($("input", $element).attr("name", $scope.config.name));

                model = $scope.model;
                pattern = cfg.input.pattern[$scope.config.input.pattern];
                $scope.tags = {
                    config: {
                        input: $.extend($scope.config.input, {
                            model: "tags.input",
                            name: "tags_input",
                            required: $scope.config.input.required
                                ? "!" + model
                                : false,
                            pattern: false,
                            prefix: {
                                model: false,
                                required: false
                            }
                        })
                    },
                    model: $scope.$eval(model) || []
                };

                if ($scope.config.input.type === "select") {
                    $scope.tags.config.input.attr = {
                        "ng-change": "onKeyDown()"
                    };
                } else {
                    $scope.tags.config.input.attr = {
                        "ng-keydown": "onKeyDown($event)",
                        "ng-blur": "onKeyDown()"
                    };
                }
                $scope.updateField = function (tag, index) {
                    var ctrl = form['tagInput_' + index];
                    if (tag && $scope.tags.model.indexOf(tag) < 0) {
                        if (pattern && !pattern.test(tag)) {
                            ctrl.$setValidity("invalid", false);
                        } else if (pattern && pattern.test(tag)) {
                            $scope.tags.model[index] = tag;
                            ctrl.$setValidity("invalid", true);
                        } else {
                            $scope.tags.model[index] = tag;
                            ctrl.$setValidity("duplicate", true);
                        }
                    } else if ($scope.tags.model.indexOf(tag) >= 0) {

                        if ($scope.tags.model[index] !== tag) {
                            ctrl.$setValidity("duplicate", false);
                        } else {
                            ctrl.$setValidity("duplicate", true);
                        }
                    }
                    if (pattern && pattern.test(tag)) {
                        ctrl.$setValidity("invalid", true);
                    }
                };
                $scope.onKeyDown = function (event) {

                    var handled = false,
                        tags = $scope.tags.model,
                        input = $scope.tags.input;
                    if (!event || (!event.shiftKey && (event.keyCode === 188 || event.keyCode === 9))) {
                        handled = true;
                        if (input && tags.indexOf(input) < 0 && (!pattern || pattern.test(input))) {
                            $scope.tags.model.push(input);
                            $scope.tags.input = "";
                        }
                    }
                    if (event && event.keyCode === 8) {
                        if (input === undefined) {
                            event.preventDefault();
                            $scope.tags.input = $scope.tags.model[$scope.tags.model.length - 1];
                            $scope.remove($scope.tags.model.length - 1);
                        }
                    }

                    if (event && handled) {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        return false;
                    }
                };

                $scope.remove = function (key) {
                    $scope.tags.model.splice(key, 1);
                };

                $scope.$watch("tags.model", function (now) {
                    if (now && now.length) {
                        $scope.$eval(model + "=" + JSON.stringify($scope.tags.model));
                    } else {
                        $scope.deleteProperty(model);
                    }
                }, true);
            }
        };
    });