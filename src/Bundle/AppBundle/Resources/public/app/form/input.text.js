/*global
    angular*/
/*property
    $formatters, $parsers, $render, $setViewValue, addClass, compile, control,
    controller, css, directive, indexOf, input, link, model, module, name,
    push, replace, require, restrict, scope, splice, template, test
*/
angular.module("cn.input.text", [
    "cn.form.config"
])
    .directive("cninputtext", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<input type="text" class="' + cfg.css.input.control + '">',
            link: function (scope, element) {
                scope.link(element);
                scope.compile(element);
            }
        };
    })
    .directive("cninputpassword", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<input type="password" class="' + cfg.css.input.control + '">',
            link: function (scope, element) {
                scope.link(element);
                scope.compile(element);
                scope.link(element, "post");
            }
        };
    })
    .directive("cninputurl", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            require: "ngModel",
            replace: true,
            scope: true,
            template: '<input type="url" class="' + cfg.css.input.control + '">',
            controller: function ($scope, $element) {
                $scope.link($element);
                $scope.compile($element);
            },
            link: function (scope, element, ignore, ctrl) {

                function httpPrefix(value) {
                    if (value && !(/^(https?):\/\//i).test(value) && 'http://'.indexOf(value) !== 0 && 'https://'.indexOf(value) !== 0) {
                        ctrl.$setViewValue('http://' + value);
                        ctrl.$render();
                        return 'http://' + value;
                    } else {
                        return value;
                    }
                }
                ctrl.$formatters.push(httpPrefix);
                ctrl.$parsers.splice(0, 0, httpPrefix);
                element.addClass("url-" + scope.name);
            }
        };
    })
    .directive("cninputemail", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<input type="email" class="' + cfg.css.input.control + '">',
            link: function (scope, element) {
                scope.link(element);
                scope.compile(element);
            }
        };
    });
