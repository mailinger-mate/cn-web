/*global angular*/
angular.module("cn.input.textarea", [
    "cn.form.config"
])
    .directive("cninputtextarea", function (cfg) {

        "use strict";

        return {
            restrict: "A",
            replace: true,
            scope: true,
            template: '<textarea class="' + cfg.css.input.control + '"></textarea>',
            controller: function ($scope, $element) {
                $scope.link($element);
                $scope.compile($element);
            }
        };
    });
