/*global angular*/
/*property
    $on, billing, controller, data, get, getPermission, home, icon, indexOf,
    label, module, name, nav, only, parent, permission, permissions, profile,
    role, special, sref, state, text, tutorial, user, vendorBlocked
*/

angular.module("cn.nav", [
    "cn.session"
])
    .controller('NavCtrl', function ($scope, $state, cfg, $filter, $rootScope, $timeout) {

        "use strict";

        $scope.getPermission = function (stateName) {

            var state = $state.get(stateName);

            if (state.data && state.data.permissions) {
                if (state.data.permissions.only.indexOf(cfg.user.role) !== -1) {
                    return true;
                }
                return false;
            }
            return true;
        };

        $scope.home = cfg.nav.home.state;

        $scope.nav = {
            tutorial: {
                sref: "tutorial",
                icon: "fa fa-graduation-cap",
                parent: "tutorial"
            },
            "connection.item": {
                sref: "connection.item.add.source",
                icon: "fa fa-plus",
                special: true
            },
            "connection.list": {
                sref: "connection",
                icon: "fa fa-exchange"
            },
            "account.list": {
                sref: "account",
                icon: "fa fa-globe"
            },
            profile: {
                sref: "settings.profile",
                icon: "fa fa-cog"
            },
            "admin.user": {
                name: "admin.user",
                sref: "admin.user.list",
                icon: "fa fa-user",
                permission: ["admin"]
            },
            billing: {
                name: "admin.billing",
                sref: "admin.billing",
                icon: "fa fa-credit-card-alt",
                permission: ["admin"]
            }
        };

        $scope.$on("vendorBlocked", function (ignore, args) {
            $timeout(function () {
                if (args) {
                    $rootScope.vendorBlocked = true;
                    $scope.nav.billing.label = {
                        text: $filter("translate")("nav.due")
                    };
                } else {
                    delete $scope.nav.billing.label;
                    $rootScope.vendorBlocked = false;
                }
            });
        }, 150);
    });
