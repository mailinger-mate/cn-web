/*global
    angular
*/
/*property
    $$finishAuthorize, $on, check, config, core, data, error, get, href,
    interval, location, logout, module, name, path, permissions,
    preventDefault, redirect, run, service, session, set, status, substr,
    success, then, token, unauthorized, url, user
*/
angular.module("cn.session", [
    "ui.router",
    "cn.config"
])
    .config(function (cfgProvider) {

        "use strict";

        cfgProvider.set({
            session: {
                check: {
                    url: "/health_check/",
                    interval: 60
                },
                error: {
                    unauthorized: {
                        path: "/signin?error=unauthorized&redirect="
                    }
                }
            }
        });
    })
    .service("session", function ($window, $location, $http, cfg) {

        "use strict";

        var service = {};

        service.logout = function (returnUrl) {

            var url = returnUrl || $location.url();
            $window.location.href = cfg.session.error.unauthorized.path + url.substr(1);
        };

        service.check = function (returnUrl) {

            $http.get(cfg.session.check.url + cfg.user.core.token).then(function (response) {
                if (response.data && response.data.success !== true) {
                    service.logout(returnUrl);
                }
            });
        };

        return service;
    })
    .run(function ($rootScope, $interval, $state, session, cfg) {

        "use strict";

        $rootScope.$on('$stateChangeStart', function (ignore, state) {
            var url;
            if (state && !state.redirect && (!state.data || (!state.data.permissions || state.$$finishAuthorize))) {
                url = $state.href(state.name);
                if (url) {
                    session.check(url.substr(1));
                }
            }
        });
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            if (error && error.status === 401) {
                event.preventDefault(toState, toParams, fromState, fromParams);
            }
        });
        $rootScope.$on('session.expired', function (ignore, returnUrl) {
            session.logout(returnUrl);
        });

        $interval(function () {
            session.check();
        }, cfg.session.check.interval * 1000);
    });
