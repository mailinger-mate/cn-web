/*global angular*/
/*property
    $error, $setDirty, $setPristine, $valid, Profile, Email, Password, account,
    address, config, confirm, contact, contacts, controller, current, data,
    email, error, field, forEach, form, help, id, message, model, module, name,
    new, password, pattern, placeholder, prefix, required, saveProfile,
    saveEmail, savePassword, success, translate, type, update, user,
    validateForm, value, verified, verify, verifyEmail
*/
angular.module("cn.settings.profile", [])

    .controller("cnSettingsProfile", function ($scope) {

        "use strict";

        $scope.form = {
            config: {
                prefix: {
                    translate: "settings.profile"
                },
                field: {}
            },
            error: {
                field: {}
            }
        };

        $scope.validateForm = function (form) {

            angular.forEach(form.$error.required, function (field) {
                field.$setDirty();
            });

            return form;
        };
    })

    .controller('cnSettingsProfileAccount', function ($scope, cfg, action) {

        "use strict";

        $scope.form.config.field = {
            name: {
                model: "account.name",
                required: true,
                placeholder: true,
                value: cfg.user.name
            }
        };

        $scope.saveProfile = function () {

            if ($scope.validateForm($scope.Profile).$valid) {

                return action.user.update($scope.form.field.account, {
                    success: function () {
                        cfg.user.name = $scope.form.field.account.name;
                        return true;
                    },
                    error: function (error) {
                        $scope.form.error.field.account = error.data.message;
                    }
                });

            } else {
                return false;
            }
        };
    })

    .controller('cnSettingsProfilePassword', function ($scope, action) {

        "use strict";

        $scope.form.config.field = {
            current: {
                type: "password",
                model: "password.current_password",
                required: true,
                placeholder: true,
                pattern: "password"
            },
            new: {
                type: "password",
                model: "password.password_first",
                required: true,
                placeholder: true,
                pattern: "password",
                help: "password"
            },
            confirm: {
                type: "password",
                model: "password.password_second",
                required: true,
                placeholder: true,
                pattern: "password",
                help: true,
                confirm: "password.password_first"
            }
        };

        $scope.savePassword = function () {

            if ($scope.validateForm($scope.Password).$valid) {

                return action.user.update($scope.form.field.password, {
                    name: "updatePassword",
                    success: function () {
                        $scope.Password.$setPristine();
                        return true;
                    },
                    error: function (error) {
                        $scope.form.error.field.password = error.data.message;
                    }
                });

            } else {
                return false;
            }
        };
    })

    .controller('cnSettingsProfileEmail', function ($scope, cfg, action) {

        "use strict";

        $scope.form.config.verified = cfg.user.email.verified;
        $scope.form.config.verify = !cfg.user.email.verified;

        $scope.form.config.field = {
            email: {
                model: "email.contact",
                type: "email",
                placeholder: true,
                value: cfg.user.email.address
            },
            password: {
                model: "email.current_password",
                type: "password",
                required: true,
                placeholder: true,
                pattern: "password"
            }
        };

        $scope.saveEmail = function () {

            if ($scope.validateForm($scope.Email).$valid) {

                return action.user.update($scope.form.field.email, {
                    name: "updateEmail",
                    success: function () {
                        $scope.Email.$setPristine();
                        $scope.form.config.verify = true;
                        $scope.form.config.verified = false;
                        cfg.user.email.verified = false;
                        cfg.user.email.address = $scope.form.field.email.contact;
                        return true;
                    },
                    error: function (error) {
                        $scope.form.error.field.email = error.data.message;
                    }
                });

            } else {
                return false;
            }
        };

        $scope.verifyEmail = function () {

            if ($scope.Email.email.$valid) {

                return action.contact.email({
                    id: cfg.user.email.id,
                    success: function () {
                        $scope.form.config.verify = false;
                        $scope.Email.email.$setPristine();
                        return true;
                    }
                });

            } else {
                return false;
            }
        };
    });
