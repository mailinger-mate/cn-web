/*global angular*/
angular.module("cn.settings", [
    "cn.settings.profile"
])
    .config(function ($translatePartialLoaderProvider, $stateProvider) {

        "use strict";

        $translatePartialLoaderProvider.addPart("settings/settings");

        $stateProvider
            .state("settings", {
                url: "/settings",
                abstract: true,
                templateUrl: "html/settings/settings.html"
            })
            .state('settings.profile', {
                url: "/profile",
                templateUrl: "html/settings/profile.html",
                controller: "cnSettingsProfile",
                data: {
                    pageTitle: "nav.profile"
                }
            });
    });
