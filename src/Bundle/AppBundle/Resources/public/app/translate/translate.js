/*global angular*/

angular.module("cn.translate", [
    "pascalprecht.translate"
])
    .config(function ($translateProvider) {

        "use strict";

        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '/translate/{part}.{lang}.json'
        });
    });
