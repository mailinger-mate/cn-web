/*global angular*/
/*property
    $scope, account, action, capitalizeString, config, connection, context,
    controller, destination, field, form, go, id, info, keys, module, notify,
    options, source, success, tutorial, type, view
*/

angular.module("cn.tutorial.account.destination", [
    "cn.account"
])
    .controller("cnTutorialAccountDestination", function ($controller, $scope, $state, $stateParams, cfg, helper, action) {

        "use strict";

        if (!$stateParams.source && !$scope.tutorial.account.source) {
            $state.go("tutorial.account.source");
        }

        $controller("cnAccountItem" + (helper.capitalizeString($stateParams.type) || "Default"), {
            $scope: $scope,
            account: false
        });

        $scope.view.action = {
            info: true,
            context: true
        };

        $scope.form.config.field.type.options = Object.keys(cfg.connection.config.destination);

        $scope.form.info = {
            context: "tutorial.info.account.destination"
        };

        $scope.form.action.success = function (account) {

            $scope.tutorial.account.destination = account;
            $state.go("tutorial.connection.source.add", {
                account: $stateParams.source || $scope.tutorial.account.source.id,
                destination: account.id
            });
        };

        if ($stateParams.notify) {
            action.notify($stateParams.notify);
        }

        if ($stateParams.account) {

            $scope.form.action.success({
                id: $stateParams.account
            });
        }
    });
