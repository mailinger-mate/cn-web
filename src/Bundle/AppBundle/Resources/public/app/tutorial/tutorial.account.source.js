/*global angular*/
/*property
    $scope, account, action, capitalizeString, config, connection, context,
    controller, field, form, go, id, info, keys, module, notify, options,
    source, success, tutorial, type, view
*/

angular.module("cn.tutorial.account.source", [
    "cn.account",
    "cn.action"
])
    .controller("cnTutorialAccountSource", function ($controller, $scope, $state, $stateParams, cfg, helper, action) {

        "use strict";

        $controller("cnAccountItem" + (helper.capitalizeString($stateParams.type) || "Default"), {
            $scope: $scope,
            account: false
        });

        $scope.view.action = {
            info: true,
            context: true
        };

        $scope.form.config.field.type.options = Object.keys(cfg.connection.config.source);

        $scope.form.info = {
            context: "tutorial.info.account.source"
        };

        $scope.form.action.success = function (account) {

            $scope.tutorial.account.source = account;
            $state.go("tutorial.account.destination", {
                source: account.id
            });
        };

        if ($stateParams.notify) {
            action.notify($stateParams.notify);
        }

        if ($stateParams.account) {

            $scope.tutorial.account.source = {id: $stateParams.account};
            $state.go("tutorial.account.destination", {
                source: $stateParams.account
            });
        }
    });
