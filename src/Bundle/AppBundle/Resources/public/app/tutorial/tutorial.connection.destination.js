/*global angular*/
/*property
    $scope, account, action, capitalizeString, config, connection, context,
    controller, destination, form, go, info, initAccounts, module, success,
    type, view
*/
angular.module("cn.tutorial.connection.destination", [
    "cn.connection.item.destination"
])
    .controller("cnTutorialConnectionDestination", function ($controller, $scope, $state, $stateParams, helper, cfg, connection, account) {

        'use strict';

        if (!$stateParams.connection || !$stateParams.account || !cfg.connection.config.destination[account.type]) {
            $state.go("tutorial.start");
        }

        $scope.initAccounts([account]);

        $controller("cnDestinationItem" + helper.capitalizeString(account.type), {
            $scope: $scope,
            connection: connection,
            account: account,
            destination: false
        });

        $scope.view.action = {
            info: true,
            context: true,
            account: false
        };

        $scope.form.info = {
            context: "tutorial.info.connection.destination"
        };

        $scope.form.action.success = function () {

            $state.go("tutorial.end");
        };
    });
