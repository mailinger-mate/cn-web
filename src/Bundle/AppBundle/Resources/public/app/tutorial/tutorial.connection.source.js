/*global angular*/
/*property
    $scope, account, action, capitalizeString, config, connection, context,
    controller, destination, form, go, id, info, initAccounts, module, source,
    success, tutorial, type, view
*/

angular.module("cn.tutorial.connection.source", [
    "cn.connection.item.source"
])
    .controller("cnTutorialConnectionSource", function ($controller, $scope, $state, $stateParams, helper, cfg, account) {

        "use strict";

        if (!$stateParams.account || !cfg.connection.config.source[account.type]) {
            $state.go("tutorial.account.source");
        }
        if (!$stateParams.destination && !$scope.tutorial.account.destination) {
            $state.go("tutorial.account.destination");
        }

        $scope.initAccounts([account]);

        $controller("cnSourceItem" + helper.capitalizeString(account.type), {
            $scope: $scope,
            connection: false,
            account: account
        });

        $scope.view.action = {
            info: true,
            context: true,
            account: false
        };

        $scope.form.info = {
            context: "tutorial.info.connection.source"
        };

        $scope.form.action.success = function (connection) {

            $state.go("tutorial.connection.destination.add", {
                connection: connection.id,
                account: $stateParams.destination || $scope.tutorial.account.destination.id
            });
        };
    });
