/*global angular*/

angular.module("cn.tutorial", [
    "oc.lazyLoad",
    "cn.core",
    "cn.action",
    "cn.account",
    "cn.connection.item",
    "cn.tutorial.account.source",
    "cn.tutorial.account.destination",
    "cn.tutorial.connection.source",
    "cn.tutorial.connection.destination"
])
    .config(function ($translatePartialLoaderProvider, $stateProvider) {

        "use strict";

        $translatePartialLoaderProvider.addPart('tutorial/tutorial');

        $stateProvider
            .state("tutorial", {
                url: "/tutorial",
                controller: "cnTutorial",
                templateUrl: "html/tutorial/tutorial.html",
                redirect: "tutorial.start"
            })
            .state("tutorial.start", {
                url: "/start",
                controller: "cnTutorialStart",
                templateUrl: "html/tutorial/start.html",
                data: {
                    pageTitle: "tutorial.nav.start"
                }
            })
            .state("tutorial.account", {
                url: "/account",
                controller: "cnAccountItem",
                template: '<div ui-view autoscroll="false"></div>',
                abstract: true
            })
            .state("tutorial.account.source", {
                url: "/source/:type?notify&account",
                controller: "cnTutorialAccountSource",
                templateUrl: "html/account/item.form.html",
                data: {
                    pageTitle: "tutorial.nav.account.source"
                }
            })
            .state("tutorial.account.destination", {
                url: "/destination/:type?source&notify&account",
                controller: "cnTutorialAccountDestination",
                templateUrl: "html/account/item.form.html",
                data: {
                    pageTitle: "tutorial.nav.account.destination"
                }
            })
            .state("tutorial.connection", {
                url: "/connection",
                resolve: {
                    accounts: function () {
                        return false;
                    }
                },
                controller: "cnConnectionItem",
                template: '<div ui-view autoscroll="false"></div>',
                abstract: true
            })
            .state("tutorial.connection.source", {
                url: "/source",
                controller: "cnSourceItem",
                templateUrl: "html/connection/item.source.html",
                abstract: true,
                data: {
                    pageTitle: "tutorial.nav.connection.source.add"
                }
            })
            .state("tutorial.connection.source.add", {
                url: "/:account?destination",
                views: {
                    form: {
                        resolve: {
                            connection: function () {
                                return false;
                            },
                            account: function ($stateParams, action) {
                                return action.account.get({id: $stateParams.account});
                            }
                        },
                        controller: "cnTutorialConnectionSource",
                        templateUrl: "html/connection/item.source.form.html"
                    }
                }
            })
            .state("tutorial.connection.destination", {
                url: "/:connection/destination",
                controller: "cnDestinationItem",
                templateUrl: "html/connection/item.destination.html",
                abstract: true,
                data: {
                    pageTitle: "tutorial.nav.connection.destination.add"
                }
            })
            .state("tutorial.connection.destination.add", {
                url: "/:account",
                views: {
                    form: {
                        resolve: {
                            connection: function ($stateParams, action) {
                                return action.connection.get({id: $stateParams.connection});
                            },
                            account: function ($stateParams, action) {
                                return action.account.get({id: $stateParams.account});
                            }
                        },
                        controller: "cnTutorialConnectionDestination",
                        templateUrl: "html/connection/item.destination.form.html"
                    }
                }
            })
            .state("tutorial.end", {
                url: "/end",
                templateUrl: "html/tutorial/end.html",
                controller: "cnTutorialEnd",
                data: {
                    pageTitle: "tutorial.nav.end"
                }
            });
    })
    .controller("cnTutorial", function ($scope, $state) {

        "use strict";

        $scope.tutorial = {
            account: {},
            current: function () {
                return $scope.tutorial.nav.indexOf($state.current.name.replace("tutorial.", ""));
            },
            nav: [
                "start",
                "account.source",
                "account.destination",
                "connection.source.add",
                "connection.destination.add",
                "end"
            ]
        };
    })
    .controller("cnTutorialStart", function ($scope) {

        "use strict";

        $scope.button = [{
            label: "continue",
            type: "link",
            sref: "tutorial.account.source",
            class: "btn-primary"
        }];
    })
    .controller("cnTutorialEnd", function ($scope, $state, notification) {

        "use strict";

        $scope.button = [{
            label: "finish",
            click: "end()",
            class: "btn-primary"
        }];

        $scope.end = function () {

            notification.push({
                prefix: "tutorial",
                name: "finish",
                level: "success"
            });

            $state.go("connection.list");
        };
    });
