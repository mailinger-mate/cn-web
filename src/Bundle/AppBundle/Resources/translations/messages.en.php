<?php

return array(
    'message' => array(
        'error' => array(
            'session_expired'   => 'Your session has expired. Please sign in again.',
            'access_denied'     => 'Access has been denied. Please contact our support team.'
        )
    )
);
