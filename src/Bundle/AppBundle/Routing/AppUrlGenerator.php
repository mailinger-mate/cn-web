<?php

namespace Connect\Bundle\AppBundle\Routing;

use InvalidArgumentException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AppUrlGenerator
 */
class AppUrlGenerator
{
    /**
     * @var string
     */
    private $baseAppUrl;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->baseAppUrl = $router->generate('app');
    }

    /**
     * @param $appRoute
     * @param array $parameters
     * @return string
     */
    public function generate($appRoute, array $parameters = array())
    {
        $appUrl = $this->baseAppUrl . '#/' . $appRoute;

        if (count($parameters) > 0) {
            $appUrl .= (strpos($appUrl, '?') === false) ? '?' : '&';
            $appUrl .= implode('&', array_map(
                $this->getCallbackFunction(),
                array_keys($parameters),
                $parameters
            ));
        }

        return $appUrl;
    }

    /**
     * @return callable
     */
    private function getCallbackFunction()
    {
        return function ($key, $value) {
            if (!is_string($key) || strlen($key) === 0) {
                throw new InvalidArgumentException('Key in parameters array must by a non-empty string.');
            }

            if (!is_numeric($value) && (!is_string($value) || strlen($value) === 0) && $value !== null) {
                throw new InvalidArgumentException('Value must be a numeric or non-empty string or null: ' . gettype($value));
            }

            return $key . ($value === null ? '' : '=' . $value);
        };
    }
}