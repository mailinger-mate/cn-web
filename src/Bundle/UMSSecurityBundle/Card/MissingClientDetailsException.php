<?php

namespace Connect\Bundle\UMSSecurityBundle\Card;

use LogicException;

/**
 * Class MissingClientDetailsException
 */
class MissingClientDetailsException extends LogicException {}