<?php

namespace Connect\Bundle\UMSSecurityBundle\Client;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use Connect\Domain\ContactAction\Type;
use Connect\Domain\User;
use Connect\Domain\UserAction;
use Connect\Domain\Vendor;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use LogicException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class Facade
 */
class Facade
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var UMSUserRepository
     */
    private $userRepository;
    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * Facade constructor.
     * @param ClientInterface $client
     * @param UMSUserRepository $userRepository
     * @param JsonEncoder $encoder
     */
    public function __construct(ClientInterface $client, UMSUserRepository $userRepository, JsonEncoder $encoder)
    {
        $this->client = $client;
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    /**
     * @param Contact $contact
     * @return mixed
     */
    public function contactVerify(Contact $contact)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    sprintf('verify/%d', $contact->getId()),
                    ['query' => ['user' => $contact->getUser()->getId()]]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_NOT_FOUND) {
                throw new NotFoundHttpException();
            }
            throw $exception;
        }
        $responseData = $this->decodeResponse($response);
        $contact->setValue($responseData['contact']);

        return $responseData['action_id'];
    }

    /**
     * @param Contact $contact
     * @return bool
     */
    public function contactConfirm(Contact $contact)
    {
        $action = $contact->getAction(Type::CONTACT_VERIFY);
        if (!$action instanceof ContactAction) {
            throw new LogicException('Contact does not have action with "contact_verify" type.');
        }

        try {
            $this->client
                ->request(
                    Request::METHOD_GET,
                    sprintf('confirm/%s', $action->getId())
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_NOT_FOUND) {
                throw new NotFoundHttpException();
            }
            throw $exception;
        }

        return true;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function registerUser(array $data)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    'register',
                    [
                        'body' => $this->formatUserRegisterData($data),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode($exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT);
            }
            throw $exception;
        }

        $user = $this->userRepository->find($this->decodeResponse($response)['user_id']);

        return $user;
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function remindUserPassword($email)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    'remind',
                    [
                        'body' => sprintf('{"email":"%s"}', $email),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode($exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT);
            }
            throw $exception;
        }

        $user = $this->userRepository->find($this->decodeResponse($response)['user_id']);

        return $user;
    }

    /**
     * @param UserAction $action
     * @param array $data
     * @return mixed
     */
    public function resetUserPassword(UserAction $action, array $data)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    sprintf('resetpass/%s', $action->getId()),
                    [
                        'body' => $this->formatPasswordData($data),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode($exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT);
            }
            throw $exception;
        }

        $user = $this->userRepository->find($this->decodeResponse($response)['user_id']);

        return $user;
    }

    /**
     * @param UserAction $action
     * @param array $data
     * @return mixed
     */
    public function activateUserAccount(UserAction $action, array $data)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    sprintf('activate/%s', $action->getId()),
                    [
                        'body' => $this->formatPasswordData($data),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode($exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT);
            }
            throw $exception;
        }

        $user = $this->userRepository->find($this->decodeResponse($response)['user_id']);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return mixed
     */
    public function addUser(User $user, array $data)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    'add',
                    [
                        'query' => ['session_user' => $user->getId()],
                        'body'  => $this->formatUserData($data),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode($exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT);
            }
            throw $exception;
        }

        $user = $this->userRepository->find($this->decodeResponse($response)['user_id']);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return mixed
     */
    public function editUser(User $user, array $data)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    sprintf('edit/%d', $data['id']),
                    [
                        'query' => ['session_user' => $user->getId()],
                        'body'  => $this->formatUserData($data),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode(
                    $exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT
                );
            }
            throw $exception;
        }

        if ($response->getStatusCode() === Response::HTTP_OK) {
            return $this->decodeResponse($response)['action_id'];
        } else {
            return true;
        }
    }

    /**
     * @param User $user
     * @param $umsSessionId
     * @param array $data
     * @return mixed
     */
    public function editUserProfile(User $user, $umsSessionId, array $data)
    {
        try {
            $response = $this->client
                ->request(
                    Request::METHOD_GET,
                    'profile/edit',
                    [
                        'query' => [
                            'session_id'   => $umsSessionId,
                            'session_user' => $user->getId()
                        ],
                        'body'  => $this->formatUserProfileData($data),
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode(
                    $exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT
                );
            }
            throw $exception;
        }

        if ($response->getStatusCode() === Response::HTTP_OK) {
            return $this->decodeResponse($response)['action_id'];
        } else {
            return true;
        }
    }

    /**
     * @param Vendor $vendor
     * @param array $data
     * @return bool
     */
    public function editVendor(Vendor $vendor, array $data)
    {
        try {
            $this->client
                ->request(
                    Request::METHOD_GET,
                    sprintf('vendor/edit/%d', $vendor->getId()),
                    [
                        'body'  => $this->encoder->encode($data, JsonEncoder::FORMAT)
                    ]
                );
        } catch (ClientException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_BAD_REQUEST) {
                return $this->encoder->decode(
                    $exception->getResponse()->getBody()->getContents(), JsonEncoder::FORMAT
                );
            }
            throw $exception;
        }

        return true;
    }

    /**
     * @param array $data
     * @return string
     */
    private function formatUserRegisterData(array $data)
    {
        $resultData = [
            'name'     => $data['name'],
            'vendor'   => ['name' => $data['vendor']],
            'contacts' => [['value' => $data['contact']]],
            'password' => ['first' => $data['password_first'], 'second' => $data['password_second']],
        ];

        return $this->encoder->encode($resultData, JsonEncoder::FORMAT);
    }

    /**
     * @param array $data
     * @return string
     */
    private function formatUserData(array $data)
    {
        $resultData = [
            'name'     => $data['name'],
            'contacts' => [['value' => $data['contact']]],
            'role'     => $data['role'],
        ];

        return $this->encoder->encode($resultData, JsonEncoder::FORMAT);
    }

    /**
     * @param array $data
     * @return string
     */
    private function formatPasswordData(array $data)
    {
        $resultData = ['password' => ['first' => $data['password_first'], 'second' => $data['password_second']]];

        return $this->encoder->encode($resultData, JsonEncoder::FORMAT);
    }

    /**
     * @param array $data
     * @return string
     */
    private function formatUserProfileData(array $data)
    {
        $resultData = [];
        if (array_key_exists('name', $data) && strlen($data['name']) > 0) {
            $resultData = array_merge($resultData, [
                'name' => $data['name'],
            ]);
        }
        if (array_key_exists('contact', $data) && strlen($data['contact']) > 0) {
            $resultData = array_merge($resultData, [
                'contact'          => $data['contact'],
                'current_password' => $data['current_password'],
            ]);
        }
        if (array_key_exists('password_first', $data) && strlen($data['password_first']) > 0) {
            $resultData = array_merge($resultData, [
                'password'         => [
                    'first'  => $data['password_first'],
                    'second' => $data['password_second'],
                ],
                'current_password' => $data['current_password'],
            ]);
        }

        return $this->encoder->encode($resultData, JsonEncoder::FORMAT);
    }

    private function decodeResponse(ResponseInterface $response)
    {
        return $this->encoder->decode($response->getBody(), JsonEncoder::FORMAT);
    }
}