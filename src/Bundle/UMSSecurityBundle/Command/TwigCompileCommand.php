<?php

namespace Connect\Bundle\UMSSecurityBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class TwigCompileCommand
 */
class TwigCompileCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('twig:compile')
            ->setDescription('Compile twig templates')
            ->addArgument(
                'resource',
                InputArgument::REQUIRED,
                'Resource directory with templates to compile in format <bundle>:<resource_direcotry> e.g. `AcmeBundle:Emails`'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resourceParts = $this->parseArgs($input);
        if (!$resourceParts) {
            throw new \InvalidArgumentException('Invalid resource');
        }
        list($bundle, $resourceDir) = $resourceParts;
        $templating = $this->getContainer()->get('templating');
        $filesystem = $this->getContainer()->get('filesystem');
        $finder = $this->getContainer()->get('finder');

        $bundlePath = $this->getContainer()->get('kernel')->getBundle($bundle)->getPath();
        $templatesRoot = $this->joinPath([$bundlePath, '/Resources/views/', $resourceDir]);


        $finder->name('*.twig')->depth('== 0');
        $files = $finder->files()->in($templatesRoot);
        if (count($files) === 0) {
            $output->writeln('No templates found');
            return;
        }
        else {
            $output->writeln(sprintf("Found %d templates\n", count($files)));
        }
        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $output->write(sprintf("%'.-80s", $file->getFilename()));
            $filesystem->dumpFile(
                $this->joinPath([
                    $this->getContainer()->getParameter('kernel.root_dir'),
                    '..',
                    'build',
                    $file->getBasename('.' . $file->getExtension()),

                ]),
                $templating->render(
                    join(':', [$bundle, $resourceDir, $file->getFilename()])
                )
            );
            $output->writeln(sprintf("\r%'.-78sOK", $file->getFilename()));
        }
    }

    /**
     * @param InputInterface $input
     * @return array
     */
    private function parseArgs(InputInterface $input)
    {
        $resourceParts = explode(':', $input->getArgument('resource'));

        return count($resourceParts) !== 2 ? false : $resourceParts;
    }

    /**
     * @param array $parts
     * @return string
     */
    private function joinPath(array $parts)
    {
        array_walk(
            $parts,
            function (&$value, $index) {
                $value = $index === 0 ? rtrim($value, DIRECTORY_SEPARATOR) : trim($value, DIRECTORY_SEPARATOR);
            }
        );

        return join(DIRECTORY_SEPARATOR, $parts);
    }
}
