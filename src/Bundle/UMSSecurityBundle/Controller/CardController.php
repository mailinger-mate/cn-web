<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Connect\Bundle\UMSSecurityBundle\Card\MissingClientDetailsException;
use Connect\Bundle\UMSSecurityBundle\Form\MessageBuilder;
use Connect\Bundle\UMSSecurityBundle\Form\Type\TokenCardType;
use Connect\Bundle\UMSSecurityBundle\Repository\StripeCustomerRepository;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSStripeTokenRepository;
use Connect\Bundle\UMSSecurityBundle\Sender\ClientInterface;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\FactoryProvider;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Bundle\UMSSecurityBundle\Sender\Type;
use Connect\Domain\Card\TokenCard;
use Connect\Domain\Vendor;
use Exception;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializerInterface;
use Stripe\Error\InvalidRequest;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CardController
 */
class CardController extends JsonController
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;
    /**
     * @var StripeCustomerRepository
     */
    private $customerRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var MessageBuilder
     */
    private $messageBuilder;
    /**
     * @var UMSStripeTokenRepository
     */
    private $umsTokenRepository;
    /**
     * @var FactoryProvider
     */
    private $senderMessageFactoryProvider;
    /**
     * @var ClientInterface
     */
    private $senderClient;

    /**
     * CardController constructor.
     * @param TranslatorInterface $translator
     * @param TokenStorage $tokenStorage
     * @param StripeCustomerRepository $customerRepository
     * @param SerializerInterface $serializer
     * @param FormFactoryInterface $formFactory
     * @param MessageBuilder $messageBuilder
     * @param UMSStripeTokenRepository $umsTokenRepository
     * @param FactoryProvider $senderMessageFactoryProvider
     * @param ClientInterface $senderClient
     */
    public function __construct(
        TranslatorInterface $translator,
        TokenStorage $tokenStorage,
        StripeCustomerRepository $customerRepository,
        SerializerInterface $serializer,
        FormFactoryInterface $formFactory,
        MessageBuilder $messageBuilder,
        UMSStripeTokenRepository $umsTokenRepository,
        FactoryProvider $senderMessageFactoryProvider,
        ClientInterface $senderClient
    ) {
        parent::__construct($translator);
        $this->tokenStorage = $tokenStorage;
        $this->customerRepository = $customerRepository;
        $this->serializer = $serializer;
        $this->formFactory = $formFactory;
        $this->messageBuilder = $messageBuilder;
        $this->umsTokenRepository = $umsTokenRepository;
        $this->senderMessageFactoryProvider = $senderMessageFactoryProvider;
        $this->senderClient = $senderClient;
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function ajaxGetAction()
    {
        /** @var Vendor $vendor */
        $vendor = $this->tokenStorage->getToken()->getUser()->getVendor();

        try {
            $this->customerRepository->refresh($vendor->getCustomer());
            $card = $vendor->getCustomer()->getCard();

            if (is_null($card)) {
                return $this->jsonError('message.error.not_found', Response::HTTP_NOT_FOUND);
            }
        } catch (Exception $exception) {
            return $this->jsonUnknownError(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $cardData = $this->serializer->serialize(
            $card, JsonEncoder::FORMAT
        );

        return $this->jsonResponse(true, ['card' => json_decode($cardData, true)]);
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxAddAction(Request $request)
    {
        $tokenCard = new TokenCard();
        $form = $this->formFactory->create(new TokenCardType(), $tokenCard);
        $form->submit(
            (new JsonEncoder())->decode($request->getContent(), JsonEncoder::FORMAT)
        );
        if ($form->isValid()) {
            try {
                /** @var Vendor $vendor */
                $vendor = $this->tokenStorage->getToken()->getUser()->getVendor();

                if (empty($vendor->getBillingName())) {
                    throw new MissingClientDetailsException("Missing vendor billing details");
                }

                $customer = $vendor->getCustomer();
                $this->customerRepository->refresh($customer);
                if (!is_null($customer->getId())) {
                    $senderMessages = [];
                    $this->customerRepository->remove($customer);
                    $this->umsTokenRepository->remove($customer);
                    $customer->setId(null);
                    $customer->removeCard();
                }
                else {
                    $senderMessages = [
                        $this->senderMessageFactoryProvider
                            ->getForType(Type::HIPCHAT)
                            ->createForUser(
                                $this->tokenStorage->getToken()->getUser(),
                                TriggerType::CARD_REGISTRATION
                            )
                    ];
                }

                $customer->setCard($tokenCard);
                $this->customerRepository->add($customer);
                $this->umsTokenRepository->add($customer);
                $this->senderClient->send($senderMessages);

                return $this->jsonResponse();
            } catch (MissingClientDetailsException $exception) {
                $form->get('card_token')->addError(new FormError('Cannot add card details for vendor without client details.'));
            } catch (InvalidRequest $exception) {
                if ($exception->getHttpStatus() === Response::HTTP_BAD_REQUEST) {
                    $form->get('card_token')->addError(new FormError('This is not a valid value.'));
                } else {
                    return $this->jsonUnknownError(Response::HTTP_INTERNAL_SERVER_ERROR);
                }

            } catch (Exception $exception) {
                return $this->jsonUnknownError(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return $this->jsonResponse(
            false,
            $this->messageBuilder->build($form),
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @return JsonResponse
     */
    public function ajaxDeleteAction()
    {
        /** @var Vendor $vendor */
        $vendor = $this->tokenStorage->getToken()->getUser()->getVendor();
        $customer = $vendor->getCustomer();
        try {
            $this->customerRepository->refresh($customer);
            if (!is_null($customer->getId())) {
                $this->customerRepository->remove($customer);
                $this->umsTokenRepository->remove($customer);
                $customer->setId(null);
                $customer->removeCard();
            }

            return $this->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonUnknownError(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}