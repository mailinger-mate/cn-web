<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Connect\Bundle\UMSSecurityBundle\Client\Facade;
use Connect\Bundle\UMSSecurityBundle\Mail\Message\Factory;
use Connect\Domain\Contact;
use GuzzleHttp\Exception\ClientException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Swift_Mailer;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ContactController
 */
class ContactController extends JsonController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var TwigEngine
     */
    private $templating;
    /**
     * @var Facade
     */
    private $UMSClient;
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Factory
     */
    private $messageFactory;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param TwigEngine $templating
     * @param TranslatorInterface $translator
     * @param Facade $UMSClient
     * @param Swift_Mailer $mailer
     * @param Factory $messageFactory
     */
    function __construct(
        TokenStorageInterface $tokenStorage,
        TwigEngine $templating,
        TranslatorInterface $translator,
        Facade $UMSClient,
        Swift_Mailer $mailer,
        Factory $messageFactory
    ) {
        parent::__construct($translator);

        $this->tokenStorage = $tokenStorage;
        $this->templating = $templating;
        $this->UMSClient = $UMSClient;
        $this->mailer = $mailer;
        $this->messageFactory = $messageFactory;
    }

    /**
     * @param Contact $contact
     * @return Response
     */
    public function confirmAction(Contact $contact)
    {
        try {
            $this->UMSClient->contactConfirm($contact);

            return $this->templating->renderResponse(
                'ConnectUMSSecurityBundle:Confirm/Contact:confirm.html.twig',
                ['email' => $contact->getValue()]
            );
        } catch (ClientException $exception) {
            return $this->jsonUnknownError();
        }
    }

    /**
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     *
     * @param Contact $contact
     * @return JsonResponse
     */
    public function ajaxVerifyAction($contact)
    {
        try {
            $user = $this->tokenStorage->getToken()->getUser();
            $contact = (new Contact())
                ->setId($contact)
                ->setUser($user);

            $action = $this->UMSClient->contactVerify($contact);

            $this->mailer->send(
                $this->messageFactory
                    ->getContactConfirmMessage($action, $contact)
            );

            return $this->jsonResponse();
        } catch (ClientException $exception) {
            return $this->jsonUnknownError();
        }
    }
}
