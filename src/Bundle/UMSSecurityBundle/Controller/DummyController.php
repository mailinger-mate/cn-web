<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DummyController
 */
class DummyController
{
    /**
     * @throws NotFoundHttpException
     */
    public function dummyAction()
    {
        throw new NotFoundHttpException();
    }
}
