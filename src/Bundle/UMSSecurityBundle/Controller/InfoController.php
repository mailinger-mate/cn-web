<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InfoController
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @param EngineInterface $templating
     */
    function __construct(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function confirmAction(Request $request)
    {
        switch ($request->query->get('action')) {
            case 'remind_password':
                return $this->templating
                    ->renderResponse('ConnectUMSSecurityBundle:Confirm/Password:confirm.html.twig');
        }

        throw new NotFoundHttpException();
    }
}
