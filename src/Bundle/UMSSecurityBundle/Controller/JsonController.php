<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class JsonController
 */
abstract class JsonController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param bool $success
     * @param array $data
     * @param int $responseStatus
     * @return JsonResponse
     */
    protected function jsonResponse(
        $success = true,
        array $data = [],
        $responseStatus = Response::HTTP_OK
    ) {
        return new JsonResponse(
            array_merge(
                [$this->translator->trans('label.success') => $success],
                $data
            ),
            $responseStatus
        );
    }

    /**
     * @param int $responseStatus
     * @return JsonResponse
     */
    protected function jsonUnknownError($responseStatus = Response::HTTP_OK)
    {
        return $this->jsonResponse(
            false,
            [
                $this->translator->trans('label.message') => $this->translator->trans('message.error.unknown_error'),
            ],
            $responseStatus
        );
    }

    /**
     * @param string $message
     * @param int $responseStatus
     * @return JsonResponse
     */
    protected function jsonError($message, $responseStatus = Response::HTTP_BAD_REQUEST)
    {
        return $this->jsonResponse(
            false,
            [
                $this->translator->trans('label.message') => $this->translator->trans($message),
            ],
            $responseStatus
        );
    }
}
