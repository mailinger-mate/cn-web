<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SecurityController
 */
class SecurityController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'ConnectUMSSecurityBundle:Security:login.html.twig',
            array(
                'last_username' => $lastUsername,
                'error'         => $error,
                'redirect_url'  => ($request->get('redirect')) ?
                    $this->container->get('app.routing.app_url_generator')->generate($request->get('redirect')) :
                    $this->container->get('router')->generate('app')
            )
        );
    }
}
