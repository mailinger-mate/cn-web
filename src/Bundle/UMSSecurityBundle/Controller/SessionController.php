<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SessionController
 */
class SessionController extends JsonController
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var UMSSessionRepository
     */
    private $sessionRepository;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorageInterface $tokenStorage
     * @param TranslatorInterface $translator
     * @param UMSSessionRepository $sessionRepository
     */
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        UMSSessionRepository $sessionRepository
    ) {
        parent::__construct($translator);

        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * Returns false when:
     * - user is not authenticated
     * - ums_session in missing in session
     * - provided ums_session do not match ums_session in session
     * - ums_session do not exist in ums
     *
     * @param Request $request
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function ajaxHealthCheckAction(Request $request)
    {
        if (!$this->isUserAuthenticated()) {
            return $this->jsonResponse(false);
        }

        $session = $request->getSession();
        $requestUmsSessionId = $request->attributes->get('ums_session');
        if (!$session->has('ums_session') || $session->get('ums_session') !== $requestUmsSessionId) {
            return $this->jsonResponse(false);
        }

        try {
            $this->sessionRepository->find($requestUmsSessionId);
        } catch (RequestException $exception) {
            if ($exception->getResponse()->getStatusCode() === Response::HTTP_NOT_FOUND) {
                return $this->jsonResponse(false);
            }

            return $this->jsonUnknownError();
        }

        return $this->jsonResponse();
    }

    /**
     * @return bool
     */
    private function isUserAuthenticated()
    {
        return $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY');
    }
}
