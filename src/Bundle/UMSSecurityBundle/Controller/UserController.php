<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Connect\Bundle\UMSSecurityBundle\Client\Facade;
use Connect\Bundle\UMSSecurityBundle\Form\MessageBuilder;
use Connect\Bundle\UMSSecurityBundle\Form\Type\ForgotPasswordType;
use Connect\Bundle\UMSSecurityBundle\Form\Type\UserAdminType;
use Connect\Bundle\UMSSecurityBundle\Form\Type\UserSetPasswordType;
use Connect\Bundle\UMSSecurityBundle\Form\Type\UserType;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\AjaxErrorHandler;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\Exception\InvalidFormatException;
use Connect\Bundle\UMSSecurityBundle\Mail\Message\Factory;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Bundle\UMSSecurityBundle\Sender\ClientInterface;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\FactoryProvider;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Bundle\UMSSecurityBundle\Sender\Type;
use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use Connect\Domain\ContactAction\Type as ContactActionType;
use Connect\Domain\Session;
use Connect\Domain\User;
use Connect\Domain\UserAction\Type as UserActionType;
use Exception;
use GuzzleHttp\Exception\ClientException;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializerInterface;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserController
 */
class UserController extends JsonController
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var UMSUserRepository
     */
    private $userRepository;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Factory
     */
    private $mailMessageFactory;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var MessageBuilder
     */
    private $messageBuilder;
    /**
     * @var Facade
     */
    private $UMSClient;
    /**
     * @var AjaxErrorHandler
     */
    private $registerFormErrorHandler;
    /**
     * @var UMSSessionRepository
     */
    private $sessionRepository;
    /**
     * @var AjaxErrorHandler
     */
    private $remindPassFormErrorHandler;
    /**
     * @var AjaxErrorHandler
     */
    private $userPassFormErrorHandler;
    /**
     * @var AjaxErrorHandler
     */
    private $adminUserFormErrorHandler;
    /**
     * @var FactoryProvider
     */
    private $senderMessageFactoryProvider;
    /**
     * @var ClientInterface
     */
    private $senderClient;

    /**
     * @param EngineInterface $templating
     * @param Router $router
     * @param FormFactoryInterface $formFactory
     * @param UMSUserRepository $userRepository
     * @param TranslatorInterface $translator
     * @param TokenStorageInterface $tokenStorage
     * @param Swift_Mailer $mailer
     * @param Factory $mailMessageFactory
     * @param SerializerInterface $serializer
     * @param MessageBuilder $messageBuilder
     * @param Facade $UMSClient
     * @param AjaxErrorHandler $registerFormErrorHandler
     * @param UMSSessionRepository $sessionRepository
     * @param AjaxErrorHandler $remindPassFormErrorHandler
     * @param AjaxErrorHandler $userPassFormErrorHandler
     * @param AjaxErrorHandler $adminUserFormErrorHandler
     * @param FactoryProvider $senderMessageFactoryProvider
     * @param ClientInterface $senderClient
     */
    function __construct(
        EngineInterface $templating,
        Router $router,
        FormFactoryInterface $formFactory,
        UMSUserRepository $userRepository,
        TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage,
        Swift_Mailer $mailer,
        Factory $mailMessageFactory,
        SerializerInterface $serializer,
        MessageBuilder $messageBuilder,
        Facade $UMSClient,
        AjaxErrorHandler $registerFormErrorHandler,
        UMSSessionRepository $sessionRepository,
        AjaxErrorHandler $remindPassFormErrorHandler,
        AjaxErrorHandler $userPassFormErrorHandler,
        AjaxErrorHandler $adminUserFormErrorHandler,
        FactoryProvider $senderMessageFactoryProvider,
        ClientInterface $senderClient
    ) {
        parent::__construct($translator);

        $this->templating = $templating;
        $this->router = $router;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
        $this->translator = $translator;
        $this->tokenStorage = $tokenStorage;
        $this->mailer = $mailer;
        $this->mailMessageFactory = $mailMessageFactory;
        $this->serializer = $serializer;
        $this->messageBuilder = $messageBuilder;
        $this->UMSClient = $UMSClient;
        $this->registerFormErrorHandler = $registerFormErrorHandler;
        $this->sessionRepository = $sessionRepository;
        $this->remindPassFormErrorHandler = $remindPassFormErrorHandler;
        $this->userPassFormErrorHandler = $userPassFormErrorHandler;
        $this->adminUserFormErrorHandler = $adminUserFormErrorHandler;
        $this->senderMessageFactoryProvider = $senderMessageFactoryProvider;
        $this->senderClient = $senderClient;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        $form = $this->formFactory->create(new UserType());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            try {
                $result = $this->UMSClient->registerUser($form->getData());

                if ($result instanceof UMSUser) {
                    $session = (new Session())->setUser($result);
                    $this->sessionRepository->add($session);
                    $request->getSession()->set('ums_session', $session->getId());

                    $contact = $result->getPrimaryContact();
                    $this->mailer->send(
                        $this->mailMessageFactory
                            ->getVendorRegistrationMessage(
                                $contact->getAction(ContactActionType::CONTACT_VERIFY)->getId(),
                                $contact
                            )
                    );

                    $this->notifyAboutRegistration(
                        $result,
                        $form->get('newsletter_agreement')->getData() === true
                    );

                    $this->tokenStorage->setToken(
                        new UsernamePasswordToken($result, null, 'connect', $result->getRoles())
                    );

                    return new RedirectResponse(
                        $this->router->generate('app')
                    );
                } else {
                    $this->registerFormErrorHandler->handle($result, $form);
                }
            } catch (Exception $exception) {
                $generalError = $this->translator->trans('message.error.unknown_error');
            }
        } elseif ($request->get('email')) {
            $form->get('contact')->setData($request->get('email'));
        }

        return $this->templating->renderResponse(
            'ConnectUMSSecurityBundle:User:add.html.twig',
            [
                'form'  => $form->createView(),
                'error' => empty($generalError) ? '' : $generalError,
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function remindPasswordAction(Request $request)
    {
        $form = $this->formFactory->create(new ForgotPasswordType());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $result = $this->UMSClient->remindUserPassword($form->getData()['email']);

            if ($result instanceof User) {
                $this->mailer->send(
                    $this->mailMessageFactory->getResetPasswordMessage($result)
                );

                return new RedirectResponse(
                    $this->router->generate('confirm', ['action' => 'remind_password'])
                );
            } else {
                $this->remindPassFormErrorHandler->handle($result, $form);
            }
        }

        return $this->templating->renderResponse(
            'ConnectUMSSecurityBundle:User:forgotPassword.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param UMSUser $user
     * @return RedirectResponse|Response
     */
    public function resetPasswordAction(Request $request, UMSUser $user)
    {
        $form = $this->formFactory->create(
            new UserSetPasswordType(),
            ['contact' => $user->getPrimaryContact()->getValue()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $result = $this->UMSClient->resetUserPassword(
                $user->getAction(UserActionType::PASSWORD_RESET),
                $form->getData()
            );

            if ($result instanceof User) {
                $this->tokenStorage->setToken(
                    new UsernamePasswordToken($user, null, 'connect', $user->getRoles())
                );

                return new RedirectResponse(
                    $this->router->generate('app')
                );
            } else {
                $this->userPassFormErrorHandler->handle($result, $form);
            }
        }

        return $this->templating->renderResponse(
            'ConnectUMSSecurityBundle:User:resetPassword.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param UMSUser $user
     * @return RedirectResponse|Response
     */
    public function accountActivateAction(Request $request, UMSUser $user)
    {
        $form = $this->formFactory->create(
            new UserSetPasswordType(),
            ['contact' => $user->getPrimaryContact()->getValue()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $result = $this->UMSClient->activateUserAccount(
                $user->getAction(UserActionType::ACCOUNT_ACTIVATE),
                $form->getData()
            );

            if ($result instanceof User) {
                $this->tokenStorage->setToken(
                    new UsernamePasswordToken($user, null, 'connect', $user->getRoles())
                );

                return new RedirectResponse(
                    $this->router->generate('app')
                );
            } else {
                $this->userPassFormErrorHandler->handle($result, $form);
            }
        }

        return $this->templating->renderResponse(
            'ConnectUMSSecurityBundle:User:activateAccount.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function ajaxGetUsers()
    {
        /** @var UMSUser $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $usersList = $this->userRepository->findByVendor($user->getVendor());

        $usersList = $this->serializer->serialize(
            $usersList, JsonEncoder::FORMAT
        );

        return $this->jsonResponse(true, ['users' => json_decode($usersList, true)]);
    }

    /**
     * @PreAuthorize("hasRole('ROLE_ADMIN') AND belongsToVendor(#user)")
     *
     * @param UMSUser $user
     * @return JsonResponse
     * @throws Exception
     */
    public function ajaxDeleteAction(UMSUser $user)
    {
        if ($this->tokenStorage->getToken()->getUser()->getId() === $user->getId()) {
            return $this->jsonResponse(
                false,
                [
                    $this->translator->trans('label.message') => $this->translator->trans('message.error.delete_yourself'),
                ]
            );
        }

        $this->userRepository->remove($user);

        return $this->jsonResponse();
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @param int $user
     * @return JsonResponse
     */
    public function ajaxEditAction(Request $request, $user)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();

        $form = $this->formFactory->create(new UserAdminType());
        $form->submit(
            (new JsonEncoder())->decode($request->getContent(), JsonEncoder::FORMAT)
        );

        if ($form->isSubmitted()) {
            try {
                $result = $this->UMSClient->editUser(
                    $currentUser,
                    array_merge($form->getData(), ['id' => $user])
                );

                if (is_string($result)) {
                    $userModel = $this->userRepository->find($user);

                    $this->mailer->send(
                        $this->mailMessageFactory
                            ->getContactConfirmMessage($result, $userModel->getPrimaryContact())
                    );
                    return $this->jsonResponse();
                } elseif (is_array($result)) {
                    $this->adminUserFormErrorHandler->handle($result, $form);
                } else {
                    return $this->jsonResponse();
                }
            } catch (InvalidFormatException $exception) {
                return $this->jsonResponse(false, $result);
            } catch (ClientException $exception) {
                if ($exception->getResponse()->getStatusCode() === Response::HTTP_NOT_FOUND) {
                    throw new NotFoundHttpException();
                }
            } catch (Exception $exception) {
                return $this->jsonUnknownError();
            }
        }

        return $this->jsonResponse(false, $this->messageBuilder->build($form));
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxCreateAction(Request $request)
    {
        /** @var UMSUser $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        $form = $this->formFactory->create(new UserAdminType());
        $form->submit(
            (new JsonEncoder())->decode($request->getContent(), JsonEncoder::FORMAT)
        );

        if ($form->isSubmitted()) {
            try {
                $result = $this->UMSClient->addUser($currentUser, $form->getData());

                if ($result instanceof User) {
                    $session = (new Session())->setUser($result);
                    $this->sessionRepository->add($session);

                    $this->mailer->send(
                        $this->mailMessageFactory->getActivateAccountMessage($result)
                    );

                    return $this->jsonResponse();
                } else {
                    $this->adminUserFormErrorHandler->handle($result, $form);
                }
            } catch (Exception $exception) {
                return $this->jsonUnknownError();
            }
        }

        return $this->jsonResponse(false, $this->messageBuilder->build($form));
    }

    /**
     * @param User $user
     * @param bool $newsletterAgreement
     * @return bool
     */
    private function notifyAboutRegistration(User $user, $newsletterAgreement)
    {
        $senderMessages = [
            $this->senderMessageFactoryProvider
                ->getForType(Type::HIPCHAT)
                ->createForUser($user, TriggerType::USER_REGISTRATION)
        ];

        if ($newsletterAgreement) {
            $senderMessages[] = $this->senderMessageFactoryProvider
                ->getForType(Type::MAILCHIMP)
                ->createForUser($user, TriggerType::USER_REGISTRATION);
        }

        return $this->senderClient->send($senderMessages);
    }
}
