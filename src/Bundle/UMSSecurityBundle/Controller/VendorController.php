<?php

namespace Connect\Bundle\UMSSecurityBundle\Controller;

use Connect\Bundle\UMSSecurityBundle\Client\Facade;
use Connect\Bundle\UMSSecurityBundle\Form\MessageBuilder;
use Connect\Bundle\UMSSecurityBundle\Form\Type\VendorType;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\AjaxErrorHandler;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\Exception\InvalidFormatException;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Exception;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class VendorController
 */
class VendorController extends JsonController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var Facade
     */
    private $UMSClient;
    /**
     * @var AjaxErrorHandler
     */
    private $vendorFormErrorHandler;
    /**
     * @var MessageBuilder
     */
    private $messageBuilder;

    /**
     * VendorController constructor.
     * @param TranslatorInterface $translator
     * @param SerializerInterface $serializer
     * @param TokenStorageInterface $tokenStorage
     * @param FormFactoryInterface $formFactory
     * @param Facade $UMSClient
     * @param AjaxErrorHandler $vendorFormErrorHandler
     * @param MessageBuilder $messageBuilder
     */
    public function __construct(
        TranslatorInterface $translator,
        SerializerInterface $serializer,
        TokenStorageInterface $tokenStorage,
        FormFactoryInterface $formFactory,
        Facade $UMSClient,
        AjaxErrorHandler $vendorFormErrorHandler,
        MessageBuilder $messageBuilder
    ) {
        parent::__construct($translator);
        $this->serializer = $serializer;
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
        $this->UMSClient = $UMSClient;
        $this->vendorFormErrorHandler = $vendorFormErrorHandler;
        $this->messageBuilder = $messageBuilder;
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function ajaxGetAction()
    {
        /** @var UMSUser $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $vendor = $this->serializer->serialize(
            $user->getVendor(), JsonEncoder::FORMAT
        );

        return $this->jsonResponse(true, ['vendor' => json_decode($vendor, true)]);
    }

    /**
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxEditAction(Request $request)
    {
        /** @var UMSUser $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $form = $this->formFactory->create(new VendorType());
        $form->submit(
            (new JsonEncoder())->decode($request->getContent(), JsonEncoder::FORMAT)
        );

        if ($form->isSubmitted()) {
            try {
                $result = $this->UMSClient->editVendor($user->getVendor(), $form->getData());

                if (is_array($result)) {
                    $this->vendorFormErrorHandler->handle($result, $form);
                } else {
                    return $this->jsonResponse();
                }

            } catch (InvalidFormatException $exception) {
                return $this->jsonResponse(false, $result, Response::HTTP_BAD_REQUEST);
            } catch (Exception $exception) {
                return $this->jsonUnknownError(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return $this->jsonResponse(
            false,
            $this->messageBuilder->build($form),
            Response::HTTP_BAD_REQUEST
        );
    }
}