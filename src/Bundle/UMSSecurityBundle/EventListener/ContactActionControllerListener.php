<?php

namespace Connect\Bundle\UMSSecurityBundle\EventListener;

use Connect\Bundle\UMSSecurityBundle\Controller\ContactController;
use Connect\Bundle\UMSSecurityBundle\Controller\DummyController;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSContactRepository;
use Connect\Domain\Contact;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class ContactActionControllerListener
 */
class ContactActionControllerListener
{
    /**
     * @var ContactController
     */
    private $contactController;
    /**
     * @var UMSContactRepository
     */
    private $contactRepository;

    /**
     * @param ContactController $contactController
     * @param UMSContactRepository $contactRepository
     */
    function __construct(
        ContactController $contactController,
        UMSContactRepository $contactRepository
    ) {
        $this->contactController = $contactController;
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof DummyController) {
            try {
                $contact = $this->contactRepository->findOneByAction($request->attributes->get('action_id'));

                if ($contact instanceof Contact) {
                    $request->attributes->set('contact', $contact);
                    $event->setController([$this->contactController, 'confirmAction']);
                }
            } catch (RequestException $exception) {
                if ($exception->getResponse()->getStatusCode() !== Response::HTTP_NOT_FOUND) {
                    throw $exception;
                }
            }
        }
    }
}
