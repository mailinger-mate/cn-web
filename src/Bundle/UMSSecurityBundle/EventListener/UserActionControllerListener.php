<?php

namespace Connect\Bundle\UMSSecurityBundle\EventListener;

use Connect\Bundle\UMSSecurityBundle\Controller\DummyController;
use Connect\Bundle\UMSSecurityBundle\Controller\UserController;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Domain\User;
use Connect\Domain\UserAction\Type;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class UserActionControllerListener
 */
class UserActionControllerListener
{
    /**
     * @var UserController
     */
    private $userController;
    /**
     * @var UMSUserRepository
     */
    private $userRepository;

    /**
     * UserActionControllerListener constructor.
     * @param UserController $userController
     * @param UMSUserRepository $userRepository
     */
    public function __construct(UserController $userController, UMSUserRepository $userRepository)
    {
        $this->userController = $userController;
        $this->userRepository = $userRepository;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof DummyController) {
            try {
                $actionId = $request->attributes->get('action_id');
                $user = $this->userRepository->findUserByAction($actionId);
                if ($user instanceof User) {
                    switch ($this->getActionById($user, $actionId)->getType()) {
                        case Type::PASSWORD_RESET:
                            $event->setController([$this->userController, 'resetPasswordAction']);
                            break;
                        case Type::ACCOUNT_ACTIVATE:
                            $event->setController([$this->userController, 'accountActivateAction']);
                            break;
                        default:
                            return;
                    }
                    $request->attributes->set('user', $user);
                }
            } catch (RequestException $exception) {
                if ($exception->getResponse()->getStatusCode() !== Response::HTTP_NOT_FOUND) {
                    throw $exception;
                }
            }
        }
    }

    /**
     * @param User $user
     * @param $actionId
     * @return mixed
     */
    private function getActionById(User $user, $actionId)
    {
        foreach ($user->getActions() as $action) {
            if ($action->getId() === $actionId) {
                return $action;
            }
        }
    }
}