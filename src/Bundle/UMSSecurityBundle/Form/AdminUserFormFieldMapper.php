<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

/**
 * Class AdminUserFormFieldMapper
 */
class AdminUserFormFieldMapper implements FieldMapperInterface
{
    /**
     * @return array
     */
    public function getMap()
    {
        return [
            'name' => 'name',
            'contacts.0.value' => 'contact',
            'role' => 'role',
        ];
    }
}