<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

/**
 * Interface FieldMapperInterface
 */
interface FieldMapperInterface
{
    /**
     * @return array
     */
    public function getMap();
}
