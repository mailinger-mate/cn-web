<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MessageBuilder
 */
class MessageBuilder
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    public function build(FormInterface $form)
    {
        return [
            $this->translator->trans('label.message') => $this->transformErrorsToArray(
                $form->getErrors(true, false)
            )
        ];
    }

    /**
     * @param FormErrorIterator $errorIterator
     * @return array
     */
    private function transformErrorsToArray(FormErrorIterator $errorIterator)
    {
        $errors = [];

        foreach ($errorIterator as $error) {
            switch ($error) {
                case $error instanceof FormError:
                    $errors[] = $error->getMessage();
                    break;

                case $error instanceof FormErrorIterator:
                    $newErrors = $this->transformErrorsToArray($error);
                    $errors[$error->getForm()->getName()] = $newErrors;
                    break;
            }
        }

        return $errors;
    }
}
