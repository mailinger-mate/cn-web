<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

/**
 * Class ProfileFormFieldMapper
 */
class ProfileFormFieldMapper implements FieldMapperInterface
{
    /**
     * @return array
     */
    public function getMap()
    {
        return [
            'name' => 'name',
            'contact' => 'contact',
            'password.first' => 'password_first',
            'current_password' => 'current_password',
        ];
    }
}