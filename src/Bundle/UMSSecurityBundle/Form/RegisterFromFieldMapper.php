<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;


/**
 * Class RegisterFromFieldMapper
 */
class RegisterFromFieldMapper implements FieldMapperInterface
{
    /**
     * @return array
     */
    public function getMap()
    {
        return [
            'name' => 'name',
            'vendor.name' => 'vendor',
            'contacts.0.value' => 'contact',
            'password.first' => 'password_first',
        ];
    }
}