<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

/**
 * Class RemindPassFromFieldMapper
 */
class RemindPassFromFieldMapper implements FieldMapperInterface
{
    /**
     * @return array
     */
    public function getMap()
    {
        return ['email' => 'email'];
    }
}