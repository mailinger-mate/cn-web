<?php

namespace Connect\Bundle\UMSSecurityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ForgotPasswordType
 */
class ForgotPasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email')
            ->add('reset', 'submit');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'forgot_password';
    }
}
