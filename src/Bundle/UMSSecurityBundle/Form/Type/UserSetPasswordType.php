<?php

namespace Connect\Bundle\UMSSecurityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UserSetPasswordType
 */
class UserSetPasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contact', 'email', ['disabled' => true])
            ->add('password_first', 'password')
            ->add('password_second', 'password')
            ->add('save', 'submit');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_set_password';
    }
}
