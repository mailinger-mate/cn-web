<?php

namespace Connect\Bundle\UMSSecurityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class VendorType
 */
class VendorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('billing_name', 'text', ['property_path' => '[billingName]'])
            ->add('street', 'text')
            ->add('city', 'text')
            ->add('state', 'text')
            ->add('country', 'text')
            ->add('vat_number', 'text', ['property_path' => '[vatNumber]'])
            ->add('note', 'text')
            ->add('po_number', 'text', ['property_path' => '[poNumber]']);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'vendor';
    }
}