<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

/**
 * Class UserPassFormFieldMapper
 */
class UserPassFormFieldMapper implements FieldMapperInterface
{
    /**
     * @return array
     */
    public function getMap()
    {
        return ['password.first' => 'password_first'];
    }
}