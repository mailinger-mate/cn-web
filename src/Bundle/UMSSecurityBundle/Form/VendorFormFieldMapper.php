<?php

namespace Connect\Bundle\UMSSecurityBundle\Form;

/**
 * Class VendorFormFieldMapper
 */
class VendorFormFieldMapper implements FieldMapperInterface
{
    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'billingName' => 'billing_name',
            'street'      => 'street',
            'city'        => 'city',
            'state'       => 'state',
            'country'     => 'country',
            'vatNumber'   => 'vat_number',
            'note'        => 'note',
            'poNumber'    => 'po_number',
        ];
    }
}