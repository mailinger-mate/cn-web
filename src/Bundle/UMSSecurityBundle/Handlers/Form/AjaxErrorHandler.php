<?php

namespace Connect\Bundle\UMSSecurityBundle\Handlers\Form;

use Connect\Bundle\UMSSecurityBundle\Form\FieldMapperInterface;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\Exception\InvalidFormatException;
use LogicException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

/**
 * Class AjaxErrorHandler
 */
class AjaxErrorHandler
{
    /**
     * @var FieldMapperInterface
     */
    private $fieldMap;

    /**
     * @param FieldMapperInterface $fieldMap
     */
    function __construct(FieldMapperInterface $fieldMap)
    {
        $this->fieldMap = $fieldMap;
    }

    /**
     * @param array $responseData
     * @param FormInterface $form
     * @throws InvalidFormatException
     */
    public function handle(array $responseData, FormInterface $form)
    {
        $messageKey = 'message';

        if (!array_key_exists($messageKey, $responseData)) {
            throw new LogicException("Response body does not contain `$messageKey` key");
        }

        if (!is_array($responseData[$messageKey])) {
            throw new InvalidFormatException(
                sprintf('"%s" expected to be array, "%s" given', $messageKey, gettype($responseData[$messageKey]))
            );
        }

        foreach ($this->fieldMap->getMap() as $key => $formName) {
            foreach ($this->getErrors($key, $responseData[$messageKey]) as $error) {
                $form->get($formName)->addError(new FormError($error));
            }
        }
    }

    /**
     * @param string $key
     * @param array $responseData
     * @return array
     */
    private function getErrors($key, array $responseData){
        $keyParts = explode('.', $key);
        $result = $responseData;
        foreach ($keyParts as $part) {
            if (!array_key_exists($part, $result)) {
                return [];
            }
            $result = $result[$part];
        }
        return $result;
    }
}