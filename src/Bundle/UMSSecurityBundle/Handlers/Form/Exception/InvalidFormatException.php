<?php

namespace Connect\Bundle\UMSSecurityBundle\Handlers\Form\Exception;

use Exception;

/**
 * Class InvalidFormatException
 */
class InvalidFormatException extends Exception
{

}