<?php

namespace Connect\Bundle\UMSSecurityBundle\Mail\Message;

use Connect\Domain\Contact;
use Connect\Domain\User;
use Connect\Domain\UserAction\Type;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Error;

/**
 * Class Factory
 */
class Factory
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @param Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     * @param Router $router
     * @param TwigEngine $templating
     */
    function __construct(
        Swift_Mailer $mailer,
        TranslatorInterface $translator,
        Router $router,
        TwigEngine $templating
    ) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->router = $router;
        $this->templating = $templating;
    }

    /**
     * @param User $user
     * @return Swift_Message
     */
    public function getResetPasswordMessage(User $user)
    {
        $contact = $user->getPrimaryContact();
        $requestContext = $this->router->getContext();

        return $this
            ->getMessage(
                $contact->getValue(),
                'email.password_reset.subject',
                'ConnectUMSSecurityBundle:Email/Content:passwordReset.html.twig',
                [
                    'name'            => $user->getName(),
                    'vendor'          => $user->getVendor()->getName(),
                    'email'           => $contact->getValue(),
                    'link'            => $this->getActionLink($user->getAction(Type::PASSWORD_RESET)->getId()),
                    'scheme_and_host' => $this->getSchemeAndHost($requestContext),
                ]
            );
    }

    /**
     * @param User $user
     * @return Swift_Message
     */
    public function getActivateAccountMessage(User $user)
    {
        $contact = $user->getPrimaryContact();
        $requestContext = $this->router->getContext();

        return $this
            ->getMessage(
                $contact->getValue(),
                'email.activate_account.subject',
                'ConnectUMSSecurityBundle:Email/Content:activateAccount.html.twig',
                [
                    'name'            => $user->getName(),
                    'vendor'          => $user->getVendor()->getName(),
                    'email'           => $contact->getValue(),
                    'link'            => $this->getActionLink($user->getAction(Type::ACCOUNT_ACTIVATE)->getId()),
                    'scheme_and_host' => $this->getSchemeAndHost($requestContext),
                ]
            );
    }

    /**
     * @param string $action
     * @param Contact $contact
     * @return Swift_Message
     * @throws Exception
     * @throws Twig_Error
     */
    public function getContactConfirmMessage($action, Contact $contact)
    {
        $requestContext = $this->router->getContext();

        return $this
            ->getMessage(
                $contact->getValue(),
                'email.contact_verification.subject',
                'ConnectUMSSecurityBundle:Email/Content:contactVerification.html.twig',
                [
                    'name'            => $contact->getUser()->getName(),
                    'link'            => $this->getActionLink($action),
                    'scheme_and_host' => $this->getSchemeAndHost($requestContext),

                ]
            );
    }

    /**
     * @param string $action
     * @param Contact $contact
     * @return Swift_Message
     * @throws Exception
     * @throws Twig_Error
     */
    public function getVendorRegistrationMessage($action, Contact $contact)
    {
        $requestContext = $this->router->getContext();

        return $this
            ->getMessage(
                $contact->getValue(),
                'email.vendor_registration.subject',
                'ConnectUMSSecurityBundle:Email/Content:vendorRegistration.html.twig',
                [
                    'name'            => $contact->getUser()->getName(),
                    'link'            => $this->getActionLink($action),
                    'scheme_and_host' => $this->getSchemeAndHost($requestContext),

                ]
            );
    }

    /**
     * @param string $recipient
     * @param string $subjectKey
     * @param string $template
     * @param array $templateArgs
     * @return Swift_Message
     * @throws Exception
     * @throws Twig_Error
     */
    private function getMessage($recipient, $subjectKey, $template, array $templateArgs)
    {
        /** @var Swift_Message $message */
        $message = $this->mailer->createMessage();
        $message
            ->setFrom('noreply@x-formation.com', 'Connect')
            ->setTo($recipient)
            ->setSubject($this->translator->trans($subjectKey))
            ->setBody(
                $this->templating->render(
                    $template,
                    $templateArgs
                ),
                'text/html'
            );

        return $message;
    }

    /**
     * @param RequestContext $requestContext
     * @return string
     */
    private function getSchemeAndHost(RequestContext $requestContext)
    {
        return sprintf('%s://%s', $requestContext->getScheme(), $requestContext->getHost());
    }

    /**
     * @param string $action
     * @return string
     */
    private function getActionLink($action)
    {
        return $this
            ->router
            ->generate(
                'ums_action',
                [
                    'action_id' => $action,
                ],
                Router::ABSOLUTE_URL
            );
    }
}
