<?php

namespace Connect\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Stripe\Client;
use Connect\Domain\Card\SimpleCard;
use Connect\Domain\Card\SimpleCardFactory;
use Connect\Domain\Card\TokenCard;
use Connect\Domain\Customer;
use InvalidArgumentException;
use Stripe\Error\InvalidRequest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StripeCustomerRepository
 */
class StripeCustomerRepository
{
    /**
     * @var Client
     */
    private $client;

    /**
     * StripeCardRepository constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Customer $customer
     */
    public function add(Customer $customer)
    {
        $tokenCard = $customer->getCard();

        if (!$tokenCard instanceof TokenCard) {
            throw new InvalidArgumentException('Add customer is supported only using TokenCard');
        }

        $stripeCustomer = $this->client
            ->createCustomer($tokenCard->getCardToken())
            ->jsonSerialize();

        $customer->setId($stripeCustomer['id']);
    }

    /**
     * @param Customer $customer
     * @return SimpleCard
     * @throws InvalidRequest
     */
    public function refresh(Customer $customer)
    {
        if (is_null($customer->getId())) {
            return;
        }

        try {
            $stripeCustomer = $this->client
                ->retrieveCustomer($customer->getId())
                ->jsonSerialize();

            if ($this->isDeleted($stripeCustomer) || !$this->hasCard($stripeCustomer)) {
                $customer->setId(null);
                $customer->removeCard();

                return;
            }

            $customer->setCard(
                (new SimpleCardFactory())
                    ->createFromArray($stripeCustomer['sources']['data'][0])
            );

        } catch (InvalidRequest $exception) {
            if ($exception->getHttpStatus() === Response::HTTP_NOT_FOUND) {
                $customer->setId(null);
                $customer->removeCard();

                return;
            }
            throw $exception;
        }
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function remove(Customer $customer)
    {
        if (!is_null($customer->getId())) {
            $this->client->retrieveCustomer($customer->getId())->delete();
        }

        return true;
    }

    /**
     * @param array $customer
     * @return bool
     */
    private function isDeleted(array $customer)
    {
        return array_key_exists('deleted', $customer) && $customer['deleted'] === true;
    }

    /**
     * @param array $customer
     * @return bool
     */
    private function hasCard(array $customer)
    {
        return count($customer['sources']['data']) === 1;
    }
}