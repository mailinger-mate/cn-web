<?php

namespace Connect\Bundle\UMSSecurityBundle\Repository;

use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use Connect\Domain\ContactActionFactory;

/**
 * Class UMSContactRepository
 */
class UMSContactRepository extends UMSRepositoryAbstract
{
    /**
     * @param $actionId
     * @return bool|Contact
     */
    public function findOneByAction($actionId)
    {
        $contactsData = $this->jsonToArray($this
            ->get(
                sprintf('contacts?action=%s', $actionId)
            )
            ->getBody()
        );

        if (count($contactsData) !== 1) {
            return false;
        }
        $contactActionFactory = new ContactActionFactory();
        $contact = (new Contact())
            ->setId($contactsData[0]['id'])
            ->setType($contactsData[0]['type'])
            ->setValue($contactsData[0]['value'])
            ->setVerified($contactsData[0]['verified']);
        foreach ($contactsData[0]['_embedded']['contact_actions'] as $contactActionData) {
            $contactAction = $contactActionFactory->createFromArray($contactActionData, $contact);
            $contact->addAction($contactAction);
        }

        return $contact;
    }
}
