<?php

namespace Connect\Bundle\UMSSecurityBundle\Repository;

use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class UMSRepositoryAbstract
 */
abstract class UMSRepositoryAbstract
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param ClientInterface $client
     * @param SerializerInterface $serializer
     */
    function __construct(ClientInterface $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * @param mixed $data
     * @param string $uri
     * @param SerializationContext $serializationContext
     * @return ResponseInterface
     */
    protected function post($data, $uri, SerializationContext $serializationContext = null)
    {
        $data = $this->serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            $serializationContext
        );

        return $this->client->request(
            'POST',
            $this->getUri($uri),
            [
                'body'    => $data,
                'headers' => [
                    'Accept'       => 'application/json',
                    'Content-Type' => 'application/json'
                ],
            ]
        );
    }

    /**
     * @param mixed $data
     * @param string $uri
     * @param SerializationContext $serializationContext
     * @return ResponseInterface
     */
    protected function put($data, $uri, SerializationContext $serializationContext = null)
    {
        $data = $this->serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            $serializationContext
        );

        return $this->client->request(
            Request::METHOD_PUT,
            $this->getUri($uri),
            [
                'body'    => $data,
                'headers' => [
                    'Accept'       => 'application/json',
                    'Content-Type' => 'application/json'
                ],
            ]
        );
    }

    /**
     * @param $uri
     * @return bool
     */
    protected function delete($uri)
    {
        $response = $this->client->request(
            'DELETE',
            $this->getUri($uri),
            [
                'headers' => [
                    'Accept' => 'application/json'
                ],
            ]
        );

        return $response->getStatusCode() === Response::HTTP_NO_CONTENT;
    }

    /**
     * @param mixed $data
     * @param string $uri
     * @param SerializationContext $serializationContext
     * @return bool
     */
    protected function patch($data, $uri, SerializationContext $serializationContext = null)
    {
        $data = $this->serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            $serializationContext
        );

        $response = $this->client->request(
            'PATCH',
            $this->getUri($uri),
            [
                'body'    => $data,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json'
                ]
            ]
        );

        return $response->getStatusCode() === Response::HTTP_NO_CONTENT;
    }

    /**
     * @param $uri
     * @param array $query
     * @return ResponseInterface
     */
    protected function get($uri, array $query = [])
    {
        $query = count($query) > 0
            ? ['query' => $query]
            : [];

        $response = $this->client->request(
            'GET',
            $this->getUri($uri),
            array_merge(
                $query,
                [
                    'headers' => [
                        'Accept' => 'application/json'
                    ]
                ]
            )
        );

        return $response;
    }

    /**
     * @return ClientInterface
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * @param $json
     * @return array
     */
    protected function jsonToArray($json) {
        return $this->serializer
            ->deserialize(
                $json,
                'array',
                JsonEncoder::FORMAT
        );
    }

    /**
     * @param $uri
     * @return string
     */
    private function getUri($uri)
    {
        return 'rest/v1/' . $uri;
    }
}
