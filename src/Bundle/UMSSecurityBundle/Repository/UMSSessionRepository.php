<?php

namespace Connect\Bundle\UMSSecurityBundle\Repository;

use Connect\Domain\Session;
use Connect\Domain\SessionFactory;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UMSSessionRepository
 */
class UMSSessionRepository extends UMSRepositoryAbstract
{
    /**
     * @var
     */
    private $productName;

    /**
     * @param ClientInterface $client
     * @param SerializerInterface $serializer
     * @param string $productName
     */
    public function __construct(
        ClientInterface $client,
        SerializerInterface $serializer,
        $productName
    ) {
        parent::__construct($client, $serializer);
        $this->productName = $productName;
    }

    /**
     * @param Session $session
     * @return bool
     */
    public function add(Session $session)
    {
        $data = [
            'user' => $session->getUser()->getId(),
            'product' => $this->productName,
        ];
        $response = $this->post($data, 'sessions');

        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $responseData = $this->jsonToArray($response->getBody());
            $session->setId($responseData['id']);

            return true;
        }

        return false;
    }

    /**
     * @param Session $session
     * @return bool
     */
    public function remove(Session $session)
    {
        return $this->delete(sprintf('sessions/%s', $session->getId()));
    }

    /**
     * @param string $id
     * @return Session
     */
    public function find($id)
    {
        $sessionData = $this->jsonToArray($this
            ->get(
                sprintf('sessions/%s', $id)
            )->getBody());

        return (new SessionFactory())->createFromArray($sessionData);
    }

}