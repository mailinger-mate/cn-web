<?php

namespace Connect\Bundle\UMSSecurityBundle\Repository;

use Connect\Domain\Customer;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UMSStripeTokenRepository
 */
class UMSStripeTokenRepository extends UMSRepositoryAbstract
{
    /**
     * @param Customer $customer
     * @return bool
     */
    public function add(Customer $customer)
    {
        $response = $this->put(
            $customer,
            sprintf('stripe_tokens/%s', $customer->getId())
        );

        return $response->getStatusCode() === Response::HTTP_NO_CONTENT;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function remove(Customer $customer)
    {
        return $this->delete(
            sprintf('stripe_tokens/%s', $customer->getId())
        );
    }
}