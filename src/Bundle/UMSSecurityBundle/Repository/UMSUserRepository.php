<?php

namespace Connect\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUserFactory;
use Connect\Domain\Contact;
use Connect\Domain\ContactActionFactory;
use Connect\Domain\Customer;
use Connect\Domain\UserActionFactory;
use Connect\Domain\Vendor;
use Connect\Domain\VendorFactory;
use GuzzleHttp\Exception\ClientException;
use LogicException;

/**
 * Class UMSUserRepository
 */
class UMSUserRepository extends UMSRepositoryAbstract
{
    /**
     * @param $email
     * @throws ClientException|LogicException
     * @return bool|UMSUser
     */
    public function findUserByEmail($email)
    {
        $response = $this->get(
            'users',
            ['email' => $email]
        );

        $users = $this->jsonToArray($response->getBody());
        if (count($users) > 1) {
            throw new LogicException(
                sprintf('Multiple users received from UMS for email: %s', $email)
            );
        } elseif (count($users) === 0) {
            return false;
        }

        return $this->createUser($users[0]);
    }

    /**
     * @param $action
     * @throws ClientException|LogicException
     * @return bool|UMSUser
     */
    public function findUserByAction($action)
    {
        $response = $this->get(
            'users',
            ['action' => $action]
        );

        $users = $this->jsonToArray($response->getBody());
        if (count($users) > 1) {
            throw new LogicException(
                sprintf('Multiple users received from UMS for action: %s', $action)
            );
        } elseif (count($users) === 0) {
            return false;
        }

        return $this->createUser($users[0]);
    }

    /**
     * @param UMSUser $user
     * @return bool
     */
    public function remove(UMSUser $user)
    {
        return $this->delete(sprintf('users/%d', $user->getId()));
    }

    /**
     * @param Vendor $vendor
     * @return array
     */
    public function findByVendor(Vendor $vendor)
    {
        $users = $this->jsonToArray(
            $this->get('users', ['vendor' => $vendor->getId()])->getBody()
        );

        return array_map([$this, 'createUser'], $users);
    }

    /**
     * @param integer $id
     * @return UMSUser
     */
    public function find($id)
    {
        $userData = $this->jsonToArray($this
            ->get(
                sprintf('users/%s', $id)
            )->getBody());

        return $this->createUser($userData);
    }

    /**
     * @param array $userData
     * @return UMSUser
     */
    protected function createUser(array $userData)
    {
        $customer = (new Customer())
            ->setId(
                array_key_exists('card_token', $userData['_embedded']['vendor'])
                    ? $userData['_embedded']['vendor']['card_token']
                    : null
            );
        $vendor = (new VendorFactory())->createFromArray($userData['_embedded']['vendor'], $customer);
        $customer->setVendor($vendor);
        $user = (new UMSUserFactory())->createFromArray($userData, $vendor);

        $userActionFactory = new UserActionFactory();
        foreach ($userData['_embedded']['user_actions'] as $userActionData) {
            $user->addAction($userActionFactory->createFromArray($userActionData, $user));
        }

        $contactActionFactory = new ContactActionFactory();
        foreach ($userData['_embedded']['contacts'] as $contactData) {
            $contact = (new Contact())
                ->setId($contactData['id'])
                ->setValue($contactData['value'])
                ->setType($contactData['type'])
                ->setVerified($contactData['verified'])
                ->setUser($user);
            foreach ($contactData['_embedded']['contact_actions'] as $contactActionData) {
                $contactAction = $contactActionFactory->createFromArray($contactActionData, $contact);
                $contact->addAction($contactAction);
            }
            $user->addContact($contact);
        }

        return $user;
    }
}
