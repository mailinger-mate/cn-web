<?php

namespace Connect\Bundle\UMSSecurityBundle\Request;

use GuzzleHttp\Exception\RequestException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ParamConverterAbstract
 */
abstract class ParamConverterAbstract implements ParamConverterInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param = $configuration->getName();
        if (!$request->attributes->has($param)) {
            return false;
        }

        try {
            $request->attributes->set(
                $param,
                $this->findObject(
                    $request->attributes->get($param)
                )
            );
        } catch (RequestException $exception) {
            throw new NotFoundHttpException($this->translator->trans('message.error.not_found'));
        }

        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    abstract protected function findObject($id);
}