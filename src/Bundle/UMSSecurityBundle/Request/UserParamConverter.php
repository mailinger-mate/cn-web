<?php

namespace Connect\Bundle\UMSSecurityBundle\Request;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserParamConverter
 */
class UserParamConverter
    extends ParamConverterAbstract
    implements ParamConverterInterface
{
    /**
     * @var UMSUserRepository
     */
    private $repository;

    /**
     * @param UMSUserRepository $repository
     * @param TranslatorInterface $translator
     */
    public function __construct(UMSUserRepository $repository, TranslatorInterface $translator)
    {
        parent::__construct($translator);

        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return UMSUser::class === $configuration->getClass();
    }

    /**
     * @param $id
     * @return UMSUser
     */
    protected function findObject($id)
    {
        return $this->repository->find($id);
    }
}
