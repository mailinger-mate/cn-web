<?php

return [
    'login'            => [
        'title'           => 'Welcome to Connect',
        'text'            => 'Connect applications you use every day and keep your business running smoothly.',
        'account'         => 'Do not have an account?',
        'forgot_password' => 'Forgot your password?',
        'placeholder'     => [
            'password' => 'Enter your password',
            'email'    => 'Enter your email address'
        ]
    ],
    'register'         => [
        'title'       => 'Welcome to Connect',
        'text'        => 'Register for Connect and make<br>the most of your data.',
        'account'     => 'Already have an account?',
        'placeholder' => [
            'name'             => 'Enter your name',
            'email'            => 'Enter your email address',
            'company'          => 'Enter your company name',
            'password'         => 'Enter your password',
            'password_confirm' => 'Retype your password'
        ],
        'terms'       => [
            'title'             => 'By clicking the register button,  I agree to the ',
            'privacy_statement' => 'Privacy Statement',
            'terms_of_service'  => 'Terms of Service'
        ],
        'newsletter'    => [
            'label' =>  'Notify me about the latest product and marketing news.'
        ]
    ],
    'confirm'          => [
        'title'                => 'Welcome to Connect',
        'contact_verification' => 'Thank you for verifying your Connect email address.',
        'password'             => 'A password reset email has been sent to your registered email address. Please click on the verification link included in the email to reset your password. The link will expire in 24 hours, so be sure to use it right away.'
    ],
    'forgot_password'  => [
        'title'       => 'Welcome to Connect',
        'text'        => 'Forgot your password? Don\'t worry, it happens to everyone. Just enter your registered email address and we will send you instructions on how to reset your password.',
        'placeholder' => 'Enter your email'
    ],
    'reset_password'   => [
        'title'       => 'Welcome to Connect',
        'text'        => 'To reset your password, please fill out the form below and click Save.',
        'placeholder' => [
            'password'         => 'Enter your new password',
            'password_confirm' => 'Re-type your new password'
        ]
    ],
    'account_activate' => [
        'title'       => 'Welcome to Connect',
        'text'        => 'To activate your account, please fill out the form below and click Save.',
        'placeholder' => [
            'password'         => 'Enter your password',
            'password_confirm' => 'Retype your password'
        ]
    ],
    'message'          => [
        'error' => [
            'unknown_error'   => 'Unknown error occurred.',
            'not_found'       => 'Page not found.',
            'delete_yourself' => 'You can not delete yourself',
            'edit_yourself'   => 'You can not edit yourself',
            'unauthorized'    => 'Sorry, your session has expired. Please sign in again to continue using Connect.'
        ]
    ],
    'email'            => [
        'text'                 => 'Please do not reply to this email. It has been sent automatically by Connect. For further assistance, please contact ',
        'link_company'         => 'https://www.x-formation.com/',
        'support'              => 'X-Formation Support.',
        'subject' => 'Connect',
        'contact_verification' => [
            'subject' => 'Please verify your X-Formation Connect email address'
        ],
        'password_reset'       => [
            'subject' => 'Here\'s the link to reset your Connect password'
        ],
        'activate_account'     => [
            'subject' => 'A new Connect account has been created for you'
        ],
        'vendor_registration'     => [
            'subject' => 'Welcome to Connect: Your 30-day free trial begins today'
        ]
    ],
    'label'            => [
        'success' => 'success',
        'message' => 'message'
    ],
    'sender'           => [
        'hipchat' => [
            'user_registered' => '<strong>New Connect Registration</strong><br/><br/>' .
                'Vendor: <i>%vendor_name% (id:%vendor_id%)</i><br>' .
                'User: <i>%user_name%</i><br>' .
                'Email: <i><a href="mailto:%user_email%">%user_email%</a></i>',
            'card_registered' => '<strong>New Connect Credit Card Registration</strong><br/><br/>' .
                'Vendor: <i>%vendor_name%</i><br>' .
                'User: <i>%user_name%</i><br>' .
                'Email: <i><a href="mailto:%user_email%">%user_email%</a></i>'
        ]
    ]
];
