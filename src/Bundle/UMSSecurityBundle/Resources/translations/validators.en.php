<?php

return array(
    'form' => array(
        'user' => array(
            'errors' => array(
                'passwords_mismatch'    => 'Passwords are not matching.',
                'passwords_not_current' => 'The value should be your current password.',
                'password_too_short'    => 'The password must have at least {{ limit }} characters.'
            )
        )
    )
);
