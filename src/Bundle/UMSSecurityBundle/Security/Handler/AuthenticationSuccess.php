<?php

namespace Connect\Bundle\UMSSecurityBundle\Security\Handler;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Session;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;

/**
 * Class AuthenticationSuccess
 */
class AuthenticationSuccess extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var UMSSessionRepository
     */
    private $sessionRepository;

    /**
     * LoginSuccess constructor.
     * @param Router $router
     * @param UMSSessionRepository $sessionRepository
     * @param HttpUtils $httpUtils
     * @param array $options
     */
    public function __construct(
        Router $router,
        UMSSessionRepository $sessionRepository,
        HttpUtils $httpUtils,
        array $options = array()
    ) {
        parent::__construct($httpUtils, $options);

        $this->router = $router;
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse|Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->handleUmsSession($request, $token);

        return parent::onAuthenticationSuccess($request, $token);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     */
    private function handleUmsSession(Request $request, TokenInterface $token)
    {
        $session = $request->getSession();

        try {
            if (!$session->has('ums_session')) {
                throw new MissingSessionException();
            }

            try {
                $this->sessionRepository->find($session->get('ums_session'));
            } catch (ClientException $exception) {
                if ($exception->getResponse()->getStatusCode() === Response::HTTP_NOT_FOUND) {
                    throw new MissingSessionException();

                }
                throw $exception;
            }

        } catch (MissingSessionException $exception) {
            /** @var UMSUser $user */
            $user = $token->getUser();

            $umsSession = (new Session())
                ->setUser($user);

            $this->sessionRepository->add($umsSession);

            $session->set('ums_session', $umsSession->getId());
        }
    }
}