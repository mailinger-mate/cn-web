<?php

namespace Connect\Bundle\UMSSecurityBundle\Security\Handler;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use Connect\Domain\Session;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

/**
 * Class LogoutSuccess
 */
class LogoutSuccess implements LogoutSuccessHandlerInterface
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var UMSSessionRepository
     */
    private $sessionRepository;

    /**
     * LogoutSuccess constructor.
     * @param Router $router
     * @param UMSSessionRepository $sessionRepository
     */
    public function __construct(Router $router, UMSSessionRepository $sessionRepository)
    {
        $this->router = $router;
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function onLogoutSuccess(Request $request)
    {
        $session = $request->getSession();
        if ($session->has('ums_session')) {
            $umsSession = (new Session())->setId($session->get('ums_session'));
            $this->sessionRepository->remove($umsSession);
            $session->remove('ums_session');
        }

        return new RedirectResponse($this->router->generate('app'));
    }
}