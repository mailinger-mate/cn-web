<?php

namespace Connect\Bundle\UMSSecurityBundle\Security\Handler;

use Exception;

/**
 * Class MissingSessionException
 */
class MissingSessionException extends Exception
{

}