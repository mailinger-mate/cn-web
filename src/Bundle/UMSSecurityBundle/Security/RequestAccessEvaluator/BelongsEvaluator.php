<?php

namespace Connect\Bundle\UMSSecurityBundle\Security\RequestAccessEvaluator;

use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Contact;
use Connect\Domain\Vendor;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\SecurityFunction;
use JMS\DiExtraBundle\Annotation\Service;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Service
 *
 * Class BelongsEvaluator
 */
class BelongsEvaluator
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @InjectParams({
     *     "tokenStorage" = @Inject("security.token_storage"),
     * })
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @SecurityFunction("belongsToVendor")
     *
     * @param $object
     * @return bool
     */
    public function belongsToVendor($object)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user instanceof UMSUser) {
            return false;
        }

        return $user->getVendor()->getId() === $this->getVendor($object)->getId();
    }

    /**
     * @SecurityFunction("belongsToUser")
     *
     * @param $object
     * @return bool
     */
    public function belongsToUser($object)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user instanceof UMSUser) {
            return false;
        }

        return $user->getId() === $this->getUser($object)->getId();
    }

    /**
     * @param $object
     * @return Vendor
     * @throws InvalidArgumentException
     */
    private function getVendor($object)
    {
        switch(true) {
            case $object instanceof UMSUser:
                return $object->getVendor();

            default:
                throw new InvalidArgumentException('Not supported object to get related vendor: ' . get_class($object));
        }
    }

    /**
     * @param $object
     * @return Vendor
     * @throws InvalidArgumentException
     */
    private function getUser($object)
    {
        switch(true) {
            case $object instanceof Contact:
                return $object->getUser();

            default:
                throw new InvalidArgumentException('Not supported object to get related user: ' . get_class($object));
        }
    }
}