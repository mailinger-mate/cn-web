<?php

namespace Connect\Bundle\UMSSecurityBundle\Security;

use Connect\Domain\Contact;
use Connect\Domain\User;
use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UMSUser
 */
class UMSUser extends User implements UserInterface, Serializable
{
    /**
     * @var array
     */
    private $roles;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $salt;

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getBaseRole()
    {
        return strtolower(str_replace('ROLE_', '', $this->roles[0]));
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->getPrimaryContact()->getValue();
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            $this->getPrimaryContact()->getValue()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list(
            $contact
            ) = unserialize($serialized);

        $this->addContact((new Contact())->setValue($contact)->setType('email')->setVerified(true));
    }
}
