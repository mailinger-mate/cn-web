<?php

namespace Connect\Bundle\UMSSecurityBundle\Security;

use Connect\Domain\Vendor;

/**
 * Class Factory
 */
class UMSUserFactory
{
    /**
     * @param array $data
     * @param Vendor $vendor
     * @return UMSUser
     */
    public function createFromArray(array $data, Vendor $vendor)
    {
        return (new UMSUser())
            ->setId((int)$data['id'])
            ->setName($data['name'])
            ->setActivated($data['activated'] === true)
            ->setEnabled($data['enabled'] === true)
            ->setVendor($vendor)
            ->setPassword($data['password'])
            ->setRoles($this->formatRoles($data['role']))
            ->setSalt($data['salt']);
    }

    /**
     * @param string $roles
     * @return array
     */
    private function formatRoles($roles)
    {
        return array_map(
            function ($role) {
                return 'ROLE_' . strtoupper($role);
            },
            explode(',', $roles)
        );
    }
}
