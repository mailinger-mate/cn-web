<?php

namespace Connect\Bundle\UMSSecurityBundle\Security;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UMSUserProvider
 */
class UMSUserProvider implements UserProviderInterface
{
    /**
     * @var UMSUserRepository
     */
    private $repository;

    /**
     * @param UMSUserRepository $repository
     */
    function __construct(UMSUserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     *
     */
    public function loadUserByUsername($username)
    {
        $user = $this->repository->findUserByEmail($username);

        if ($user instanceof UMSUser) {
            return $user;
        }

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof UMSUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        /** @var UserInterface $user */
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === UMSUser::class;
    }
}
