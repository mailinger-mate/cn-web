<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender;

use Connect\Bundle\UMSSecurityBundle\Sender\ClientInterface as SenderClientInterface;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Client
 */
class Client implements SenderClientInterface
{
    /**
     * @var GuzzleClientInterface
     */
    private $httpClient;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @inheritdoc
     */
    public function __construct(
        GuzzleClientInterface $httpClient,
        LoggerInterface $logger
    ) {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * @param Message[] $messages
     * @return bool
     */
    public function send(array $messages)
    {
        if (count($messages) === 0) {
            return false;
        }

        try {
            $response = $this->httpClient->request(
                'POST',
                'messages',
                [
                    RequestOptions::JSON => array_map(
                        function(Message $message) {
                            return $message->getAsArray();
                        },
                        $messages
                    )
                ]
            );

            if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
                return true;
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $this->logger->error(sprintf(
            "Error on sending message to connect sender with response code: %d, body: %s",
            $response->getStatusCode(),
            $response->getBody()->getContents()
        ));

        return false;
    }
}