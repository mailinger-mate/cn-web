<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender;

/**
 * Interface ClientInterface
 */
interface ClientInterface
{
    /**
     * @param Message[] $messages
     * @return bool
     */
    public function send(array $messages);
}