<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender;

/**
 * Class ClientNull
 */
class ClientNull implements ClientInterface
{
    /**
     * @inheritdoc
     */
    public function send(array $messages)
    {
        return count($messages) > 0;
    }
}