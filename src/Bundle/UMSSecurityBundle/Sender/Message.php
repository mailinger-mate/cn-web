<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender;

/**
 * Class Message
 */
class Message
{
    /**
     * @var string
     */
    private $connectionToken;
    /**
     * @var string
     */
    private $messageToken;
    /**
     * @var string
     */
    private $destinationId;
    /**
     * @var string
     */
    private $type;
    /**
     * @var array
     */
    private $accountConfig;
    /**
     * @var array
     */
    private $messageConfig;
    /**
     * @var array
     */
    private $attachments;

    /**
     * Message constructor.
     * @param string $connectionToken
     * @param string $messageToken
     * @param string $destinationId
     * @param string $type
     * @param array $accountConfig
     * @param array $messageConfig
     * @param array $attachments
     */
    public function __construct(
        $connectionToken,
        $messageToken,
        $destinationId,
        $type,
        array $accountConfig,
        array $messageConfig,
        array $attachments
    ) {
        $this->connectionToken  = $connectionToken;
        $this->messageToken     = $messageToken;
        $this->destinationId    = $destinationId;
        $this->type             = $type;
        $this->accountConfig    = $accountConfig;
        $this->messageConfig    = $messageConfig;
        $this->attachments      = $attachments;
    }

    /**
     * @return array
     */
    public function getAsArray()
    {
       return [
           "con_token"      => $this->connectionToken,
           "msg_token"      => $this->messageToken,
           "dst_id"         => $this->destinationId,
           "type"           => $this->type,
           "account_config" => $this->accountConfig,
           "message_config" => $this->messageConfig,
           "attachments"    => $this->attachments
       ];
    }
}