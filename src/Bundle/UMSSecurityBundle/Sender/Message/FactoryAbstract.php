<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Domain\User;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class FactoryAbstract
 */
abstract class FactoryAbstract
{
    /**
     * @var UuidInterface
     */
    private $uuidFactory;

    /**
     * @param UuidFactoryInterface $uuidFactory
     */
    public function __construct(UuidFactoryInterface $uuidFactory)
    {
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * @param User $user
     * @param string $triggerType
     * @return Message
     */
    public function createForUser(User $user, $triggerType)
    {
        return new Message(
            'internal_connect_message',
            $this->uuidFactory->uuid4()->toString(),
            0,
            $this->getType(),
            $this->getAccountConfig(),
            $this->getMessageConfig($user, $triggerType),
            []
        );
    }

    /**
     * @return string
     */
    abstract protected function getType();

    /**
     * @return array
     */
    abstract protected function getAccountConfig();

    /**
     * @param User $user
     * @param string $triggerType
     * @return array
     */
    abstract protected function getMessageConfig(User $user, $triggerType);
}