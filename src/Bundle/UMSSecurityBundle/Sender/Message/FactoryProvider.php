<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Type;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FactoryProvider
 */
class FactoryProvider
{
    /**
     * @var ContainerInterface
     */
    private $serviceContainer;

    /**
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * @param string $type
     * @return FactoryAbstract
     */
    public function getForType($type)
    {
        switch($type)
        {
            case Type::MAILCHIMP:
                return $this->serviceContainer->get('sender.message.factory.mailchimp');

            case Type::HIPCHAT:
                return $this->serviceContainer->get('sender.message.factory.hipchat');

            default:
                throw new InvalidArgumentException('Not supported sender type: ' . $type);
        }
    }
}