<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Bundle\UMSSecurityBundle\Sender\Type;
use Connect\Domain\User;
use LogicException;
use Ramsey\Uuid\UuidFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class HipChatFactory
 */
class HipChatFactory extends FactoryAbstract
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $salesRoomId;
    /**
     * @var string
     */
    private $newsRoomId;

    /**
     * @param UuidFactoryInterface $uuidFactory
     * @param TranslatorInterface $translator
     * @param string $token
     * @param string $salesRoomId
     * @param string $newsRoomId
     */
    public function __construct(
        UuidFactoryInterface $uuidFactory,
        TranslatorInterface $translator,
        $token,
        $salesRoomId,
        $newsRoomId
    ) {
        parent::__construct($uuidFactory);

        $this->translator = $translator;
        $this->token = $token;
        $this->salesRoomId = $salesRoomId;
        $this->newsRoomId = $newsRoomId;
    }

    /**
     * @inheritdoc
     */
    protected function getType()
    {
        return Type::HIPCHAT;
    }

    /**
     * @inheritdoc
     */
    protected function getAccountConfig()
    {
        return [
            'token' => [$this->token],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getMessageConfig(User $user, $triggerType)
    {
        switch ($triggerType)
        {
            case TriggerType::USER_REGISTRATION:
                return $this->getHipChatConfig(
                    $this->salesRoomId,
                    $this->translator->trans('sender.hipchat.user_registered', [
                        '%vendor_id%'   => $user->getVendor()->getId(),
                        '%vendor_name%' => $user->getVendor()->getName(),
                        '%user_name%'   => $user->getName(),
                        '%user_email%'  => $user->getPrimaryContactValue(),
                    ])
                );

            case TriggerType::CARD_REGISTRATION:
                return $this->getHipChatConfig(
                    $this->newsRoomId,
                    $this->translator->trans('sender.hipchat.card_registered', [
                        '%vendor_name%' => $user->getVendor()->getName(),
                        '%user_name%'   => $user->getName(),
                        '%user_email%'  => $user->getPrimaryContactValue(),
                    ])
                );

            default:
                throw new LogicException('Not supported trigger type: ' . $triggerType);
        }
    }

    /**
     * @param string $room
     * @param string $value
     * @return array
     */
    private function getHipChatConfig($room, $value)
    {
        return [
            'room_id' => [
                'values' => [$room]
            ],
            'content' => [
                'values' => [$value],
                'meta' => [
                    'message_format'    => ['html'],
                    'color'             => ['green'],
                    'notify'            => ['true'],
                ]
            ]
        ];
    }
}