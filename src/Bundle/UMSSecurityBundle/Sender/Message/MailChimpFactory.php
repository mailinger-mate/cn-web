<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Bundle\UMSSecurityBundle\Sender\Type;
use Connect\Domain\User;
use LogicException;
use Ramsey\Uuid\UuidFactoryInterface;

/**
 * Class MailChimpFactory
 */
class MailChimpFactory extends FactoryAbstract
{
    /**
     * @var string
     */
    private $accessToken;
    /**
     * @var string
     */
    private $apiEndpoint;
    /**
     * @var string
     */
    private $listId;
    /**
     * @var array
     */
    private $interestIds;

    /**
     * @param UuidFactoryInterface $uuidFactory
     * @param string $accessToken
     * @param string $apiEndpoint
     * @param string $listId
     * @param array $interestIds
     */
    public function __construct(UuidFactoryInterface $uuidFactory, $accessToken, $apiEndpoint, $listId, array $interestIds = [])
    {
        parent::__construct($uuidFactory);

        $this->accessToken = $accessToken;
        $this->apiEndpoint = $apiEndpoint;
        $this->listId = $listId;
        $this->interestIds = $interestIds;
    }

    /**
     * @inheritdoc
     */
    protected function getType()
    {
        return Type::MAILCHIMP;
    }

    /**
     * @inheritdoc
     */
    protected function getAccountConfig()
    {
        return [
            'access_token' => [$this->accessToken],
            'api_endpoint' => [$this->apiEndpoint]
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getMessageConfig(User $user, $triggerType)
    {
        if ($triggerType !== TriggerType::USER_REGISTRATION) {
            throw new LogicException('Not supported trigger type: ' . $triggerType);
        }

        return [
            'action' => [
                'values' => ['subscribe']
            ],
            'list_id' => [
                'values' => [$this->listId]
            ],
            'subscriber_email' => [
                'values' => [$user->getPrimaryContactValue()]
            ],
            'manage_fields_keys' => [
                'values' => ['LNAME']
            ],
            'manage_fields_values' => [
                'values' => [$user->getName()]
            ],
            'interests' => [
                'values' => $this->interestIds,
                'meta'   => ['append' => ['true']]
            ]
        ];
    }
}