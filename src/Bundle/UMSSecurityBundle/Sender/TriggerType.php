<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender;

/**
 * Class TriggerType
 */
class TriggerType
{
    const USER_REGISTRATION = 'user_registration';
    const CARD_REGISTRATION = 'card_registration';
}