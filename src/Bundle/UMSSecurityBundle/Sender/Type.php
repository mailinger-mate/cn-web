<?php

namespace Connect\Bundle\UMSSecurityBundle\Sender;

/**
 * Class Type
 */
class Type
{
    const MAILCHIMP = 'mailchimp';
    const HIPCHAT = 'hipchat';
}