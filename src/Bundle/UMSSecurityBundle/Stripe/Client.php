<?php

namespace Connect\Bundle\UMSSecurityBundle\Stripe;

use Stripe\Customer;

/**
 * Class Client
 */
class Client
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * Client constructor.
     * @param string $apiKey
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $id
     * @return Customer
     */
    public function retrieveCustomer($id)
    {
        return Customer::retrieve($id, ['api_key' => $this->apiKey]);
    }

    /**
     * @param string $source
     * @return Customer
     */
    public function createCustomer($source)
    {
        return Customer::create(['source' => $source], ['api_key' => $this->apiKey]);
    }
}