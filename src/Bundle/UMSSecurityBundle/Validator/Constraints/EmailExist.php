<?php

namespace Connect\Bundle\UMSSecurityBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EmailExists
 */
class EmailExist extends Constraint
{
    public $message = 'Email does not exist in our system.';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'email_exist_validator';
    }

}
