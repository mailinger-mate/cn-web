<?php

namespace Connect\Bundle\UMSSecurityBundle\Validator\Constraints;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class EmailExistsValidator
 */
class EmailExistValidator extends ConstraintValidator
{
    /**
     * @var UMSUserRepository
     */
    private $userRepository;

    /**
     * @param UMSUserRepository $userRepository
     */
    function __construct(UMSUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$this->userRepository->findUserByEmail($value) instanceof UMSUser) {
            /** @var EmailExist $constraint */
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
