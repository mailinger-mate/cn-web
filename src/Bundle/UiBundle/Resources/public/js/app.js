/*global $, window, setTimeout*/

// Custom scripts
$(function () {

    "use strict";

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body");

    $("[data-toggle=popover]").popover();

    // Minimalize menu when screen is less than 768px
    $(window).bind("load resize", function (event) {
        $('body').toggleClass("body-small", $(event.target).width() < 769);
    });

    function fix_height() {

        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeigh = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeigh > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeigh + "px");
        }

        if (navbarHeigh < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }
    }

    fix_height();

});
