/*global $, window */

$(document).ready(function () {
    "use strict";

    var targetPath = $('input[name="_target_path"]');
    targetPath.val(targetPath.val() + window.location.hash);
});