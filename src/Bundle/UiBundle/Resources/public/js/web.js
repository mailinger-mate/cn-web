/*global $, window, WOW*/

$(document).ready(function () {

    "use strict";

    //checking for anchor value

    if (!window.location.hash) {
        $("input[name='email']").focus();
    }

    //fancybox setup
    $('.fancybox').fancybox();
    $(".fancybox-effects-d").fancybox({
        padding: 0,

        openEffect: 'elastic',
        openSpeed: 150,

        closeEffect: 'elastic',
        closeSpeed: 150,

        closeClick: true,

        helpers: {
            overlay: null
        }
    });

    // Fix navbar

    function navbarScroll() {
        var $nav = $(".navbar-default");
        $nav.toggleClass("navbar-scroll", $nav.offset().top > 50);
    }

    navbarScroll();

    $(window).on("scroll", navbarScroll);

    // Highlight the top nav as scrolling
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 80
    });

    // Page scrolling feature

    $('a.page-scroll').bind('click', function (event) {
        $('html, body').stop().animate({
            scrollTop: $(event.target.hash).offset().top - 70
        }, 500);
        event.preventDefault();
    });

    $('body').cookieDisclaimer({
        layout: "bar",
        position: "bottom",
        style: "dark",
        text: $(".cookieText").text(),
        cssPosition: "fixed",

        acceptBtn: {
            text: $(".cookieButton").text(),
            cssClass: "cdbtn cookie",
            cssId: "cookieAcceptBtn",
            onAfter: function () {
                return;
            }
        },

        policyBtn: {
            active: true,
            text: $(".cookieLink").text(),
            link: "https://www.x-formation.com/company/privacy-statement",
            linkTarget: "_blank",
            cssClass: "cdbtn privacy",
            cssId: "policyPageBtn"
        },

        cookie: {
            name: "Cookie Disclaimer",
            val: "confirmed",
            path: "/",
            expire: 365
        }
    });

    $(".cookie-block").remove();


    $('.nav a').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

});

// Activate WOW.js plugin for animation on scrol
var wow = new WOW();
wow.init();
