<?php

namespace Connect\Bundle\WebBundle\Controller;

use LogicException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomeController
 */
class HomeController
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @param EngineInterface $templating
     */
    function __construct(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @throws LogicException
     */
    public function indexAction()
    {
        throw new LogicException('This action can not be used directly.');
    }

    /**
     * @return Response
     */
    public function landingPageAction()
    {
        return $this->templating->renderResponse('ConnectWebBundle:Home:landingPage.html.twig');
    }
}
