<?php

namespace Connect\Bundle\WebBundle\Controller;

use Connect\Bundle\WebBundle\DataTransformer\NameToUrlTransformer;
use Connect\Bundle\WebBundle\Entity\Post;
use Connect\Bundle\WebBundle\Generator\S3Url;
use Connect\Bundle\WebBundle\Markdown\Parser;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PostController
 */
class PostController
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var ObjectManager
     */
    private $objectManager;
    /**
     * @var Parser
     */
    private $parser;
    /**
     * @var S3Url
     */
    private $urlGenerator;

    /**
     * PostController constructor.
     * @param EngineInterface $templating
     * @param ObjectManager $objectManager
     * @param MarkdownParserInterface $parser
     * @param S3Url $urlGenerator
     */
    public function __construct(
        EngineInterface $templating,
        ObjectManager $objectManager,
        MarkdownParserInterface $parser,
        S3Url $urlGenerator
    ) {
        $this->templating = $templating;
        $this->objectManager = $objectManager;
        $this->parser = $parser;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param $year
     * @param $month
     * @return Response
     */
    public function listAction($year, $month)
    {
        $postRepository = $this->objectManager->getRepository(Post::class);
        $posts = $postRepository->findByYearMonthAndSlug(
            $year > 0 ? $year : null,
            $month > 0 ? $month : null
        );

        return $this->templating->renderResponse(
            'ConnectWebBundle:Post:list.html.twig',
            ['posts' => $posts]
        );
    }

    /**
     * @param $year
     * @param $month
     * @param $slug
     * @return Response
     */
    public function getAction($year, $month, $slug)
    {
        $postRepository = $this->objectManager->getRepository(Post::class);
        /** @var Post[] $posts */
        $posts = $postRepository->findByYearMonthAndSlug(
            $year, $month, $slug
        );
        if (count($posts) != 1) {
            throw new NotFoundHttpException();
        }
        $post = $posts[0];

        $post->setContent(
            $this->parser->transformMarkdown(
                $post->getContent(),
                new NameToUrlTransformer($this->urlGenerator, $post->getImages())
            )
        );

        return $this->templating->renderResponse(
            'ConnectWebBundle:Post:get.html.twig',
            ['post' => $post]
        );
    }
}