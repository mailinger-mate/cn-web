<?php

namespace Connect\Bundle\WebBundle\DQL;

use Doctrine\ORM\Query\AST\AggregateExpression;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class MonthFunction
 *
 * DateDiffFunction ::= "MONTH" "(" ArithmeticPrimary ")"
 */
class MonthFunction extends FunctionNode
{
    /**
     * @var AggregateExpression
     */
    public $monthExpression;

    /**
     * {@inheritdoc}
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('MONTH(%s)', $this->monthExpression->dispatch($sqlWalker));
    }

    /**
     * {@inheritdoc}
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->monthExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}