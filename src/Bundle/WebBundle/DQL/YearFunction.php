<?php

namespace Connect\Bundle\WebBundle\DQL;

use Doctrine\ORM\Query\AST\AggregateExpression;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class YearFunction
 *
 * DateDiffFunction ::= "YEAR" "(" ArithmeticPrimary ")"
 */
class YearFunction extends FunctionNode
{
    /**
     * @var AggregateExpression
     */
    public $yearExpression;

    /**
     * {@inheritdoc}
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('YEAR(%s)', $this->yearExpression->dispatch($sqlWalker));
    }

    /**
     * {@inheritdoc}
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->yearExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}