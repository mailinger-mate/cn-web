<?php

namespace Connect\Bundle\WebBundle\DataTransformer;

use Connect\Bundle\WebBundle\Entity\PostImage;
use Connect\Bundle\WebBundle\Generator\S3Url;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class NameToUrlTransformer implements DataTransformerInterface
{
    /**
     * @var S3Url
     */
    private $generator;

    /**
     * @var PostImage[]
     */
    private $images;

    /**
     * NameToUrlTransformer constructor.
     * @param S3Url $generator
     * @param array $images
     */
    public function __construct(S3Url $generator, $images)
    {
        $this->generator = $generator;
        $this->images = $images;
    }

    /**
     * [@inheritdoc]
     */
    public function transform($name)
    {
        foreach ($this->images as $image) {
            if ($image->getName() === $name) {
                return $this->generator->generate($image->getPath());
            }
        }

        return $name;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        throw new TransformationFailedException('Reverse transform not supported');
    }
}