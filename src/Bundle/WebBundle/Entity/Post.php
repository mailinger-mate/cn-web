<?php

namespace Connect\Bundle\WebBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 *
 * @ORM\Entity(repositoryClass="PostRepository")
 * @ORM\Table(name="post", uniqueConstraints={@ORM\UniqueConstraint(name="slug_value_unique_idx", columns="slug")})
 */
class Post
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="title", length=255)
     */
    private $title;

    /**
     * @var
     * @ORM\Column(type="text", name="intro", nullable=true)
     */
    private $intro;

    /**
     * @var
     * @ORM\Column(type="text", name="content")
     */
    private $content;

    /**
     * @var
     * @ORM\Column(type="string", name="author", length=255)
     */
    private $author;

    /**
     * @var
     * @ORM\Column(type="datetime", name="publish_date")
     */
    private $publishDate;

    /**
     * @var
     * @ORM\Column(type="string", name="slug", length=255)
     */
    private $slug;

    /**
     * @var ArrayCollection|PostImage[]
     * @ORM\OneToMany(targetEntity="PostImage", mappedBy="post")
     */
    private $images;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="created")
     */
    private $created;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="updated")
     */
    private $updated;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Post
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     *
     * If intro is not define take first 30 words from content
     */
    public function getIntro()
    {
        if (is_null($this->intro)) {
            return join(
                ' ',
                array_slice(
                    str_word_count($this->content, 2, '/.,\';\][=-?><":|}{+_)(*&^%$#@!1234567890'),
                    0,
                    30
                )
            );
        } else {
            return $this->intro;
        }
    }

    /**
     * @param mixed $intro
     * @return Post
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Post
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param mixed $publishDate
     * @return Post
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return PostImage[]|ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param PostImage
     * @return Post
     */
    public function addImage(PostImage $image)
    {
        $this->images->add($image);

        return $this;
    }

    /**
     * @param PostImage
     * @return Post
     */
    public function removeImage(PostImage $image)
    {
        $this->images->remove($image);

        return $this;
    }
}