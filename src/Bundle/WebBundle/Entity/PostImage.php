<?php

namespace Connect\Bundle\WebBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PostImage
 *
 * @ORM\Entity()
 * @ORM\Table(name="post_image")
 */
class PostImage
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="name", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="path", length=255)
     * @var string
     */
    private $path;

    /**
     * @var Post
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="images")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $post;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="created")
     */
    private $created;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="updated")
     */
    private $updated;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PostImage
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return PostImage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return PostImage
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return PostImage
     */
    public function setPost(Post $post)
    {
        $this->post = $post;

        return $this;
    }
}