<?php

namespace Connect\Bundle\WebBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class PostRepository
 */
class PostRepository extends EntityRepository
{
    /**
     * @param int $year
     * @param int $month
     * @param string $slug
     * @return array
     */
    public function findByYearMonthAndSlug($year = null, $month = null, $slug = null)
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from('ConnectWebBundle:Post', 'p')
            ->orderBy('p.publishDate', 'DESC');

        if (!is_null($year)) {
            $queryBuilder
                ->andWhere('YEAR(p.publishDate) = :year')
                ->setParameter('year', $year);
        }

        if (!is_null($month)) {
            $queryBuilder
                ->andWhere('MONTH(p.publishDate) = :month')
                ->setParameter('month', $month);
        }

        if (!is_null($slug)) {
            $queryBuilder
                ->andWhere('p.slug = :slug')
                ->setParameter('slug', $slug);
        }

        return $queryBuilder->getQuery()
            ->getResult();
    }
}