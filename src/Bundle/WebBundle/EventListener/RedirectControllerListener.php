<?php

namespace Connect\Bundle\WebBundle\EventListener;

use Connect\Bundle\AppBundle\Controller\AppController;
use Connect\Bundle\WebBundle\Controller\HomeController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class RequestListener
 */
class RedirectControllerListener
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;
    /**
     * @var HomeController
     */
    private $homeController;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param HomeController $homeController
     * @param RouterInterface $router
     */
    function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        HomeController $homeController,
        RouterInterface $router
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->homeController = $homeController;
        $this->router = $router;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof HomeController ) {
            if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
                $event->setController(function() {
                    return new RedirectResponse($this->router->generate('app'));
                });

            } elseif ($controller[1] === 'indexAction') {
                $event->setController([$this->homeController, 'landingPageAction']);
            }
        }
    }
}
