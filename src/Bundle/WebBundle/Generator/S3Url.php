<?php

namespace Connect\Bundle\WebBundle\Generator;

class S3Url
{
    /**
     * @var string
     */
    private $s3Url;

    /**
     * S3Url constructor.
     * @param string $s3Url
     */
    public function __construct($s3Url)
    {
        $this->s3Url = trim($s3Url, '/');
    }

    /**
     * @param string $path
     * @return string
     */
    public function generate($path)
    {
        return sprintf(
            '%s/%s',
            $this->s3Url,
            trim($path, '/')
        );
    }
}