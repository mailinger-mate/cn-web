<?php

namespace Connect\Bundle\WebBundle\Markdown;

use Knp\Bundle\MarkdownBundle\Parser\MarkdownParser;
use Symfony\Component\Form\DataTransformerInterface;

class Parser extends MarkdownParser
{
    /**
     * @var DataTransformerInterface
     */
    private $imageUrlTransformer;

    /**
     * {@inheritdoc}
     */
    public function transformMarkdown($text, DataTransformerInterface $imageUrlTransformer = null)
    {
        $this->imageUrlTransformer = $imageUrlTransformer;

        return parent::transformMarkdown($text);
    }

    /**
     * {@inheritdoc}
     */
    protected function _doImages_inline_callback($matches)
    {
        if ($matches[3] === '') {
            $url = &$matches[4];
        } else {
            $url = &$matches[3];
        }

        if (!is_null($this->imageUrlTransformer)) {
            $url = $this->imageUrlTransformer->transform($url);
        }

        return parent::_doImages_inline_callback($matches);
    }
}