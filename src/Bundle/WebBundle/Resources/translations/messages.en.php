<?php

return array(
    'link'   => array(
        'features'  => 'Features',
        'apps'      => 'Apps',
        'pricing'   => 'Pricing',
        'about'   => 'About us',
        'blog' => 'Blog'
      ),
    'top_page'   => array(
        'title' => 'Connect the <a href="#apps" class="page-scroll">apps</a> you use every day',
        'text'  => 'Automate your business and save your company\'s time, money and resources.',
        'placeholder' => 'Your email address',
        'button' => 'START YOUR 30-DAY TRIAL'
    ),
    'feature'  => array(
        'title'  => 'Overview of the greatest features',
        'text'   => 'Take a quick walk through Connect’s features and see how easily you can automate lots of repetitive tasks and save lots of time',
        'section' => array(
            'real_time' => array(
                'title' => 'Real-time data',
                'subtitle' => 'Immediate flow of information',
                'text' => '<p>For most of your applications, Connect can extract and forward information in real time, giving you immediate access to the data you need so that you can respond quickly and effectively.</p><p>Waiting for important information that you don’t want to miss or wading through large amounts of data in search of one item is often a waste of time. With Connect, you can forget about the habit of compulsively checking the status of your applications and instead focus on tasks that really matter.</p>',
                'link' => 'https://docs.x-formation.com/display/CN/Monitoring+applications+in+real+time'

            ),
            'content_filtering' => array(
                'title' => 'Advanced filters',
                'subtitle' => 'Focus on data that matters',
                'text' => '<p>Notifications can be a distraction, especially for emails that pile up quickly. If you’ve ever wondered how to take full control of your email delivery, look no further!</p><p>Connect lets you configure its built-in filter to receive updates only about the emails you care about by setting rules like forwarding emails based on the sender’s address or with a specific subject to a given destination.</p>',
                'link' => 'https://docs.x-formation.com/display/CN/Content+filtering'
            ),
            'extract' => array(
                'title' => 'Content extractions',
                'subtitle' => 'Forward only relevant information',
                'text' => '<p>Connect extracts parts from a large source message based on defined keywords and forwards such information further — even to destinations that do not support filtering at all!</p><p>Wouldn’t it be great if you could use HipChat’s message output area to get latest information you have received via email, such as the booking reference number for the flight you’re about to take, the date of the delivery of the product you’ve ordered or whatever information you wish to see?</p>',
                'link' => 'https://docs.x-formation.com/display/CN/Extracting+content+based+on+defined+keywords'
            ),
            'multiple_destinations' => array(
                'title' => 'Multiple destinations',
                'subtitle' => 'Simultaneous flow of information from one app to multiple apps',
                'text' => '<p>With Connect, you can quickly design your own robust integrations to get data and content from one source and simultaneously push it to multiple destinations. </p><p>Connect can read from and write to multiple of popular cloud applications to better adapt to the way you work. Services Connect can sync include Twitter, HipChat, RSS, Email and Webhook.</p>',
                'link' => 'https://docs.x-formation.com/display/CN/One+source+and+multiple+destinations'
            ),
            'cloud' => array(
                'title' => 'Online access',
                'subtitle' => 'Software as a Service',
                'text' => '<p>Connect is an online service, so you can access it on demand, anywhere you are connected.</p><p>As a cloud-based application, Connect does not require any upfront investment such as servers or disk space, sparing you lots of money, installation hassles and maintenance worries. You will always have the latest upgrades simply by logging in. </p><p>Connect also gives you a clear idea of what your costs will be, allowing for more precise budget forecast than in case of on-premise software. </p>',
                'link' => 'https://docs.x-formation.com/display/CN/Software+as+a+Service'
            )
        )
    ),
    'applications'  => array(
        'title' => 'Apps you can connect',
        'text'  => 'Integrate different apps and keep all your business data under control',
        'twitter' => array(
            'title' => 'Twitter',
            'text'  => 'Automatically update your Twitter status or respond to posts on a specific topic published by your favorite followers'
          ),
        'imap' => array(
            'title' => 'Email',
            'text'  => 'Manage hundreds of emails a day by handling them automatically, without any effort on your side'
          ),
        'hipchat' => array(
            'title' => 'HipChat',
            'text'  => 'Push real-time updates or reminders from source applications to a HipChat room or to a specific person'
          ),
        'rss' => array(
            'title' => 'RSS',
            'text'  => 'Automatically check your favorite sites for new updates'
          ),
        'webhook' => array(
            'title' => 'Webhook',
            'text'  => 'Push relevant information to any of your custom business services and applications'
          ),
        'salesforce' => array(
            'title' => 'Salesforce',
            'text'  => 'Instantly react to and manage incoming Salesforce leads, sales or opportunities'
          ),
        'jira' => array(
            'title' => 'Jira',
            'text'  => 'Automatically create a JIRA task based on information you receive from your source applications'
          ),
        'slack' => array(
            'title' => 'Slack',
            'text'  => 'Push real-time updates or reminders from source applications into the Slack channel of your choice',
          ),
    ),
    'future-applications'  => array(
        'title'   => 'Future apps',
        'text'    => 'We are working hard on supporting the following applications in the near future',
        'subtext' => 'We are extending our supported applications regularly as we progress further with our development',
        'mail_chimp' => array(
            'title' => 'MailChimp',
            'text'  => 'Reach your customers effectively by sending automatic email campaigns'
          ),
        'gmail' => array(
            'title' => 'Gmail',
            'text'  => 'Mix and match different Google Apps to automatically move your business data to where you need it most'
          ),
        'stripe' => array(
            'title' => 'Stripe',
            'text'  => 'Get real-time updates about incoming payments without any effort'
          ),
        'confluence' => array(
            'title' => 'Confluence',
            'text'  => 'Get notified when someone extends your product documentation'
          ),
    ),
    'pricing'  => array(
        'title'   => 'Plans and pricing',
        'text'    => 'Enjoy free testing and brace yourself for flexible prices',
        'section_one' => array(
            'title'       => 'No credit card',
            'subtitle'    => 'Test it for free',
            'text'         => 'Available as a service, Connect is a paid application designed for business users. You can access all its features for free by signing up for a trial version. Connect is currently  accessible at no charge as a beta version (we’re still testing it out) and will remain available for unlimited use until we release the official, final version.'
        ),
        'section_two' => array(
            'title'       => 'No commitments',
            'subtitle'    => 'Pay only for what you use',
            'text'         => 'Connect will be used as a postpaid solution and fees for what you used will be automatically charged to your credit each month. Business clients will be able to buy Connect at a highly competitive price, which will depend on the number of simultaneously running connections and the number of messages sent out by the service.'
        ),
        'plan' => array(
            'name'      => 'Standard',
            'price_one' => '$15',
            'line_1'    => '/ month',
            'line_2'    => '30-days trial',
            'line_3'    => '15 Connections',
            'line_4'    => '3000 Messages',
            'line_5'    => 'Free Support'
        ),
    ),
    'cookie'  => array(
        'text'   => 'Cookies help us deliver our services. By using our services, you agree to our use of cookies.',
        'button' => 'I Accept',
        'link'   => 'Learn More'
    ),
    'blog' => array(
        'header' => array(
            'title' => 'Connect N Simplify',
            'subtitle' => 'Get first-hand updates about the latest features, tips to boost your business and information about new integrations you can make with Connect.'
        ),
        'post' => array(
            'read' => 'Read more',
            'back' => 'Back to list'
        )
    )
);
