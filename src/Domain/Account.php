<?php

namespace Connect\Domain;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

/**
 * Class Account
 */
class Account 
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $config;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Account
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Account
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param null $field
     * @return array
     */
    public function getConfig($field = null)
    {
        if ($field === null) {
            return $this->config;
        }

        if (array_key_exists($field, $this->config)) {
            return $this->config[$field];
        }

        throw new InvalidArgumentException('Config does not contain field: ' . $field);
    }

    /**
     * @param array $config
     * @return Account
     */
    public function setConfig(array $config)
    {
        $this->config = $config;

        return $this;
    }
}