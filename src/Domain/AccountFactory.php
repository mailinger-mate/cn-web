<?php

namespace Connect\Domain;

/**
 * AccountFactory
 */
class AccountFactory 
{
    /**
     * @param array $data
     * @return Account
     */
    public function createFromArray(array $data)
    {
        return (new Account())
            ->setId((int)$data['id'])
            ->setName($data['name'])
            ->setType($data['type'])
            ->setConfig($data['config']);
    }
}