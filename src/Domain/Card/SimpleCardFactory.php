<?php

namespace Connect\Domain\Card;

/**
 * Class SimpleCardFactory
 */
class SimpleCardFactory
{
    /**
     * @param array $data
     * @return SimpleCard
     */
    public function createFromArray(array $data)
    {
        return (new SimpleCard())
            ->setBrand($data['brand'])
            ->setExpirationMonth($data['exp_month'])
            ->setExpirationYear($data['exp_year'])
            ->setLastFourDigits($data['last4']);
    }
}