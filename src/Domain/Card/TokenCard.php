<?php

namespace Connect\Domain\Card;

/**
 * Class TokenCard
 */
class TokenCard extends AbstractCard
{
    private $cardToken;

    /**
     * @return mixed
     */
    public function getCardToken()
    {
        return $this->cardToken;
    }

    /**
     * @param mixed $cardToken
     * @return TokenCard
     */
    public function setCardToken($cardToken)
    {
        $this->cardToken = $cardToken;

        return $this;
    }
}