<?php

namespace Connect\Domain;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Contact
 */
class Contact
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $value;
    /**
     * @var string
     */
    private $type;
    /**
     * @var bool
     */
    private $verified;

    /**
     * @var User
     */
    private $user;

    /**
     * @var ContactAction[]
     */
    private $actions;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        $this->actions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVerified()
    {
        return $this->verified;
    }

    /**
     * @param boolean $verified
     * @return $this
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user->getId();
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param ContactAction $action
     * @return Contact
     */
    public function addAction(ContactAction $action)
    {
        $this->actions->add($action);

        return $this;
    }

    /**
     * @param ContactAction $action
     */
    public function removeAction(ContactAction $action)
    {
        $this->actions->removeElement($action);
    }

    /**
     * @return ContactAction[]
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param string $type
     * @return ContactAction|false
     */
    public function getAction($type)
    {
        foreach ($this->actions as $action) {
            if ($action->getType() === $type) {
                return $action;
            }
        }

        return false;
    }
}
