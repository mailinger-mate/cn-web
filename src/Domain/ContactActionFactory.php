<?php

namespace Connect\Domain;

use DateTime;

/**
 * Class ContactActionFactory
 */
class ContactActionFactory
{
    /**
     * @param array $data
     * @param Contact $contact
     * @return ContactAction
     */
    public function createFromArray(array $data, Contact $contact)
    {
        return (new ContactAction())
            ->setId($data['id'])
            ->setExpirationTime(new DateTime($data['expiration_time']))
            ->setType($data['type'])
            ->setContact($contact);
    }
}