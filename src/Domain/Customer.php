<?php

namespace Connect\Domain;

use Connect\Domain\Card\AbstractCard;
use Connect\Domain\Card\SimpleCard;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Customer
 */
class Customer
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var ArrayCollection|SimpleCard[]
     */
    private $cards;

    /**
     * @var Vendor
     */
    private $vendor;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Customer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return AbstractCard|null
     */
    public function getCard()
    {
        return $this->cards->count() !== 1 ? null : $this->cards->first();
    }

    /**
     * @param AbstractCard $card
     * @return Customer
     */
    public function setCard(AbstractCard $card)
    {
        $this->cards->clear();
        $this->cards->add($card);

        return $this;
    }

    /**
     * @return $this
     */
    public function removeCard()
    {
        $this->cards->clear();

        return $this;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @return integer
     */
    public function getVendorId()
    {
        return $this->vendor->getId();
    }

    /**
     * @param Vendor $vendor
     * @return Customer
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;

        return $this;
    }
}