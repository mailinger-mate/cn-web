<?php

namespace Connect\Domain;

/**
 * Class Invoice
 */
class Invoice
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $pdfName;
    /**
     * @var string
     */
    private $pdfContent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getPdfName()
    {
        return $this->pdfName;
    }

    /**
     * @param string $pdfName
     * @return $this
     */
    public function setPdfName($pdfName)
    {
        $this->pdfName = $pdfName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPdfContent()
    {
        return $this->pdfContent;
    }

    /**
     * @param string $pdfContent
     * @return $this
     */
    public function setPdfContent($pdfContent)
    {
        $this->pdfContent = $pdfContent;

        return $this;
    }
}