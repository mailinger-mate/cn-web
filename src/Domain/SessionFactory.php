<?php

namespace Connect\Domain;


/**
 * Class SessionFactory
 */
class SessionFactory
{
    /**
     * @param array $data
     * @param User $user
     * @return Session
     */
    public function createFromArray(array $data, User $user = null)
    {
        $session = (new Session())
            ->setId($data['id']);
        if (!is_null($user)) {
            $session->setUser($user);
        }

        return $session;
    }
}