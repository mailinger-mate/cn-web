<?php

namespace Connect\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use LogicException;

/**
 * Class User
 */
class User
{
    /**
     * @var
     */
    private $id;

    /**
     * @var Vendor
     */
    private $vendor;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $activated;

    /**
     * @var boolean
     */
    private $enabled;

    /**
     * @var ArrayCollection
     */
    private $contacts;

    /**
     * @var UserAction[]
     */
    private $actions;

    function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->actions = new ArrayCollection();
    }

    /**
     * @param boolean $activated
     * @return $this
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isActivated()
    {
        return $this->activated;
    }

    /**
     * @param boolean $enabled
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Vendor $vendor
     * @return $this
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @return int
     */
    public function getVendorId()
    {
        return $this->vendor->getId();
    }

    /**
     * @param Contact $contact
     * @return User
     */
    public function addContact(Contact $contact)
    {
        if (is_null($this->contacts)) {
            $this->contacts = new ArrayCollection();
        }

        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @return Contact
     */
    public function getPrimaryContact()
    {
        if ($this->contacts->count() !== 1) {
            throw new LogicException('User do not have any contacts');
        }

        return $this->contacts->first();
    }

    /**
     * @return Contact
     */
    public function getPrimaryContactValue()
    {
        return $this->getPrimaryContact()->getValue();
    }

    /**
     * @return Contact
     */
    public function isPrimaryContactVerified()
    {
        return $this->getPrimaryContact()->isVerified();
    }

    /**
     * @return UserAction[]
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param $type
     * @return UserAction
     */
    public function getAction($type)
    {
        foreach ($this->actions as $action) {
            if ($action->getType() === $type) {
                return $action;
            }
        }

        return false;
    }

    /**
     * @param UserAction $action
     * @return User
     */
    public function addAction($action)
    {
        $this->actions->add($action);

        return $this;
    }

    /**
     * @param UserAction $action
     */
    public function removeAction($action)
    {
        $this->actions->removeElement($action);
    }
}
