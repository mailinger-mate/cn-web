<?php

namespace Connect\Domain;

use DateTime;

/**
 * Class UserAction
 */
class UserAction
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $type;
    /**
     * @var DateTime
     */
    private $expirationTime;
    /**
     * @var User
     */
    private $user;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return UserAction
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return UserAction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpirationTime()
    {
        return $this->expirationTime;
    }

    /**
     * @param DateTime $expirationTime
     * @return UserAction
     */
    public function setExpirationTime(DateTime $expirationTime)
    {
        $this->expirationTime = $expirationTime;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserAction
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}