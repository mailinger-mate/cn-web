<?php

namespace Connect\Domain\UserAction;

/**
 * Class Type
 */
class Type
{
    const PASSWORD_RESET = 'password_reset';
    const ACCOUNT_ACTIVATE = 'account_activate';
}