<?php

namespace Connect\Domain;

use DateTime;

/**
 * Class UserActionFactory
 */
class UserActionFactory
{
    /**
     * @param array $data
     * @param User $user
     * @return UserAction
     */
    public function createFromArray(array $data, User $user)
    {
        return (new UserAction())
            ->setId($data['id'])
            ->setExpirationTime(new DateTime($data['expiration_time']))
            ->setType($data['type'])
            ->setUser($user);
    }
}