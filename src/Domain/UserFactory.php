<?php

namespace Connect\Domain;

/**
 * Class UserFactory
 */
class UserFactory
{
    /**
     * @param array $data
     * @param Vendor $vendor
     * @return User
     */
    public function createFromArray(array $data, Vendor $vendor)
    {
        return (new User())
            ->setId($data['id'])
            ->setActivated($data['activated'])
            ->setEnabled($data['enabled'])
            ->setName($data['name'])
            ->setVendor($vendor);
    }
}
