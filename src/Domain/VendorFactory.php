<?php

namespace Connect\Domain;

/**
 * Class VendorFactory
 */
class VendorFactory
{
    /**
     * @param array $data
     * @param Customer $customer
     * @return Vendor
     */
    public function createFromArray(array $data, Customer $customer)
    {
        return (new Vendor())
            ->setId((int)$data['id'])
            ->setName($data['name'])
            ->setCustomer($customer)
            ->setBillingName($this->get('billing_name', $data))
            ->setStreet($this->get('street', $data))
            ->setCity($this->get('city', $data))
            ->setState($this->get('state', $data))
            ->setCountry($this->get('country', $data))
            ->setVatNumber($this->get('vat_number', $data))
            ->setNote($this->get('note', $data))
            ->setPoNumber($this->get('po_number', $data));
    }

    /**
     * @param $key
     * @param array $data
     * @param mixed $default
     * @return mixed
     */
    private function get($key, array $data, $default = null)
    {
        return array_key_exists($key, $data) ? $data[$key] : $default;
    }
}
