/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $$state, $digest, abstract, account, accounts, and, any, api_url, auth,
    callFake, callThrough, check, config, connection, controller,
    controllerProvider, core, createSpy, current, data, defer, destination,
    error, flush, get, go, id, level, load, loadPlugin, name,
    objectContaining, pageTitle, path, prefix, redirect, resolve, respond,
    return, returnValue, session, some, status, template, templateUrl, then,
    toBe, toEqual, toHaveBeenCalledWith, token, type, unauthorized, url, user,
    value, whenGET
*/

describe("config: cn.account", function () {
    'use strict';
    var cfg, $state, helper, $rootScope, action, account, $timeout, $q, $httpBackend, ocLazyLoad, $stateParams;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var ocLazyLoadMocked = {
        load: jasmine.createSpy().and.callFake(function () {
            return "loaded";
        })
    };
    var accountMock = {type: 'type'};
    var cfgMock = {
        account: {
            config: {
                type: {name: ''}
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.account');
        module('cn.action');
        module('cn.helper');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        module(function ($provide) {
            $provide.value('account', accountMock);
        });
        module(function ($provide) {
            $provide.value('ocLazyLoad', ocLazyLoadMocked);
        });
        inject(function (_$rootScope_, _cfg_, _$state_, _action_, _$httpBackend_, _ocLazyLoad_, _$stateParams_, _helper_, _account_, _$q_, _$timeout_) {
            $timeout = _$timeout_;
            $q = _$q_;
            $rootScope = _$rootScope_;
            $stateParams = _$stateParams_;
            helper = _helper_;
            account = _account_;
            ocLazyLoad = _ocLazyLoad_;
            $httpBackend = _$httpBackend_;
            action = _action_;
            cfg = _cfg_;
            $state = _$state_;

        });
        $httpBackend.whenGET('/health_check/e909fcd76cc25bb2a4bb19f17ea466e4').respond(200);
    });

    it('state account works correctly', function () {
        var state = 'account';
        $state.go(state);
        $rootScope.$digest();
        expect($state.current).toEqual(jasmine.objectContaining({
            url: "/account",
            template: "<div ui-view autoscroll='false'></div>",
            redirect: "account.list",
            name: state
        }));
    });

    it('state account.list works correctly', function () {
        spyOn(action.account, 'get').and.callFake(function () {
            return {
                then: function (accounts) {
                    return accounts([{some: 'accounts'}]);
                }
            };
        });
        var state = $state.get('account.list');
        expect(state.name).toBe('account.list');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/list",
            controller: "cnAccountList",
            templateUrl: "html/account/list.html",
            data: {pageTitle: 'nav.account.list'},
            name: 'account.list',
            resolve: jasmine.any(Object)
        }));
        expect(state.resolve.accounts($state, $q, action).$$state.value).toEqual([{some: 'accounts'}]);
        expect(state.resolve.loadPlugin(ocLazyLoad)).toEqual('loaded');
    });

    it('state account.list works correctly (rejected)', function () {
        spyOn($state, 'go').and.callThrough();
        spyOn(action.account, 'get').and.callFake(function () {
            return {
                then: function (accounts) {
                    return accounts({});
                }
            };
        });
        var state = $state.get('account.list');
        expect(state.name).toBe('account.list');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/list",
            controller: "cnAccountList",
            templateUrl: "html/account/list.html",
            data: {pageTitle: 'nav.account.list'},
            name: 'account.list',
            resolve: jasmine.any(Object)
        }));
        expect(state.resolve.accounts($state, $q, action).$$state.value.$$state).toEqual({status: 0});
        expect(state.resolve.loadPlugin(ocLazyLoad)).toEqual('loaded');
        expect($state.go).toHaveBeenCalledWith('account.none');
    });

    it('abstract state account.item works correctly', function () {
        var state = $state.get('account.item');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/item?auth&connection&destination",
            controller: "cnAccountItem",
            templateUrl: "html/account/item.html",
            abstract: true,
            name: 'account.item'
        }));
    });

    it('state account.item resolve return true', function () {
        var state = $state.get('account.item');
        expect(state.resolve.auth($stateParams, action)).toEqual(true);
    });

    it('state account.item.add works correctly (controller provider)', function () {
        $stateParams.type = 'type';
        var state = $state.get('account.item.add');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/add/:type?account",
            templateUrl: "html/account/item.form.html",
            data: {pageTitle: 'account.item.title.add'},
            name: 'account.item.add',
            resolve: jasmine.any(Object),
            controllerProvider: jasmine.any(Function)
        }));
        expect(state.controllerProvider($stateParams, helper, cfg)).toEqual('cnAccountItemType');
        cfg.account.config.type = undefined;
        expect(state.controllerProvider($stateParams, helper, cfg)).toEqual('cnAccountItemDefault');
    });

    it('state account.item.add works correctly (resolve return false)', function () {
        $stateParams.type = 'type';
        var state = $state.get('account.item.add');
        expect(state.resolve.return($state, $stateParams, $q, $timeout).$$state.value).toEqual(false);
    });

    it('state account.item.add works correctly (resolve return source add) $stateParams.connection', function () {
        spyOn($state, 'go');
        $stateParams.type = 'type';
        $stateParams.account = 'account';
        $stateParams.connection = 'add';
        var state = $state.get('account.item.add');
        expect(state.resolve.return($state, $stateParams, $q, $timeout));
        $timeout.flush();
        expect($state.go).toHaveBeenCalledWith('connection.item.add.source', {type: 'type', account: 'account'});
    });

    it('state account.item.add works correctly (resolve return destination add) $stateParams.connection', function () {
        spyOn($state, 'go');
        $stateParams.type = 'type';
        $stateParams.account = 'account';
        $stateParams.connection = 'connection';
        $stateParams.destination = 'destination';
        var state = $state.get('account.item.add');
        expect(state.resolve.return($state, $stateParams, $q, $timeout));
        $timeout.flush();
        expect($state.go).toHaveBeenCalledWith('connection.item.edit.destination.add', {
            type: 'type',
            account: 'account',
            connection: 'connection'
        });
    });

    it('state account.item.edit works correctly', function () {
        $stateParams.account = 5;
        spyOn(action.account, 'get').and.returnValue({id: $stateParams.account});
        var state = $state.get('account.item.edit');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/edit/:account",
            templateUrl: "html/account/item.form.html",
            data: {pageTitle: 'account.item.title.edit'},
            name: 'account.item.edit',
            resolve: jasmine.any(Object),
            controllerProvider: jasmine.any(Function)
        }));
        expect(state.resolve.account($stateParams, action)).toEqual({id: 5});
        expect(state.controllerProvider(helper, account)).toEqual('cnAccountItemType');
    });
});
