/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, button, css, icon, name, status, title, toEqual, toHaveAttr,
    toHaveClass, type, value
*/

describe("config: cn.account", function () {
    'use strict';
    var $compile, scope, $rootScope;

    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.account');
        module("cn.config.style");

        inject(function (_$compile_, _$rootScope_) {
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $compile = _$compile_;
        });
    });

    describe("directive: AccountType", function () {

        it('AccountType directive works correctly', function () {
            var element = $compile("<cn-account-type>")(scope);
            scope.$digest();
            expect(scope.title).toEqual('account.item.select.type.unknown');
            expect(element).toHaveClass('ng-scope');
            expect(element).toHaveAttr('title');
            element = $compile("<cn-account-type type='test'>")(scope);
            scope.$digest();
            expect(scope.title).toEqual('account.item.select.type.test');
            expect(element).toHaveClass('test');
            expect(element).toHaveAttr('title');
        });
    });
});
