/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, initAccount, log, not, toBeDefined, toBeTruthy,
    toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnAccountItemHipchat", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemHipchat, account, accountMock;
    accountMock = {account: 'account'};
    beforeEach(function () {
        module('cn.account.item.hipchat');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function ($controller, _$rootScope_, _account_) {
            $rootScope = _$rootScope_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemHipchat = function () {
                return $controller('cnAccountItemHipchat', {
                    $scope: scope,
                    account: account
                });
            };
        });
    });

    it('cnAccountItemHipchat controller is defined and works fine', function () {
        expect(cnAccountItemHipchat).toBeDefined();
        expect(cnAccountItemHipchat()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemHipchat();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'hipchat', account: accountMock});
    });

});