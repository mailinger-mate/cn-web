/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, $pristine, $scope, $setPristine, account, cfg, config, ctrl,
    email, email_provider, field, form, host, imap, initAccount, not, provider,
    toBeDefined, toBeTruthy, toEqual, toHaveBeenCalled, toHaveBeenCalledWith,
    type, value
*/


describe("controller: cnAccountItemImap", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemImap, account, accountMock, cfgMock, cfg;
    accountMock = {account: 'account'};
    cfgMock = {
        email: {
            provider: [{
                imap: {
                    host: 'hostMock'
                }
            }]
        }
    };
    beforeEach(function () {

        module('cn.account.item.imap');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function ($controller, _$rootScope_, _account_, _cfg_) {
            $rootScope = _$rootScope_;
            cfg = _cfg_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemImap = function () {
                return $controller('cnAccountItemImap', {
                    $scope: scope,
                    account: account,
                    cfg: cfg
                });
            };
        });
        scope.form = {
            ctrl: {
                host: {
                    $pristine: {
                    },
                    $setPristine: function () {
                        return;
                    }
                }
            },
            config: {
                email_provider: {
                    imap: {
                        host: {
                        }
                    }
                }
            },
            field: {
                config: {
                    host: 'imapHost'
                }
            }
        };
    });

    it('cnAccountItemImap controller is defined and works fine', function () {
        expect(cnAccountItemImap).toBeDefined();
        expect(cnAccountItemImap()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemImap();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'imap', account: accountMock});

    });

    it('watch form.config.email_provider', function () {
        spyOn(scope.form.ctrl.host, '$setPristine');
        scope.form.config.email_provider.imap.host = 'imapHost';
        scope.form.ctrl.host.$pristine = false;
        cnAccountItemImap();
        scope.$digest();
        expect(scope.form.ctrl.host.$setPristine).toHaveBeenCalled();
        scope.form.config.email_provider.imap.host = 'imapHost';
        scope.form.ctrl.host.$pristine = true;
        scope.$digest();
        expect(scope.form.field.config.host).toEqual(scope.form.config.email_provider.imap.host);

    });

    it('watch form.field.config.host', function () {
        cnAccountItemImap();
        scope.$digest();
        expect(scope.form.field.config.host).toEqual(cfg.email.provider[0].imap.host);
        scope.form.config.email_provider.imap.host = null;
        scope.$digest();
        expect(scope.form.config.email_provider).toEqual(cfg.email.provider[0]);
    });
});