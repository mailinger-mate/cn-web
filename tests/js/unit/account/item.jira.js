/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, initAccount, log, not, toBeDefined, toBeTruthy,
    toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnAccountItemJira", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemJira, account, accountMock;
    accountMock = {account: 'account'};
    beforeEach(function () {
        module('cn.account.item.jira');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function ($controller, _$rootScope_, _account_) {
            $rootScope = _$rootScope_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemJira = function () {
                return $controller('cnAccountItemJira', {
                    $scope: scope,
                    account: account
                });
            };
        });
    });

    it('cnAccountItemJira controller is defined and works fine', function () {
        expect(cnAccountItemJira).toBeDefined();
        expect(cnAccountItemJira()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemJira();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'jira', account: accountMock});
    });

});