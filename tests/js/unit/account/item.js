/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $current, $digest, $error, $new, $scope, $stateParams, $valid,
    account, action, and, any, api_url, authAccount, back, button, callFake,
    callThrough, cfg, cfg1, cfg2, check, class, config, connect,
    connectAccount, connection, core, createSpy, ctrl, data, defer,
    deleteAccount, destination, email_provider, error, field, flush, form, go,
    goBack, history, id, initAccount, label, login_endpoint, mode, name, not,
    oauth, objectContaining, params, path, promise, reset, resolve,
    respond, returnValue, return_url, rpc_url, salesforce, save, saveAccount, session,
    some, success, test, toBeDefined, toBeFalsy, toBeTruthy, toEqual,
    toHaveBeenCalled, toHaveBeenCalledWith, token, type, unauthorized, update,
    url, user, validateForm, value, view, wait, whenPOST
*/

describe("module: cn.account.item", function () {
    'use strict';
    var scope, $q, deferred, cnAccountItemDefault, $httpBackend, core, $window, $state, $stateParams, action, $rootScope, cfg, cnAccountItem;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var buttonAfter = [{
        label: "save",
        type: "submit",
        action: "saveAccount",
        class: "btn-primary"
    },
            {
        label: "back",
        type: "link",
        action: "goBack",
        class: "btn-default"
    }];
    var cfgMock = {
        account: {
            oauth: {
                salesforce: {
                    url: 'outh/salesforce/'
                }
            },
            config: {
                cfg1: 'cfg1',
                cfg2: 'cfg2'
            }
        },
        connection: {
            config: {
                destination: ''
            }
        },
        core: {
            api_url: 'api_url/',
            rpc_url: 'rpc_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        $window = {history: {back: jasmine.createSpy()}};
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.action');
        module('cn.account.item');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('$window', $window);
        });
        inject(function (_$httpBackend_, _$q_, $controller, _$rootScope_, _cfg_, _$stateParams_, _$state_, _core_, _action_) {
            $httpBackend = _$httpBackend_;
            core = _core_;
            $q = _$q_;
            deferred = $q.defer();
            action = _action_;
            cfg = _cfg_;
            $stateParams = _$stateParams_;
            $state = _$state_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            cnAccountItem = $controller('cnAccountItem', {
                $scope: scope,
                cfg: cfg,
                core: core,
                action: action,
                $stateParams: $stateParams
            });
            cnAccountItemDefault = function () {
                return $controller('cnAccountItemDefault', {
                    $scope: scope
                });
            };

        });
        scope.data = {
            account: {
                id: 2
            }
        };
        scope.form = {
            action: {
                success: ''
            },
            error: {
                field: ''
            },
            field: {
                config: {
                    test: 'test',
                    login_endpoint: 'loginEndpointMocked'
                },
                name: 'mockedName'
            },
            button: {
                account: {
                }
            },
            config: {
                oauth: {
                    config: {some: 'test'}
                },
                type: 'testType',
                field: {
                    name: {
                        reset: ''
                    },
                    config: 'test'
                },
                account: {
                    email_provider: ''
                }
            }
        };
    });
    it('cnAccountItem is defined', function () {
        expect(cnAccountItem).toBeDefined();
    });
    it('initAccount: scopes are removed after scope.initAccount', function () {
        scope.initAccount({});
        expect(scope.view.mode).toEqual('add');
        expect(scope.form.config.email_provider).toEqual({});
        expect(scope.data.account).not.toBeDefined();
        expect(scope.form.config.oauth).not.toBeDefined();
        expect(scope.form.config.type).not.toBeDefined();
    });

    it('initAccount: if config.type in initAccount', function () {
        var config = {
            type: 'salesforce'
        };
        scope.initAccount(config);
        expect(scope.view.mode).toEqual('add');
        expect(scope.form.config.email_provider).toEqual({});
        expect(scope.form.config.oauth).toEqual(cfg.account.oauth[config.type]);
        expect(scope.form.button.config[0]).toEqual(jasmine.objectContaining({
            label: "authorize",
            type: "button",
            action: "authAccount",
            class: "btn-primary",
            wait: true
        }));
    });

    it('initAccount: if config.account in initAccount', function () {
        var config = {account: {name: 'test', config: 'mockedConfig'}, type: 'typeMock'};
        scope.initAccount({type: 'typeMock'});
        expect(scope.form.field.name).toEqual('account.item.select.type.typeMock');
        expect(scope.form.config.field.name.reset).toBeTruthy();
        scope.initAccount(config);
        expect(scope.data.account).toEqual({name: 'test', config: 'mockedConfig'});
        expect(scope.view.mode).toEqual('edit');
        expect(scope.form.field).toEqual({name: 'test', config: 'mockedConfig'});
    });

    it('initAccount: scope.form.button.push works fine', function () {
        expect(scope.form.button.account).toEqual({});
        scope.initAccount({account: null});
        expect(scope.form.button.account).toEqual(buttonAfter);

    });

    it('saveAccount: scope.validateForm view mode edit and empty', function () {
        scope.form.action = 'success';
        spyOn(scope, 'validateForm').and.returnValue({$valid: true});
        spyOn(action, 'run').and.callThrough();
        spyOn(core.account, 'update').and.returnValue(deferred.promise);
        deferred.resolve({});
        scope.view.mode = 'edit';
        scope.saveAccount(scope);
        scope.$digest();
        expect(core.account.update).toHaveBeenCalledWith({id: 2}, {name: 'mockedName'});
        expect(scope.form.error.field).toEqual({});
        spyOn(core.account, 'save').and.returnValue(deferred.promise);
        deferred.resolve();
        scope.view.mode = '';
        scope.saveAccount(scope);
        expect(core.account.save).toHaveBeenCalledWith({type: 'testType', name: 'mockedName', config: {test: 'test', login_endpoint: 'loginEndpointMocked'}});


    });

    it('save account: if form invalid return false', function () {
        spyOn(scope, 'validateForm').and.returnValue({$valid: false});
        expect(scope.saveAccount(scope)).toBeFalsy();
    });

    it('connectAccount: return false or if data.account exist return', function () {
        spyOn(action.account, 'connect');
        scope.connectAccount(scope);
        expect(action.account.connect).toHaveBeenCalledWith({name: 'mockedName', id: 2, config: {test: 'test', login_endpoint: 'loginEndpointMocked'}, success: jasmine.any(Function)});
        scope.data.account = null;
        spyOn(scope, 'validateForm').and.returnValue({$valid: false});
        scope.$digest();
        expect(scope.connectAccount(scope)).toEqual(false);
    });

    it('connectAccount: if form invalid', function () {
        $httpBackend.whenPOST('api_url/accounts').respond(200);
        spyOn(scope.form.action, 'success').and.callFake(function (myAction) {
            return myAction;
        });
        spyOn(action.account, 'connect');
        spyOn(scope, 'validateForm').and.returnValue({$valid: true});
        scope.data.account = null;
        scope.connectAccount(scope);
        expect(scope.form.action.success(action)).toEqual(action);
        $httpBackend.flush();
    });

    it('authAccount return false', function () {
        spyOn(scope, 'validateForm').and.returnValue({$valid: false});
        expect(scope.authAccount(scope)).toBeFalsy();
    });

    it('authAccount works fine', function () {
        var spy = spyOn(action.account, 'auth').and.callFake(function () {
            deferred.resolve('mocked Result');
            return deferred.promise;
        });
        deferred.resolve('solved');
        spyOn(action, 'run').and.callThrough();
        spyOn(scope, 'validateForm').and.returnValue({$valid: true});
        spyOn($state, 'href').and.returnValue('mockedCurrentName');
        scope.authAccount(scope);
        expect(spy).toHaveBeenCalledWith(jasmine.objectContaining({
            id: 2,
            type: "testType",
            name: "mockedName",
            data: {test: 'test'},
            params: {return_url: 'ckedCurrentName'}
        }));
    });

    it('deleteAccount works fine', function () {
        var spy = spyOn(action.account, 'delete').and.callFake(function () {
            deferred.resolve('mocked Result');
            return deferred.promise;
        });
        scope.deleteAccount();
        expect(spy).toHaveBeenCalledWith(jasmine.objectContaining({
            name: 'mockedName',
            id: 2
        }));
    });

    it('validateForm return form if not defined', function () {
        scope.form.ctrl = {$error: {}};
        expect(scope.validateForm()).toEqual(scope.form.ctrl);
    });

    it('validateForm return form', function () {
        scope.form.ctrl = {$error: {}};
        expect(scope.validateForm()).toEqual(scope.form.ctrl);
    });

    it('watch form.config.type works fine', function () {
        spyOn($state, 'go');
        $state.$current.name = 'testState';
        scope.form.ctrl = {$error: {}};
        scope.validateForm();
        scope.form.config.type = 'test';
        scope.$apply();
        scope.form.config.type = 'test2';
        scope.$digest();
        expect($state.go).toHaveBeenCalledWith($state.$current.name, {type: scope.form.config.type});
    });

    it('cnAccountItemDefault is defined and call scope.initAccount', function () {
        spyOn(scope, 'initAccount');
        expect(cnAccountItemDefault).toBeDefined();
        cnAccountItemDefault();
        expect(scope.initAccount).toHaveBeenCalled();
    });

    it('goBack works fine', function () {
        scope.goBack();
        expect($window.history.back).toHaveBeenCalled();
    });
});
