/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, initAccount, log, not, toBeDefined, toBeTruthy,
    toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnAccountItemRss", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemRss, account, accountMock;
    accountMock = {account: 'account'};
    beforeEach(function () {
        module('cn.account.item.rss');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function ($controller, _$rootScope_, _account_) {
            $rootScope = _$rootScope_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemRss = function () {
                return $controller('cnAccountItemRss', {
                    $scope: scope,
                    account: account
                });
            };
        });
    });

    it('cnAccountItemRss controller is defined and works fine', function () {
        expect(cnAccountItemRss).toBeDefined();
        expect(cnAccountItemRss()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemRss();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'rss', account: accountMock});
    });

});