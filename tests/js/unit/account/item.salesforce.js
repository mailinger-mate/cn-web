/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, initAccount, log, not, toBeDefined, toBeTruthy,
    toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnAccountItemSalesforce", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemSalesforce, account, accountMock;
    accountMock = {account: 'account'};
    beforeEach(function () {
        module('cn.account.item.salesforce');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function ($controller, _$rootScope_, _account_) {
            $rootScope = _$rootScope_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemSalesforce = function () {
                return $controller('cnAccountItemSalesforce', {
                    $scope: scope,
                    account: account
                });
            };
        });
    });

    it('cnAccountItemSalesforce controller is defined and works fine', function () {
        expect(cnAccountItemSalesforce).toBeDefined();
        expect(cnAccountItemSalesforce()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemSalesforce();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'salesforce', account: accountMock});
    });

});