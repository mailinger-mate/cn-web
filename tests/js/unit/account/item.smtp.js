/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, $pristine, $scope, $setPristine, account, cfg, config, ctrl,
    email, email_provider, field, form, host, smtp, initAccount, not, provider,
    toBeDefined, toBeTruthy, toEqual, toHaveBeenCalled, toHaveBeenCalledWith,
    type, value
*/


describe("controller: cnAccountItemSmtp", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemSmtp, account, accountMock, cfgMock, cfg;
    accountMock = {account: 'account'};
    cfgMock = {
        email: {
            provider: [{
                smtp: {
                    host: 'hostMock'
                }
            }]
        }
    };
    beforeEach(function () {

        module('cn.account.item.smtp');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function ($controller, _$rootScope_, _account_, _cfg_) {
            $rootScope = _$rootScope_;
            cfg = _cfg_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemSmtp = function () {
                return $controller('cnAccountItemSmtp', {
                    $scope: scope,
                    account: account,
                    cfg: cfg
                });
            };
        });
        scope.form = {
            ctrl: {
                host: {
                    $pristine: {
                    },
                    $setPristine: function () {
                        return;
                    }
                }
            },
            config: {
                email_provider: {
                    smtp: {
                        host: {
                        }
                    }
                }
            },
            field: {
                config: {
                    host: 'smtpHost'
                }
            }
        };
    });

    it('cnAccountItemSmtp controller is defined and works fine', function () {
        expect(cnAccountItemSmtp).toBeDefined();
        expect(cnAccountItemSmtp()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemSmtp();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'smtp', account: accountMock});

    });

    it('watch form.config.email_provider', function () {
        spyOn(scope.form.ctrl.host, '$setPristine');
        scope.form.config.email_provider.smtp.host = 'smtpHost';
        scope.form.ctrl.host.$pristine = false;
        cnAccountItemSmtp();
        scope.$digest();
        expect(scope.form.ctrl.host.$setPristine).toHaveBeenCalled();
        scope.form.config.email_provider.smtp.host = 'smtpHost';
        scope.form.ctrl.host.$pristine = true;
        scope.$digest();
        expect(scope.form.field.config.host).toEqual(scope.form.config.email_provider.smtp.host);

    });

    it('watch form.field.config.host', function () {
        cnAccountItemSmtp();
        scope.$digest();
        expect(scope.form.field.config.host).toEqual(cfg.email.provider[0].smtp.host);
        scope.form.config.email_provider.smtp.host = null;
        scope.$digest();
        expect(scope.form.config.email_provider).toEqual(cfg.email.provider[0]);
    });
});