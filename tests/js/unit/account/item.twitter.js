/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, initAccount, log, not, toBeDefined, toBeTruthy,
    toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnAccountItemTwitter", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemTwitter, account, accountMock;
    accountMock = {account: 'account'};
    beforeEach(function () {
        module('cn.account.item.twitter');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function ($controller, _$rootScope_, _account_) {
            $rootScope = _$rootScope_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemTwitter = function () {
                return $controller('cnAccountItemTwitter', {
                    $scope: scope,
                    account: account
                });
            };
        });
    });

    it('cnAccountItemTwitter controller is defined and works fine', function () {
        expect(cnAccountItemTwitter).toBeDefined();
        expect(cnAccountItemTwitter()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemTwitter();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'twitter', account: accountMock});
    });
});