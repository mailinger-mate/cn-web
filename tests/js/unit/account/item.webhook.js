/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, initAccount, log, not, toBeDefined, toBeTruthy,
    toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnAccountItemWebhook", function () {
    'use strict';
    var scope, $rootScope, cnAccountItemWebhook, account, accountMock;
    accountMock = {account: 'account'};
    beforeEach(function () {
        module('cn.account.item.webhook');
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function ($controller, _$rootScope_, _account_) {
            $rootScope = _$rootScope_;
            account = _account_;
            scope = $rootScope.$new();
            scope.initAccount = function () {
                return true;
            };
            cnAccountItemWebhook = function () {
                return $controller('cnAccountItemWebhook', {
                    $scope: scope,
                    account: account
                });
            };
        });
    });

    it('cnAccountItemWebhook controller is defined and works fine', function () {
        expect(cnAccountItemWebhook).toBeDefined();
        expect(cnAccountItemWebhook()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initAccount');
        expect(scope.initAccount).not.toHaveBeenCalled();
        cnAccountItemWebhook();
        expect(scope.initAccount).toHaveBeenCalledWith({type: 'webhook', account: accountMock});
    });
});