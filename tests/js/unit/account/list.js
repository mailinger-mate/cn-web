/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, DTOptionsBuilder, account, accounts, and, callFake, config,
    connection, createSpy, destination, dtColumnDefs, dtOptions, newOptions,
    objectContaining, orderable, searchable, source, targets, test,
    toBeDefined, toEqual, type, value, withOption
*/

describe("module: cn.account.item", function () {
    'use strict';
    var scope, cnAccountList, $rootScope, DTOptionsBuilder;
    var accountsMock = {account: 'mock'};
    var cfgMock = {
        connection: {
            config: {
                destination: '',
                source: {
                    test: 'test'
                }
            }
        }
    };
    beforeEach(function () {
        DTOptionsBuilder = {
            newOptions: jasmine.createSpy('newOptions').and.callFake(function () {
                return {
                    withOption: jasmine.createSpy('withOption').and.callFake(function () {
                        return {
                            withOption: jasmine.createSpy('withOption').and.callFake(function () {
                                return 'got some data';
                            })

                        };
                    })
                };
            })
        };
        module('ui.router');
        module('cn.account.list');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('accounts', accountsMock);
            $provide.value('DTOptionsBuilder', DTOptionsBuilder);
        });
        inject(function ($controller, _$rootScope_, _DTOptionsBuilder_) {
            DTOptionsBuilder = _DTOptionsBuilder_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            cnAccountList = function () {
                return $controller('cnAccountList', {
                    $scope: scope,
                    DTOptionsBuilder: DTOptionsBuilder
                });
            };
        });
    });
    it('cnAccountItem is defined', function () {
        expect(cnAccountList).toBeDefined();
        cnAccountList();
    });

    it('DTOptionsBuilder works', function () {
        cnAccountList();
        expect(scope.dtOptions).toEqual('got some data');
    });

    it('dtColumnDefs', function () {
        cnAccountList();
        expect(scope.dtColumnDefs).toEqual(jasmine.objectContaining([
            {
                targets: 'no-sort',
                orderable: false
            },
            {
                targets: [0],
                searchable: false
            }
        ]));
    });

    it('accounts exist', function () {
        cnAccountList();
        expect(scope.accounts).toEqual({account: 'mock'});
    });

    it('scope type exist', function () {
        cnAccountList();
        expect(scope.type).toEqual({source: ['test']});
    });
});