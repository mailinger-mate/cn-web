/*global core describe module  beforeEach afterEach it expect jasmine inject spyOn window*/
/*property
    $digest, $new, $promise, Stripe, account, accountInfo, add, and, any,
    api_url, auth, billing, call, callFake, callThrough, card, card_token,
    check, config, connect, connection, contact, core, createSpy, css, data,
    defer, delete, edit, email, error, expect, field, field1, field2, flush,
    get, id, info, invoice, level, logout, message, mocked, name, not,
    notification, notify, objectContaining, params, path, prefix, promise,
    property, push, query, reject, resolve, resource, respond, returnValue,
    run, session, show, some, status, success, text, toEqual, toHaveBeenCalled,
    toHaveBeenCalledWith, token, translate, trypay, type, unauthorized, update,
    user, value, vendor, when
*/

describe('module: cn.action', function () {

    'use strict';

    var core, $httpBackend, scope, session, $rootScope, action, $q, config, notification;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var mockedCfg = {
        css: {
            notification: ''
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        module('ngResource');
        module('pascalprecht.translate');
        module('cn.session');
        module('cn.action');
        module(function ($provide) {
            $provide.value('cfg', mockedCfg);
        });
    });

    beforeEach(inject(function (_$httpBackend_, _core_, _session_, _action_, _$q_, _$rootScope_, _notification_) {

        $rootScope = _$rootScope_;
        scope = $rootScope.$new();
        session = _session_;
        $q = _$q_;
        action = _action_;
        core = _core_;
        $httpBackend = _$httpBackend_;
        notification = _notification_;

        $httpBackend.when('GET', 'notify.html').respond("<h1>notification template</h1>");
    }));

    it('action run service works (success function)', function () {
        spyOn(notification, 'push').and.callThrough();
        var deferred = $q.defer();
        deferred.resolve('data in promise');
        spyOn(core.connection, 'query').and.returnValue(deferred.promise);
        config = {
            resource: 'mockedResource',
            name: 'mockedName',
            notify: {mocked: 'notify', translate: 'mockedTranslate'},
            success: jasmine.createSpy(config, 'success'),
            call: {
                $promise: core.connection.query()
            }
        };
        action.run(config);
        scope.$digest();
        expect(config.success).toHaveBeenCalledWith('data in promise');
        expect(notification.push).toHaveBeenCalledWith(jasmine.objectContaining({
            level: "success",
            prefix: 'mockedResource',
            name: 'mockedName',
            translate: 'mockedTranslate'
        }));
    });

    it('action run service works (error function)', function () {
        spyOn(notification, 'push').and.callThrough();
        var deferred = $q.defer();
        deferred.reject({data: {message: {mocked: 'message'}}});
        spyOn(core.connection, 'query').and.returnValue(deferred.promise);
        config = {
            resource: 'mockedResource',
            name: 'mockedName',
            notify: {mocked: 'notify', translate: 'mockedTranslate'},
            error: jasmine.createSpy(config, 'error'),
            call: {
                $promise: core.connection.query()
            }
        };
        action.run(config);
        scope.$digest();
        expect(config.error).toHaveBeenCalledWith({data: {message: {mocked: 'message'}}});
        expect(notification.push).toHaveBeenCalledWith(jasmine.objectContaining({
            level: "error",
            prefix: 'mockedResource',
            name: 'mockedName',
            translate: 'mockedTranslate',
            text: ['mockedResource', 'notification', 'mockedName', 'error']
        }));
    });

    it('action run service works (error function) error data non string', function () {
        spyOn(notification, 'push').and.callThrough();
        var deferred = $q.defer();
        deferred.reject({data: {message: '51'}});
        spyOn(core.connection, 'query').and.returnValue(deferred.promise);
        config = {
            resource: 'mockedResource',
            name: 'mockedName',
            notify: {mocked: 'notify', translate: 'mockedTranslate'},
            error: jasmine.createSpy(config, 'error'),
            call: {
                $promise: core.connection.query()
            }
        };
        action.run(config);
        scope.$digest();
        expect(config.error).toHaveBeenCalledWith({data: {message: '51'}});
        expect(notification.push).toHaveBeenCalledWith(jasmine.objectContaining({
            text: '51'
        }));
    });

    it('action run service works (error 401 status)', function () {
        spyOn(session, 'logout');
        var deferred = $q.defer();
        deferred.reject({status: 401, data: {message: {mocked: 'message'}}});
        spyOn(core.connection, 'query').and.returnValue(deferred.promise);
        config = {
            resource: 'mockedResource',
            name: 'mockedName',
            notify: {mocked: 'notify', translate: 'mockedTranslate'},
            error: jasmine.createSpy(config, 'error'),
            call: {
                $promise: core.connection.query()
            }
        };
        action.run(config);
        scope.$digest();
        expect(config.error).toHaveBeenCalledWith({status: 401, data: {message: {mocked: 'message'}}});
    });
    it('action service return service', function () {
        expect(action).toEqual(jasmine.any(Object));
    });

    it('action.connection.get if config exist', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.connection, 'query').and.returnValue(promise);
        spyOn(core.connection, 'get').and.returnValue(promise);
        deferred.resolve('Result');
        spyOn(action, 'run').and.callThrough();
        action.connection.get({id: 4});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {show: 'error'},
            resource: 'connection',
            name: 'get',
            call: promise
        }));
        expect(core.connection.get).toHaveBeenCalledWith({id: 4});
        expect(core.connection.query).not.toHaveBeenCalled();
    });

    it('action.connection.get if config exist', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.connection, 'query').and.returnValue(promise);
        spyOn(core.connection, 'get').and.returnValue(promise);
        deferred.resolve('Result');
        spyOn(action, 'run').and.callThrough();
        action.connection.get();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {show: 'error'},
            resource: 'connection',
            name: 'list',
            call: promise
        }));
        expect(core.connection.get).not.toHaveBeenCalled();
        expect(core.connection.query).toHaveBeenCalled();
    });

    it('action.account.delete works fine', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.account, 'delete').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.delete({id: 5, name: 'deleteMe', success: 'Mockedsuccess'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            resource: "account",
            name: "delete",
            success: 'Mockedsuccess',
            notify: {
                show: true,
                translate: {
                    name: 'deleteMe'
                }
            }
        }));
        expect(core.account.delete).toHaveBeenCalledWith({id: 5});
    });

    it('action.account.info (config field is array)', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.accountInfo, 'query').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.info({id: 5, field: ['somequery', 'longConfigField'], success: 'testSuccess'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            resource: "account",
            name: "getInfo",
            success: 'testSuccess',
            notify: {
                show: "error",
                translate: {
                    field: 'somequery.longConfigField'
                }
            }
        }));
        expect(core.accountInfo.query).toHaveBeenCalledWith({id: 5, field: 'somequery', field1: 'somequery', field2: 'longConfigField'});
    });

    it('action.account.info (no array)', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.accountInfo, 'query').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.info({id: 5, field: {}, success: 'testSuccess'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            resource: "account",
            name: "getInfo",
            success: 'testSuccess'
        }));
        expect(core.accountInfo.query).toHaveBeenCalledWith({id: 5, field: {}});
    });

    it('action.account.get (with config)', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.account, 'get').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.get({id: 5});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            notify: {
                show: "error"
            },
            resource: "account",
            name: "get"
        }));
        expect(core.account.get).toHaveBeenCalledWith({id: 5});
    });

    it('action.account.get (no config)', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.account, 'query').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.get();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            notify: {
                show: "error"
            },
            resource: "account"
        }));
        expect(core.account.query).toHaveBeenCalledWith();
    });

    it('action.account.connect (with config.success)', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.account, 'update').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.connect({id: 5, config: {mocked: 'config'}, name: 'mockedName', success: 'mockedSuccess'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            notify: {
                show: true,
                translate: {
                    name: 'mockedName'
                }
            },
            success: 'mockedSuccess',
            resource: "account",
            name: "reconnect"
        }));
        expect(core.account.update).toHaveBeenCalledWith({id: 5}, {config: {mocked: 'config'}});
    });

    it('action.account.connect (without config.success)', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.account, 'update').and.returnValue(promise);
        deferred.resolve({});
        spyOn(action, 'run').and.callThrough();
        action.account.connect({id: 5, config: {mocked: 'config'}, name: 'mockedName'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            call: promise,
            notify: {
                show: true,
                translate: {
                    name: 'mockedName'
                }
            },
            resource: "account",
            name: "reconnect"
        }));
        expect(action.run).not.toHaveBeenCalledWith({success: 'mockedSuccess'});
        expect(core.account.update).toHaveBeenCalledWith({id: 5}, {config: {mocked: 'config'}});
    });

    it('action.account.auth (with config.id)', function () {
        $httpBackend.when("POST", '/oauth/mockedType/5/ajaxReAuthorize?mocked=param').respond([{mocked: 'authorize'}]);
        spyOn(action, 'run').and.callThrough();
        action.account.auth({id: 5, data: {name: {}}, config: {id: 'config'}, name: 'mockedName', params: {mocked: 'param'}, success: 'mockedSuccess', type: 'mockedType'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            resource: "account",
            notify: {
                show: "error",
                translate: {
                    name: 'mockedName'
                }
            },
            name: 'reauth',
            success: 'mockedSuccess'
        }));
        $httpBackend.flush();
    });

    it('action.account.auth (without config.id)', function () {
        $httpBackend.when("POST", '/oauth/mockedType/ajaxAuthorize?mocked=param').respond([{mocked: 'authorize'}]);
        spyOn(action, 'run').and.callThrough();
        action.account.auth({data: {name: {}}, config: {id: 'config'}, name: 'mockedName', params: {mocked: 'param'}, success: 'mockedSuccess', type: 'mockedType'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            resource: "account",
            notify: {
                show: "error",
                translate: {
                    name: 'mockedName'
                }
            },
            name: 'auth',
            success: 'mockedSuccess'
        }));
        $httpBackend.flush();
    });

    it('action.user.get works fine', function () {
        $httpBackend.when("GET", '/user/list').respond([{}]);
        spyOn(action, 'run').and.callThrough();
        action.user.get({mocked: 'data'});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: "error"
            },
            resource: "admin.user",
            name: "get",
            property: "users"
        }));
        $httpBackend.flush();
    });

    it('action.user.update (with config.name)', function () {
        $httpBackend.when("POST", '/user/edit').respond([{}]);
        spyOn(action, 'run').and.callThrough();
        action.user.update({mocked: 'data'}, {name: 'mockedName', success: 'mockedSuccess', error: ''});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true
            },
            resource: "settings",
            name: 'mockedName',
            success: 'mockedSuccess',
            error: ''
        }));
        $httpBackend.flush();
    });

    it('action.user.update (without config.name)', function () {
        $httpBackend.when("POST", '/user/edit').respond([{}]);
        spyOn(action, 'run').and.callThrough();
        action.user.update({mocked: 'data'}, {success: 'mockedSuccess', error: ''});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true
            },
            resource: "settings",
            name: 'updateProfile',
            success: 'mockedSuccess',
            error: ''
        }));
        $httpBackend.flush();
    });

    it('action.contact.email works fine', function () {
        $httpBackend.when("GET", '/contact/verify/323').respond([{}]);
        spyOn(action, 'run').and.callThrough();
        action.contact.email({id: 323, success: 'mockedSuccess', error: ''});
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: true,
            resource: "settings",
            name: "sendEmailVerification",
            success: 'mockedSuccess',
            error: ''
        }));
        $httpBackend.flush();
    });

    it('vendor.invoicehistory.get works fine', function () {
        spyOn(action, 'run').and.callThrough();
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.billing, 'query').and.returnValue(promise);
        action.vendor.invoice.get();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: false,
            resource: 'billing',
            name: 'get',
            call: promise
        }));
    });

    describe("card.get action", function () {

        it('gets card data', function () {

            spyOn(action, 'run').and.callThrough();

            action.vendor.card.get({
                success: function (response) {
                    expect(response).toEqual("mockCard");
                }
            });

            $httpBackend.expect("GET", "/vendor/card").respond({
                success: true,
                card: "mockCard"
            });

            expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
                resource: "admin.billing.card",
                name: "get",
                property: "card"
            }));

            $httpBackend.flush();
        });

    });

    describe("card.add action", function () {

        it('posts card data', function () {

            spyOn(action, 'run').and.callThrough();

            spyOn(window.Stripe, "createToken").and.callFake(function (ignore, callback) {
                callback("success", {
                    id: "mockToken"
                });
            });

            action.vendor.card.add({
                card: "card"
            });

            $httpBackend.expect("POST", '/vendor/card/add', {
                card_token: "mockToken"
            }).respond({
                success: true
            });

            expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
                resource: "admin.billing.card",
                name: "add"
            }));

            $httpBackend.flush();
        });


        it("fails with error from Stripe", function () {

            spyOn(action, 'run');

            spyOn(window.Stripe, "createToken").and.callFake(function (ignore, callback) {
                callback("error", {
                    error: 500
                });
            });

            expect(action.run).not.toHaveBeenCalled();
        });
    });

    describe("card.delete action", function () {

        it('makes delete card request', function () {

            spyOn(action, 'run').and.callThrough();

            action.vendor.card.delete();

            $httpBackend.expect("GET", "/vendor/card/delete").respond({
                success: true
            });

            expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
                resource: "admin.billing.card",
                name: "delete"
            }));

            $httpBackend.flush();
        });
    });

    it('action.vendor.billing.get works fine', function () {
        $httpBackend.when("GET", '/vendor').respond('some data');
        spyOn(action, 'run').and.callThrough();
        action.vendor.billing.get();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: false,
            resource: 'admin.billing',
            name: 'get'
        }));
        $httpBackend.flush();
    });

    it('action.vendor.billing.edit works fine', function () {
        $httpBackend.when("POST", '/vendor/edit').respond('some data');
        spyOn(action, 'run').and.callThrough();
        action.vendor.billing.edit({
            name: 'fakeName',
            data: {
                some: 'data'
            },
            success: 'mockedSuccess',
            error: ''
        });
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'fakeName'
                }
            },
            resource: 'admin.billing',
            name: 'edit',
            success: 'mockedSuccess',
            error: ''
        }));
        $httpBackend.flush();
    });

    it('action.vendor.get', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.vendor, 'get').and.returnValue(promise);
        deferred.resolve('Result');
        spyOn(action, 'run').and.callThrough();
        action.vendor.get();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: false,
            resource: 'vendors',
            name: 'get',
            call: promise
        }));
        expect(core.vendor.get).toHaveBeenCalled();
    });

    it('action.vendor.billing.trypay', function () {
        var deferred = $q.defer();
        var promise = deferred.promise;
        spyOn(core.trypay, 'get').and.returnValue(promise);
        deferred.resolve('Result');
        spyOn(action, 'run').and.callThrough();
        action.vendor.billing.trypay();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            resource: 'vendors',
            name: 'get',
            call: promise
        }));
        expect(core.trypay.get).toHaveBeenCalled();
    });
});
