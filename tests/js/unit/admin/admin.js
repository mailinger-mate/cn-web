/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject Permission $rootScope*/
/*property
    Client, Invoice, Stripe, abstract, activated, addPart, and, api_url,
    billing, callFake, callThrough, check, controller, core, createSpy, data,
    error, get, id, invoice, load, loadPlugin, name, objectContaining, only,
    pageTitle, path, permissions, redirect, resolve, returnValue, role,
    session, some, template, templateUrl, toBe, toEqual, toHaveBeenCalled,
    toHaveBeenCalledWith, token, unauthorized, url, user, users, value, vendor
*/

describe('config: admin', function () {

    'use strict';

    var $state, $stateParams, action, ocLazyLoad, users;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var ocLazyLoadMock = {
        load: jasmine.createSpy().and.callFake(function () {
            return "loaded";
        })
    };
    var usersMock = [
        {id: 13, name: "Agata", activated: true, role: "user"},
        {id: 58, name: "Iryna", activated: true, role: "admin"}
    ];

    var cfgMock = {
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            },
            role: "admin"
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {

        module('ui.router');
        module('permission');
        module('oc.lazyLoad');
        module('cn.action');
        module('pascalprecht.translate');
        module(function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('admin/admin');
        });
        module('cn.admin');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('ocLazyLoad', ocLazyLoadMock);
            $provide.value('users', usersMock);
        });


        inject(function ($rootScope, _$state_, _$stateParams_, _action_, _ocLazyLoad_, _users_) {
            action = _action_;
            ocLazyLoad = _ocLazyLoad_;
            users = _users_;
            $stateParams = _$stateParams_;
            $rootScope = $rootScope;
            $state = _$state_;
        });
    });

    it("should resolve state admin", function () {
        var state = $state.get('admin');
        expect(state).toEqual(jasmine.objectContaining({
            name: "admin",
            url: "/admin",
            redirect: "admin.user.list",
            data: {
                permissions: {
                    only: ['admin']
                }
            }
        }));
    });

    it('should resolve state admin.user', function () {
        var state = $state.get('admin.user');
        expect(state).toEqual(jasmine.objectContaining({
            name: "admin.user",
            url: "/user",
            templateUrl: "html/admin/user.html",
            redirect: "admin.user.list"
        }));
    });

    it("should resolve state admin.user.list", function () {
        spyOn(action.user, 'get').and.callFake(function () {
            return true;
        });
        var state = $state.get('admin.user.list');

        expect(state).toEqual(jasmine.objectContaining({
            name: "admin.user.list",
            url: "/list",
            controller: "cnUserList",
            templateUrl: "html/admin/user.list.html",
            data: {
                permissions: {
                    only: ['admin']
                },
                pageTitle: "nav.admin"
            }
        }));
        expect(state.resolve.users(action)).toEqual(true);
        expect(state.resolve.loadPlugin(ocLazyLoad)).toEqual('loaded');
    });

    it('should resolve abstract state admin.user.item', function () {
        var state = $state.get('admin.user.item');
        expect(state).toEqual(jasmine.objectContaining({
            name: "admin.user.item",
            url: "/item",
            template: '<div ui-view autoscroll="false"></div>',
            abstract: true
        }));
    });

    it("should resolve state admin.user.item.add", function () {
        var state = $state.get('admin.user.item.add');
        expect(state).toEqual(jasmine.objectContaining({
            name: "admin.user.item.add",
            url: "/add",
            controller: "cnUserItem",
            templateUrl: "html/admin/user.item.html",
            data: {
                pageTitle: "nav.user.add",
                permissions: {
                    only: ['admin']
                }
            }
        }));
        expect(state.resolve.user()).toEqual(false);
    });

    it("should resolve state admin.user.item.edit", function () {
        spyOn(action.user, 'get').and.callThrough();
        var state = $state.get('admin.user.item.edit');
        expect(state).toEqual(jasmine.objectContaining({
            name: "admin.user.item.edit",
            url: "/edit/{user:int}",
            controller: "cnUserItem",
            templateUrl: "html/admin/user.item.html",
            data: {
                pageTitle: "nav.user.edit",
                permissions: {
                    only: ['admin']
                }
            }
        }));
        state.resolve.users(action);
        expect(action.user.get).toHaveBeenCalledWith();
        $stateParams = {user: 58};
        expect(state.resolve.user($stateParams, users)).toEqual(users[1]);
        $stateParams = {user: 13};
        expect(state.resolve.user($stateParams, users)).toEqual(users[0]);
        $stateParams = {user: 100};
        expect(state.resolve.user($stateParams, users)).toBe(undefined);
    });

    describe("billing state", function () {

        it("exists", function () {

            expect($state.get('admin.billing')).toEqual(jasmine.objectContaining({
                name: "admin.billing",
                url: "/billing",
                templateUrl: "html/admin/billing.html",
                redirect: "admin.billing.invoice",
                data: {
                    permissions: {
                        only: ['admin']
                    }
                }
            }));
        });
    });

    describe("billing invoice state", function () {

        it("exists", function () {

            spyOn(action.vendor.invoice, 'get').and.returnValue('invoiceMock');
            var state = $state.get('admin.billing.invoice');
            expect(state).toEqual(jasmine.objectContaining({
                name: "admin.billing.invoice",
                url: "/invoice",
                controller: "cnBillingInvoice",
                templateUrl: "html/admin/billing.invoice.list.html",
                data: {
                    permissions: {
                        only: ['admin']
                    },
                    pageTitle: "nav.billing.invoice"
                }
            }));
            expect(state.resolve.Invoice(action)).toEqual('invoiceMock');
            expect(action.vendor.invoice.get).toHaveBeenCalled();
            expect(state.resolve.loadPlugin(ocLazyLoad)).toEqual('loaded');
        });
    });

    describe("admin.billing.client state", function () {

        it("exists", function () {
            expect($state.get('admin.billing.client')).toEqual(jasmine.objectContaining({
                name: "admin.billing.client",
                url: "/client",
                templateUrl: "html/admin/billing.client.html",
                controller: "cnBillingClient",
                data: {
                    permissions: {
                        only: ['admin']
                    }
                }
            }));
        });
    });

    describe("admin.billing.card state", function () {

        it("exists", function () {
            expect($state.get('admin.billing.card')).toEqual(jasmine.objectContaining({
                name: "admin.billing.card",
                url: "/card",
                templateUrl: "html/admin/billing.card.html",
                controller: "cnBillingCard",
                data: {
                    permissions: {
                        only: ['admin']
                    }
                }
            }));

        });

        it('resolve function works fine', function () {
            var state = $state.get('admin.billing.card');
            expect(state.resolve.Stripe(ocLazyLoad)).toEqual('loaded');
        });
    });

    it("resolve function (Client) of state admin.billing return false", function () {
        spyOn(action.vendor.billing, 'get').and.callFake(function () {
            return false;
        });
        var state = $state.get('admin.billing');
        expect(state.resolve.Client(action)).toEqual(false);
    });

    it("resolve function (Client) of state admin.billing success - return", function () {
        spyOn(action.vendor.billing, 'get').and.callFake(function () {
            return {some: 'dataInSuccess'};
        });
        var state = $state.get('admin.billing');
        expect(state.resolve.Client(action)).toEqual({some: 'dataInSuccess'});
    });
});
