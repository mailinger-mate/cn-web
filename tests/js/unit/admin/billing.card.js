/*global jasmine, describe, it, inject, spyOn, expect, window, beforeEach, module*/
/*property
    $digest, $new, $scope, $setPristine, $valid, Card, MasterCard, Stripe, add,
    and, billing, brand, callFake, card, config, createToken, cvc, delete,
    deleteCard, exp_month, exp_year, expiry, field, flush, form, formCard,
    four, has_card, icon, mode, month, name, not, number, objectContaining,
    publishable_key, resetFour, saveCard, setPublishableKey, stripe, success,
    toBe, toBeDefined, toBeFalsy, toEqual, toHaveBeenCalled,
    toHaveBeenCalledWith, type, value, vendor, view, year
*/


describe("cnBillingCard controller", function () {

    "use strict";

    var $controller, $scope, Common, action, cfg, $timeout, $compile;

    var mockExpiry = {
            month: "02",
            year: "2016"
        },
        mockCard = {
            brand: "MasterCard",
            number: "5555555555554444",
            name: "Card Holder",
            expiry: mockExpiry.month + "/" + mockExpiry.year,
            cvc: "123"
        },
        mockToken = "tok_u5dg20Gra";

    window.Stripe = {
        setPublishableKey: function () {
            return true;
        },
        createToken: function () {
            return mockToken;
        }
    };

    beforeEach(function () {

        module('cn.billing.card', function ($provide) {
            $provide.value('cfg', {
                stripe: {
                    publishable_key: "pk_test_JjIqs8IZAwTV9BsxIUV84v8R"
                },
                billing: {
                    card: {
                        brand: {
                            MasterCard: 'testCard'
                        },
                        icon: 'icoMock'
                    }
                }
            });
        });

        inject(function ($rootScope, _$controller_, _Common_, _action_, _cfg_, _$timeout_, _$compile_) {
            $compile = _$compile_;
            $timeout = _$timeout_;
            cfg = _cfg_;
            $scope = $rootScope.$new();
            $controller = _$controller_;
            Common = _Common_;
            action = _action_;
        });

    });

    describe("view mode", function () {

        it("config works fine", function () {
            spyOn(window.Stripe, 'setPublishableKey');
            $controller('cnBillingCard', {
                $scope: $scope,
                Card: mockCard
            });
            $timeout.flush();
            expect($scope.icon).toEqual('icoMock');
            expect($scope.form).toBeDefined();
            expect($scope.form.config.type).toEqual('testCard');
            expect($scope.card).toEqual(mockCard);
            expect(window.Stripe.setPublishableKey).toHaveBeenCalledWith('pk_test_JjIqs8IZAwTV9BsxIUV84v8R');
        });

        it("scope.resetFour works fine", function () {
            $compile("<div id='cardNumber'>")($scope);
            $scope.$digest();
            $controller('cnBillingCard', {
                $scope: $scope,
                Card: false
            });
            $scope.resetFour();
            expect($scope.form.config.four).toEqual(false);

        });

        it("is add", function () {

            $controller('cnBillingCard', {
                $scope: $scope,
                Card: false
            });

            expect($scope.view.mode).toEqual("add");
        });

        it("is edit", function () {

            $controller('cnBillingCard', {
                $scope: $scope,
                Card: mockCard
            });

            expect($scope.view.mode).toEqual("edit");
            expect($scope.card).toEqual(mockCard);
        });
    });

    describe("saveCard method", function () {

        beforeEach(function () {

            $controller('cnBillingCard', {
                $scope: $scope,
                Card: false
            });
        });

        it("fails with invalid form", function () {

            spyOn($scope, "validateForm").and.callFake(function () {
                return {
                    $valid: false
                };
            });

            expect($scope.saveCard($scope)).toBeFalsy();
        });

        describe("valid form", function () {

            it("calls add card action", function () {

                $scope.form.field = mockCard;

                spyOn($scope, "validateForm").and.callFake(function () {
                    return {
                        $valid: true
                    };
                });

                spyOn(Common, "parseExpiry").and.callFake(function () {
                    return mockExpiry;
                });

                spyOn(action.vendor.card, 'add').and.callFake(function (config) {
                    config.success();
                });

                $scope.saveCard($scope);

                expect(action.vendor.card.add).toHaveBeenCalledWith(jasmine.objectContaining({
                    card: {
                        number: mockCard.number,
                        name: mockCard.name,
                        exp_month: mockExpiry.month,
                        exp_year: mockExpiry.year,
                        cvc: mockCard.cvc
                    }
                }));
                expect(cfg.billing.card.has_card).toEqual(true);
                expect($scope.view.mode).toEqual("edit");
            });
        });
    });

    describe("deleteCard method", function () {

        it("fails with no card", function () {

            $controller('cnBillingCard', {
                $scope: $scope,
                Card: false
            });

            spyOn(action.vendor.card, 'delete');

            expect($scope.deleteCard($scope)).toEqual(false);
            expect(action.vendor.card.delete).not.toHaveBeenCalled();
        });

        it("calls delete card action", function () {

            $controller('cnBillingCard', {
                $scope: $scope,
                Card: mockCard
            });

            $scope.formCard = {
                $setPristine: function () {
                    return;
                }
            };

            spyOn($scope.formCard, '$setPristine');
            spyOn(action.vendor.card, 'delete').and.callFake(function (config) {
                config.success();
            });

            $scope.deleteCard($scope);

            expect(action.vendor.card.delete).toHaveBeenCalled();
            expect($scope.form.field).toEqual({});
            expect($scope.view.mode).toEqual("add");
            expect($scope.card).toBe(undefined);
            expect($scope.formCard.$setPristine).toHaveBeenCalled();
            expect(cfg.billing.card.has_card).toEqual(false);
        });

    });
});
