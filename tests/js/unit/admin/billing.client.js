/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $emit, $error, $invalid, $new, $scope, $state, $valid, AD, Client, ISO3166,
    PL, and, any, api_url, billing, callFake, check, codeToCountry, config,
    connection, core, countries, country, countryInEU, createSpy, ctrl, data,
    destination, edit, error, field, form, go, message, mocked, mode, name,
    not, path, returnValue, saveBilling, session, some, source, success,
    toBeDefined, toEqual, toHaveBeenCalled, toHaveBeenCalledWith, token,
    unauthorized, user, validateForm, value, vendor,
    verifyNoOutstandingExpectation, verifyNoOutstandingRequest, view
*/

describe("controller: cnBillingClient", function () {
    'use strict';
    var scope, $rootScope, $state, cnBillingClient, action, ISO3166, $httpBackend, Client;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var mockedISO3166 = {
        codeToCountry: {AD: 'ANDORRA', PL: 'POLAND'}
    };
    var cfgMock = {
        connection: {
            config: {
                source: {
                    mocked: 'source'
                },
                destination: {
                    mocked: 'source'
                }
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('pascalprecht.translate');
        module('ui.router');
        module('cn.action');
        module('cn.billing.client');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('Client', {country: {PL: 'POLAND'}});
            $provide.value('ISO3166', mockedISO3166);
        });
        inject(function ($controller, _$rootScope_, _$state_, _action_, _$httpBackend_, _ISO3166_, _Client_) {
            Client = _Client_;
            ISO3166 = _ISO3166_;
            action = _action_;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            $state = _$state_;
            scope = $rootScope.$new();
            cnBillingClient = function () {
                return $controller('cnBillingClient', {
                    $scope: scope,
                    $state: $state,
                    Client: Client,
                    ISO3166: ISO3166
                });
            };
        });
        scope.form = {
            ctrl: {
                $error: {$invalid: false}
            },
            field: {
                country: {}
            }
        };
        cnBillingClient();

    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('cnBillingClient is defined', function () {
        expect(cnBillingClient).toBeDefined();
    });

    it('scope.form is defined', function () {
        expect(scope.form).toBeDefined();
    });

    it('scope.countryInEU is defined', function () {
        expect(scope.countryInEU).toBeDefined();
    });

    it('scope.countries is defined correctly', function () {
        expect(scope.countries).toEqual([{AD: 'ANDORRA', PL: 'POLAND'}]);
    });

    it('scope.validateForm works fine (no form)', function () {
        scope.form = {
            ctrl: {
                $error: {$invalid: false}
            }
        };
        expect(scope.validateForm()).toEqual(scope.form.ctrl);
    });

    describe("saveBilling action", function () {

        it("works", function () {
            spyOn(action.vendor.billing, 'edit');
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            expect(scope.saveBilling(scope)).toEqual();
            expect(action.vendor.billing.edit).toHaveBeenCalledWith(
                {
                    name: 'edit',
                    data: {
                        country: {PL: 'POLAND'}
                    },
                    success: jasmine.any(Function),
                    error: jasmine.any(Function)
                }
            );
        });

        it("redirects to card view mode add (success function)", function () {
            spyOn($state, 'go');
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            spyOn(scope, '$emit');
            action.vendor.billing.edit = jasmine.createSpy().and.callFake(function (config) {
                config.success({some: 'successData'});
            });
            scope.view.mode = "add";
            scope.saveBilling(scope);
            expect(scope.$emit).toHaveBeenCalledWith('billing.client.add');
            expect($state.go).toHaveBeenCalledWith('admin.billing.card');
        });

        it("action.vendor.billing.edit (success function)", function () {
            spyOn($state, 'go');
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            spyOn(scope, '$emit');
            action.vendor.billing.edit = jasmine.createSpy().and.callFake(function (config) {
                config.success({some: 'successData'});
            });
            scope.view.mode = "";
            expect(scope.$emit).not.toHaveBeenCalled();
            expect($state.go).not.toHaveBeenCalled();
        });

        it("action.vendor.billing.edit (error function, message exist)", function () {
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            action.vendor.billing.edit = jasmine.createSpy().and.callFake(function (config) {
                config.error({data: {message: 'mockedErrorMessage'}});
            });
            scope.view.mode = "add";
            scope.saveBilling(scope);
            expect(scope.form.error.field).toEqual('mockedErrorMessage');
        });

        it("action.vendor.billing.edit (error function, message exist)", function () {
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            action.vendor.billing.edit = jasmine.createSpy().and.callFake(function (config) {
                config.error({data: {}});
            });
            scope.view.mode = "add";
            scope.saveBilling(scope);
            expect(scope.form.error.field).toEqual();
        });
    });
});
