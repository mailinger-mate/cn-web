/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject Permission $rootScope*/
/*property
    $digest, $new, $scope, $translate, _links, and, api_url, billingHistory,
    billing_history, callFake, charge_date, check, core, createSpy, css,
    current, disabled, dtColumnDefs, dtOptions, end, error, failure, href,
    invoice, max_connections, messages, newOptions, objectContaining,
    orderable, paid, path, price, processing, role, searchable, session, start,
    status, targets, toBeTruthy, toEqual, toHaveAttr, toHaveClass, toHaveText,
    token, trial, unauthorized, use, user, value, visible, withOption
*/

describe('module: cn.billing.invoice', function () {

    'use strict';

    var timeZone, centToDollar, cnBillingInvoice, scope, DTOptionsBuilder, Invoice, $compile, cfg, $rootScope;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';

    var cfgMock = {
        css: {
            invoice: {
                failure: 'icon-box status-warn fa fa-warning',
                processing: 'fa fa-spinner fa-pulse fa-lg text-navy',
                paid: 'icon-box status-info fa fa-check',
                error: 'icon-box status-error fa fa-warning',
                trial: 'icon-box status-info fa fa-gift',
                disabled: 'icon-box status-disabled fa fa-warning',
                current: 'icon-box twitter fa fa-street-view'
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            },
            role: "admin"
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        Invoice = {
            status: "paid",
            start: "2015-04-01",
            end: "2015-04-29",
            messages: 631,
            max_connections: 44,
            price: 30,
            charge_date: "2015-02-11",
            _links: {
                invoice: {
                    href: "http://cn-core-deploy.int.x-formation.com/rest/v1/billings/1"
                }
            }
        };
        DTOptionsBuilder = {
            newOptions: jasmine.createSpy('newOptions').and.callFake(function () {
                return {
                    withOption: jasmine.createSpy('withOption').and.callFake(function () {
                        return {
                            withOption: jasmine.createSpy('withOption').and.callFake(function () {
                                return 'got some data';
                            })

                        };
                    })
                };
            })
        };
        module('ui.router');
        module('oc.lazyLoad');
        module('cn.action');
        module('pascalprecht.translate');

        module('cn.billing.invoice', function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('DTOptionsBuilder', DTOptionsBuilder);
            $provide.value('Invoice', Invoice);
        });

        inject(function (_$rootScope_, _DTOptionsBuilder_, $controller, _Invoice_, _$compile_, _cfg_, $translate, $filter) {
            centToDollar = function () {
                return $filter('centToDollar');
            };
            timeZone = function () {
                return $filter('timeZone');
            };
            $translate.use("en_US");
            $compile = _$compile_;
            cfg = _cfg_;
            Invoice = _Invoice_;
            DTOptionsBuilder = _DTOptionsBuilder_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            cnBillingInvoice = function () {
                return $controller('cnBillingInvoice', {
                    $scope: scope
                });
            };
        });
    });
    describe("billing invoice controller", function () {

        it("controller is defined", function () {
            expect(cnBillingInvoice()).toBeTruthy();
        });

        it("scope.billingHistory is defined correctly", function () {
            cnBillingInvoice();
            expect(scope.invoice).toEqual(Invoice);
        });

        it('DTOptionsBuilder works', function () {
            cnBillingInvoice();
            expect(scope.dtOptions).toEqual('got some data');
        });

        it('dtColumnDefs is defined', function () {
            cnBillingInvoice();
            expect(scope.dtColumnDefs).toEqual(jasmine.objectContaining([
                {
                    targets: 'no-sort',
                    orderable: false
                },
                {
                    targets: [0],
                    visible: false,
                    searchable: true
                }
            ]));
        });
    });

    describe("changeStatus directive", function () {
        it("directive changeStatus works fine", function () {
            var element = $compile("<change-Status status='current'></change-Status>")(scope);
            $rootScope.$digest();
            expect(element).toHaveClass(cfg.css.invoice.current);
            expect(element).toHaveAttr('title', 'admin.billing.invoice.status.current');
            expect(element).toHaveAttr('status', 'current');
        });
    });


    describe('filter ', function () {
        it("centToDollar  filter works fine", function () {
            var filter = centToDollar();
            expect(filter(123123)).toEqual('$1231.23');
            expect(filter(123100)).toEqual('$1231');
        });

        it("timeZone filter works fine if text exist", function () {
            var filter = timeZone();
            expect(filter('2015-01-04 23:12 +3000')).toEqual('2015-01-03');
        });

        it("timeZone filter works fine if text doesnt exist", function () {
            var filter = timeZone();
            expect(filter('')).toEqual('---');
        });
    });
});
