/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject Permission $rootScope*/
/*property
    $broadcast, $digest, $new, $rootScope, $scope, and, any, api_url, billing,
    billing_name, buttonTryPay, callFake, card, check, client, core, error,
    forEach, go, has_card, isClient, nav, not, notify, objectContaining, path,
    role, sample, session, show, sref, success, toBeTruthy, toEqual,
    toHaveBeenCalled, toHaveBeenCalledWith, token, translate, tryPay, trypay,
    unauthorized, user, value, vendor, vendorBlocked
*/


describe('module: cn.billing', function () {

    'use strict';

    var cnBilling, scope, $rootScope, action, cfg, $state, Client;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var ClientMock = {
        billing_name: 'mockedBillingName'
    };
    var cfgMock = {
        billing: {
            card: {
                has_card: true
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            },
            role: "admin"
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        module('ui.router');
        module('oc.lazyLoad');
        module('cn.action');
        module('pascalprecht.translate');
        module('cn.billing', function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('Client', ClientMock);
        });

        inject(function (_$rootScope_, $controller, _action_, _cfg_, _$state_, _Client_) {
            Client = _Client_;
            $state = _$state_;
            cfg = _cfg_;
            action = _action_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            cnBilling = function () {
                return $controller('cnBilling', {
                    $scope: scope,
                    $rootScope: $rootScope
                });
            };
        });
    });
    describe("billing controller", function () {

        it("controller is defined", function () {
            cnBilling();
            expect(cnBilling()).toBeTruthy();
        });

        it("scope.client exist", function () {
            cnBilling();
            expect(scope.client).toEqual(true);
        });

        it("scope.nav exist", function () {
            cnBilling();
            scope.nav.forEach(function (navItem) {
                expect(navItem.sref).toEqual(jasmine.any(String));
            });
        });

        it("scope.on.billing.client.add works fine", function () {
            cnBilling();
            $rootScope.$broadcast('billing.client.add');
            scope.$digest();
            expect(scope.client).toBeTruthy();
        });

        it("scope.isClient works fine", function () {
            cnBilling();
            scope.client = 'testClient';
            expect(scope.isClient()).toEqual('testClient');
        });

        it("scope buttontrypay is defined", function () {
            $rootScope.vendorBlocked = true;
            cnBilling();
            expect(scope.buttonTryPay).toEqual(true);
        });

        it("Scope.trypay works fine (call button action on click)", function () {
            $rootScope.vendorBlocked = true;
            spyOn(action.vendor.billing, 'trypay');
            cnBilling();
            scope.tryPay();
            expect(action.vendor.billing.trypay).toHaveBeenCalledWith(jasmine.objectContaining({
                notify: {
                    show: true,
                    translate: {}
                },
                success: jasmine.any(Function),
                error: jasmine.any(Function)
            }));
        });

        it("Scope.trypay works fine (success function)", function () {
            $rootScope.vendorBlocked = true;
            spyOn($rootScope, '$broadcast');
            spyOn(action.vendor.billing, 'trypay').and.callFake(function (obj) {
                obj.success({sample: 'data'});
            });
            cnBilling();
            expect(scope.tryPay()).toEqual();
            expect($rootScope.$broadcast).toHaveBeenCalledWith('vendorBlocked', false);
        });

        it("Scope.trypay works fine (error function)", function () {
            $rootScope.vendorBlocked = true;
            spyOn(action.vendor.billing, 'trypay').and.callFake(function (obj) {
                obj.error({sample: 'data'});
            });
            cnBilling();
            expect(scope.tryPay()).toEqual();
        });

        it("Scope.trypay works fine (no card details) redirect to client", function () {
            cfg.billing.card.has_card = false;
            Client.billing_name = false;
            $rootScope.vendorBlocked = true;
            spyOn(action.vendor.billing, 'trypay');
            spyOn($state, 'go');
            cnBilling();
            scope.tryPay();
            expect(action.vendor.billing.trypay).not.toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('admin.billing.client');
        });

        it("Scope.trypay works fine (no card details) redirect to card", function () {
            Client.billing_name = true;
            cfg.billing.card.has_card = false;
            $rootScope.vendorBlocked = true;
            spyOn(action.vendor.billing, 'trypay');
            spyOn($state, 'go');
            cnBilling();
            scope.tryPay();
            expect(action.vendor.billing.trypay).not.toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('admin.billing.card');
        });
    });
});
