/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $error, $invalid, $new, $scope, $setDirty, $valid, action, and, api_url,
    button, callThrough, check, class, config, confirm, connection, contacts,
    core, createSpy, ctrl, defer, deleteUser, destination, error, expect,
    field, flush, form, id, label, mocked, mode, name, notify,
    objectContaining, path, resource, respond, run, saveUser, session, show,
    source, test, toBeDefined, toEqual, toHaveBeenCalledWith, token, translate,
    type, unauthorized, user, value, verifyNoOutstandingExpectation,
    verifyNoOutstandingRequest, view
*/

describe("cnUserItem controller", function () {

    'use strict';

    var scope, $rootScope, cnUserItem, action, user, respond, $q, $httpBackend;

    beforeEach(function () {

        module('pascalprecht.translate');
        module('ui.router');
        module('cn.action');
        module('cn.user.item');

        module(function ($provide) {
            $provide.value('user', {
                id: 3
            });
        });

        inject(function ($controller, _$rootScope_, _$q_, $state, _action_, _user_, _$httpBackend_) {

            user = _user_;
            action = _action_;
            $q = _$q_;
            $state = $state;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();

            cnUserItem = function () {
                return $controller('cnUserItem', {
                    $scope: scope
                });
            };
        });
        respond = $q.defer();
        cnUserItem();
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('is defined', function () {
        expect(cnUserItem).toBeDefined();
    });

    it('has form is defined', function () {
        expect(scope.form).toBeDefined();
    });

    it('initUser function works fine (config.user)', function () {
        expect(scope.view.mode).toEqual('edit');
        expect(scope.form.field).toEqual(user);
        expect(scope.form.button).toEqual([
            {
                label: "save",
                type: "submit",
                action: "saveUser",
                class: "btn-primary"
            },
            {
                label: 'delete',
                type: 'button',
                action: 'deleteUser',
                confirm: true,
                class: 'btn-warning'
            }
        ]);
    });

    describe("saveUser action", function () {

        it('fails with invalid form', function () {
            scope.form = {
                ctrl: {
                    $error: {
                        $invalid: false
                    },
                    $valid: false
                }
            };
            expect(scope.saveUser(scope)).toEqual(false);
        });

        it('saves user with valid form', function () {
            scope.view.mode = 'mockedMode';
            $httpBackend.expect("POST", 'user/edit/3').respond(respond);
            spyOn(action, 'run');
            scope.form = {
                field: {
                    contacts: [{
                        value: 'mockedValue'
                    }],
                    name: 'mockedName'
                },
                ctrl: {
                    $error: {$invalid: false}
                }
            };
            scope.form.ctrl.$valid = true;
            scope.saveUser(scope);
            expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
                notify: {
                    show: true,
                    translate: {
                        name: 'mockedName'
                    }
                },
                resource: "admin.user",
                name: scope.view.mode
            }));
            $httpBackend.flush();
        });
    });

    it('deleteUser works fine', function () {
        scope.form.field.name = 'test';
        $httpBackend.expect("GET", 'user/delete/3').respond(respond);
        spyOn(action, 'run').and.callThrough();
        scope.deleteUser(scope);
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'test'
                }
            },
            resource: "admin.user",
            name: "delete"
        }));
        $httpBackend.flush();
    });
});
