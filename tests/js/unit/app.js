/*global afterEach spyOn jasmine angular describe module beforeEach it expect window inject*/
/*property
    $digest, $emit, $new, $on, $scope, $translate, CN, FS, address, and, any,
    api_url, billing, blocked, callFake, card, check, clear, controller, core,
    createSpy, data, defer, defineManyRoles, dismiss, dismissAll, displayName,
    email, error, get, getItem, go, has_card, id, identify, module, name, not,
    objectContaining, open, path, promise, redirect, remaining_trial_days,
    resolve, respond, role, service, session, size, some, success, templateUrl,
    toBe, toBeDefined, toContainHtml, toEqual, toHaveBeenCalled,
    toHaveBeenCalledWith, token, unauthorized, url, use, useLoader, user,
    value, vendor, vendorName_str, whenGET, windowClass, interval, pageTitle
*/
describe('module: CN (app.js)', function () {
    'use strict';
    var $rootScope, scope, appCtrl, $compile, cfg, $state, state, $httpBackend, $modalStack;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var modalController;
    var Permission = {
        defineManyRoles: jasmine.createSpy()
    };

    var action = {
        vendor: {
            get: jasmine.createSpy().and.callFake(function (config) {
                config.success({blocked: true});
            })
        }
    };
    var $modal = {
        open: jasmine.createSpy().and.callFake(function (obj) {
            modalController = obj.controller;
        })
    };

    var cfgMock = {
        billing: {
            card: {
                has_card: ''
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            },
            role: "admin"
        },
        session: {
            check: {
                url: "/mockHealthCheck/",
                interval: 60
            },
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        angular.module("cn.init", []);
    });

    beforeEach(function () {

        module('ui.router');
        module('cn', function ($provide) {
            $provide.value('action', action);
            $provide.value('Permission', Permission);
            $provide.value('$modal', $modal);
            $provide.value('cfg', cfgMock);
            $provide.value('cn.init');
            $provide.service('customLoader', function ($q) {
                return function () {
                    var deferred = $q.defer();
                    deferred.resolve({
                        "product.name": "mockedProductName",
                        "mock.title": "mockTitle"
                    });
                    return deferred.promise;
                };
            });
        });
        module(function ($translateProvider) {
            $translateProvider.useLoader('customLoader');
        });

        inject(function (_$compile_, _$rootScope_, $controller, _$state_, _$httpBackend_, $translate, _cfg_, _$modal_, _action_, _$modalStack_) {
            $modalStack = _$modalStack_;
            action = _action_;
            $modal = _$modal_;
            cfg = _cfg_;
            $translate.use("en_US");
            $compile = _$compile_;
            $state = _$state_;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            appCtrl = function () {
                return $controller('AppCtrl', {
                    $scope: scope,
                    $translate: $translate
                });
            };
        });
        $httpBackend.whenGET('/translate/error/error.en.json').respond(200);
        $httpBackend.whenGET('/translate/account/account.en.json').respond(200);
        $httpBackend.whenGET('/translate/connection/connection.en.json').respond(200);
        $httpBackend.whenGET('/translate/admin/admin.en.json').respond(200);
        $httpBackend.whenGET('/translate/tutorial/tutorial.en.json').respond(200);
        $httpBackend.whenGET('/translate/settings/settings.en.json').respond(200);
        $httpBackend.whenGET('html/connection/list.html').respond(200);
        $httpBackend.whenGET('/translate/app.en.json').respond(200);
        $httpBackend.whenGET(cfg.session.check.url + token).respond(200);
        $httpBackend.whenGET('api_url/connections').respond(200);
        $httpBackend.whenGET('error.html').respond(200);
        $httpBackend.whenGET('api_url/vendors').respond(200);
        $httpBackend.whenGET('/translate/error/error.en_US.json').respond(200);
        $httpBackend.whenGET('/html/form/modal.blocked.html').respond(200);

    });

    afterEach(function () {
        localStorage.clear();
    });

    describe('AppCtrl controller', function () {

        it('controller is defined', function () {
            appCtrl();
            expect(appCtrl).toBeDefined();
        });

        it('scope.CN is defined correctly', function () {
            appCtrl();
            expect(scope.CN).toEqual(cfg);
        });
    });

    describe('run block (blocked modal)', function () {

        it('state root is defined', function () {
            state = $state.get('root');
            scope.$digest();
            expect(state).toEqual(jasmine.objectContaining({
                url: '',
                redirect: 'connection.list',
                name: 'root'
            }));
            expect(Permission.defineManyRoles).toHaveBeenCalledWith(["admin", "user"], jasmine.any(Function));
        });

        it('action.vendor.get works fine', function () {
            expect($modal.open).toHaveBeenCalledWith(jasmine.objectContaining({
                templateUrl: '/html/form/modal.blocked.html',
                size: 'lg',
                windowClass: 'modal-window',
                controller: jasmine.any(Function)
            }));
            expect(action.vendor.get).toHaveBeenCalled();
        });

        it('modal role is defined correctly', function () {
            modalController(scope);
            expect(scope.role).toEqual('admin');
        });

        it('modal close function works (no redirect)', function () {
            spyOn($modalStack, 'dismissAll');
            expect(action.vendor.get).toHaveBeenCalled();
            modalController(scope);
            scope.dismiss();
            expect($modalStack.dismissAll).toHaveBeenCalled();
        });

        it('modal close function works (redirect)', function () {
            spyOn($modalStack, 'dismissAll');
            spyOn($state, 'go');
            expect(action.vendor.get).toHaveBeenCalled();
            modalController(scope);
            scope.dismiss(true);
            expect($modalStack.dismissAll).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('admin.billing.invoice');
        });

        it('$stateChangeStart changed with params', function () {

            var eventEmitted = false;
            $rootScope.$on("$stateChangeStart", function (event, to, params) {
                if (to.redirect) {
                    eventEmitted = true;
                    return event;
                }
                return params;
            });
            $state.go('root');
            expect(eventEmitted).toBe(true);
        });

        it('$stateChangeStart changed with no params', function () {
            var errorEmitted = false;
            $rootScope.$on("$stateChangeError", function () {
                errorEmitted = true;
            });
            scope.$emit('$stateChangeError', 'test');
            expect(errorEmitted).toBe(true);
        });
    });

    describe('pageTitle directive', function () {

        it('sets title correctly', function () {
            var element = $compile("<page-title>")(scope);
            scope.$emit('$stateChangeSuccess', {
                name: 'mockState',
                data: {
                    pageTitle: "mock.title"
                }
            });
            $rootScope.$digest();
            expect(element).toContainHtml('mockTitle - mockedProductName');
        });
    });
});

describe('module: CN (app.js) trial mode', function () {
    'use strict';
    var action = {
        vendor: {
            get: jasmine.createSpy().and.callFake(function (config) {
                config.success({remaining_trial_days: 5, has_card: false});
            })
        }
    };
    var $modal = {
        open: jasmine.createSpy()
    };

    beforeEach(function () {
        module('cn', function ($provide) {
            $provide.value('action', action);
            $provide.value('$modal', $modal);
        });
        inject(function () {
            return;
        });

    });
    afterEach(function () {
        localStorage.clear();
    });

    describe('run block (trial modal)', function () {

        it('action.vendor.get works fine', function () {
            expect($modal.open).toHaveBeenCalledWith(jasmine.objectContaining({
                templateUrl: '/html/form/modal.endtrial.html',
                size: 'lg',
                windowClass: 'modal-window',
                controller: jasmine.any(Function)
            }));
            expect(action.vendor.get).toHaveBeenCalled();
            expect(localStorage.getItem('modal.endtrial.html')).toEqual('true');
        });
    });
});

describe('module: CN (app.js) disabled trial mode ', function () {
    'use strict';
    var cfg;
    var cfgMock = {
        billing: {
            card: {
                has_card: ''
            }
        },
        session: {
            check: 60
        }
    };

    var action = {
        vendor: {
            get: jasmine.createSpy().and.callFake(function (config) {
                config.success({remaining_trial_days: 5, has_card: true});
            })
        }
    };
    var $modal = {
        open: jasmine.createSpy()
    };

    beforeEach(function () {
        module('cn', function ($provide) {
            $provide.value('action', action);
            $provide.value('$modal', $modal);
            $provide.value('cfg', cfgMock);
        });
        inject(function (_cfg_) {
            cfg = _cfg_;
        });

    });
    afterEach(function () {
        localStorage.clear();
    });

    describe('run block (trial modal)', function () {

        it('has card config is seted', function () {
            expect(cfg.billing.card.has_card).toEqual(true);
        });

        it('action.vendor.get works fine', function () {
            expect($modal.open).not.toHaveBeenCalled();
            expect(localStorage.getItem('modal.endtrial.html')).toEqual(null);
        });
    });
});


describe('run block with FS enabled', function () {
    'use strict';
    var cfgMock = {
        user: {
            id: 123,
            name: 'some_user',
            email: {
                address: 'some@test.com'
            },
            vendor: 'some_vendor'
        },
        session: {
            check: {
                interval: 60,
                url: "/mockHealthCheck/"
            }
        }
    };

    var $window = {
        FS: {
            identify: jasmine.createSpy()
        }
    };

    beforeEach(function () {
        module('cn', function ($provide) {
            $provide.value('$window', $window);
            $provide.value('cfg', cfgMock);
        });
        inject(function () {
            return;
        });
    });

    it('should run identify with user params', function () {
        expect($window.FS.identify).toHaveBeenCalledWith('connect_123', {
            displayName: 'some_user',
            email: 'some@test.com',
            vendorName_str: 'some_vendor'
        });
    });
});

describe('run block with FS disabled', function () {
    'use strict';
    var cfgMock = {
        session: {
            check: {
                interval: 60,
                url: "/mockHealthCheck/"
            }
        }
    };

    var $window = {};

    beforeEach(function () {
        module('cn', function ($provide) {
            $provide.value('$window', $window);
            $provide.value('cfg', cfgMock);
        });
    });

    it('should not try to run identify', function () {
        expect($window.FS).not.toBeDefined();
    });
});
