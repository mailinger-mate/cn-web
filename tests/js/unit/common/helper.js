/*global angular describe module beforeEach it expect inject spyOn*/
/*property
    and, capitalizeString, charAt, forEach, host, hostname, isFunction, name,
    not, path, processURL, returnValue, slice, toBe, toBeUndefined, toEqual,
    toHaveBeenCalledWith, toUpperCase
*/

describe('Service: helper', function () {

    'use strict';

    var helper;
    var capitalizedHostName = location.hostname.charAt(0).toUpperCase() + location.hostname.slice(1);
    var capitalizeStrings = [
        'HALT',
        'HaLt',
        'HAlt',
        'halt',
        'hALT'
    ];
    var validResult = {
        host: "angular.com",
        name: "Angular",
        path: "about"
    };
    var validURL = 'http://' + location.hostname + '.com';

    beforeEach(function () {
        module('cn.helper');
    });

    beforeEach(inject(function (_helper_) {
        helper = _helper_;
    }));

    describe('method: capitalizeString', function () {

        it('method capitalizeString should be defined', function () {
            expect(angular.isFunction(helper.capitalizeString)).toBe(true);
        });

        it('capitalize array correctly', function () {
            capitalizeStrings.forEach(function (item) {
                expect(helper.capitalizeString(item)).toBe('Halt');
            });
        });

        it('return null when non string was provided', function () {
            expect(helper.capitalizeString('')).toBeUndefined();
            expect(helper.capitalizeString()).toBeUndefined();
        });
    });

    describe('method: processURL', function () {

        it('method processURL should be defined', function () {
            expect(angular.isFunction(helper.processURL)).toBe(true);
        });

        it('call capitalize method inside processURL', function () {
            spyOn(helper, "capitalizeString").and.returnValue(location.hostname);
            helper.processURL(validURL);
            expect(helper.capitalizeString).toHaveBeenCalledWith(location.hostname);
        });

        it('receive not valid url', function () {
            var result = helper.processURL(location.hostname);
            expect(result.host).toEqual(location.hostname);
            expect(result.name).toEqual(capitalizedHostName);
        });

        it('receive empty string', function () {
            var result = helper.processURL('');
            expect(result.host).toEqual(location.hostname);
            expect(result.name).toEqual(capitalizedHostName);
        });

        it('receive non string', function () {
            var result = helper.processURL(2);
            expect(result.host).toEqual(location.hostname);
            expect(result.name).toEqual(capitalizedHostName);
        });

        it('is path replace works correctly', function () {
            var result = helper.processURL(location.hostname + '/' + location.hostname);
            expect(result.path).toEqual(location.hostname + ' ' + location.hostname);
        });

        it('is http without www correctny interpreted', function () {
            var result = helper.processURL('http://angular.com/about');
            expect(result).toEqual(validResult);
        });

        it('is http with www correctly interpreted', function () {
            var result = helper.processURL('http://www.' + location.hostname + '.com');
            expect(result.host).toEqual('www.' + location.hostname + '.com');
        });

        it('is www correctly interpreted', function () {
            var result = helper.processURL('www.' + location.hostname + '.com');
            expect(result.host).not.toEqual('www.' + location.hostname + '.com');
        });

        it('if host.length===1 or !isNAN slice correctly', function () {
            var result = helper.processURL('http://');
            expect(result.path).toEqual('');
        });

        it('hotslice join reverse works correctly ', function () {
            var result = helper.processURL('http://' + location.hostname + '.co.uk');
            expect(result.name).toEqual('Co localhost');
        });
    });
});