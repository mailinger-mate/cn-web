/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, _embedded, abstract, account, accounts, and, api_url,
    callFake, check, config, connection, connections, controller,
    controllerProvider, core, createSpy, current, data, destination,
    destinations, error, form, get, go, hoursInfo, id, isOpen, load,
    loadPlugin, mocked, name, objectContaining, pageTitle, path, put, redirect,
    resolve, respond, returnValue, session, some, source, stringify, template,
    templateUrl, then, toBeFalsy, toEqual, toHaveBeenCalledWith, token,
    transitionTo, type, unauthorized, url, user, value, views, whenGET
*/

describe("config: cn.connection", function () {
    'use strict';
    var cfg, $state, $rootScope, scope, $httpBackend, destination, connection, account, helper, $stateParams, ocLazyLoad, $q, action;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var ocLazyLoadMocked = {
        load: jasmine.createSpy().and.callFake(function () {
            return "loaded";
        })
    };
    var connectionMock = {
        _embedded: {
            account: {id: 4},
            destinations: [{id: 44, mocked: 'destinations'}]
        }
    };
    var destinationMock = {
        _embedded: {
            account: {id: 44242}
        }
    };
    var accountMock = {
        type: 'mockedAccountType'
    };
    var cfgMock = {
        connection: {
            config: {
                source: {
                    mocked: 'source'
                },
                destination: {
                    mocked: 'source'
                }
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.connection');
        module('cn.helper');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('connection', connectionMock);
            $provide.value('destination', destinationMock);
            $provide.value('account', accountMock);
            $provide.value('ocLazyLoad', ocLazyLoadMocked);
        });
        inject(function (_$rootScope_, _$state_, _$httpBackend_, $templateCache, _$q_, _action_, _ocLazyLoad_, _$stateParams_, _helper_, _cfg_, _connection_, _account_, _destination_) {
            connection = _connection_;
            destination = _destination_;
            account = _account_;
            ocLazyLoad = _ocLazyLoad_;
            $httpBackend = _$httpBackend_;
            $stateParams = _$stateParams_;
            $rootScope = _$rootScope_;
            $state = _$state_;
            $q = _$q_;
            action = _action_;
            helper = _helper_;
            cfg = _cfg_;
            scope = $rootScope.$new();
            $templateCache.put('html/connection/none.html', '');
            $templateCache.put('html/connection/list.html', '');
        });

        $httpBackend.whenGET('/health_check/e909fcd76cc25bb2a4bb19f17ea466e4').respond(200);
        $httpBackend.whenGET('api_url/connections').respond([{connection: 'mockedConnection'}]);
    });

    it('state connection works correctly', function () {
        $state.transitionTo('connection');
        $rootScope.$digest();
        expect($state.current).toEqual(jasmine.objectContaining({
            url: "/connection",
            template: '<div ui-view autoscroll="false"></div>',
            redirect: "connection.list",
            name: 'connection'
        }));
    });

    it('state connection none works correctly', function () {
        $state.transitionTo('connection.none');
        $rootScope.$digest();
        expect($state.current).toEqual(jasmine.objectContaining({
            url: "/none",
            templateUrl: 'html/connection/none.html',
            data: {pageTitle: 'connection.none.pageTitle'},
            name: 'connection.none'
        }));
    });

    it('state connection list works correctly with promise', function () {
        var state = $state.get('connection.list');
        spyOn(action.connection, 'get').and.callFake(function () {
            return {
                then: function (connections) {
                    return connections([{some: "thing", hoursInfo: {isOpen: true}}]);
                }
            };
        });
        expect(state).toEqual(jasmine.objectContaining({
            url: "/list",
            templateUrl: 'html/connection/list.html',
            data: {pageTitle: 'nav.connection.list'},
            name: 'connection.list'
        }));
        expect(JSON.stringify(state.resolve.connections($state, $q, action))).toEqual('{"$$state":{"status":1,"value":[{"some":"thing","hoursInfo":{"isOpen":true}}]}}');
    });

    it('state connection list works correctly with promise (rejected)', function () {
        var state = $state.get('connection.list');
        spyOn($state, 'go');
        spyOn(action.connection, 'get').and.callFake(function () {
            return {
                then: function (connections) {
                    return connections(null);
                }
            };
        });
        expect(JSON.stringify(state.resolve.connections($state, $q, action))).toEqual('{"$$state":{"status":2}}');
        expect($state.go).toHaveBeenCalledWith('connection.none');
    });


    it('state connection list works with lazyload resolve function', function () {
        var state = $state.get('connection.list');
        expect(state.resolve.loadPlugin(ocLazyLoad)).toEqual('loaded');
    });

    it('abstract state connection item works', function () {
        var state = $state.get('connection.item');
        spyOn(action.account, 'get').and.returnValue('done');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/item",
            templateUrl: 'html/connection/item.html',
            name: 'connection.item',
            controller: 'cnConnectionItem',
            abstract: true
        }));
        expect(state.resolve.accounts(action)).toEqual('done');
    });

    it('abstrac state connection.item.add works', function () {
        var state = $state.get('connection.item.add');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/add",
            templateUrl: 'html/connection/item.source.html',
            name: 'connection.item.add',
            controller: 'cnSourceItem',
            abstract: true,
            data: {
                pageTitle: 'nav.connection.add.source'
            }
        }));
    });

    it('connection.item.add.source resolve connection ', function () {
        var state = $state.get('connection.item.add.source');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/source/:type?account",
            name: 'connection.item.add.source',
            views: {
                form: jasmine.objectContaining({templateUrl: 'html/connection/item.source.form.html'})
            }
        }));
        expect(state.views.form.resolve.connection()).toEqual(false);
    });

    it('connection.item.add.source resolve account', function () {
        var state = $state.get('connection.item.add.source');
        $stateParams.account = '';
        expect(state.views.form.resolve.account($stateParams)).toEqual(false);
        $stateParams.account = {mocked: 'data'};
        scope.$digest();
        expect(state.views.form.resolve.account($stateParams)).toEqual({id: {mocked: 'data'}});
    });

    it('connection.item.add.source controllerProvider works fine with type', function () {
        var state = $state.get('connection.item.add.source');
        $stateParams.type = 'mocked';
        expect(state.views.form.controllerProvider($stateParams, helper, cfg)).toEqual('cnSourceItemMocked');
    });

    it('connection.item.add.source controllerProvider works without type', function () {
        var state = $state.get('connection.item.add.source');
        $stateParams.type = '';
        expect(state.views.form.controllerProvider($stateParams, helper, cfg)).toEqual('cnSourceItemDefault');
    });

    it('connection.item.edit state works', function () {
        var state = $state.get('connection.item.edit');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/edit/:connection",
            name: 'connection.item.edit',
            abstract: true,
            template: '<div ui-view autoscroll="false"></div>'
        }));
    });

    it('connection.item.edit.source state works', function () {
        var state = $state.get('connection.item.edit.source');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/source",
            controller: 'cnSourceItem',
            name: 'connection.item.edit.source',
            abstract: true,
            templateUrl: 'html/connection/item.source.html'
        }));
    });

    it('connection.item.edit.source.edit state works', function () {
        var state = $state.get('connection.item.edit.source.edit');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/edit",
            name: 'connection.item.edit.source.edit',
            views: {
                form: jasmine.objectContaining({templateUrl: 'html/connection/item.source.form.html'})
            }
        }));
    });

    it('connection.item.edit.source.edit resolve functions works', function () {
        spyOn(action.connection, 'get').and.returnValue('done');
        var state = $state.get('connection.item.edit.source.edit');
        $stateParams.connection = 5;
        expect(state.views.form.resolve.connection($stateParams, action)).toEqual('done');
        expect(action.connection.get).toHaveBeenCalledWith({id: 5});
        expect(state.views.form.resolve.account(connection)).toEqual({id: 4});
    });

    it('connection.item.edit.source.edit controllerProvider works', function () {
        var state = $state.get('connection.item.edit.source.edit');
        expect(state.views.form.controllerProvider(account, helper)).toEqual('cnSourceItemMockedaccounttype');
    });

    it('connection.item.edit.destination state works', function () {
        var state = $state.get('connection.item.edit.destination');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/destination",
            name: 'connection.item.edit.destination',
            controller: 'cnDestinationItem',
            templateUrl: 'html/connection/item.destination.html',
            abstract: true
        }));
    });

    it('connection.item.edit.destination.add state works', function () {
        var state = $state.get('connection.item.edit.destination.add');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/add/:type?account",
            data: {
                pageTitle: 'connection.item.title.destination.add'
            },
            views: {
                form: {
                    resolve: {
                        connection: state.views.form.resolve.connection,
                        destination: state.views.form.resolve.destination,
                        account: state.views.form.resolve.account
                    },
                    controllerProvider: state.views.form.controllerProvider,
                    templateUrl: "html/connection/item.destination.form.html"
                }
            },
            name: 'connection.item.edit.destination.add'
        }));
    });

    it('connection.item.edit.destination.add state resolve connection works', function () {
        spyOn(action.connection, 'get').and.returnValue('done');
        var state = $state.get('connection.item.edit.destination.add');
        expect(state.views.form.resolve.connection($stateParams, action)).toEqual('done');
        expect(action.connection.get).toHaveBeenCalledWith({id: 5});
    });

    it('connection.item.edit.destination.add state resolve return false', function () {
        var state = $state.get('connection.item.edit.destination.add');
        $stateParams.account = '';
        expect(state.views.form.resolve.account($stateParams)).toBeFalsy();
    });

    it('connection.item.edit.destination.add state resolve return id', function () {
        var state = $state.get('connection.item.edit.destination.add');
        $stateParams.account = 123;
        expect(state.views.form.resolve.account($stateParams)).toEqual({id: 123});
    });

    it('connection.item.edit.destination.add controllerProvider works with type', function () {
        var state = $state.get('connection.item.edit.destination.add');
        $stateParams.type = 'mocked';
        expect(state.views.form.controllerProvider($stateParams, helper, cfg)).toEqual('cnDestinationItemMocked');
    });

    it('connection.item.edit.destination.add controllerProvider works without type', function () {
        var state = $state.get('connection.item.edit.destination.add');
        $stateParams.type = '';
        expect(state.views.form.controllerProvider($stateParams, helper, cfg)).toEqual('cnDestinationItemDefault');
    });

    it('connection.item.edit.destination.edit state works', function () {
        var state = $state.get('connection.item.edit.destination.edit');
        expect(state).toEqual(jasmine.objectContaining({
            url: "/edit/:destination",
            name: 'connection.item.edit.destination.edit',
            views: {
                form: jasmine.objectContaining({templateUrl: 'html/connection/item.destination.form.html'})
            }
        }));
    });

    it('connection.item.edit.destination.edit resolve destination function works', function () {
        spyOn(action.connection, 'get').and.returnValue('done');
        var state = $state.get('connection.item.edit.destination.edit');
        expect(state.views.form.resolve.connection($stateParams, action)).toEqual('done');
        expect(action.connection.get).toHaveBeenCalledWith({id: 5});
        $stateParams.destination = '';
        expect(state.views.form.resolve.destination($stateParams, connection)).toBeFalsy();
        $stateParams.destination = 44;
        expect(state.views.form.resolve.destination($stateParams, connection)).toEqual({id: 44, mocked: 'destinations'});
    });

    it('connection.item.edit.destination.edit resolve destination without destination', function () {
        spyOn(action.connection, 'get').and.returnValue('done');
        var state = $state.get('connection.item.edit.destination.edit');
        expect(state.views.form.resolve.connection($stateParams, action)).toEqual('done');
        expect(action.connection.get).toHaveBeenCalledWith({id: 5});
        $stateParams.destination = '';
        expect(state.views.form.resolve.destination($stateParams, connection)).toBeFalsy();
    });

    it('connection.item.edit.destination.edit resolve account', function () {
        var state = $state.get('connection.item.edit.destination.edit');
        expect(state.views.form.resolve.account(destination)).toEqual({id: 44242});
    });


    it('connection.item.edit.destination.edit controllerProvider works', function () {
        var state = $state.get('connection.item.edit.destination.edit');
        expect(state.views.form.controllerProvider(account, helper)).toEqual('cnDestinationItemMockedaccounttype');
    });
});