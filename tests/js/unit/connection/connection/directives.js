/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $new, alert, check, css, databind, disabled, enabled,
    getStatusClass, global_code, icon, level, mockedEnable, mockedStatus, not,
    scope, session, setTitle, state, status, testIco, title, toEqual,
    toHaveAttr, toHaveClass, toHaveHtml, toHaveProp, type, value, warn
*/


describe("directives: cn.connection", function () {
    'use strict';
    var $rootScope, scope, $compile;
    var cfgMock = {
        session: {
            check: 2
        },
        css: {
            icon: {
                testIco: 'testIco',
                status: {warn: 'mockedWarn'},
                state: {
                    enabled: 'mockedEnabled',
                    disabled: 'mockDisabled'
                }
            }
        }
    };
    var status_global = {global_code: "G22", level: "WARN"};
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.connection');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.databind = status_global;
    });


    describe('directive cnIconApp', function () {
        it('link function getStatusClass works correctly', function () {
            var element = "<cn-Icon-App cn-status-global='{{databind}}' account='testIco'>";
            $rootScope.$digest();
            element = $compile(element)(scope);
            expect(element.scope().getStatusClass()).toEqual('status-warning');
        });

        it('link function getStatusClass works correctly (level exist)', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco'>")(scope);
            $rootScope.$digest();
            expect(element.scope().getStatusClass('mock')).toEqual('status-mock');
        });

        it('link function setTitle works correctly with argument', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco'>")(scope);
            scope.alert = 'alertMock';
            element.scope().setTitle('mock');
            $rootScope.$digest();
            expect(element).toHaveAttr('title', 'mock - alertMock');
        });

        it('link function setTitle works correctly with title', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' cn-Title='mockedTitle'>")(scope);
            scope.alert = 'alertMock';
            element.scope().setTitle();
            $rootScope.$digest();
            expect(element).toHaveAttr('title', 'mockedTitle - alertMock');
        });
        it('settings works fine', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' cn-Enabled='enable'>")(scope);
            scope.$digest();
            expect(element.scope().type).toEqual('testIco');
            expect(element.scope().getStatusClass('testStatusClass')).toEqual('status-teststatusclass');
        });
        it('setTitle works fine', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' cn-Enabled='enable'>")(scope);
            scope.$digest();
            element.scope().setTitle();
            expect(element.scope().title).toEqual('undefined');
            element.scope().setTitle('testSetTitle');
            expect(element.scope().title).toEqual('testSetTitle');
            element = $compile("<cn-Icon-App cn-status-global='{{databind}}' cn-title='mockTitle' cn-Enabled='enable'>")(scope);
            scope.alert = 'mockedAlert';
            scope.$digest();
            element.scope().setTitle('testSetTitle');
            expect(element.scope().title).toEqual('testSetTitle - mockedAlert');

        });
        it('cnEnabled attr', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' cn-Enabled='enable'>")(scope);
            scope.$digest();
            expect(element).toHaveClass('testIco status-disabled');
            expect(element).toHaveHtml('<span class="txt ng-binding">testIco</span>');
            expect(element).toHaveAttr('cn-enabled', 'enable');
            expect(element.scope().getStatusClass()).toEqual('status-warning');
        });

        it('cnTitle attr', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' cn-Title='mockedTitle'>")(scope);
            scope.$digest();
            expect(element).toHaveClass('testIco status-disabled');
            expect(element).toHaveHtml('<span class="txt ng-binding">testIco</span>');
            expect(element).toHaveAttr('cn-title', 'mockedTitle');
            expect(element.scope().getStatusClass()).toEqual('status-warning');
        });

        it('cnStatus attr status with no enable mode', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' cn-status='mockedStatus'>")(scope);
            scope.$digest();
            element = $compile("<cn-Icon-App cn-status-global='{{databind}}' cn-status='change'>")(scope);
            scope.$digest();
            expect(element).toHaveClass('status-disabled');
        });

        it('cnStatus attr status with enable mode', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' class='toDelete' cn-status='status' cn-enabled='false'>")(scope);
            $rootScope.$digest();
            element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' class='toDelete' cn-status='status' cn-enabled='true'>")(scope);
            expect(element).not.toHaveClass('status-disabled');
        });

        it('cnStatus attr status with blocked attr', function () {
            var element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' class='toDelete' cn-status='mockedStatus.level.toDelete' cn-blocked=G23 >")(scope);
            scope.$apply(function () {
                element = $compile("<cn-Icon-App cn-status-global='{{databind}}' account='testIco' cn-status='mockedStatus.level.test' cn-blocked=G23>")(scope);
            });
            expect(element).toHaveClass('status-disabled');
        });

    });

    it('directive cnIconEnabled works correctly (enabled)', function () {
        var element = $compile("<cn-Icon-Enabled enabled='true'>")(scope);
        scope.$digest();
        expect(element).toHaveClass('status-enabled mockedEnabled');
        expect(element).toHaveAttr('title');
        expect(element).toHaveProp('title', 'connection.state.enabled');
    });

    it('directive cnIconEnabled works correctly (disabled)', function () {
        expect($compile("<cn-Icon-Enabled disabled='true'>")(scope)).toHaveClass('status-disabled mockDisabled');
    });

    describe('cnIconStatus directive', function () {

        it('directive works fine with cn-status attr', function () {
            scope.mockedStatus = {global_code: "G22", level: "warn"};
            scope.mockedEnable = {global_code: "G22", level: "warn"};
            var element = $compile('<cn-icon-status cn-status="{{mockedStatus}}" cn-Enabled="{{mockedEnable}}">')(scope);
            scope.$digest();
            expect(element).toHaveProp('title', 'connection.status.global.G22');
            expect(element).toHaveClass('icon-box status-warn mockedWarn');
        });

        it('directive works fine with cn-status with G23 blocked status attr', function () {
            scope.status = {global_code: 'G23'};
            var element = $compile('<cn-icon-status cn-status="{{ status }}">')(scope);
            scope.$digest();
            expect(element).toHaveProp('title', 'connection.status.global.G23');
            expect(element).toHaveClass('status-disabled');
        });

        it('directive cnIconStatus works correctly without cnStatus attr', function () {
            scope.mockedStatus = {level: ''};
            scope.mockedEnable = {};
            var element = $compile('<cn-icon-status cn-status="{{mockedStatus}}" cn-Enabled="{{mockedEnable}}">')(scope);
            scope.$digest();
            expect(element).toHaveAttr('cn-enabled');
            expect(element).toHaveProp('title', 'connection.status.global.undefined');
        });

    });

});