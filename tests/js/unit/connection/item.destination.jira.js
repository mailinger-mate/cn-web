/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, $scope, $timeout, account, action, and, api_url,
    callThrough, check, config, connection, core, data, destination, dynamic,
    error, field, flush, form, id, info, initDestination, issue_type, jira,
    key, label, model, name, not, objectContaining, path, placeholder,
    project_key, required, respond, rpc_url, session, template, toBeDefined, toBeTruthy,
    toEqual, toHaveBeenCalled, toHaveBeenCalledWith, token, translate, trolo,
    type, unauthorized, user, value, verifyNoOutstandingExpectation,
    verifyNoOutstandingRequest, whenGET
*/


describe("controller: cnDestinationItemJira", function () {
    'use strict';
    var scope, $rootScope, $httpBackend, $timeout, cnDestinationItemJira, account, connection, destination, action;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    var destinationMock = {destination: 'destination'};
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var cfgMock = {
        connection: {
            template: {
                destination: {
                    jira: []
                }
            }
        },
        core: {
            api_url: 'api_url/',
            rpc_url: 'rpc_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        module('pascalprecht.translate');
        module('cn.destination.jira');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
            $provide.value('destination', destinationMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_, _destination_, _action_, _$httpBackend_, _$timeout_) {
            $rootScope = _$rootScope_;
            $timeout = _$timeout_;
            account = _account_;
            connection = _connection_;
            destination = _destination_;
            action = _action_;
            $httpBackend = _$httpBackend_;
            scope = $rootScope.$new();
            scope.initDestination = function () {
                return true;
            };
            cnDestinationItemJira = function () {
                return $controller('cnDestinationItemJira', {
                    $scope: scope,
                    account: account,
                    connection: connection,
                    destination: destination,
                    action: action,
                    $timeout: $timeout
                });
            };
        });
        scope.data = {
            destination: ''
        };
        scope.form =
                {
            config: {
                account: {
                    id: 342
                },
                field: {
                    config: [2, 3, 4],
                    dynamic: {
                        model: 0
                    }
                }
            },
            field: {
                config: {
                    project_key: {
                        value: 5123
                    },
                    field: {
                        dynamic: ''
                    },
                    issue_type: {
                        value: ''
                    },
                    action: {
                        value: ''
                    }
                }
            }
        };
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('cnDestinationItemJira controller is defined and works fine', function () {
        expect(cnDestinationItemJira).toBeDefined();
        expect(cnDestinationItemJira()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initDestination');
        expect(scope.initDestination).not.toHaveBeenCalled();
        cnDestinationItemJira();
        expect(scope.initDestination).toHaveBeenCalledWith({type: 'jira', connection: connection, destination: destination, account: accountMock});
    });

    it('delete scopes depending on form.field.config.action.value', function () {
        scope.form.field.config.action.value = 'update';
        scope.form.config.field.dynamic = [{model: 'action'}];
        cnDestinationItemJira();
        scope.$digest();
        expect(scope.form.config.field.dynamic).not.toEqual('toDelete');
        expect(scope.form.field.config.action).not.toBeDefined();
    });

    it('form.field.config.issue_type.value before timeout', function () {
        spyOn(action.account, 'info').and.callThrough();
        $httpBackend.whenGET('api_url/accounts/342/info/project/5123/issue_type.value?field1=project').respond([{data: 'data'}]);
        scope.form.field.config.issue_type.value = 'issue_type.value';
        cnDestinationItemJira();
        $httpBackend.flush();
        expect(scope.form.config.field.dynamic).toEqual([]);
        expect(action.account.info).toHaveBeenCalledWith(jasmine.objectContaining({
            id: 342,
            field: ['project', 5123, 'issue_type.value']
        }));
    });

    it('timeout if template exist', function () {
        $httpBackend.whenGET('api_url/accounts/342/info/project/5123/issue_type.value?field1=project').respond([{key: 'fieldMocked', label: 'mockedLabel'}]);
        scope.form.field.config.issue_type.value = 'issue_type.value';
        cnDestinationItemJira();
        $httpBackend.flush();
        expect(scope.form.config.field.dynamic).toEqual([]);
        $timeout.flush();
        expect(scope.form.config.field.dynamic).toEqual([{
            name: 'fieldMocked',
            model: 'fieldMocked',
            required: false,
            label: 'mockedLabel',
            translate: false,
            placeholder: 'fieldMocked'
        }]);

    });
});