/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $current, $digest, $new, $scope, $setPristine, $valid, account, account1,
    action, addAttachment, all, and, api_url, attachment, attachments, button,
    call, callFake, check, class, config, connection, core, ctrl, current,
    data, defer, deleteAttachment, deleteDestination, destination, dynamic,
    error, errors, extra, field, form, getRefText, getRefTextRaw, go, id,
    initConnection, initDestination, initReference, label, length, mocked,
    mode, name, not, notify, objectContaining, options, path, placeholder,
    promise, ref, resolve, resource, returnValue, run, save, saveDestination,
    searchRef, selectAttachment, session, show, some, stringify, success,
    template, templates, test, text, toBeDefined, toBeFalsy, toBeTruthy,
    toContain, toEqual, toHaveBeenCalled, toHaveBeenCalledWith, token,
    translate, type, unauthorized, update, user, validateForm, value, view
*/

describe("module: cn.connection.item.destination", function () {
    'use strict';
    var scope, cnDestinationItem, action, $rootScope, deferred, cnDestinationItemDefault, cfg, core, $q, $state, connection, $stateParams;
    var connectionsMock = {account: 'mock'};
    var connectionMock = {account1: 'mock1'};
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var config = {
        type: '',
        connection: {
            destination: {}
        }
    };
    var cfgMock = {
        connection: {
            template: {
                destination: {
                    '100': 'template1'
                }
            },
            attachment: {mocked: 'attachment'},
            config: {
                destination: {'100': 'destination1', '2': 'destination2', '7': 'destination3'}
            }
        },
        account: {
            config: {
                type: {name: ''}
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.connection.item.destination');
        module(function ($provide) {
            $provide.value('connections', connectionsMock);
            $provide.value('connection', connectionMock);
            $provide.value('cfg', cfgMock);
        });
        inject(function ($controller, _$rootScope_, _cfg_, _core_, _$q_, _$state_, _connection_, _$stateParams_, _action_) {
            $q = _$q_;
            action = _action_;
            connection = _connection_;
            $stateParams = _$stateParams_;
            $state = _$state_;
            core = _core_;
            cfg = _cfg_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            deferred = $q.defer();
            cnDestinationItem = function () {
                return $controller('cnDestinationItem', {
                    $scope: scope,
                    core: core
                });
            };
            cnDestinationItemDefault = function () {
                return $controller('cnDestinationItemDefault', {
                    $scope: scope,
                    connection: connection
                });
            };
        });
        scope.initConnection = function () {
            return;
        };
        scope.initReference = function () {
            return;
        };
        scope.validateForm = function () {
            return;
        };
        scope.initDestination = function () {
            return;
        };
        scope.view = {
            name: ''
        };
        scope.data = {
            connection: {
                id: 421412
            },
            destination: {
                id: 123
            },
            ref: {
                extra: ''
            }
        };
        scope.form = {
            action: {
                success: ''
            },
            error: {
                field: ''
            },
            ctrl: {
                $setPristine: function () {
                    return;
                }
            },
            field: {
                attachments: [{test: 'attachments'}]
            },
            config: {
                account: {
                    id: 300
                },
                type: 'mockedType',
                field: {
                    type: {
                        options: {},
                        placeholder: ''
                    }
                }
            }
        };

    });

    it('cnDestinationItem controller is defined', function () {
        expect(cnDestinationItem()).toBeTruthy();
    });

    it('scopes are defined', function () {
        cnDestinationItem();
        expect(scope.view.name).toEqual('destination');
        scope.view.name = "destination";
        expect(scope.form.config.field.type.placeholder).toEqual('type_destination');
    });

    it('selectAttachment works good', function () {
        var type = 'mocked';
        var reset = true;
        scope.form.field.attachments = 'data';
        cnDestinationItem();
        scope.selectAttachment(type, reset);
        expect(scope.form.config.attachments).toBeTruthy();
        expect(scope.form.field.attachments).toEqual([]);
        scope.selectAttachment('', reset);
        expect(scope.form.config.attachments).toBeFalsy();
        scope.selectAttachment({}, false);
        expect(scope.form.config.attachments).toBeFalsy();
    });

    it('addAttachment works good', function () {
        cnDestinationItem();
        scope.addAttachment();
        expect(scope.form.field.attachments).toEqual([{test: 'attachments'}, {}]);
        scope.form.field = [];
        scope.form.field.attachments = [];
        scope.addAttachment();
        expect(scope.form.field.attachments).toEqual([{}]);
    });

    it('deleteAttachment works good', function () {
        scope.form.field.attachments = ['test0', 'test1', 'test2'];
        cnDestinationItem();
        scope.deleteAttachment(1);
        expect(scope.form.field.attachments).toEqual(['test0', 'test2']);
    });
    it('searchRef works good with string', function () {
        cnDestinationItem();
        scope.data.ref.all = ['string'];
        scope.searchRef();
        expect(scope.ref).toEqual([{name: 'string'}]);
    });

    it('searchRef works good with number', function () {
        cnDestinationItem();
        scope.data.ref.all = [2];
        scope.searchRef();
        expect(scope.ref).toEqual([2]);
    });
    it('searchRef works good with term', function () {
        cnDestinationItem();
        scope.data.ref.all = ['string'];
        scope.searchRef('str');
        expect(scope.ref).toEqual([{name: 'string'}]);
    });

    it('getRefText works good', function () {
        cnDestinationItem();
        expect(JSON.stringify(scope.getRefText({name: 'mockedName'}))).toEqual('"%{mockedName}"');
    });

    it('getRefTextRaw works good', function () {
        cnDestinationItem();
        expect(JSON.stringify(scope.getRefTextRaw({name: 'mockedName'}))).toEqual('"%{mockedName}"');
    });

    it('initDestination works without config', function () {
        spyOn(scope, 'initReference');
        cnDestinationItem();
        scope.initDestination(config);
        expect(scope.view.mode).toEqual('add');
        expect(scope.form.config.field.type.options).toContain('100');
        expect(scope.form.config.field.type.options).toContain('2');
        expect(scope.form.config.field.type.options).toContain('7');
        expect(scope.form.config.field.type.options.length).toEqual(3);
        expect(scope.form.field).toEqual();
        expect(scope.form.config.type).toEqual();
        expect(scope.form.config.account).toEqual();
        expect(scope.form.config.field.config).toEqual();
        expect(scope.form.config.field.dynamic).toEqual();
        expect(scope.form.config.field.template).toEqual();
        expect(scope.form.config.field.attachment).toEqual();
        expect(scope.data.destination).toEqual();
        expect(scope.form.button).toEqual(jasmine.objectContaining([{
            label: "save",
            type: "submit",
            action: "saveDestination",
            class: "btn-primary"
        }]));
        expect(scope.initReference).toHaveBeenCalled();
    });

    it('initDestination works with config.type', function () {
        //CN-1976
        config.type = 100;
        cfg.connection.attachment = {'100': 'attachment1'};
        cnDestinationItem();
        scope.initDestination(config);
        expect(scope.form.config.type).toEqual(100);
        expect(scope.form.config.field.config).toEqual('destination1');
        expect(scope.form.config.field.template).toEqual('template1');
        expect(scope.form.config.attachment).toEqual('attachment1');
    });

    it('initDestination works with config.connection', function () {
        spyOn(scope, 'initConnection');
        config.connection = 100;
        cnDestinationItem();
        scope.initDestination(config);
        expect(scope.initConnection).toHaveBeenCalledWith(100);
    });

    it('initDestination works with config.account', function () {
        config.account = 100;
        cnDestinationItem();
        scope.initDestination(config);
        expect(scope.form.config.account).toEqual(100);
    });

    it('initDestination works with config.destination', function () {
        config = {};
        config.destination = {config: {mocked: 'config'}, attachments: {mocked: 'attachments'}, templates: {mocked: 'templates'}};
        cnDestinationItem();
        expect(scope.form.button).toEqual();
        scope.initDestination(config);
        expect(scope.view.mode).toEqual('edit');
        expect(scope.data.destination).toEqual(config.destination);
        expect(scope.form.field).toEqual(jasmine.objectContaining({
            config: {mocked: 'config'},
            templates: {mocked: 'templates'},
            attachments: {mocked: 'attachments'}
        }));
    });

    it('saveDestination function return false is not valid', function () {
        spyOn(scope, 'validateForm');
        cnDestinationItem();
        expect(scope.saveDestination()).toEqual(false);
        expect(scope.validateForm).toHaveBeenCalled();
    });

    it('saveDestination with valid form (edit mode) success function', function () {
        spyOn(scope.form.action, 'success');
        spyOn(scope, 'validateForm');
        deferred.resolve({data: {some: 'stuff'}});
        spyOn(core.destination, 'update').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success('destination');
        });
        scope.view.mode = 'edit';
        cnDestinationItem();
        scope.form.ctrl.$valid = true;
        expect(scope.saveDestination()).toEqual('destination');
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    text: 'account.item.select.type.mockedType'
                }
            },
            resource: "connection",
            name: "destination.edit",
            call: deferred.promise
        }));
        expect(scope.form.error.field).not.toBeDefined();
        expect(scope.form.action.success).toHaveBeenCalledWith('destination');
        expect(core.destination.update).toHaveBeenCalledWith({id: 123}, {attachments: [{test: 'attachments'}]});

    });

    it('saveDestination with valid form (edit mode) success function change state', function () {
        scope.form.action = {};
        spyOn($state, 'go');
        spyOn(scope, 'validateForm');
        deferred.resolve({data: {some: 'stuff'}});
        spyOn(core.destination, 'update').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success({id: 'test'});
        });
        scope.view.mode = 'edit';
        cnDestinationItem();
        scope.form.ctrl.$valid = true;
        expect(scope.saveDestination()).toEqual({id: 'test'});
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    text: 'account.item.select.type.mockedType'
                }
            },
            resource: "connection",
            name: "destination.edit",
            call: deferred.promise
        }));
        expect(scope.form.error.field).not.toBeDefined();
        expect($state.go).toHaveBeenCalledWith('connection.item.edit.destination.edit', {connection: 421412, destination: 'test'});
        expect(core.destination.update).toHaveBeenCalledWith({id: 123}, {attachments: [{test: 'attachments'}]});
    });

    it('saveDestination with valid form (edit mode) error function', function () {
        scope.form.action = {};
        spyOn(scope, 'validateForm');
        deferred.resolve();
        spyOn(core.destination, 'update').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.error({data: {errors: 'test'}});
        });
        scope.view.mode = 'edit';
        cnDestinationItem();
        scope.form.ctrl.$valid = true;
        scope.saveDestination();
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    text: 'account.item.select.type.mockedType'
                }
            },
            resource: "connection",
            name: "destination.edit",
            call: deferred.promise
        }));
        expect(scope.form.error.field).toEqual('test');
        expect(core.destination.update).toHaveBeenCalledWith({id: 123}, {attachments: [{test: 'attachments'}]});
    });

    it('saveDestination with valid form success function', function () {
        spyOn(scope.form.action, 'success');
        spyOn(scope, 'validateForm');
        deferred.resolve({data: {some: 'stuff'}});
        spyOn(core.destination, 'save').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success('destination');
        });
        scope.view.mode = '';
        cnDestinationItem();
        scope.form.ctrl.$valid = true;
        expect(scope.saveDestination()).toEqual('destination');
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    text: 'account.item.select.type.mockedType'
                }
            },
            resource: "connection",
            name: "destination.",
            call: deferred.promise
        }));
        expect(scope.form.error.field).not.toBeDefined();
        expect(scope.form.action.success).toHaveBeenCalledWith('destination');
        expect(core.destination.save).toHaveBeenCalledWith({attachments: [{test: 'attachments'}], account: 300, connection: 421412});
    });

    it('scope.deleteDestination works fine', function () {
        spyOn($state, 'go');
        spyOn(scope, 'validateForm');
        deferred.resolve({data: {some: 'stuff'}});
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success('destination');
        });
        spyOn(core.destination, 'save').and.returnValue(deferred.promise);
        cnDestinationItem();
        expect(scope.deleteDestination()).toEqual('destination');
        expect($state.go).toHaveBeenCalledWith('connection.item.edit.source.edit', {connection: 421412});
    });

    it('watch scope form.config.type works', function () {
        cnDestinationItem();
        spyOn($state, 'go');
        scope.initDestination({type: {account1: 'mock1'}});
        $state.$current.name = 'someStateMock';
        scope.form.config.type = {some: 'data'};
        scope.$digest();
        scope.initDestination({type: {account1: 'mock2'}});
        scope.$digest();
        expect($state.go).toHaveBeenCalledWith('someStateMock', {type: {account1: 'mock2'}, account: undefined});
    });

    it('cnDestinationItemDefault controller is defined and works fine ', function () {
        spyOn(scope, 'initDestination');
        spyOn($state, 'go');
        $state.current.name = 'mockedState';
        $stateParams.type = 'mock';
        cnDestinationItemDefault();
        expect(scope.initDestination).toHaveBeenCalledWith({connection: {account1: 'mock1'}});
        expect($state.go).toHaveBeenCalledWith('mockedState', {type: null});
    });
});