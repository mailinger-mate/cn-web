/*global describe module beforeEach it expect inject*/
/*property
    $apply, $new, $scope, attachmentDisable, attachments, field, form,
    initDestination, not, service, toBeDefined, toEqual
*/


describe('Controller: cnDestinationItemSlack', function () {

    'use strict';

    var cnDestinationItemSlack,
        scope;

    beforeEach(function () {
        module('cn.destination.slack', function ($provide) {
            $provide.service('connection', function () {
                return '5';
            });
            $provide.service('destination', function () {
                return false;
            });
            $provide.service('account', function () {
                return false;
            });

            return null;
        });
    });


    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();


        scope.initDestination = function () {
            return null;
        };

        cnDestinationItemSlack = $controller('cnDestinationItemSlack', {
            $scope: scope
        });

    }));

    it("controller should be defined", function () {
        expect(cnDestinationItemSlack).toBeDefined();
    });

    it("should disable attachments if user is bot", function () {
        scope.$apply('form.field.config.content.meta.as_bot = false');
        scope.$apply('form.field.config.content.meta.as_bot = true');
        expect(scope.attachmentDisable).toEqual(true);
        expect(scope.form.field.attachments).not.toBeDefined();
    });

    it("should enable attachments if user it not a bot", function () {
        scope.$apply('form.field.config.content.meta.as_bot = false');
        expect(scope.attachmentDisable).toEqual(false);
    });

});