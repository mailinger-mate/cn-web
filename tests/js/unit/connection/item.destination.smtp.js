/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, connection, destination, initDestination, not,
    toBeDefined, toBeTruthy, toHaveBeenCalled, toHaveBeenCalledWith, type,
    value
*/

describe("controller: cnDestinationItemSmtp", function () {
    'use strict';
    var scope, $rootScope, cnDestinationItemSmtp, account, connection, destination;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    var destinationMock = {destination: 'destination'};
    beforeEach(function () {
        module('cn.destination.smtp');
        module(function ($provide) {
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
            $provide.value('destination', destinationMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_, _destination_) {
            $rootScope = _$rootScope_;
            account = _account_;
            connection = _connection_;
            destination = _destination_;
            scope = $rootScope.$new();
            scope.initDestination = function () {
                return true;
            };
            cnDestinationItemSmtp = function () {
                return $controller('cnDestinationItemSmtp', {
                    $scope: scope,
                    account: account,
                    connection: connection
                });
            };
        });
    });

    it('cnDestinationItemSmtp controller is defined and works fine', function () {
        expect(cnDestinationItemSmtp).toBeDefined();
        expect(cnDestinationItemSmtp()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initDestination');
        expect(scope.initDestination).not.toHaveBeenCalled();
        cnDestinationItemSmtp();
        expect(scope.initDestination).toHaveBeenCalledWith({type: 'smtp', connection: connection, destination: destination, account: accountMock});
    });
});