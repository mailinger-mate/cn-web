/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $error, $new, $scope, _embedded, account, accounts,
    action, all, and, api_url, callThrough, check, config, connection, core,
    ctrl, data, destination, destinations, dynamic, error, extra, field, flush,
    form, id, info, initAccounts, initConnection, initReference, mockedType,
    mode, name, not, objectContaining, path, placeholder, ref, required,
    resetRef, respond, rpc_url, session, some, source, stringify, template, test,
    toBeDefined, toBeTruthy, toEqual, toHaveBeenCalledWith, token, type,
    unauthorized, user, value, view, whenGET
*/


describe("controller: cnConnectionItem", function () {
    'use strict';
    var scope, $rootScope, cnConnectionItem, accounts, action, $httpBackend;
    var accountsMock = {accounts: 'account'};
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var cfgMock = {
        connection: {
            placeholder: [],
            ref: {
                mockedType: {
                    dynamic: {
                        field: 'mockedField',
                        name: 'mockedName'
                    }
                }
            },
            template: {
                destination: {
                }
            }
        },
        core: {
            api_url: 'api_url/',
            rpc_url: 'rpc_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        module('pascalprecht.translate');
        module('cn.connection.item');
        module(function ($provide) {
            $provide.value('accounts', accountsMock);
            $provide.value('cfg', cfgMock);
        });

        inject(function ($controller, _$rootScope_, _accounts_, _action_, _$httpBackend_) {
            $rootScope = _$rootScope_;
            $httpBackend = _$httpBackend_;
            accounts = _accounts_;
            action = _action_;
            scope = $rootScope.$new();
            cnConnectionItem = function () {
                return $controller('cnConnectionItem', {
                    $scope: scope,
                    action: action
                });
            };
        });
        scope.data = {
            connection: ''
        };
    });

    it('cnConnectionItem controller is defined and works fine', function () {
        expect(cnConnectionItem()).toBeTruthy();
    });


    it('scope view is defined', function () {
        cnConnectionItem();
        expect(scope.view).toEqual({mode: 'add', action: {account: true}});
    });

    it('scope data is defined', function () {
        cnConnectionItem();
        expect(scope.data).toEqual({accounts: {accounts: 'account'}});
    });

    it('scope form is defined', function () {
        cnConnectionItem();
        expect(scope.form).toBeDefined();
    });

    it('scope initConnection delete scopes', function () {
        cnConnectionItem();
        scope.data.connection = 'somedata';
        scope.data.destinations = 'somedata';
        scope.data.source = 'somedata';
        scope.initConnection();
        expect(scope.data.connection).not.toBeDefined();
        expect(scope.data.destinations).not.toBeDefined();
        expect(scope.data.source).not.toBeDefined();
    });

    it('scope initConnection assigne new data', function () {
        scope.data.connection = 'beforeChange';
        cnConnectionItem();
        scope.initConnection({some: 'data', _embedded: {destinations: 'mockedDestination', account: 'mockedAccount'}});
        expect(scope.data.destinations).toEqual('mockedDestination');
        expect(scope.data.source).toEqual({account: 'mockedAccount'});
    });

    it('scope initAccounts works corectly', function () {
        cnConnectionItem();
        scope.initAccounts(accounts);
        expect(scope.data.accounts).toEqual({accounts: 'account'});
    });


    it('initReference works fine when !scope.data.source action.account.info is called with success', function () {
        $httpBackend.whenGET('api_url/accounts/5123/info/mockedName').respond([{name: 'test'}, {name: 'test1'}]);
        spyOn(action.account, 'info').and.callThrough();
        cnConnectionItem();
        scope.initReference();
        scope.form.config.account = {
            type: 'mockedType',
            id: 5123,
            extra: [{extra: 'field'}]
        };
        scope.$digest();
        $httpBackend.flush();
        expect(action.account.info).toHaveBeenCalledWith(jasmine.objectContaining({
            id: 5123,
            field: 'mockedName'
        }));
    });


    it('initReference works fine when !scope.data.source action.account.info is called with success', function () {
        var respondData = [{name: 'test'}, {name: 'test1'}];
        $httpBackend.whenGET('api_url/accounts/5123/info/mockedName').respond(respondData);
        spyOn(action.account, 'info').and.callThrough();
        cnConnectionItem();
        scope.initReference();
        scope.form.config.account = {
            type: 'mockedType',
            id: 5123,
            extra: [{extra: 'field'}]
        };
        scope.$digest();
        $httpBackend.flush();
        expect(action.account.info).toHaveBeenCalledWith(jasmine.objectContaining({
            id: 5123,
            field: 'mockedName'
        }));
        expect(JSON.stringify(scope.data.ref.source)).toEqual(JSON.stringify(respondData));
        expect(JSON.stringify(scope.data.ref.all)).toEqual(JSON.stringify(respondData));
    });

    it('initReference works fine when !scope.data.source action.account.info is called and get no data', function () {
        $httpBackend.whenGET('api_url/accounts/5123/info/mockedName').respond(400);
        spyOn(action.account, 'info').and.callThrough();
        cnConnectionItem();
        scope.initReference();
        scope.form.config.account = {
            type: 'mockedType',
            id: 5123,
            extra: [{extra: 'field'}]
        };
        scope.$digest();
        $httpBackend.flush();
        expect(action.account.info).toHaveBeenCalledWith(jasmine.objectContaining({
            id: 5123,
            field: 'mockedName'
        }));
        expect(scope.data.ref.source).toEqual([]);
    });

    it('scope.resetRef works fine', function () {
        scope.ref = 'mock';
        cnConnectionItem();
        scope.resetRef();
        expect(scope.ref).not.toEqual('mock');
    });

    it('scope.validateform works fine', function () {
        cnConnectionItem();
        scope.form = {
            ctrl: {
                $error: {required: {}}
            }
        };
        scope.form.ctrl.$error.required = [{test: 'test'}, {test: 'test2'}];
        scope.$apply();
    });
});