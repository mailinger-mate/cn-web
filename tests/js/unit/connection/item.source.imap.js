/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, connection, initSource, not, toBeDefined,
    toBeTruthy, toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnSourceItemImap", function () {
    'use strict';
    var scope, $rootScope, cnSourceItemImap, account, connection;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    beforeEach(function () {
        module('cn.source.imap');
        module(function ($provide) {
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_) {
            $rootScope = _$rootScope_;
            account = _account_;
            connection = _connection_;
            scope = $rootScope.$new();
            scope.initSource = function () {
                return true;
            };
            cnSourceItemImap = function () {
                return $controller('cnSourceItemImap', {
                    $scope: scope,
                    account: account,
                    connection: connection
                });
            };
        });
    });

    it('cnSourceItemImap controller is defined and works fine', function () {
        expect(cnSourceItemImap).toBeDefined();
        expect(cnSourceItemImap()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initSource');
        expect(scope.initSource).not.toHaveBeenCalled();
        cnSourceItemImap();
        expect(scope.initSource).toHaveBeenCalledWith({type: 'imap', connection: connection, account: accountMock});
    });
});