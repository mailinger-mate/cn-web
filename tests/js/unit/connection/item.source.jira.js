/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, connection, initSource, not, toBeDefined,
    toBeTruthy, toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnSourceItemJira", function () {
    'use strict';
    var scope, $rootScope, cnSourceItemJira, account, connection;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    beforeEach(function () {
        module('cn.source.jira');
        module(function ($provide) {
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_) {
            $rootScope = _$rootScope_;
            account = _account_;
            connection = _connection_;
            scope = $rootScope.$new();
            scope.initSource = function () {
                return true;
            };
            cnSourceItemJira = function () {
                return $controller('cnSourceItemJira', {
                    $scope: scope,
                    account: account,
                    connection: connection
                });
            };
        });
    });

    it('cnSourceItemJira controller is defined and works fine', function () {
        expect(cnSourceItemJira).toBeDefined();
        expect(cnSourceItemJira()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initSource');
        expect(scope.initSource).not.toHaveBeenCalled();
        cnSourceItemJira();
        expect(scope.initSource).toHaveBeenCalledWith({type: 'jira', connection: connection, account: accountMock});
    });
});