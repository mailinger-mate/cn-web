/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $current, $digest, $new, $pristine, $scope, $setPristine,
    $stateParams, $translate, $valid, account, account1, action, addFieldRule,
    and, api_url, attachment, button, call, callFake, check, class, config,
    connection, core, ctrl, data, defer, delete, deleteConnection,
    deleteFieldRule, destination, dynamic, enabled, error, errors, extra,
    field, field_rules, form, fuled_rules, go, id, initConnection,
    initDestination, initReference, initSource, interval, label, length, mock,
    mocked, mocked0, mocked1, mode, name, not, notify, objectContaining,
    options, path, placeholder, promise, ref, resolve, resource, returnValue,
    run, save, saveSource, service, session, show, some, source, success,
    template, toBeDefined, toBeFalsy, toBeTruthy, toContain, toEqual,
    toHaveBeenCalled, toHaveBeenCalledWith, token, translate, type,
    unauthorized, update, use, useLoader, user, validateForm, value, view
*/

describe("module: cn.connection.item.source", function () {
    'use strict';
    var scope, $rootScope, cnSourceItem, cnSourceItemDefault, deferred, action, core, $q, $state, connection, $stateParams;
    var connectionsMock = {account: 'mock'};
    var connectionMock = {account1: 'mock1'};
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var config = {
        type: '',
        connection: {
            destination: {}
        }
    };

    var cfgMock = {
        connection: {
            template: {
                destination: {
                    '100': 'template1'
                }
            },
            attachment: {mocked: 'attachment'},
            config: {
                source: {'100': 'source1', '2': 'source2', '7': 'source3'}
            }
        },
        account: {
            config: {
                type: {name: ''}
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.connection.item.source');
        module(function ($translateProvider) {
            $translateProvider.useLoader('customLoader');
        });

        module(function ($provide) {
            $provide.value('connections', connectionsMock);
            $provide.value('connection', connectionMock);
            $provide.value('cfg', cfgMock);
            $provide.service('customLoader', function ($q) {
                return function () {
                    deferred = $q.defer();
                    deferred.resolve({
                        "connection.name.new": "mockedTranslate {{text}}"
                    });
                    return deferred.promise;
                };
            });
        });

        inject(function ($controller, _$rootScope_, _core_, _$q_, _$state_, _connection_, _$stateParams_, _action_, $translate) {
            $translate.use("en_US");
            $q = _$q_;
            action = _action_;
            connection = _connection_;
            $stateParams = _$stateParams_;
            $state = _$state_;
            core = _core_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            deferred = $q.defer();
            cnSourceItem = function () {
                return $controller('cnSourceItem', {
                    $scope: scope,
                    action: action,
                    $translate: $translate
                });
            };
            cnSourceItemDefault = function () {
                return $controller('cnSourceItemDefault', {
                    $scope: scope,
                    connection: connection,
                    $stateParams: $stateParams
                });
            };
        });
        scope.initConnection = function () {
            return;
        };
        scope.initReference = function () {
            return;
        };
        scope.initSource = function () {
            return;
        };
        scope.validateForm = function () {
            return;
        };
        scope.initDestination = function () {
            return;
        };
        scope.view = {
            name: ''
        };
        scope.data = {
            connection: {id: 2},
            ref: {
                extra: ''
            }
        };
        scope.form = {
            action: {
                success: ''
            },
            field: {
                field_rules: [{mock: 'fieldRule'}],
                config: '',
                interval: ''
            },
            ctrl: {
                $setPristine: function () {
                    return;
                }
            },
            config: {
                type: '',
                account: {
                    id: ''
                },
                field: {
                    type: {
                        options: {},
                        placeholder: ''
                    }
                }
            }
        };
    });

    it('cnSourceItem controller is defined', function () {
        expect(cnSourceItem()).toBeTruthy();
    });

    it('starting scopes are defined', function () {
        cnSourceItem();
        expect(scope.form.config.field.type.placeholder).toEqual('type_source');
        expect(scope.view.name).toEqual('source');
        expect(scope.form.field).not.toBeDefined();
    });
    it('initSource works without config', function () {
        spyOn(scope, 'initReference');
        config.connection = null;
        cnSourceItem();
        scope.initSource(config);
        expect(scope.view.mode).toEqual('add');
        expect(scope.form.config.field.type.options).toContain('7');
        expect(scope.form.config.field.type.options).toContain('2');
        expect(scope.form.config.field.type.options).toContain('100');
        expect(scope.form.config.field.type.options.length).toEqual(3);
        expect(scope.form.field).toEqual();
        expect(scope.form.config.type).toEqual();
        expect(scope.form.config.account).toEqual();
        expect(scope.form.config.field.config).toEqual();
        expect(scope.form.config.field.dynamic).toEqual();
        expect(scope.initReference).toHaveBeenCalled();
    });
    it('initSource provides save button with existing connection', function () {
        config.connection = {name: 'mockedName', enabled: 'mockedEnabled', interval: 'mockedInterval', config: 'mockedConfig', field_rules: 'mocked_field_rules'};
        cnSourceItem();
        scope.initSource(config);
        expect(scope.form.button).toEqual(jasmine.objectContaining([{
            label: "save",
            type: "submit",
            action: "saveSource",
            class: "btn-primary"
        }]));
    });
    it('initSource provides continue button without existing connection', function () {
        config.connection = null;
        cnSourceItem();
        scope.initSource(config);
        expect(scope.form.button).toEqual(jasmine.objectContaining([{
            label: "continue",
            type: "submit",
            action: "saveSource",
            class: "btn-primary"
        }]));
    });
    it('initSource works delete scopes when scope.form.field ', function () {
        cnSourceItem();
        scope.form.field = 'test';
        scope.initSource(config);
        expect(scope.form.field.interval).toEqual();
        expect(scope.form.config.account).toEqual();
        expect(scope.form.config.field.config).toEqual();
    });

    it('initSource works with config.type', function () {
        cnSourceItem();
        config.type = '100';
        scope.initSource(config);
        expect(scope.form.config.type).toEqual('100');
        expect(scope.form.config.field.config).toEqual('source1');
    });

    it('initSource works with config.account', function () {
        cnSourceItem();
        config.account = '100';
        scope.initSource(config);
        expect(scope.form.config.account).toEqual('100');
    });

    it('initSource works with config.connection', function () {
        cnSourceItem();
        config.connection = {name: 'mockedName', enabled: 'mockedEnabled', interval: 'mockedInterval', config: 'mockedConfig', field_rules: 'mocked_field_rules'};
        scope.initSource(config);
        expect(scope.view.mode).toEqual('edit');
        expect(scope.form.field).toEqual(config.connection);
    });

    it('saveSource method works with invalid form', function () {
        spyOn(scope, 'validateForm');
        cnSourceItem();
        expect(scope.saveSource()).toBeFalsy();
        expect(scope.validateForm).toHaveBeenCalled();
    });

    it('saveSource method with edit success property', function () {
        spyOn(scope, 'validateForm');
        deferred.resolve({});
        spyOn(core.connection, 'update').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success('connection');
        });
        scope.view.mode = 'edit';
        cnSourceItem();
        scope.form = {
            action: {
                success: 'succeddMock'
            },
            error: {
                field: 'field'
            },
            ctrl: {$valid: true},
            field: {name: 'mockedname'}
        };
        spyOn(scope.form.action, 'success');
        expect(scope.saveSource()).toEqual('connection');
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'mockedname'
                }
            },
            resource: "connection",
            name: "edit",
            call: deferred.promise
        }));
        expect(scope.form.error.field).not.toBeDefined();
        expect(scope.form.action.success).toHaveBeenCalledWith('connection');
        expect(core.connection.update).toHaveBeenCalledWith({id: 2}, {name: 'mockedname'});
    });

    it('saveSource method with edit success (no success property)', function () {
        spyOn(scope, 'validateForm');
        spyOn($state, 'go');
        deferred.resolve({});
        spyOn(core.connection, 'update').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success({id: 444});
        });
        scope.view.mode = 'edit';
        cnSourceItem();
        scope.form = {
            action: {
            },
            error: {
                field: 'field'
            },
            ctrl: {$valid: true},
            field: {name: 'mockedname'}
        };
        expect(scope.saveSource()).toEqual({id: 444});
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'mockedname'
                }
            },
            resource: "connection",
            name: "edit",
            call: deferred.promise
        }));
        expect(scope.form.error.field).not.toBeDefined();
        expect(core.connection.update).toHaveBeenCalledWith({id: 2}, {name: 'mockedname'});
        expect($state.go).toHaveBeenCalledWith('connection.item.edit.destination.add', {connection: 444});
    });

    it('saveSource method with success ', function () {
        spyOn(scope, 'validateForm');
        spyOn($state, 'go');
        deferred.resolve({});
        spyOn(core.connection, 'save').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success({id: 444});
        });
        scope.view.mode = '';
        cnSourceItem();
        scope.form = {
            config: {
                account: {
                    id: 7
                }
            },
            action: {
            },
            error: {
                field: 'field'
            },
            ctrl: {$valid: true},
            field: {name: 'mockedname'}
        };
        expect(scope.saveSource()).toEqual({id: 444});
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'mockedname'
                }
            },
            resource: "connection",
            name: "",
            call: deferred.promise
        }));
        expect(scope.form.error.field).not.toBeDefined();
        expect(core.connection.save).toHaveBeenCalledWith({name: 'mockedname', account: 7});
        expect($state.go).toHaveBeenCalledWith('connection.item.edit.destination.add', {connection: 444});
    });

    it('saveSource method with error ', function () {
        spyOn(scope, 'validateForm');
        deferred.resolve({});
        spyOn(core.connection, 'save').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.error({data: {errors: 'someErrors'}});
        });
        cnSourceItem();
        scope.form = {
            config: {
                account: {
                    id: 7
                }
            },
            error: {
                field: ''
            },
            ctrl: {$valid: true},
            field: {name: 'mockedname'}
        };
        scope.saveSource();
        expect(scope.validateForm).toHaveBeenCalled();
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'mockedname'
                }
            },
            resource: "connection",
            name: undefined,
            call: deferred.promise
        }));
        expect(scope.form.error.field).toBeDefined('someErrors');
        expect(core.connection.save).toHaveBeenCalledWith({name: 'mockedname', account: 7});
    });

    it('deleteConnection', function () {
        spyOn($state, 'go');
        deferred.resolve({data: {some: 'stuff'}});
        spyOn(core.connection, 'delete').and.returnValue(deferred.promise);
        spyOn(action, 'run').and.callFake(function (obj) {
            return obj.success('deleteData');
        });
        cnSourceItem();
        scope.form = {
            field: {
                name: 'mockedName'
            }
        };
        expect(scope.deleteConnection()).toEqual('deleteData');
        expect(action.run).toHaveBeenCalledWith(jasmine.objectContaining({
            notify: {
                show: true,
                translate: {
                    name: 'mockedName'
                }
            },
            resource: "connection",
            name: "delete",
            call: deferred.promise
        }));
        expect($state.go).toHaveBeenCalledWith('connection.list');
        expect(core.connection.delete).toHaveBeenCalledWith({id: 2});
    });

    it('addFieldRule function works', function () {
        cnSourceItem();
        scope.form.field = {field_rules: [{mocked: 'test'}]};
        scope.addFieldRule();
        expect(scope.form.field.field_rules).toEqual([{mocked: 'test'}, {}]);
        scope.form.field = {fuled_rules: null};
        scope.addFieldRule();
        expect(scope.form.field.field_rules).toEqual([{}]);
    });

    it('deleteFieldRule function works', function () {
        cnSourceItem();
        scope.form.field = {field_rules: [{mocked0: 'test0'}, {mocked1: 'test1'}]};
        scope.deleteFieldRule(0);
        expect(scope.form.field.field_rules).toEqual([{mocked1: 'test1'}]);
    });

   /* it('watch scope.form.config.type', function () {
        spyOn($state, 'go');
        cnSourceItem();
        $state.$current.name = 'mockedCurrentName';
        scope.initSource({type: 'someType'});
        scope.$digest();
        expect($state.go).toHaveBeenCalledWith('mockedCurrentName', {type: 'mockedType'});
    });*/

    it('translate in scope.watch(form.config.account)', function () {
        cnSourceItem();
        scope.view.mode = 'add';
        scope.form = {
            ctrl: {name: {$pristine: true}},
            config: {
                field: {
                    type: {
                        options: {}
                    }
                },
                account: {name: 'name'}
            },
            field: {
                name: {}
            }
        };
        scope.initSource({account: {some: 'type', name: 'mockedName'}, type: 'typeMock'});
        scope.$apply();
        expect(scope.form.field.name).toEqual('mockedTranslate mockedName');
    });

    it('return empty translate object in scope.watch(form.config.account) if !$pristine', function () {
        scope.view.mode = 'add';
        cnSourceItem();
        scope.form = {
            ctrl: {name: {$pristine: false}},
            config: {account: {name: 'name'}},
            field: {
                name: {
                }
            }
        };
        scope.$apply();
        expect(scope.form.field.name).toEqual({});
    });

    it('cnSourceItemDefault is defined and works fine', function () {
        $stateParams.type = 'mockedType';
        $state.$current.name = 'currentNameMock';
        spyOn($state, 'go');
        spyOn(scope, 'initSource');
        cnSourceItemDefault();
        expect(scope.initSource).toHaveBeenCalledWith({connection: {account1: 'mock1'}});
        expect($state.go).toHaveBeenCalledWith('currentNameMock', {type: null});
    });
});