/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, connection, initSource, not, toBeDefined,
    toBeTruthy, toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnSourceItemRss", function () {
    'use strict';
    var scope, $rootScope, cnSourceItemRss, account, connection;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    beforeEach(function () {
        module('cn.source.rss');
        module(function ($provide) {
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_) {
            $rootScope = _$rootScope_;
            account = _account_;
            connection = _connection_;
            scope = $rootScope.$new();
            scope.initSource = function () {
                return true;
            };
            cnSourceItemRss = function () {
                return $controller('cnSourceItemRss', {
                    $scope: scope,
                    account: account,
                    connection: connection
                });
            };
        });
    });

    it('cnSourceItemRss controller is defined and works fine', function () {
        expect(cnSourceItemRss).toBeDefined();
        expect(cnSourceItemRss()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initSource');
        expect(scope.initSource).not.toHaveBeenCalled();
        cnSourceItemRss();
        expect(scope.initSource).toHaveBeenCalledWith({type: 'rss', connection: connection, account: accountMock});
    });
});