/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, connection, initSource, not, toBeDefined,
    toBeTruthy, toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnSourceItemSalesforce", function () {
    'use strict';
    var scope, $rootScope, cnSourceItemSalesforce, account, connection;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    beforeEach(function () {
        module('cn.source.salesforce');
        module(function ($provide) {
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_) {
            $rootScope = _$rootScope_;
            account = _account_;
            connection = _connection_;
            scope = $rootScope.$new();
            scope.initSource = function () {
                return true;
            };
            cnSourceItemSalesforce = function () {
                return $controller('cnSourceItemSalesforce', {
                    $scope: scope,
                    account: account,
                    connection: connection
                });
            };
        });
    });

    it('cnSourceItemSalesforce controller is defined and works fine', function () {
        expect(cnSourceItemSalesforce).toBeDefined();
        expect(cnSourceItemSalesforce()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initSource');
        expect(scope.initSource).not.toHaveBeenCalled();
        cnSourceItemSalesforce();
        expect(scope.initSource).toHaveBeenCalledWith({type: 'salesforce', connection: connection, account: accountMock});
    });
});