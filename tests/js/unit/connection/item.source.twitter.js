/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, account, connection, initSource, not, toBeDefined,
    toBeTruthy, toHaveBeenCalled, toHaveBeenCalledWith, type, value
*/

describe("controller: cnSourceItemTwitter", function () {
    'use strict';
    var scope, $rootScope, cnSourceItemTwitter, account, connection;
    var accountMock = {account: 'account'};
    var connectionMock = {connection: 'connection'};
    beforeEach(function () {
        module('cn.source.twitter');
        module(function ($provide) {
            $provide.value('account', accountMock);
            $provide.value('connection', connectionMock);
        });

        inject(function ($controller, _$rootScope_, _account_, _connection_) {
            $rootScope = _$rootScope_;
            account = _account_;
            connection = _connection_;
            scope = $rootScope.$new();
            scope.initSource = function () {
                return true;
            };
            cnSourceItemTwitter = function () {
                return $controller('cnSourceItemTwitter', {
                    $scope: scope,
                    account: account,
                    connection: connection
                });
            };
        });
    });

    it('cnSourceItemTwitter controller is defined and works fine', function () {
        expect(cnSourceItemTwitter).toBeDefined();
        expect(cnSourceItemTwitter()).toBeTruthy();
    });

    it('scope init is called correctly', function () {
        spyOn(scope, 'initSource');
        expect(scope.initSource).not.toHaveBeenCalled();
        cnSourceItemTwitter();
        expect(scope.initSource).toHaveBeenCalledWith({type: 'twitter', connection: connection, account: accountMock});
    });
});