/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $new, $scope, DTOptionsBuilder, account, and, callFake, connections,
    createSpy, dtColumnDefs, dtOptions, newOptions, objectContaining,
    orderable, searchable, targets, toBeDefined, toBeTruthy, toEqual, value,
    withOption
*/

describe("controller: cnConnectionList", function () {
    'use strict';
    var scope, cnConnectionList, $rootScope, DTOptionsBuilder;
    var connectionsMock = {account: 'mock'};
    beforeEach(function () {
        DTOptionsBuilder = {
            newOptions: jasmine.createSpy('newOptions').and.callFake(function () {
                return {
                    withOption: jasmine.createSpy('withOption').and.callFake(function () {
                        return {
                            withOption: jasmine.createSpy('withOption').and.callFake(function () {
                                return 'got some data';
                            })

                        };
                    })
                };
            })
        };
        module('ui.router');
        module('cn.connection.list');
        module(function ($provide) {
            $provide.value('connections', connectionsMock);
            $provide.value('DTOptionsBuilder', DTOptionsBuilder);
        });
        inject(function ($controller, _$rootScope_, _DTOptionsBuilder_) {
            DTOptionsBuilder = _DTOptionsBuilder_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            cnConnectionList = function () {
                return $controller('cnConnectionList', {
                    $scope: scope,
                    DTOptionsBuilder: DTOptionsBuilder
                });
            };
        });
    });
    it('cnConnectionList is defined', function () {
        expect(cnConnectionList).toBeDefined();
        expect(cnConnectionList()).toBeTruthy();
    });

    it('DTOptionsBuilder works', function () {
        cnConnectionList();
        expect(scope.dtOptions).toEqual('got some data');
    });

    it('scope.dtColumnDefs is defined', function () {
        cnConnectionList();
        expect(scope.dtColumnDefs).toEqual(jasmine.objectContaining([
            {
                targets: 'no-sort',
                orderable: false
            },
            {
                targets: [0],
                searchable: false
            }
        ]));
    });

    it('accounts exist', function () {
        cnConnectionList();
        expect(scope.connections).toEqual({account: 'mock'});
    });
});