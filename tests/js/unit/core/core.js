/*global core describe module  beforeEach afterEach it expect jasmine inject spyOn*/
/*property
    Authorization, account, accountInfo, adress, any, api_url, billing, common,
    connection, core, defaults, destination, email, expectGET, expectPATCH,
    field, field2, field3, flush, get, headers, id, name, objectContaining,
    query, respond, rpc_url, substr, toEqual, token, trypay, update, user, value,
    vendor, verified
*/


describe('module: cn.settings.profile', function () {

    'use strict';

    var core, $httpBackend, cfg, $http;
    var id = 1, field1 = '1', field2 = '2', field3 = '3';
    var mockedCfg = {
        core: {
            api_url: 'url_api/',
            rpc_url: 'rpc_url/'
        },
        user: {
            name: 'mockedName',
            email: {
                adress: 'mocked@add.ress',
                id: 1,
                verified: true
            },
            core: {
                token: 'fakeToken'
            }
        }
    };

    beforeEach(function () {
        module('ngResource');
        module('cn.core');

        module(function ($provide) {
            $provide.value('cfg', mockedCfg);
        });
    });

    beforeEach(inject(function (_core_, _$httpBackend_, _cfg_, _$http_) {
        $http = _$http_;
        cfg = _cfg_;
        core = _core_;
        $httpBackend = _$httpBackend_;
    }));

    it('return core correctly', function () {
        expect(core).toEqual(jasmine.any(Object));
    });

    it('should send a update request', function () {

        $httpBackend.expectPATCH(cfg.core.api_url + "connections").respond(200);
        core.connection.update();
        $httpBackend.flush();
    });

    it('connection.get: GET on /api/connections/1', function () {

        $httpBackend.expectGET(cfg.core.api_url + "connections/1").respond(200);
        core.connection.get({id: 1});
        $httpBackend.flush();
    });

    it('connect.update: PATCH on /api/connections/1', function () {

        $httpBackend.expectPATCH(cfg.core.api_url + "connections/" + id).respond(200);
        core.connection.update({id: 1});
        $httpBackend.flush();
    });

    it('account.update: PATCH on /api/accounts/1', function () {

        $httpBackend.expectPATCH(cfg.core.api_url + "accounts/" + id).respond(200);
        core.account.update({id: 1});
        $httpBackend.flush();
    });

    it('accountinfo: GET on api/accounts/:id/info/field1/field2/field3/', function () {

        $httpBackend.expectGET(cfg.core.api_url + "accounts/" + id + "/info/" + field1 + "/" + field2 + "/" + field3 + "").respond(200);
        core.accountInfo.query({id: id, field: field1, field2: field2, field3: field3});
        $httpBackend.flush();
    });

    it('destination.update: PATCH - api/accounts/:id/info/field1/field2/field3/', function () {

        $httpBackend.expectPATCH(cfg.core.api_url + "destinations/" + id).respond(200);
        core.destination.update({id: id});
        $httpBackend.flush();
    });

    it('vendors: GET on api/vendors', function () {

        $httpBackend.expectGET(cfg.core.api_url + "vendors").respond(200);
        core.vendor.get();
    });

    it('billing.history: GET - billings/:id', function () {

        $httpBackend.expectGET(cfg.core.api_url + "billings/" + id).respond(200);
        core.billing.get({id: id});
        $httpBackend.flush();
    });

    it('trypay: GET - rpc/v1/trypay/', function () {

        $httpBackend.expectGET(cfg.core.rpc_url + "trypay").respond(200);
        core.trypay.get();
        $httpBackend.flush();
    });

    it('set headers for all http calls in', function () {

        expect($http.defaults.headers.common).toEqual(jasmine.objectContaining({Authorization: 'Bearer fakeToken', 'Content-Type': 'application/json'}));
    });
});