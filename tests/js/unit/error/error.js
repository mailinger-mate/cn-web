/*global jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $new, $scope, addPart, any, controller, current, data,
    error, go, pageTitle, put, redirect, slice, template, templateUrl,
    toBeDefined, toEqual, url
*/

describe('config: error', function () {
    'use strict';
    var $rootScope, $state, scope, createController;

    beforeEach(function () {
        module('pascalprecht.translate');
        module('ui.router');
        module('cn.error', function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('error/error');
        });

        inject(function (_$rootScope_, _$state_, $templateCache, $controller) {
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $state = _$state_;
            $templateCache.put('error.html', '');

            createController = function () {
                return $controller('cnError', {
                    $scope: scope
                });
            };
        });
    });

    it('error state is defined and works correctly', function () {

        $state.go('error');
        $rootScope.$digest();
        expect($state.current.data).toEqual(jasmine.any(Object));
        expect($state.current.data.pageTitle).toEqual('error.self');
        expect($state.current.template).toEqual('<div ui-view autoscroll="false"></div>');
        expect($state.current.redirect).toEqual('error.404');
        expect($state.current.url).toEqual('/error');
    });

    it('error 400 state is defined and works correctly', function () {

        $state.go('error.400');
        $rootScope.$digest();
        expect($state.current.data).toEqual(jasmine.any(Object));
        expect($state.current.data.pageTitle).toEqual('error.self');
        expect($state.current.controller).toEqual('cnError');
        expect($state.current.url).toEqual('/400');
        expect($state.current.templateUrl).toEqual('error.html');
    });

    it('error 404 state is defined and works correctly', function () {

        $state.go('error.404');
        $rootScope.$digest();
        expect($state.current.data).toEqual(jasmine.any(Object));
        expect($state.current.data.pageTitle).toEqual('error.self');
        expect($state.current.controller).toEqual('cnError');
        expect($state.current.templateUrl).toEqual('error.html');
        expect($state.current.url).toEqual('/404');
    });

    describe('controller: cnError', function () {

        it('cnError controller is defined', function () {
            expect(createController).toBeDefined();
        });

        it('cnError controller works correctly', function () {
            $state.go('error.404');
            $rootScope.$apply();
            createController();
            expect(scope.error).toEqual($state.current.url.slice(1));
        });
    });
});
