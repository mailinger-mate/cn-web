/*global jasmine angular describe module beforeEach it expect window inject*/
/*property
    $new, $scope, _ues, async, category, cfg, createSpyObj, feedback, forum,
    getElementsByTagName, host, objectContaining, params, preload, show, src,
    sso_token, tab_show, toBeDefined, toEqual, token, type, user, value
*/
describe('controller: FeedbackCtrl', function () {
    'use strict';
    var $rootScope, scope, feedbackCtrl, cfg;
    var cfgMock = {
        user: {
            feedback: {
                token: 'token'
            }
        },
        feedback: {
            host: 'host',
            forum: 'forum',
            category: 'category'
        }
    };

    beforeEach(function () {
        module('cn.feedback');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });

        inject(function (_$rootScope_, $controller, _cfg_) {
            cfg = _cfg_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            feedbackCtrl = function () {
                return $controller('FeedbackCtrl', {
                    $scope: scope,
                    cfg: cfg
                });
            };
        });
    });

    it('controller is defined', function () {
        expect(feedbackCtrl).toBeDefined();
    });

    it('_ues config exist', function () {
        feedbackCtrl();
        expect(window._ues).toEqual(jasmine.objectContaining({
            host: cfg.feedback.host,
            forum: cfg.feedback.forum,
            category: cfg.feedback.category,
            tab_show: false,
            params: {
                sso_token: cfg.user.feedback.token
            }
        }));
    });

    it('insertBefore works', function () {
        var href = 'http://cdn.userecho.com/js/widget-1.4.gz.js';
        feedbackCtrl();
        expect(document.getElementsByTagName('script')[0]).toEqual(jasmine.objectContaining({
            src: href,
            type: 'text/javascript',
            async: true
        }));
    });

    it('scope.show defined', function () {
        feedbackCtrl();
        expect(scope.show).toBeDefined();
    });

    it('scope.preload defined', function () {
        feedbackCtrl();
        expect(scope.preload).toBeDefined();
    });
});
