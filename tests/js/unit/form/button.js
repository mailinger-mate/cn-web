/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $broadcast, $digest, $eval, $new, action, and, animate, base, button,
    callFake, callThrough, check, class, click, compile, configAction,
    configClass, configClick, configConfirm, configDisabled, confirm, control,
    css, ctrl, danger, data, defer, disabled, done, error, extendConfig, find,
    flush, form, group, hold, href, icon, input, length, link, load, not,
    promise, replaceInput, resolve, scope, service, session, someConfig, sref,
    success, testRemove, then, toBe, toBeDefined, toContainHtml, toEqual,
    toHaveAttr, toHaveBeenCalled, toHaveBeenCalledWith, toHaveClass, tryPay,
    type, use, useLoader, value
*/

describe("directive: cnButton", function () {
    'use strict';
    var $rootScope, scope, $compile, $timeout;
    var cfgMock = {
        animate: {
            hold: 'MockedHold'
        },
        session: {
            check: 2
        },
        css: {
            button: {
                danger: 'danger',
                group: 'group',
                base: 'base'
            },
            input: {
                control: 'mockedControl'
            },
            icon: {
                done: 'doneMock',
                error: 'errorMock',
                load: 'mockedLoad'
            }
        }
    };
    beforeEach(function () {
        module('pascalprecht.translate');
        module('cn.button');
        module(function ($translateProvider) {
            $translateProvider.useLoader('customLoader');
        });
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.service('customLoader', function ($q) {
                return function () {
                    var deferred = $q.defer();
                    deferred.resolve({
                        "test.help": "mockedTranslateKey"
                    });
                    return deferred.promise;
                };
            });
        });
        inject(function (_$rootScope_, _$compile_, $translate, _$timeout_) {
            $timeout = _$timeout_;
            $translate.use("en_US");
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.extendConfig = function () {
            return;
        };
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.form = {
            ctrl: 'mockedCtrl'
        };
    });

    it('directive cnButton template works fine', function () {
        var element = $compile("<cn-Button>")(scope);
        $rootScope.$digest();
        expect(element).toBeDefined();
        expect(element.find('span')).toHaveAttr('translate');
        expect(element.find('i')).toBeDefined();
    });

    it('directive cnButton controller scope.start is defined', function () {
        spyOn(scope, '$eval').and.callThrough();
        var element = $compile("<cn-Button config='someConfig'>")(scope);
        $rootScope.$digest();
        expect(element.scope().click).toEqual(1);
        expect(scope.$eval).toHaveBeenCalledWith('someConfig');
    });

    it('scope.confirm works case (with argument)', function () {
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        spyOn(element.scope(), 'action');
        $rootScope.$digest();
        element.scope().confirm('action');
        expect(element.scope().click).toEqual(0);
    });

    it('scope.confirm works case scope.click = 0', function () {
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        spyOn(element.scope(), 'action');
        element.scope().click = 0;
        $rootScope.$digest();
        element.scope().confirm('action');
        expect(element.scope().click).toEqual(1);
        expect(element.scope().action).toHaveBeenCalledWith('action');
    });

    it('scope.confirm works case scope.click = 1', function () {
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        spyOn(element.scope(), 'action');
        element.scope().click = 1;
        $rootScope.$digest();
        element.scope().confirm('action');
        expect(element.scope().click).toEqual(0);
        expect(element.scope().action).not.toHaveBeenCalled();
    });

    it('directive link configAction works', function () {
        scope.someConfig = {action: 'action'};
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        $rootScope.$digest();
        element.scope().configAction();
        expect(element).toHaveAttr('ng-click', "action('action')");
    });

    it('directive link configClick works', function () {
        scope.someConfig = {click: 'click'};
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        $rootScope.$digest();
        element.scope().configClick();
        expect(element).toHaveAttr('ng-click', "click");
    });

    it('directive link configClass works', function () {
        scope.someConfig = {class: 'class'};
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        $rootScope.$digest();
        element.scope().configClass();
        expect(element).toHaveClass("base class");
    });

    it('directive link configDisabled works', function () {
        scope.someConfig = {disabled: 'disabled'};
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        $rootScope.$digest();
        element.scope().configDisabled();
        expect(element).toHaveAttr('ng-disabled', "disabled");
    });

    it('directive link config.confirm works', function () {
        scope.someConfig = {action: 'confirm'};
        var element = $compile("<cn-Button config='someConfig'><form name='test'>")(scope);
        $rootScope.$digest();
        element.scope().configConfirm();
        expect(element).toHaveAttr('ng-click', "confirm('confirm')");
        expect(element).toContainHtml('button.confirm');
    });

    it('directive link config.confirm watcher', function () {
        scope.someConfig = {action: 'confirm'};
        var element = $compile("<cn-Button class='danger' config='someConfig'>")(scope);
        $rootScope.$digest();
        element.scope().configConfirm();
        scope.click = 'click';
        $rootScope.$digest();
        expect(element).not.toHaveClass('danger');
        expect(element).not.toHaveClass('group');
        expect(element).toContainHtml('button.confirm');
    });

    it('scope.replaceinput', function () {
        var config = {
            type: 'link',
            href: 'href',
            sref: 'sref'
        };
        var element = $compile("<cn-Button class='danger' config='someConfig'><a>test</a>")(scope);
        $rootScope.$digest();
        element.scope().replaceInput(config);
        $rootScope.$digest();
        expect(element).toHaveAttr('config', 'someConfig');
    });


    it('scope.action for help attrs works (success data callback)', function () {
        scope.tryPay = function () {
            return '';
        };
        spyOn($rootScope, '$broadcast');
        spyOn(scope, 'tryPay').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback({data: {success: true}});
                }
            };
        });
        var element = $compile("<cn-Button action='tryPay' remove='testRemove' class='danger' config='someConfig' help='test.help'><i class='errorMock mockedLoad'></i>")(scope);
        $rootScope.$digest();
        expect(element).toHaveAttr('help');
        element.scope().action('tryPay');
        expect(element.find('i')).toHaveClass('mockedLoad');
        $timeout.flush();
        expect(element).toHaveAttr('disabled');
        expect(element.find('i')).toHaveClass('doneMock');
        expect(element.find('i')).not.toHaveClass('errorMock mockedLoad');
        $timeout.flush();
        expect(element).not.toHaveAttr('disabled');
        expect($rootScope.testRemove).toEqual(false);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('testRemove', false);
        expect(element.find("button").length).toBe(0);
    });

    it('scope.action for help attrs works (error data callback)', function () {
        scope.tryPay = function () {
            return '';
        };
        spyOn(scope, 'tryPay').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback({data: {success: false}});
                }
            };
        });
        var element = $compile("<cn-Button action='tryPay' remove='testRemove' class='danger' config='someConfig' help='test.help'><i class='errorMock mockedLoad'></i>")(scope);
        $rootScope.$digest();
        element.scope().action('tryPay');
        $timeout.flush();
        expect(element).not.toHaveAttr('disabled');
        expect(element.find('i')).toHaveClass('errorMock');
    });

    it('scope.action for help attrs works (no callback)', function () {
        scope.tryPay = function () {
            return '';
        };
        spyOn(scope, 'tryPay').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback({data: {success: false}});
                }
            };
        });
        var element = $compile("<cn-Button action='tryPay' remove='testRemove' class='danger' config='someConfig' help='test.help'><i class='errorMock mockedLoad'></i>")(scope);
        $rootScope.$digest();
        element.scope().action('tryPay');
        $timeout.flush();
        expect(element).not.toHaveAttr('disabled');
        expect(element.find('i')).toHaveClass('errorMock');
    });

    it('scope.action for help attrs works (no data callback)', function () {
        scope.tryPay = function () {
            return '';
        };
        spyOn(scope, 'tryPay').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback();
                }
            };
        });
        var element = $compile("<cn-Button action='tryPay' remove='testRemove' class='danger' config='someConfig' help='test.help'><i class='errorMock mockedLoad'></i>")(scope);
        $rootScope.$digest();
        element.scope().action('tryPay');
        $timeout.flush();
        expect(element).not.toHaveAttr('disabled');
        expect(element.find('i')).toHaveClass('errorMock');
    });


    it('scope.action for help attrs works (success data callback)', function () {
        scope.tryPay = function () {
            return '';
        };
        spyOn($rootScope, '$broadcast');
        spyOn(scope, 'tryPay').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback({data: {success: true}});
                }
            };
        });
        var element = $compile("<cn-Button action='tryPay' remove='testRemove' class='danger' config='someConfig' help='test.help'>")(scope);
        $rootScope.$digest();
        expect(element).toHaveAttr('help');
        element.scope().action('tryPay');
        $timeout.flush();
        expect(element).toHaveAttr('disabled');
        $timeout.flush();
        expect(element).not.toHaveAttr('disabled');
        expect($rootScope.testRemove).toEqual(false);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('testRemove', false);
        expect(element.find("button").length).toBe(0);
    });
});