/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, input, link, put, scope, toBeDefined, toHaveBeenCalledWith
*/


describe("directive: cninputattachment", function () {
    'use strict';
    var $rootScope, scope, $compile;

    beforeEach(function () {
        module('cn.input.attachment');
        inject(function (_$rootScope_, _$compile_, $templateCache) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $templateCache.put("/html/form/input.attachment.html", '<div></div>');
        });
        scope.link = function () {
            return;
        };
    });

    it('scope.link is called', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputattachment></div>")(scope);
        scope.$digest();
        expect(scope.link).toHaveBeenCalledWith(element);
    });

    it('scope.input is defined', function () {
        var element = $compile("<div cninputattachment></div>")(scope);
        scope.$digest();
        expect(element.scope().input).toBeDefined();
    });
});