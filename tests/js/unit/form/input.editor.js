/*global $ spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $eval, $new, activeElement, addMatchers, addMentio, and,
    appendTo, attr, body, callFake, callThrough, changeModel, compare, compile,
    config, content, createSpy, default, displayElements, editor, field, flush,
    form, getTextAngularId, handler, input, link, mockedConfigEditor, mode,
    model, not, pass, pre, prefix, put, replace, retrieveEditor, returnValue,
    scope, setup, someStringab, stopImmediatePropagation, text, toEqual,
    toHaveBeenCalled, toHaveBeenCalledWith, toHaveFocus, toggle, toolbar,
    trigger, type, value
*/

describe("directive: cninputeditor", function () {
    'use strict';
    var $rootScope, scope, $compile, $timeout;
    var fakeTextAngularManager = {
        retrieveEditor: jasmine.createSpy('retrieveEditor').and.callFake(function () {
            return {
                scope: {
                    displayElements: {
                        text: {
                            attr: jasmine.createSpy('attr').and.callFake(function (id) {
                                return 'fired with ' + id;
                            })
                        }
                    }
                }
            };
        })
    };

    var cfgMock = {
        input: {
            prefix: {
                form: ['someString']
            },
            editor: {
                default: 'default',
                mockedConfigEditor: 'editor'
            }
        }
    };
    beforeEach(function () {
        jasmine.addMatchers({
            toHaveFocus: function () {
                return {
                    compare: function (actual) {
                        return {
                            pass: document.activeElement === actual[0]
                        };
                    }
                };
            }
        });
        module('cn.input.editor');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
            $provide.value('textAngularManager', fakeTextAngularManager);
        });
        inject(function (_$rootScope_, _$compile_, $templateCache, _$timeout_) {
            $compile = _$compile_;
            $timeout = _$timeout_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $templateCache.put("/html/form/input.editor.html", '<div name="test"></div>');
        });
        scope.form = {
            field: {
                config: {
                    content: {
                        value: ''
                    }
                }
            }
        };
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.setup = function () {
            return;
        };
        scope.handler = {
            pre: {
                toggle: function () {
                    return;
                }
            }
        };
        scope.config = {
            editor: 'mockedConfigEditor'
        };
        scope.editor = {
            mode: 'mockedEditor'
        };
    });
    it('directive controller works', function () {
        spyOn(scope, '$eval');
        spyOn(scope, 'link');
        spyOn(scope, 'compile');
        $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        expect(scope.link).toHaveBeenCalled();
        expect(scope.compile).toHaveBeenCalled();
    });

    it('directive watcher', function () {
        scope.editor.text = 'textMock';
        spyOn(scope, '$eval').and.callThrough();
        scope.model = 'test';
        $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        expect(scope.$eval).toHaveBeenCalled();
        expect(scope.$eval(scope.model + "='" + scope.editor.text + "'")).toEqual('textMock');
    });

    it('function scope.handler.pre.toggle works fine', function () {
        spyOn(scope, '$watch');
        spyOn(scope, '$eval');
        scope.someStringab = 'test';
        $compile("<div cninputeditor></div>")(scope);
        scope.$apply(function () {
            scope.handler.pre.toggle('ab');
        });
        expect(scope.editor.mode).toEqual('mockedEditor');
    });

    it('scope.editor is defined', function () {
        spyOn(scope, '$eval').and.returnValue('mock');
        var element = $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        expect(element.scope().editor.text).toEqual('mock');
        expect(element.scope().editor.toolbar).toEqual('editor');
    });

    it('scope.setup function works', function () {
        spyOn(scope, '$eval');
        spyOn(scope, 'setup');
        var element = $compile("<div cninputeditor ></div>")(scope);
        scope.$digest();
        scope.setup(element);
        expect(scope.setup).toHaveBeenCalledWith(element);
    });

    it('addMentio works fine (no content)', function () {
        spyOn(scope, '$eval');
        scope.model = 'form.field.config.content.value';
        var textarea = $compile("<textarea id='textAreaMentio'>")(scope);
        var element = $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        textarea.appendTo(document.body);
        element.scope().addMentio();
        $timeout.flush();
        expect(scope.form.field.config.content.value).toEqual('%');
        expect(scope.editor.mode).not.toEqual('html');
        expect($('textarea')).toHaveFocus();
    });

    it('addMentio works fine (content exist)', function () {
        spyOn(scope, '$eval');
        scope.form.field.config.content.value = 'test';
        scope.model = 'form.field.config.content.value';
        $compile("<textarea id='textAreaMentio' value='test'>")(scope);
        var element = $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        element.scope().addMentio();
        $timeout.flush();
        expect(scope.form.field.config.content.value).toEqual('%');
        expect(scope.editor.mode).not.toEqual('html');
        expect($('textarea')).toHaveFocus();
    });

    it('change the value of scope model reference', function () {
        spyOn(scope, '$eval');
        scope.model = 'form.field.config.content.value';
        $compile("<textarea id='textAreaMentio' value='test'>")(scope);
        var element = $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        element.scope().changeModel('testChangeModel');
        expect(scope.form.field.config.content.value).toEqual('testChangeModel');
    });

    it('addMentio works fine (content exist)', function () {
        spyOn(scope, '$eval');
        scope.model = 'form.field.config.content.value';
        scope.form.field.config.content.value = 'test';
        $compile("<textarea id='textAreaMentio' value='test'>")(scope);
        var element = $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        element.scope().addMentio();
        $timeout.flush();
        expect(scope.form.field.config.content.value).toEqual('%');
        expect(scope.editor.mode).not.toEqual('html');
        expect($('textarea')).toHaveFocus();
    });

    it('getTextAngularId works fine', function () {
        spyOn(scope, '$eval');
        var element = $compile("<div cninputeditor></div>")(scope);
        scope.$digest();
        expect(element.scope().getTextAngularId()).toEqual('fired with id');
    });

    it('stopImmediatePropagation works fine', function () {
        var event = {
            type: 'click',
            stopImmediatePropagation: function () {
                return;
            }
        };
        spyOn(event, 'stopImmediatePropagation');
        spyOn(scope, '$eval');
        $compile("<div cninputeditor></div>")(scope);
        var ref = $compile("<button class='addRef'>")(scope);
        scope.$digest();
        ref.appendTo(document.body);
        $('.addRef').trigger(event);
        expect(event.stopImmediatePropagation).toHaveBeenCalled();
    });
    it('should convert html to plain text', function () {
        function htmlToPlaintext(text) {
            return text
                ? String(text).replace(/<[^>]+>/gm, '')
                : '';
        }
        expect(htmlToPlaintext('<a href="test">This is test</a>')).toEqual('This is test');

    });
});