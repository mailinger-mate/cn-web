/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, $parsers, $setValidity, any, compile, config, ctrl, data,
    expectGET, extra, fieldrule_test_name, form, input, key, link, name, put,
    ref, respond, scope, testfield, toBeDefined, toEqual, toHaveBeenCalledWith
*/

describe("directive: cninputfieldrule", function () {
    'use strict';
    var $rootScope, scope, $compile, $httpBackend;
    beforeEach(function () {
        module('cn.input.fieldrule');
        inject(function (_$rootScope_, _$compile_, $templateCache, _$httpBackend_) {
            $compile = _$compile_;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            scope.input = {
                config: {}
            };
            $templateCache.put("/html/form/input.fieldrule.html", '<div></div>');
        });
        $httpBackend.expectGET('/html/form/input.fieldrule.html').respond(200, '');

        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.data = {
            ref: {
                extra: {
                    name: ['someData']
                }
            }
        };
        scope.key = 'test';
        scope.form = {
            ctrl: {

                fieldrule_test_name: {
                    testfield: '',
                    name: '',
                    $parsers: ['someData'],
                    $setValidity: ['exist']
                }
            }
        };
    });

    it('directive controller works', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputfieldrule></div>")(scope);
        scope.$digest();
        expect(scope.link).toHaveBeenCalledWith(element);
        expect(element.scope().input).toBeDefined();
    });

    it('directive cninputfieldrule works fine', function () {
        $compile("<div cninputfieldrule></div>")(scope);
        scope.$digest();
        expect(scope.form.ctrl.fieldrule_test_name.$parsers).toEqual([jasmine.any(Function), 'someData']);
    });
});