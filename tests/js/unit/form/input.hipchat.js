/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $eval, $new, and, callThrough, compile, config, deleteProperty,
    expectGET, form, handler, hipchat, id, input, link, pattern, post, pre,
    prefix, put, respond, scope, string, to, toBeDefined, toHaveBeenCalledWith,
    user, value
*/

describe("directive: cninputinterval", function () {
    'use strict';
    var $rootScope, scope, $compile, $httpBackend;
    var cfgMock = {
        user: {
            id: 'test'
        },
        input: {
            prefix: {
                form: 'test'
            }
        }
    };
    beforeEach(function () {
        module('cn.form.config');
        module('cn.input.hipchat');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, $templateCache, _$httpBackend_) {
            $compile = _$compile_;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $templateCache.put("/html/form/input.hipchat.html", '<div></div>');
        });
        $httpBackend.expectGET('/html/form/input.hipchat.html').respond(200, '');

        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.deleteProperty = function () {
            return;
        };
        scope.hipchat = {
            config: {
                to: ''
            }
        };
        scope.handler = {
            pre: {
                pattern: 'mock'
            },
            post: {string: ''}
        };
    });

    it('directive cninputhipchat works fine', function () {
        spyOn(scope, '$eval').and.callThrough();
        var element = $compile("<div cninputhipchat></div>")(scope);
        scope.$digest();
        expect(scope.$eval).toHaveBeenCalledWith('testconfig.user_id');
        expect(element.scope().hipchat).toBeDefined();

    });
    it('directive cninputhipchat link function called', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputhipchat></div>")(scope);
        scope.$digest();
        expect(scope.link).toHaveBeenCalledWith(element);
    });

    it('directive cninputhipchat watcher works', function () {
        spyOn(scope, 'deleteProperty');
        var element = $compile("<div cninputhipchat></div>")(scope);
        element.scope().hipchat.config.to = 'test';
        scope.$digest();
        element.scope().hipchat.config.to = 'test1';
        scope.$digest();
        expect(element.scope().deleteProperty).toHaveBeenCalledWith('testconfig.room_id');
    });
});