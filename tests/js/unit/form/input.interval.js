/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $eval, $new, and, callFake, compile, config, expectGET, form,
    handler, id, input, interval, link, min, model, number, pattern, period,
    post, pre, prefix, put, respond, returnValue, scope, string, toBeDefined,
    toEqual, toHaveBeenCalled, toHaveBeenCalledWith, user, value
*/

describe("directive: cninputinterval", function () {
    'use strict';
    var $rootScope, scope, $compile, $httpBackend;
    var cfgMock = {
        user: {id: 'test'},
        input: {
            prefix: {
                form: 'test'
            }
        }
    };
    beforeEach(function () {
        module('cn.input.interval');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, $templateCache, _$httpBackend_) {
            $compile = _$compile_;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $templateCache.put("/html/form/input.interval.html", '<input type="hidden" class="interval">');
        });
        $httpBackend.expectGET('/html/form/input.interval.html').respond(200, '');

        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.interval = {
            value: {
                number: '',
                period: ''
            }
        };
        scope.config = {
            min: 'minMock'
        };
        scope.handler = {
            pre: {
                pattern: 'mock'
            },
            post: {string: ''}
        };
    });

    it('directive cninputinterval works fine', function () {
        spyOn(scope, '$eval');
        var element = $compile("<div cninputinterval></div>")(scope);
        expect(element).toBeDefined();
    });

    it('getQueryInterval is called (<0)', function () {
        spyOn(scope, '$eval').and.callFake(function () {
            return 1;
        });
        scope.model = 'test';
        var element = $compile("<div cninputinterval></div>")(scope);
        scope.$digest();
        expect(scope.$eval).toHaveBeenCalledWith(scope.model);
        expect(element.scope().interval.value).toEqual({period: 1, number: 1});
        expect(element.scope().interval.value.number).toEqual(1);
    });

    it('getQueryInterval is called (<0)', function () {
        spyOn(scope, '$eval').and.callFake(function () {
            return -1;
        });
        scope.model = 'test';
        var element = $compile("<div cninputinterval></div>")(scope);
        scope.$digest();
        expect(scope.$eval).toHaveBeenCalledWith(scope.model);
        expect(element.scope().interval.value).toEqual({period: -1});
    });

    it('scope.link function is called', function () {
        spyOn(scope, '$eval').and.returnValue('test');
        spyOn(scope, 'link');
        var element = $compile("<div cninputinterval></div>")(scope);
        scope.$digest();
        expect(scope.link).toHaveBeenCalled();
        expect(element.scope().interval.value).toEqual({period: 'test', number: 1});
    });

    it('scope watch works fine (period >-1)', function () {
        scope.config.min = 2;
        scope.model = 'model';
        spyOn(scope, '$eval').and.callFake(function () {
            return 1;
        });
        var element = $compile("<div cninputinterval></div>")(scope);
        scope.$digest();
        expect(scope.interval.value.number).toBeDefined();
        expect(element.scope().interval.value).toEqual({period: 1, number: 2});
    });

    it('scope watch works fine (period == -1)', function () {
        spyOn(scope, '$eval').and.callFake(function () {
            return -1;
        });
        var element = $compile("<div cninputinterval></div>")(scope);
        expect(element.scope().interval.value).toEqual({period: '', number: ''});
    });
});