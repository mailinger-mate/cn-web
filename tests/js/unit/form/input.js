/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $eval, $formatters, $new, $parent, $parsers, $pristine,
    $render, $setDirty, $setValidity, $setViewValue, alert, and, attr, blur,
    button, callFake, click, compile, config, confirm, control, createSpy, css,
    ctrl, data, deleteProperty, email, empty, enabled, error, expression,
    field, find, form, formMockmockedExpression, formMockmodelMock, getConfig,
    group, handler, help, hide, host, icon, imap, info, input, invalid, label,
    link, listener, mock, model, modelMock, name, not, objectContaining,
    pattern, placeholder, post, postfix, pre, prefix, provider, ref, required,
    reset, scope, some, somePattern, string, test, test1, toBeDefined,
    toContainText, toEqual, toHaveAttr, toHaveBeenCalled, toHaveBeenCalledWith,
    toHaveClass, toHaveHtml, translate, type, value, view, watch
*/

describe("directive: cninputnumber", function () {
    'use strict';
    var $rootScope, scope, $compile, cfgMock, cfg;

    cfgMock = {
        css: {
            button: {
                group: 'mockedButtonGroup'
            },
            icon: {
                help: 'mockedHelp',
                alert: 'mockedAlert'
            },
            form: {group: 'mockedGroup'},
            input: {
                group: 'groupMock',
                button: 'mockedButtonClass',
                control: 'mockedControl'
            },
            field: {
                invalid: 'testClass'
            }
        },
        input: {
            postfix: {
                help: 'helpMock',
                label: '',
                placeholder: 'placeholder'
            },
            prefix: {
                form: 'formMock',
                field: 'fieldMock',
                error: ''
            },
            pattern: {
                somePattern: 'patternMock'
            }
        },
        email: {
            provider: [{
                imap: {
                    host: 'hostMock'
                }
            }]
        }
    };
    beforeEach(function () {
        module('ui.bootstrap');
        module('cn.input');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, _cfg_) {
            cfg = _cfg_;
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.config = {
            prefix: {
                enabled: ''
            }
        };
        scope.form = {
            config: {
                prefix: {
                    enabled: 'enableMe',
                    translate: 'test'
                },
                field: {
                    name: ''
                }
            },
            ctrl: {
                test1: {},
                test: {
                    name: 'mockedName',
                    $parsers: [10, 20],
                    $formatters: [30]
                }
            }
        };
        scope.$parent.form = scope.form;
        scope.handler = {
            pre: {
                pattern: 'mock'
            },
            post: {string: ''}
        };
    });

    describe('cninputcontrol directive', function () {

        it('directive cninputcontrol works fine', function () {
            var element = $compile("<form name='formConfig'><div cninputcontrol label='testLabel'></div></form>")(scope);
            $rootScope.$digest();
            expect(element).toHaveAttr('name', 'formConfig');
            expect(element).toHaveClass('ng-pristine ng-valid');
        });

        it('controller works (valid)', function () {
            scope.name = 'testName';
            var element = $compile("<form name='testName'><div id='find' cninputcontrol label='testLabel'><input type='text' name='testName'/></div></form>")(scope);
            spyOn(scope, '$eval');
            $rootScope.$apply();
            expect(element.scope().form.ctrl).toBeDefined();
            scope.$digest();
            expect(element.find('#find')).not.toHaveClass('testClass');
        });

        it('controller works (invalid)', function () {
            scope.config = {model: true};
            scope.name = 'testName';
            var element = $compile("<form name='testName'><div id='find' cninputcontrol label='testLabel'><input type='text' name='testName'/></div></form>")(scope);
            $rootScope.$apply();
            expect(scope.form.ctrl).toBeDefined();
            scope.$digest();
            expect(element.find('#find')).toHaveClass('testClass');
            expect(element.find('label')).toContainText('fieldMocktestLabel');
        });

        it('link function (labeltranslate false)', function () {
            var element = $compile("<form name='testName'><div id='find' cninputcontrol labeltranslate=false label='testLabel'><input type='text' name='testName'/></div></form>")(scope);
            $rootScope.$apply();
            scope.$digest();
            expect(element.find('label')).not.toHaveAttr('translate', '');
        });

        it('link function (without labeltranslate)', function () {
            scope.form.config.prefix.translate = 'mockScopeTranslate';
            var element = $compile("<form name='testName'><div id='find' cninputcontrol label='testLabel'><input type='text' name='testName'/></div></form>")(scope);
            $rootScope.$apply();
            scope.$digest();
            expect(element.find('label')).toHaveAttr('translate', '');
            expect(element.find('label')).toContainText('mockScopeTranslatefieldMocktestLabel');
        });
    });

    describe('directive cninput', function () {

        it('directive cninput works fine', function () {
            scope.form.config.prefix.translate = 'translateMock';
            $compile("<div cnInput config='form.config.field.name' model='name' name='name'></div>")(scope);
            $rootScope.$digest();
            expect(scope.form).toEqual(scope.$parent.form);
            expect(scope.view).toEqual(scope.$parent.view);
            expect(scope.field).toEqual(scope.$parent.field);
        });

        it('scope.deleteProperty works fine', function () {
            scope.test = 'test';
            $compile("<div cnInput config='form.config.field.name' model='name' name='name'></div>")(scope);
            $rootScope.$digest();
            scope.deleteProperty(scope.test);
            expect(scope.test).toEqual();
        });

        it('scope.getConfig works fine', function () {
            spyOn(scope, '$eval');
            var attrs = {
                config: {test: 'value'}
            };
            $compile("<div cnInput config='form.config.field.name' model='name' name='name'></div>")(scope);
            $rootScope.$digest();
            scope.getConfig(attrs);
            expect(scope.config).toEqual(jasmine.objectContaining({
                model: undefined,
                type: 'text',
                error: true,
                translate: true,
                prefix: {translate: 'test'}
            }));
            expect(scope.$eval).toHaveBeenCalledWith(attrs.config);
        });

        it('scope.handler.pre.attr (object)', function () {
            var attr = {some: 'attr'};
            var element = $compile("<div cnInput></div>")(scope);
            $rootScope.$digest();
            expect(scope.handler.pre.attr(element, attr)).toEqual();
            expect(element).toHaveAttr('some', 'attr');
        });

        it('scope.handler.pre.attr (non obj)', function () {
            var attr = 'test';
            var element = $compile("<div cnInput></div>")(scope);
            $rootScope.$digest();
            expect(scope.handler.pre.attr(element, attr)).toEqual();
            expect(element).toHaveAttr('test', 'test');
        });

        it('scope.handler.pre.enabled (enable element)', function () {
            var element = $compile("<div cnInput></div>")(scope);
            $rootScope.$digest();
            scope.handler.pre.enabled(element, true);
            expect(element).toHaveAttr('ng-disabled', 'false');
        });

        it('scope.handler.pre.enabled (disable element)', function () {
            var element = $compile("<div cnInput></div>")(scope);
            $rootScope.$digest();
            scope.handler.pre.enabled(element, false);
            expect(element).toHaveAttr('ng-disabled', 'true');
        });

        it('scope.handler.pre.enabled (disabled, watcher)', function () {
            scope.model = 'someModel';
            scope.config.prefix.enabled = true;
            var element = $compile("<div cnInput></div>")(scope);
            spyOn(scope, 'deleteProperty');
            scope.config.hide = true;
            scope.handler.pre.enabled(element, false);
        });

        it('scope.handler.pre.error ', function () {
            scope.model = 'someModel';
            var element = $compile("<div class='form-group'><span ng-bind='mockedErrorsomeModel' class='error'></span></div><div class='div.form-group'><div cnInput></div></div>")(scope);
            spyOn(scope, 'compile');
            scope.handler.pre.error(element);
            expect(scope.compile).toHaveBeenCalled();
        });

        it('scope.handler.pre.info (true)', function () {
            scope.config.model = true;
            scope.model = 'someModel';
            var element = $compile("<div cnInput></div>")(scope);
            spyOn(scope, 'compile');
            scope.handler.pre.info(element, true);
            expect(scope.compile).toHaveBeenCalled();
        });

        it('scope.handler.pre.info (false)', function () {
            scope.config.model = true;
            scope.model = 'someModel';
            var element = $compile("<div cnInput></div>")(scope);
            spyOn(scope, 'compile');
            scope.handler.pre.info(element, false);
            expect(scope.compile).toHaveBeenCalled();
        });

        it('scope.handler.pre.help (true)', function () {
            scope.config.prefix.translate = 'test';
            scope.config.model = true;
            scope.model = 'someModel';
            var element = $compile("<div class='mockedButtonGroup'><div cnInput></div></div>")(scope);
            spyOn(scope, 'compile');
            expect(element.find('input')).toHaveClass('mockedControl');
            scope.handler.pre.help(element, true);
            expect(scope.compile).toHaveBeenCalled();
        });

        it('scope.handler.pre.help (false)', function () {
            scope.config.name = '<div></div>';
            scope.model = 'someModel';
            var element = $compile("<div class='mockedButtonGroup mockedSwitch'><div cnInput></div></div>")(scope);
            spyOn(scope, 'compile');
            scope.handler.pre.help(element, false);
            expect(element.find('input')).toHaveClass('mockedControl');
            expect(element.find('span')).toHaveClass('mockedButtonClass control-help');
            expect(element.find('button')).toHaveClass('input-help btn btn-white');
            expect(element.find('button')).toHaveAttr('popover', "{{ 'testfieldMockfalsehelpMock' | translate }}");
            expect(element.find('i')).toHaveClass('icon-help mockedHelp');
            expect(element.find('i')).toHaveClass('mockedAlert');
            expect(scope.compile).toHaveBeenCalled();
        });

        it('scope.handler.pre.name', function () {
            var element = $compile("<div cnInput></div>")(scope);
            scope.handler.pre.name(element, 'name');
            expect(element).toHaveAttr('name', 'name');
        });

        it('scope.handler.pre.pattern', function () {
            var element = $compile("<div cnInput></div>")(scope);
            scope.handler.pre.pattern(element, 'somePattern');
            expect(element).toHaveAttr('ng-pattern', 'patternMock');
        });

        it('scope.handler.pre.placeholder without select', function () {
            var element = $compile("<div cnInput></div>")(scope);
            scope.handler.pre.placeholder(element, 'placeHolder');
            expect(element).toHaveAttr('placeholder', "{{ 'testfieldMockplaceHolderplaceholder' | translate }}");
        });

        it('scope.handler.pre.placeholder with select', function () {
            var element = $compile("<select></select><div cnInput></div>")(scope);
            scope.handler.pre.placeholder(element, 'placeHolder');
            expect(element).toHaveHtml('<option value="" class="placeholder" disabled="" translate="">testfieldMockplaceHolderplaceholder</option>');
        });

        it('scope.handler.pre.ref', function () {
            var element = $compile("<div cnInput></div>")(scope);
            scope.handler.pre.ref(element);
            expect(element).toHaveAttr('mentio', 'mentio');
            expect(element).toHaveAttr('mentio-trigger-char', "'%'");
            expect(element).toHaveAttr('mentio-items', 'ref');
            expect(element).toHaveAttr('mentio-template-url', 'ref-search');
            expect(element).toHaveAttr('mentio-search', 'searchRef(term)');
            expect(element).toHaveAttr('mentio-select', 'getRefText(item)');
        });

        it('scope.handler.pre.required (string)', function () {
            var required = 'requiredMock';
            scope.config.prefix.required = true;
            scope.config.prefix.enabled = true;
            scope.config.enabled = 'enabledMock';
            var element = $compile("<div cnInput></div>")(scope);
            scope.handler.pre.required(element, required);
            expect(element).toHaveAttr('ng-required', 'formMockrequiredMock');
        });

        it('scope.handler.pre.required (number)', function () {
            var required = 4;
            scope.config.enabled = 'enabledMock';
            var element = $compile("<div cnInput></div>")(scope);
            scope.handler.pre.required(element, required);
            expect(element).toHaveAttr('ng-required', 'false');
        });

        it('scope.handler.pre.reset inputReset function', function () {
            scope.model = 'toDelete';
            var element = $compile("<div cnInput></div>")(scope);
            spyOn(scope, 'deleteProperty');
            scope.form.ctrl.test = {
                $pristine: true,
                $render: function () {
                    return;
                },
                $setViewValue: function () {
                    return;
                }
            };
            scope.config.name = 'test';
            scope.handler.pre.reset(element);
            element.click();
            expect(scope.deleteProperty).toHaveBeenCalledWith(scope.model);
        });

        it('scope.handler.pre.reset inputDirty function', function () {
            var element = $compile("<div cnInput></div>")(scope);
            scope.config.name = 'test';
            scope.form.ctrl.test = {
                $setDirty: function () {
                    return;
                }
            };
            spyOn(scope.form.ctrl.test, '$setDirty');
            scope.handler.pre.reset(element);
            element.blur();
            expect(scope.form.ctrl.test.$setDirty).toHaveBeenCalled();
        });

        it('scope.handler.pre.value works fine', function () {
            scope.model = 'modelMock';
            $compile("<div cnInput></div>")(scope);
            spyOn(scope, '$eval');
            scope.handler.pre.value('data');
            expect(scope.$eval).toHaveBeenCalledWith('modelMock="data"');
        });

        it('scope.handler.pre.watch watcher works fine ', function () {
            var watch = {};
            scope.model = 'modelMock';
            var done;
            $compile("<div cnInput></div>")(scope);
            var data = {data: {expression: 'mockedExpression', listener: jasmine.createSpy(watch, 'listener').and.callFake(function () {
                done = true;
            })}};
            scope.handler.pre.watch(data);
            scope.formMockmockedExpression = 'somedata';
            scope.$digest();
            expect(done).toEqual(true);
        });

        it('scope.handler.post.confirm', function () {
            $compile("<div cnInput></div>")(scope);
            spyOn(scope, '$eval');
            scope.config.model = 'mockedModel';
            scope.config.name = 'test';
            scope.handler.post.confirm('mock');
            scope.form.ctrl.test = {
                $setValidity: function () {
                    return;
                }
            };
            scope.$digest();
            expect(scope.$eval).toHaveBeenCalledWith('mockedModel===formMockmock');
        });

        it('scope.link function works (remove attr)', function () {
            var element = $compile("<div cnInput cninputmock='test'></div>")(scope);
            scope.config = {type: 'mock'};
            scope.handler.link = {
                mock: 'hendlerMock'
            };
            scope.link(element, 'link');
            expect(element).not.toHaveAttr('cninputmock', 'test');
        });

        it('scope.link function works (handler function)', function () {
            var element = $compile("<div cnInput cninputmock='test' error='error'></div>")(scope);
            scope.config = {type: 'mock'};
            scope.handler.pre = {
                mock: function () {
                    return;
                }
            };
            scope.link(element);
            expect(element).toHaveAttr('error');
        });


        it('scope.handler.pre.model (scope.model equal to cfg.input...)', function () {
            var element = $compile("<div cnInput></div>")(scope);
            cfg.input.prefix.error = 'errorMock';
            scope.handler.pre.model(element, 'modelMock');
            expect(element).toHaveAttr('ng-model', 'formMockmodelMock');
            spyOn(scope, 'deleteProperty');
            scope.formMockmodelMock = 'test';
            scope.$digest();
            scope.formMockmodelMock = 'test1';
            scope.$digest();
            expect(scope.deleteProperty).toHaveBeenCalledWith('errorMockmodelMock');
        });

        it('scope.handler.pre.model (scope.model equal model)', function () {
            var element = $compile("<div cnInput></div>")(scope);
            scope.config.prefix = false;
            cfg.input.prefix.form = 'mock';
            scope.handler.pre.model(element, 'modelMock');
            expect(element).toHaveAttr('ng-model', 'modelMock');
            spyOn(scope, 'deleteProperty');
            scope.modelMock = 'test';
            scope.$digest();
            scope.modelMock = 'test1';
            scope.$digest();
            expect(scope.deleteProperty).toHaveBeenCalledWith('errorMockmodelMock');
        });

        it('scope.handler.pre.model (watch now undefined)', function () {
            var element = $compile("<div cnInput></div>")(scope);
            spyOn(scope, 'deleteProperty');
            scope.config.prefix = false;
            scope.config.empty = null;
            cfg.input.prefix.form = 'mock';
            scope.handler.pre.model(element, 'modelMock');
            scope.modelMock = undefined;
            scope.$digest();
            expect(element).toHaveAttr('ng-model', 'modelMock');
            expect(scope.deleteProperty).toHaveBeenCalledWith('modelMock');
            expect(element).toHaveClass('input-empty');
            scope.modelMock = 'test';
            scope.config.empty = true;
            scope.$digest();
            expect(element).not.toHaveClass('input-empty');
        });
    });
});
