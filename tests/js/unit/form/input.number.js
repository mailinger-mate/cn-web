/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $formatters, $new, $parsers, any, compile, ctrl, form, handler,
    link, max, min, name, pattern, post, pre, string, test, toEqual,
    toHaveAttr, toHaveBeenCalledWith, toHaveClass
*/

describe("directive: cninputnumber", function () {
    'use strict';
    var $rootScope, scope, $compile;
    beforeEach(function () {
        module('cn.input.number');
        inject(function (_$rootScope_, _$compile_) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.form = {
            ctrl: {
                test: {
                    name: 'mockedName',
                    $parsers: [10, 20],
                    $formatters: [30]
                }
            }
        };
        scope.handler = {
            pre: {
                pattern: 'mock'
            },
            post: {string: ''}
        };
    });

    it('directive cninputnumber works fine', function () {
        var element = $compile("<div cninputnumber></div>")(scope);
        $rootScope.$digest();
        expect(element).toHaveClass('form-control');
        expect(element).toHaveAttr('type', 'number');
    });

    it('controller function scope.handler.pre.max works', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputnumber></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.max(element, 'max');
        $rootScope.$digest();
        expect(element).toHaveAttr('max', 'max');
    });

    it('controller function scope.handler.pre.min works', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputnumber></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.min(element, 'min');
        $rootScope.$digest();
        expect(element).toHaveAttr('min', 'min');
    });

    it('controller function scope.handler.pre.post.string works', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputnumber='' name='test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.post.string(element);
        $rootScope.$digest();
        expect(scope.form.ctrl.test.$parsers).toEqual([10, 20, jasmine.any(Function)]);
        expect(scope.form.ctrl.test.$formatters).toEqual([30, jasmine.any(Function)]);
    });

    it('scope.link in link function called', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputnumber></div>")(scope);
        $rootScope.$digest();
        expect(scope.link).toHaveBeenCalledWith(element, 'post');
    });

    it('scope.compile in link function called', function () {
        spyOn(scope, 'compile');
        var element = $compile("<div cninputnumber></div>")(scope);
        $rootScope.$digest();
        expect(scope.compile).toHaveBeenCalledWith(element);
    });
});