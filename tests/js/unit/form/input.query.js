/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $eval, $new, $parent, account, addGroup, addRule, and,
    application, compile, config, data, field, form, group, handler, link,
    manual, model, objectContaining, operator, options, pre, put, query,
    querygroup, removeGroup, removeRule, resetData, resetQuery, returnValue,
    rules, scope, test, toBeDefined, toEqual, toHaveBeenCalled,
    toHaveBeenCalledWith, type
*/

describe("module: cn.input.query", function () {
    'use strict';
    var $rootScope, scope, $compile;

    beforeEach(function () {
        module('cn.input.query');
        inject(function (_$rootScope_, _$compile_, $templateCache) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $templateCache.put("html/form/input.query.html", '<div name="test"></div>');
            $templateCache.put("html/form/input.query.group.html", '');

        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };

        scope.handler = {
            pre: {
                group: function () {
                    return;
                }
            }
        };

        scope.data = {
            group: {
                rules: ''
            }
        };
        scope.config = {
            application: 'mock'
        };
        scope.query = {
            config: {
                operator: {
                    options: ''
                },
                manual: ''
            }
        };
        scope.group = {
            rules: ['test']
        };
        scope.form = {
            field: {
                config: {
                    query: ''
                }
            },
            config: {
                account: ''
            }
        };
    });

    describe('directive cninputquery', function () {

        it('directive cninputquery is defined', function () {
            spyOn(scope, '$eval');
            scope.$digest();
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            expect(element).toBeDefined();
        });
        it('scope link is called', function () {
            spyOn(scope, '$eval');
            spyOn(scope, 'link');
            $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            expect(scope.link).toHaveBeenCalled();
        });

        it('scope application is defined', function () {
            spyOn(scope, '$eval');
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            expect(element.scope().application).toEqual(scope.config.application);
        });

        it('scope query is defined', function () {
            spyOn(scope, '$eval');
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            expect(element.scope().query).toEqual(jasmine.objectContaining({
                config: {
                    group: {
                        type: 'querygroup',
                        application: 'mock',
                        model: false
                    },
                    manual: false
                }
            }));
        });

        it('scope data is defined', function () {
            spyOn(scope, '$eval');
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            expect(element.scope().data).toBeDefined();
        });

        it('scope data is defined', function () {
            spyOn(scope, '$eval');
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            expect(element.scope().data).toBeDefined();
        });

        it('scope.watch(data) works fine', function () {
            spyOn(scope, '$eval');
            scope.model = 'test';
            $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            scope.data = {group: {rules: [{field: "published", operator: "equals", data: "123"}]}};
            $rootScope.$apply();
            expect(scope.$eval).toHaveBeenCalledWith('test');
        });

        it('scope.watch(query.config.manual) works - fire reset', function () {
            spyOn(scope, '$eval');
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            element.scope().query.config.manual = true;
            scope.$digest();
            expect(element.scope().data).toEqual({});
        });

        it('scope.resetQuery works ', function () {
            scope.form.field.config.query = 'mock';
            spyOn(scope, '$eval').and.returnValue('fakeEval');
            var element = $compile("<div cninputquery></div>")(scope);
            $rootScope.$digest();
            element.scope().resetQuery();
            expect(scope.form.field.config.query).toEqual();
        });

    });
    describe('directive cninputquerygroup', function () {

        it('directive cninputquerygroup is defined', function () {
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            expect(element).toBeDefined();
        });

        it('scope.handler.pre.group works fine', function () {
            scope.group = 'mockedGroup';
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            element.scope().handler.pre.group();
            expect(scope.group).toEqual('mockedGroup');
        });

        it('scope.link is called', function () {
            spyOn(scope, 'link');
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            expect(scope.link).toHaveBeenCalledWith(element);
        });

        it('scope.querygroup is defined', function () {
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            expect(element.scope().querygroup).toBeDefined();
        });

        it('scope.addRule works', function () {
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            element.scope().addRule();
            expect(scope.group.rules).toEqual(['test', {}]);
        });

        it('scope.removeRule works', function () {
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            element.scope().removeRule();
            expect(scope.group.rules).toEqual([]);
        });

        it('scope.addGroup works', function () {
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            element.scope().addGroup();
            expect(scope.group.rules).toEqual(['test', {group: {rules: [{}]}}]);
        });

        it('scope.removeGroup works', function () {
            scope.$parent = {
                group: {
                    rules: ['someRUles']
                }
            };
            var element = $compile("<div cninputquerygroup></div>")(scope);
            $rootScope.$digest();
            element.scope().removeGroup();
            expect(scope.group.rules).toEqual([]);
        });

        it('scope.resetData works', function () {
            var element = $compile("<div cninputquerygroup></div>")(scope);
            scope.test = {data: 'test'};
            $rootScope.$digest();
            element.scope().resetData(scope.test);
            expect(scope.test).toEqual({});
        });
    });
});