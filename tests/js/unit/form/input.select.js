/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $eval, $filter, $label, $new, $property, $select, $track, account,
    api_url, cfg, check, compile, config, connection, control, core, css,
    destination, dynamic, error, expression, flush, form, handler, id, input,
    link, mocked, model, multiple, name, options, path, placeholder, postfix,
    pre, prefix, respond, rpc_url, select, session, some, source, toHaveAttr,
    toHaveBeenCalledWith, toHaveClass, token, translate, unauthorized, user,
    value, whenGET
*/

describe("directive: cninputselect", function () {
    'use strict';
    var $rootScope, scope, $compile, $httpBackend;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var cfgMock = {
        input: {
            postfix: {
                form: '',
                options: 'postfixOptions'
            },
            prefix: {
                select: 'prefixOptions'
            }
        },
        connection: {
            config: {
                source: {
                    mocked: 'source'
                },
                destination: {
                    mocked: 'source'
                }
            }
        },
        core: {
            api_url: 'api_url/',
            rpc_url: 'rpc_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        },
        css: {
            input: {
                control: 'mockedControl'
            }
        }
    };
    beforeEach(function () {
        module('pascalprecht.translate');
        module('cn.action');
        module('cn.input.select');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, _$httpBackend_) {
            $httpBackend = _$httpBackend_;
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.name = {
            expression: ''
        };
        scope.cfg = {
            input: {
                prefix: {
                    form: ''
                }
            }
        };
        scope.config = {
            model: ['modelMock'],
            dynamic: {
                options: {some: 'object'}
            },
            prefix: {
                translate: ''
            },
            translate: ''
        };
        scope.form = {
            config: {
                account: ''
            }
        };
        scope.handler = {
            pre: {
                options: function () {
                    return;
                }
            }
        };
    });

    it('directive cninputselect works fine', function () {
        var element = $compile("<div cninputselect></div>")(scope);
        $rootScope.$digest();
        expect(element).toHaveAttr('cninputselect', '');
        expect(element).toHaveClass('mockedControl');
    });

    it('scope.handler.pre.options (false translate)', function () {
        spyOn(scope, '$eval');
        scope.config.translate = false;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(scope.$eval).toHaveBeenCalledWith(['modelMock']);
        expect(element).toHaveAttr('ng-options', 'key for (key, value) in TestpostfixOptions');
    });

    it('scope.handler.pre.options (true translate)', function () {
        spyOn(scope, '$eval');
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(scope.$eval).toHaveBeenCalledWith(['modelMock']);
        expect(element).toHaveAttr('ng-options', "'prefixOptionsundefined.' + key | translate for (key, value) in TestpostfixOptions");
    });

    it('scope.handler.pre.options $select exist', function () {
        scope.config.dynamic.options = {$select: 'someSelect'};
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "key.someSelect as 'prefixOptionsundefined.' + key | translate for (key, value) in TestpostfixOptions");
    });

    it('scope.handler.pre.options $label exist', function () {
        scope.config.dynamic.options = {$label: 'someLabel'};
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "someLabel for (key, value) in TestpostfixOptions");
    });

    it('scope.handler.pre.options $property exist', function () {
        scope.config.dynamic.options = {$property: 'someProperty'};
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "key.someProperty as 'prefixOptionsundefined.' + key.someProperty | translate for key in TestpostfixOptions");
    });

    it('scope.handler.pre.options $filter exist', function () {
        scope.config.dynamic.options = {$filter: 'someFilter'};
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "'prefixOptionsundefined.' + key | translate for (key, value) in TestpostfixOptions | someFilter");
    });

    it('scope.handler.pre.options $track exist', function () {
        scope.config.dynamic.options = {$track: 'someTrack'};
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "'prefixOptionsundefined.' + key | translate for (key, value) in TestpostfixOptions track by someTrack");
    });

    it('scope.handler.pre.options placeholder string', function () {
        scope.config.placeholder = 'string';
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "'prefixOptionsstring.' + key | translate for (key, value) in TestpostfixOptions");
    });

    it('scope.handler.pre.options placeholder numbers', function () {
        scope.config.name = 'someName';
        scope.config.placeholder = 123;
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "'prefixOptionssomeName.' + key | translate for (key, value) in TestpostfixOptions");
    });

    it('scope.handler.pre.options non object options', function () {
        scope.config.dynamic.options = 'nonObject';
        scope.config.translate = true;
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.options(element, {some: 'options'});
        expect(element).toHaveAttr('ng-options', "nonObject");
    });

    it('scope.handler.pre.multiple function works', function () {
        var element = $compile("<div cninputselect config='Test'></div>")(scope);
        $rootScope.$digest();
        scope.handler.pre.multiple(element, '');
        expect(element).toHaveAttr('multiple', "multiple");
    });

    it('scope.handler.pre.dynamic function works with (account.name.length)', function () {
        spyOn(scope, '$eval');
        $httpBackend.whenGET('api_url/accounts/4214/info/someName').respond(['someData']);
        var handler = function () {
            return 'some Data';
        };
        var dynamic = {name: 'someName', handler: handler};
        $compile("<div cninputselect config='Test'></div>")(scope);
        scope.handler.pre.dynamic(dynamic);
        scope.form.config.account = {id: 4214};
        $rootScope.$digest();
        $httpBackend.flush();
        expect(scope.$eval).toHaveBeenCalledWith('TestpostfixOptions="some Data"');
    });

    it('scope.handler.pre.select function works (select numbers)', function () {
        var select = 888;
        var element = $compile("<div cninputselect config='test'></div>")(scope);
        scope.handler.pre.options(element, {some: 'options'});
        scope.$digest();
        scope.handler.pre.select(element, select);
        expect(element).toHaveAttr('ng-init', 'modelMock = (testpostfixOptions)[888]');
    });

    it('scope.handler.pre.select function works (select string)', function () {
        var select = '888';
        var element = $compile("<div cninputselect config='test'></div>")(scope);
        scope.handler.pre.options(element, {some: 'options'});
        scope.$digest();
        scope.handler.pre.select(element, select);
        expect(element).toHaveAttr('ng-init', 'modelMock = (testpostfixOptions)[0]');
    });

    it('cninputselect link function', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputselect config='test'></div>")(scope);
        scope.$digest();
        expect(scope.link).toHaveBeenCalledWith(element);
    });

    it('cninputselect compile function', function () {
        spyOn(scope, 'compile');
        var element = $compile("<div cninputselect config='test'></div>")(scope);
        scope.$digest();
        expect(scope.compile).toHaveBeenCalledWith(element);
    });
});