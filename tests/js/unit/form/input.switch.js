/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $eval, $new, base, button, check, compile, config, control, css,
    deleteProperty, find, group, input, link, model, name, pattern, prefix,
    session, switch, toHaveAttr, toHaveBeenCalled, toHaveBeenCalledWith,
    toHaveClass, translate, type, value
*/

describe("directive: cninputswitch", function () {
    'use strict';
    var $rootScope, scope, $compile;
    var cfgMock = {
        input: {
            prefix: {
                button: 'Button'
            },
            pattern: 'Pattern'
        },
        session: {
            check: 2
        },
        css: {
            button: {
                switch: 'Switch',
                base: 'Base',
                group: 'Group'
            },
            input: {
                switch: 'mockedSwitch',
                control: 'Control'
            }
        }
    };
    beforeEach(function () {
        module('cn.input.switch');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.deleteProperty = function () {
            return;
        };
        scope.model = 'model';
        scope.config = {
            translate: 'translate',
            name: 'Name',
            prefix: {
                translate: 'translate'
            },
            input: {
                type: {},
                pattern: 'pattern'
            }
        };
    });

    it('directive cninputswitch works fine', function () {
        var element = $compile("<div cninputswitch=''></div>")(scope);
        $rootScope.$digest();
        expect(element).toHaveClass('mockedSwitch');
        expect(element.find('switch')).toHaveAttr('on', "{{ 'connection.item.button.status.on' | translate }}");
        expect(element.find('switch')).toHaveAttr('off', "{{ 'connection.item.button.status.off' | translate }}");
        expect(element.find('switch')).toHaveAttr('class', "green");
    });

    it('scope.link works fine', function () {
        spyOn(scope, 'link');
        $compile("<div cninputswitch=''></div>")(scope);
        $rootScope.$digest();
        expect(scope.link).toHaveBeenCalled();
    });

    it('scope.compile works fine', function () {
        spyOn(scope, 'compile');
        $compile("<div cninputswitch=''></div>")(scope);
        $rootScope.$digest();
        expect(scope.compile).toHaveBeenCalled();
    });

    it('scope.eval called', function () {
        spyOn(scope, '$eval');
        scope.model = true;
        $compile("<div cninputswitch=''></div>")(scope);
        expect(scope.$eval).toHaveBeenCalledWith(scope.model);
    });
});