/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, attr, check, compile, config, control, createSpy, css,
    deleteProperty, handler, input, keyCode, link, mockPattern, model, not,
    objectContaining, onKeyDown, pattern, pre, prefix, preventDefault, remove,
    required, scope, session, shiftKey, stopImmediatePropagation, tagInput_1,
    tags, test, testForm, toBeDefined, toBeFalsy, toContainHtml, toEqual,
    toHaveAttr, toHaveBeenCalledWith, toHaveClass, type, updateField, value
*/

describe("directive: cninputtags", function () {
    'use strict';
    var $rootScope, scope, $compile, cfg;
    var cfgMock = {
        input: {
            pattern: 'mockOnPattern'
        },
        session: {
            check: 2
        },
        css: {
            input: {
                control: 'mockedControl'
            }
        }
    };
    beforeEach(function () {
        module('cn.input.tags');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, _cfg_) {
            cfg = _cfg_;
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
        scope.deleteProperty = function () {
            return;
        };
        scope.updateField = function () {
            return;
        };
        scope.config = {
            input: {
                type: {},
                pattern: 'mockOnPattern'
            }
        };
        scope.handler = {
            pre: {
                pattern: 'mock'
            }
        };
    });

    it('directive cninputtags works fine', function () {
        var element = $compile("<div cninputtags=''></div>")(scope);
        $rootScope.$digest();
        expect(element).toHaveClass('cn-tags');
        expect(element).toHaveAttr('cninputtags');
        expect(element).toContainHtml('<div cninput="" class="input" config="tags.config.input"></div>');
        expect(element).toContainHtml('ngRepeat: (key, tag) in tags.model');
        expect(element).toContainHtml('<input type="hidden" class="model">');
    });

    it('directive cninputtags  controller function works', function () {
        $compile("<div cninputtags=''></div>")(scope);
        $rootScope.$digest();
        expect(scope.handler.pre.pattern).not.toBeDefined();
        expect(scope.config.input).toEqual(jasmine.objectContaining({
            pattern: false,
            model: 'tags.input',
            required: false,
            prefix: {model: false, required: false},
            attr: {'ng-keydown': 'onKeyDown($event)', 'ng-blur': 'onKeyDown()'}
        }));
    });

    it('directive cninputtags updateField defined', function () {
        var element = $compile('<form name="testForm"><div cninputtags=""></div></form>')(scope);
        scope.tags = {
            model: ['0', '1']
        };
        scope.testForm = {
            tagInput_1: 'mockedInput'
        };
        scope.$digest();
        element.scope().updateField('tag', 1);
    });

    it('directive cninputtags onkeyDown event undefined', function () {
        var element = $compile("<div cninputtags=''></div>")(scope);
        $rootScope.$digest();
        expect(element.scope().onKeyDown(undefined)).toBeFalsy();
    });

    it('directive cninputtags onkeyDown event 188 or 9 key', function () {
        var element = $compile("<div cninputtags=''></div>")(scope);
        scope.tags = {
            model: ['test', 'test1'],
            input: 'test2'
        };
        scope.config = {
            input: {
                pattern: 'mockPattern'
            }
        };
        cfg.input.pattern = {
            mockPattern: {
                test: 'test'
            }
        };

        $rootScope.$digest();
        var test = {
            shiftKey: false,
            keyCode: 188,
            preventDefault: jasmine.createSpy(),
            stopImmediatePropagation: jasmine.createSpy()
        };
        element.scope().onKeyDown(test);
        expect(scope.tags.model).toEqual(['test', 'test1']);
        test = {
            shiftKey: false,
            keyCode: 9,
            preventDefault: jasmine.createSpy(),
            stopImmediatePropagation: jasmine.createSpy()
        };
        element.scope().onKeyDown(test);
        expect(scope.tags.model).toEqual(['test', 'test1']);
    });

    it('directive cninputtags onkeyDown event 8 key', function () {
        var element = $compile("<div cninputtags=''></div>")(scope);
        scope.tags = {
            model: []
        };
        spyOn(element.scope(), 'remove');
        scope.tags.model = 'test';
        $rootScope.$digest();
        var test = {
            shiftKey: false,
            keyCode: 8,
            preventDefault: jasmine.createSpy(),
            stopImmediatePropagation: jasmine.createSpy()
        };
        element.scope().onKeyDown(test);
        expect(element.scope().remove).toHaveBeenCalledWith(-1);
    });

    it('directive cninputtags  controller function works input.required defined', function () {
        scope.config.input.required = 'mocked';
        scope.model = 'mocked';
        $compile("<div cninputtags=''></div>")(scope);
        $rootScope.$digest();
        expect(scope.config.input).toEqual(jasmine.objectContaining({
            required: '!mocked'
        }));
    });

    it('directive cninputtags controller function scope.config.input.type defined', function () {
        scope.config.input.type = 'select';
        $compile("<div cninputtags=''></div>")(scope);
        $rootScope.$digest();
        expect(scope.config.input).toEqual(jasmine.objectContaining({
            attr: {'ng-change': 'onKeyDown()'}
        }));
    });
});