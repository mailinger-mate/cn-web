/*global
    spyOn afterEach jasmine angular describe module beforeEach it expect inject
*/
/*property
    $digest, $new, $viewValue, check, compile, control, css, form, input, link,
    model, name, session, toBe, toHaveAttr, toHaveBeenCalledWith, toHaveClass,
    value
*/
describe("directives of: cn.input.text", function () {
    'use strict';
    var $rootScope, scope, $compile, cfg;
    var cfgMock = {
        session: {
            check: 2
        },
        css: {
            input: {
                control: 'mockedControl'
            }
        }
    };
    beforeEach(function () {
        module('cn.input.text');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, _cfg_) {
            cfg = _cfg_;
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
    });
    describe("directive: cninputtext", function () {

        it('directive cninputtext works fine', function () {
            var element = $compile("<div cninputtext></div>")(scope);
            $rootScope.$digest();
            expect(element).toHaveClass(cfg.css.input.control, 'ng-scope');
            expect(element).toHaveAttr('cninputtext', '');
        });
        it('cninputtext function link is called', function () {
            spyOn(scope, 'link');
            var element = $compile("<div cninputtext></div>")(scope);
            expect(scope.link).toHaveBeenCalledWith(element);

        });

        it('cninputtext function compile works fine', function () {
            spyOn(scope, 'compile');
            var element = $compile("<div cninputtext></div>")(scope);
            expect(scope.compile).toHaveBeenCalledWith(element);
        });
    });

    describe("directive: cninputpassword", function () {

        it('directive cninputpassword works fine', function () {
            var element = $compile("<div cninputpassword></div>")(scope);
            $rootScope.$digest();
            expect(element).toHaveClass(cfg.css.input.control, 'ng-scope');
            expect(element).toHaveAttr('cninputpassword', '');
            expect(element).toHaveAttr('type', 'password');

        });
        it('cninputpassword function link is called', function () {
            spyOn(scope, 'link');
            var element = $compile("<div cninputtext></div>")(scope);
            expect(scope.link).toHaveBeenCalledWith(element);

        });

        it('cninputpassword function compile works fine', function () {
            spyOn(scope, 'compile');
            var element = $compile("<div cninputtext></div>")(scope);
            expect(scope.compile).toHaveBeenCalledWith(element);
        });
    });

    describe("directive: cninputurl", function () {

        it("compiles", function () {

            var name = "mockName",
                element;

            spyOn(scope, "link");
            spyOn(scope, "compile");

            scope.name = name;
            element = $compile("<div cninputurl ng-model='model'></div>")(scope);

            expect(scope.link).toHaveBeenCalledWith(element);
            expect(scope.compile).toHaveBeenCalledWith(element);

            expect(element).toHaveClass(cfg.css.input.control);
            expect(element).toHaveClass("url-" + name);
            expect(element).toHaveAttr("type", "url");
        });

        it("prefixes url with http", function () {

            var url = "mockUrl";

            $compile("<form name='form'>" +
                    "<div cninputurl ng-model='model' name='input'></div>" +
                    "</form>")(scope);

            scope.model = url;
            scope.$digest();

            expect(scope.form.input.$viewValue).toBe("http://" + url);
        });

    });

    describe("directive: cninputemail", function () {

        it('directive cninputemail works fine', function () {
            var element = $compile("<div cninputemail></div>")(scope);
            $rootScope.$digest();
            expect(element).toHaveClass(cfg.css.input.control, 'ng-scope');
            expect(element).toHaveAttr('cninputemail', '');
            expect(element).toHaveAttr('type', 'email');

        });
        it('cninputemail function link is called', function () {
            spyOn(scope, 'link');
            var element = $compile("<div cninputemail></div>")(scope);
            expect(scope.link).toHaveBeenCalledWith(element);

        });

        it('cninputemail function compile works fine', function () {
            spyOn(scope, 'compile');
            var element = $compile("<div cninputemail></div>")(scope);
            expect(scope.compile).toHaveBeenCalledWith(element);
        });
    });
});
