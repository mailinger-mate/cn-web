/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, check, compile, control, css, input, link, session,
    toHaveAttr, toHaveBeenCalledWith, toHaveClass, value
*/

describe("directive: cninputTextarea", function () {
    'use strict';
    var $rootScope, scope, $compile, cfg;
    var cfgMock = {
        session: {
            check: 2
        },
        css: {
            input: {
                control: 'mockedControl'
            }
        }
    };
    beforeEach(function () {
        module('cn.input.textarea');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        inject(function (_$rootScope_, _$compile_, _cfg_) {
            cfg = _cfg_;
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
        });
        scope.link = function () {
            return;
        };
        scope.compile = function () {
            return;
        };
    });

    it('directive cninputtextarea works fine', function () {
        var element = $compile("<div cninputtextarea></div>")(scope);
        $rootScope.$digest();
        expect(element).toHaveClass(cfg.css.input.control, 'ng-scope');
        expect(element).toHaveAttr('cninputtextarea', '');
    });
    it('function link is called', function () {
        spyOn(scope, 'link');
        var element = $compile("<div cninputtextarea></div>")(scope);
        expect(scope.link).toHaveBeenCalledWith(element);

    });

    it('function compile works fine', function () {
        spyOn(scope, 'compile');
        var element = $compile("<div cninputtextarea></div>")(scope);
        expect(scope.compile).toHaveBeenCalledWith(element);
    });
});