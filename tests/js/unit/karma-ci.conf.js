/*global module*/
/*property
 LOG_INFO, autoWatch, basePath, browsers, colors, concurrency, exclude,
 exports, files, frameworks, included, junitReporter, logLevel, outputDir,
 outputFile, pattern, plugins, port, preprocessors, reporters, served, set,
 singleRun, suite, useBrowserName, watched, coverageReporter, type, dir
 */

module.exports = function (config) {

    'use strict';

    config.set({
        basePath: '',

        frameworks: ['jasmine'],

        files: [
            {
                pattern: '../../../web/js/compiled/libs.js',
                watched: false
            },
            {
                pattern: '../../../web/js/temp/js_angular.js',
                watched: false
            },
            {
                pattern: '../../../web/js/temp/js_inspinia.js',
                watched: false
            },
            {
                pattern: '../../../src/Bundle/UiBundle/Resources/public/js/app.js',
                watched: false
            },
            {
                pattern: '../../../src/Bundle/AppBundle/Resources/public/app/**/*.js',
                watched: false
            },
            {
                pattern: '../../../src/Bundle/UiBundle/Resources/public/vendor/jasmine-jquery/lib/jasmine-jquery.js',
                watched: false
            },
            {
                pattern: '../../../src/Bundle/UiBundle/Resources/public/vendor/angular-mocks/angular-mocks.js',
                watched: false
            },
            {
                pattern: '../../../src/Bundle/UiBundle/Resources/public/vendor/jasmine-ajax/lib/mock-ajax.js',
                watched: false
            },
            '**/*.js'
        ],

        exclude: [
            '*.conf.js'
        ],

        preprocessors: {
            '../../../src/Bundle/AppBundle/Resources/public/app/**/*.js': ['coverage']
        },

        reporters: ['progress', 'junit', 'coverage'],

        junitReporter: {
            outputDir: '../../results/js/unit/output',
            outputFile: 'karma-output.xml',
            suite: '',
            useBrowserName: true
        },

        coverageReporter: {
            type: 'html',
            dir: '../../results/js/unit/coverage'
        },

        port: 9876,

        colors: true,

        logLevel: config.LOG_INFO,

        autoWatch: false,

        plugins: [
            'karma-jasmine',
            'karma-firefox-launcher',
            'karma-phantomjs2-launcher',
            'karma-junit-reporter',
            'karma-coverage'
        ],

        singleRun: true,

        concurrency: Infinity
    });
};