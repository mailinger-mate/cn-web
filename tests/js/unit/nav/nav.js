/*global describe, angular, module, beforeEach, it, expect, inject, spyOn, jasmine */
/*property
    $broadcast, $digest, $new, $scope, $state, and, billing, cfg, controller,
    data, flush, forEach, getPermission, home, label, name, nav, not, only,
    permissions, push, redirect, returnValue, role, state, templateUrl, text,
    toBe, toBeDefined, toEqual, url, user, vendorBlocked
*/

describe('NavCtrl', function () {

    "use strict";

    var scope, NavCtrl, $state, $rootScope, $timeout;

    var state_name = "tutorial";

    var state_tutorial = {
        url: '/tutorial',
        templateUrl: 'html/tutorial/tutorial.html',
        controller: 'cnTutorial',
        name: 'tutorial',
        redirect: 'tutorial.start'
    };

    var state_admin = {
        url: "/admin",
        redirect: "admin.user.list",
        templateUrl: "html/admin/admin.html",
        data: {
            permissions: {
                only: ['admin']
            }
        }
    };

    var mockedCfg = {
        user: {
            role: ''
        },
        nav: {
            home: {
                state: 'connection.list'
            },
            name: ''
        }
    };

    beforeEach(function () {
        module('pascalprecht.translate');
        module('ui.router');
        module('cn.nav');
    });

    beforeEach(inject(function (_$rootScope_, $controller, _$state_, _$timeout_) {
        $timeout = _$timeout_;
        $rootScope = _$rootScope_;
        scope = $rootScope.$new();
        $state = _$state_;

        NavCtrl = $controller('NavCtrl', {
            $scope: scope,
            $state: _$state_,
            cfg: mockedCfg
        });
    }));

    it('should define the controller', function () {

        expect(NavCtrl).toBeDefined();

    });

    it('method getPermission should be defined', function () {

        expect(typeof scope.getPermission).toBe('function');

    });

    it('should resolve state', function () {

        spyOn($state, 'get').and.returnValue(state_tutorial);
        expect(state_tutorial.name).toBe(state_name);

    });

    it('method getPermission should return true on state "tutorial" and user "user"', function () {

        spyOn($state, 'get').and.returnValue(state_tutorial);
        mockedCfg.user.role = 'user';
        scope.getPermission();
        expect(scope.getPermission()).toEqual(true);

    });

    it('method getPermission should return true on state "tutorial" and user "admin"', function () {

        spyOn($state, 'get').and.returnValue(state_tutorial);
        mockedCfg.user.role = 'admin';
        scope.getPermission();
        expect(scope.getPermission()).toEqual(true);

    });

    it('method getPermission should return true on state "admin" and user "admin"', function () {

        spyOn($state, 'get').and.returnValue(state_admin);
        mockedCfg.user.role = 'admin';
        scope.getPermission();
        expect(scope.getPermission()).toEqual(true);

    });

    it('method getPermission should return false on state "admin" and user "user"', function () {

        spyOn($state, 'get').and.returnValue(state_admin);
        mockedCfg.user.role = 'user';
        scope.getPermission();
        expect(scope.getPermission()).toEqual(false);

    });

    it('should define $scope.home', function () {

        expect(scope.home).toEqual(mockedCfg.nav.home.state);

    });

    describe("config", function () {

        it('is defined', function () {
            expect(scope.nav).toBeDefined();
        });

        it('receive vendorBlocked status arg true', function () {

            $rootScope.$broadcast("vendorBlocked", true);
            scope.$digest();
            $timeout.flush();
            expect(scope.nav.billing.label.text).toEqual('nav.due');
            expect($rootScope.vendorBlocked).toEqual(true);
        });

        it('receive vendorBlocked status arg false', function () {

            $rootScope.$broadcast("vendorBlocked", false);
            scope.$digest();
            $timeout.flush();
            expect(scope.nav.billing.label).not.toBeDefined();
            expect($rootScope.vendorBlocked).toEqual(false);
        });

        it('has state reference', function () {
            var navTitles = [];
            angular.forEach(scope.nav, function (ignore, key) {
                navTitles.push(key);
            });
            expect(navTitles).toEqual(['tutorial', 'connection.item', 'connection.list', 'account.list', 'profile', 'admin.user', 'billing']);
        });
    });

});
