/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    and, callThrough, check, core, createSpy, error, flush, get, go, href,
    interval, location, logout, not, path, respond, returnValue, session,
    state, success, toEqual, toHaveBeenCalled, toHaveBeenCalledWith, token,
    unauthorized, url, user, value, verifyNoOutstandingExpectation,
    verifyNoOutstandingRequest, when
*/
describe('Session service', function () {

    'use strict';

    var session, $httpBackend, $window, $location, $state, cfg;

    var assertUserIsLogged = function (s) {
        spyOn(session, 'check').and.callThrough();
        spyOn(session, 'logout').and.callThrough();
        expect(session.check).not.toHaveBeenCalled();
        session.check();
        $httpBackend.flush();
        if (s) {
            expect(session.logout).not.toHaveBeenCalled();
        } else {
            expect(session.logout).toHaveBeenCalled();
            expect($location.url).toHaveBeenCalled();
        }
    };

    var cfgStub = {
        user: {
            core: {
                token: 'e909fcd76cc25bb2a4bb19f17ea466e4'
            }
        },
        session: {
            check: {
                url: "/mockHealthCheck/",
                interval: 60
            },
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    var mockUrl = "mockUrl";

    beforeEach(function () {

        $window = {
            location: {
                href: jasmine.createSpy()
            }
        };

        module('ui.router');
        module('cn.session');

        module(function ($provide, $stateProvider) {

            $provide.value('$location', {
                url: jasmine.createSpy().and.returnValue('/' + mockUrl)
            });
            $provide.value('$window', $window);
            $provide.value('cfg', cfgStub);

            $stateProvider.state("test", {
                url: mockUrl
            });
        });

        inject(function ($injector, _session_, _$location_, _$state_, _cfg_) {

            session = _session_;
            $location = _$location_;
            cfg = _cfg_;
            $httpBackend = $injector.get('$httpBackend');
            $state = _$state_;
        });
    });

    afterEach(function () {

        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();

    });

    it("session is maintained, server response - success:true", function () {
        $httpBackend.when("GET", cfg.session.check.url + cfg.user.core.token).respond(function () {
            return [200, {success: true}];
        });

        assertUserIsLogged(true);

    });

    it("session is not maintained, no data", function () {
        $httpBackend.when("GET", cfg.session.check.url + cfg.user.core.token).respond(function () {
            return [200, {}];
        });

        assertUserIsLogged();
    });

    it("session is not maintained, server response - success:false", function () {
        $httpBackend.when("GET", cfg.session.check.url + cfg.user.core.token).respond(function () {
            return [200, {success: false}];
        });

        assertUserIsLogged();
    });

    it("session is maintained, server error 500", function () {
        $httpBackend.when("GET", cfg.session.check.url + cfg.user.core.token).respond(function () {
            return [500, {success: false}];
        });

        assertUserIsLogged(true);
    });

    it("session is maintained, server error 501", function () {
        $httpBackend.when("GET", cfg.session.check.url + cfg.user.core.token).respond(function () {
            return [501, {success: false}];
        });

        assertUserIsLogged(true);
    });

    it("session is maintained, server error 504", function () {
        $httpBackend.when("GET", cfg.session.check.url + cfg.user.core.token).respond(function () {
            return [504, {success: false}];
        });

        assertUserIsLogged(true);
    });

    it('replace redirects after logout', function () {
        expect($window.location.href).not.toEqual('/signin?error=unauthorized&redirect=' + mockUrl);
        session.logout();
        expect($window.location.href).toEqual('/signin?error=unauthorized&redirect=' + mockUrl);
    });

    it("checks session on state change", function () {
        spyOn(session, "check");
        $state.go("test");
        expect(session.check).toHaveBeenCalledWith(mockUrl);
    });
});
