/*global describe module  beforeEach afterEach it expect jasmine inject spyOn*/
/*property
    $error, $invalid, $new, $scope, $setPristine, $valid, Email, Password,
    Profile, account, address, and, any, api_url, callFake, callThrough, check,
    config, contact, core, data, defer, email, error, field, form, id, message,
    name, not, password, path, prefix, promise, resolve, returnValue,
    saveEmail, savePassword, saveProfile, session, success, toBeDefined,
    toEqual, toHaveBeenCalled, toHaveBeenCalledWith, token, translate,
    unauthorized, update, user, validateForm, value, verified, verify,
    verifyEmail
*/

describe('module: cn.settings.profile', function () {

    'use strict';

    var cnSettingsProfile, cnSettingsProfileAccount, cfg, $q, cnSettingsProfilePassword, cnSettingsProfileEmail, action, scope;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var cfgMock = {
        core: {
            api_url: 'api_url/'
        },
        user: {
            email: {
                contact: '',
                id: 918,
                verified: 'verifiedMock'
            },
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };

    beforeEach(function () {
        module('pascalprecht.translate');
        module('cn.action');
        module('cn.settings.profile');

        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
    });

    beforeEach(inject(function ($controller, $rootScope, _action_, _$q_, _cfg_) {
        cfg = _cfg_;
        $q = _$q_;
        action = _action_;
        scope = $rootScope.$new();
        cnSettingsProfile = $controller('cnSettingsProfile', {
            $scope: scope
        });

        cnSettingsProfileAccount = $controller('cnSettingsProfileAccount', {
            $scope: scope
        });

        cnSettingsProfilePassword = $controller('cnSettingsProfilePassword', {
            $scope: scope
        });

        cnSettingsProfileEmail = $controller('cnSettingsProfileEmail', {
            $scope: scope
        });
    }));

    describe('controller: cnSettingsProfile', function () {

        it('cnSettingsProfile controller should be defined', function () {
            expect(cnSettingsProfile).toBeDefined();
        });

        it('cnSettingsProfile scope.form is defined', function () {
            expect(scope.form.config.prefix.translate).toEqual('settings.profile');
            expect(scope.form.config.field).toBeDefined();
            expect(scope.form.error.field).toBeDefined();
        });

        it('scope.validateForm works fine', function () {
            var form = {
                $error: {$invalid: false}
            };
            expect(scope.validateForm(form)).toEqual({$error: {$invalid: false}});


        });
    });

    describe('controller: cnSettingsProfileAccount', function () {

        it('cnSettingsProfileAccount controller should be defined', function () {
            expect(cnSettingsProfileAccount).toBeDefined();
        });

        it('save profile with invalid form', function () {
            scope.Profile = {
                $error: {}
            };
            expect(scope.saveProfile()).toEqual(false);
        });

        it('save profile with valid form return promise', function () {
            scope.Profile = 'mockedProfile';
            var deferred = $q.defer();
            deferred.resolve({});
            scope.form = {
                field: {
                    account: 'mockedAccount'
                }
            };
            spyOn(action.user, 'update').and.returnValue(deferred.promise);
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            expect(scope.saveProfile()).toEqual(deferred.promise);
            expect(action.user.update).toHaveBeenCalledWith('mockedAccount', {success: jasmine.any(Function), error: jasmine.any(Function)});
            expect(scope.validateForm).toHaveBeenCalledWith('mockedProfile');
        });
    });

    describe('controller: cnSettingsProfilePassword', function () {

        it('cnSettingsProfilePassword controller should be defined', function () {
            expect(cnSettingsProfilePassword).toBeDefined();
        });

        it('scope.form.config.field is defined', function () {
            expect(scope.form.config.field).toBeDefined();
        });

        it('save password with invalid form', function () {
            scope.Password = 'mockedPassword';
            scope.form = {
                field: {
                    password: 'mockedPassword'
                }
            };
            spyOn(action.user, 'update');
            spyOn(scope, 'validateForm').and.returnValue({$valid: false});
            expect(scope.savePassword()).toEqual(false);
            expect(action.user.update).not.toHaveBeenCalled();
            expect(scope.validateForm).toHaveBeenCalledWith('mockedPassword');
        });

        it('save password with valid form', function () {
            scope.Password = 'mockedPassword';
            var deferred = $q.defer();
            deferred.resolve({});
            scope.form = {
                field: {
                    password: 'mockedPassword'
                }
            };
            spyOn(action.user, 'update').and.returnValue(deferred.promise);
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            expect(scope.savePassword()).toEqual(deferred.promise);
            expect(action.user.update).toHaveBeenCalledWith('mockedPassword', {
                name: 'updatePassword',
                success: jasmine.any(Function),
                error: jasmine.any(Function)
            });
            expect(scope.validateForm).toHaveBeenCalledWith('mockedPassword');

        });

    });

    describe('controller: cnSettingsProfileEmail', function () {

        it('cnSettingsProfileEmail controller should be defined', function () {
            expect(cnSettingsProfileEmail).toBeDefined();
        });

        it('form scopes are defined correctly', function () {
            expect(scope.form.config.verified).toEqual(cfg.user.email.verified);
            expect(scope.form.config.verify).not.toEqual(cfg.user.email.verified);
            expect(scope.form.config.field).toBeDefined();
        });

        it('function saveEmail works correctly (invalid form)', function () {
            scope.Email = 'emailMock';
            scope.form = {
                field: {
                    password: 'mockedPassword'
                }
            };
            spyOn(action.user, 'update');
            spyOn(scope, 'validateForm').and.returnValue({$valid: false});
            expect(scope.saveEmail()).toEqual(false);
            expect(action.user.update).not.toHaveBeenCalled();
            expect(scope.validateForm).toHaveBeenCalledWith('emailMock');
        });

        it('function saveEmail with valid form success function', function () {
            scope.Email = {
                $error: {
                    $invalid: false
                },
                $setPristine: 'mock'
            };
            spyOn(scope.Email, '$setPristine');
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            spyOn(action, 'run').and.callThrough();
            scope.form = {
                config: {
                    verify: '',
                    verified: ''
                },
                field: {
                    email: {
                        contact: 'mockedEmail'
                    }
                }
            };
            spyOn(action.user, 'update').and.callFake(function (ignore, config) {
                config.success();
            });
            scope.saveEmail();
            expect(action.user.update).toHaveBeenCalledWith({contact: 'mockedEmail'}, {
                name: 'updateEmail',
                success: jasmine.any(Function),
                error: jasmine.any(Function)
            });
            expect(scope.Email.$setPristine).toHaveBeenCalled();
            expect(scope.form.config.verify).toEqual(true);
            expect(scope.form.config.verified).toEqual(false);
            expect(cfg.user.email.verified).toEqual(false);
            expect(cfg.user.email.address).toEqual('mockedEmail');
        });

        it('function saveEmail with valid form error function', function () {
            scope.Email = {
                $error: {
                    $invalid: false
                },
                $setPristine: 'mock'
            };
            spyOn(scope.Email, '$setPristine');
            spyOn(scope, 'validateForm').and.returnValue({$valid: true});
            spyOn(action, 'run').and.callThrough();
            scope.form = {
                error: {
                    field: {
                        email: ''
                    }
                },
                field: {
                    email: {
                        contact: 'mockedEmail'
                    }
                }
            };
            spyOn(action.user, 'update').and.callFake(function (ignore, config) {
                config.error({data: {message: 'error message'}});
            });
            scope.saveEmail();
            expect(action.user.update).toHaveBeenCalledWith({contact: 'mockedEmail'}, {
                name: 'updateEmail',
                success: jasmine.any(Function),
                error: jasmine.any(Function)
            });
            expect(scope.form.error.field.email).toEqual('error message');
        });

        it('function verifyEmail works fine (invalid form)', function () {
            scope.Email = {
                email: {
                    $valid: false
                }
            };
            expect(scope.verifyEmail()).toEqual(false);
        });

        it('function verifyEmail works fine (valid form)', function () {
            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(action.contact, 'email').and.returnValue(deferred.promise);
            scope.Email = {
                email: {
                    $valid: true
                }
            };
            expect(scope.verifyEmail()).toEqual(deferred.promise);
            expect(action.contact.email).toHaveBeenCalledWith({id: 918, success: jasmine.any(Function)});
        });
    });
});