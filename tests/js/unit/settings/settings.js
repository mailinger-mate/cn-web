/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, abstract, addPart, any, controller, current, data, get, go, name,
    pageTitle, put, templateUrl, toBe, toBeTruthy, toEqual, url
*/

describe('config: settings', function () {
    'use strict';
    var $rootScope, $state, state_name = 'settings', profileState = 'settings.profile';

    beforeEach(function () {
        module('pascalprecht.translate');
        module('ui.router');
        module('cn.settings', function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('settings/settings');
        });

        inject(function (_$rootScope_, _$state_, $templateCache) {
            $rootScope = _$rootScope_;
            $state = _$state_;
            $templateCache.put('html/settings/profile.html', '');
            $templateCache.put('html/settings/settings.html', '');
        });
    });

    it('settings abstract state is defined correctly', function () {

        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.abstract).toBeTruthy();
        expect(state.url).toBe('/settings');
        expect(state.templateUrl).toEqual('html/settings/settings.html');
    });

    it('should change state to profile', function () {

        $state.go(profileState);
        $rootScope.$digest();
        expect($state.current.name).toEqual(profileState);
    });

    it('should change data when settings.profile state', function () {

        $state.go(profileState);
        $rootScope.$digest();
        expect($state.current.data).toEqual(jasmine.any(Object));
        expect($state.current.data.pageTitle).toEqual('nav.profile');
        expect($state.current.controller).toEqual('cnSettingsProfile');
        expect($state.current.templateUrl).toEqual('html/settings/profile.html');
    });
});
