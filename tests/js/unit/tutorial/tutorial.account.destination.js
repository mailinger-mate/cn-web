/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, $new, $scope, account, action, addPart, api_url, check,
    config, connection, context, core, css, destination, error, field, form,
    go, id, info, initAccount, length, not, notification, options, path,
    session, some, source, success, toContain, toEqual, toHaveBeenCalledWith,
    token, tutorial, type, unauthorized, user, value, view
*/

describe('Controller: cnTutorialAccountDestination', function () {
    'use strict';

    var $state, $rootScope, scope, $stateParams, cnTutorialAccountDestination;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var accountMock = {
        id: 1
    };
    var cfgMock = {
        css: {
            notification: ''
        },
        connection: {
            config: {
                destination: {'3': 'teset', '5': 'test1', '7': 'test2'}
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module(function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('test/test');
        });
        module('cn.account');
        module('cn.action');
        module('cn.helper');
        module('cn.tutorial.account.destination');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function (_$rootScope_, _$state_, $controller, _$stateParams_) {
            $stateParams = _$stateParams_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $state = _$state_;
            scope.initAccount = function () {
                return true;
            };
            scope.tutorial = {
                account: {
                    source: {
                        id: 3
                    },
                    id: 1,
                    destination: 23
                }
            };
            scope.view = {
                action: {}
            };
            scope.form = {
                info: {
                    context: ''
                },
                config: {
                    field: {
                        type: {
                            options: {}
                        }
                    }
                },
                action: {
                    success: ''
                }
            };
            cnTutorialAccountDestination = function () {
                return $controller('cnTutorialAccountDestination', {
                    $scope: scope
                });
            };
        });
        $stateParams.source = '';
    });

    it('function scope.form.action.success change state corectly', function () {
        spyOn($state, 'go');
        scope.tutorial.account.source = {id: 323};
        cnTutorialAccountDestination();
        $stateParams.source = 'sourceParam';
        scope.form.action.success({id: 34, some: 'data'});
        expect(scope.tutorial.account.destination).toEqual({id: 34, some: 'data'});
        expect($state.go).toHaveBeenCalledWith('tutorial.connection.source.add', {account: 'sourceParam', destination: 34});
        $stateParams.source = '';
        scope.tutorial.account.source = {id: 222};
        scope.$apply();
        scope.form.action.success({id: 34, some: 'data'});
        expect($state.go).toHaveBeenCalledWith('tutorial.connection.source.add', {account: 222, destination: 34});
    });

    it('type options is defined', function () {
        cnTutorialAccountDestination();
        expect(scope.form.config.field.type.options).toContain('3');
        expect(scope.form.config.field.type.options).toContain('5');
        expect(scope.form.config.field.type.options).toContain('7');
        expect(scope.form.config.field.type.options.length).toEqual(3);
    });

    it('tutorial.account.source state is changed', function () {
        spyOn($state, 'go');
        scope.tutorial.account.source = undefined;
        $stateParams.source = undefined;
        scope.$digest();
        cnTutorialAccountDestination();
        expect($state.go).toHaveBeenCalledWith('tutorial.account.source');
    });

    it('tutorial.account.source state is not changed', function () {
        spyOn($state, 'go');
        scope.tutorial.account.source = 'someData';
        $stateParams.source = 'someData';
        scope.$digest();
        cnTutorialAccountDestination();
        expect($state.go).not.toHaveBeenCalledWith('tutorial.account.source');
    });

    it('config scopes are declared', function () {
        cnTutorialAccountDestination();
        expect(scope.form.info.context).toEqual('tutorial.info.account.destination');
        expect(scope.view.action).toEqual({info: true, context: true});
    });
});