/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, $scope, account, action, addPart, api_url, check, config,
    connection, context, core, css, error, field, form, go, id, info,
    initAccount, length, not, notification, notify, options, path, session,
    source, success, toContain, toEqual, toHaveBeenCalled,
    toHaveBeenCalledWith, token, tutorial, type, unauthorized, user, value,
    view
*/

describe('Controller: tutorialAccountSource', function () {
    'use strict';

    var $state, $rootScope, $stateParams, scope, cnTutorialAccountSource;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var accountMock = {
        id: 1
    };
    var cfgMock = {
        css: {
            notification: ''
        },
        connection: {
            config: {
                source: {'4': 'teset', '6': 'test1', '8': 'test2'}
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module(function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('test/test');
        });
        module('cn.account');
        module('cn.action');
        module('cn.helper');
        module('cn.tutorial.account.source');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        module(function ($provide) {
            $provide.value('account', accountMock);
        });

        inject(function (_$rootScope_, _$state_, $controller, _$stateParams_) {
            $stateParams = _$stateParams_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $state = _$state_;
            scope.initAccount = function () {
                return true;
            };
            scope.tutorial = {
                account: {
                    id: 1,
                    source: 'source'
                }
            };
            scope.view = {
                action: {}
            };
            scope.form = {
                info: {
                    context: ''
                },
                config: {
                    field: {
                        type: {
                            options: {}
                        }
                    }
                },
                action: {
                    success: ''
                }
            };
            cnTutorialAccountSource = function () {
                return $controller('cnTutorialAccountSource', {
                    $scope: scope
                });
            };
        });
    });

    it('scope.form.action.success works correctly', function () {
        spyOn($state, 'go');
        cnTutorialAccountSource();
        var data = {id: 25};
        scope.form.action.success(data);
        expect($state.go).toHaveBeenCalledWith('tutorial.account.destination', {source: 25});
        expect(scope.tutorial.account.source).toEqual(data);
    });

    it('type options is defined', function () {
        cnTutorialAccountSource();
        expect(scope.form.config.field.type.options).toContain('4');
        expect(scope.form.config.field.type.options).toContain('6');
        expect(scope.form.config.field.type.options).toContain('8');
        expect(scope.form.config.field.type.options.length).toEqual(3);
    });

    it('stateParams.account does not exist', function () {
        spyOn($state, 'go');
        cnTutorialAccountSource();
        expect($state.go).not.toHaveBeenCalled();
    });

    it('stateParams.account exist (change state)', function () {
        spyOn($state, 'go');
        cnTutorialAccountSource();
        expect($state.go).not.toHaveBeenCalled();
        $stateParams.account = 'mockedAccountParam';
        scope.$digest();
        cnTutorialAccountSource();
        expect($state.go).toHaveBeenCalledWith("tutorial.account.destination", {
            source: $stateParams.account
        });
    });

    it('config scopes are declared', function () {
        spyOn($state, 'go');
        cnTutorialAccountSource();
        expect(scope.form.info.context).toEqual('tutorial.info.account.source');
        expect(scope.view.action).toEqual({info: true, context: true});
    });

});
