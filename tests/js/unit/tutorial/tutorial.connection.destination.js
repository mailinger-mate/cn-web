/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $digest, $new, $scope, account, action, and, api_url, check, config,
    connection, context, core, css, destination, error, field, form, go, id,
    info, initAccounts, interval, not, notification, options, path,
    returnValue, session, some, source, success, toBeDefined, toHaveBeenCalled,
    toHaveBeenCalledWith, token, tutorial, type, unauthorized, user, value,
    view
*/

describe('Controller: cnTutorialConnectionDestination', function () {
    'use strict';

    var $state, $rootScope, cfg, account, $stateParams, scope, cnTutorialConnectionDestination, helper;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var accountMock = {
        id: 2,
        type: 'mockedType',
        config: {
            some: 'data'
        }
    };
    var connectionMock = {
        id: 1,
        account: 223,
        interval: 443
    };
    var cfgMock = {
        css: {
            notification: ''
        },
        connection: {
            config: {
                destination: [{

                }]
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.helper');
        module('cn.tutorial.connection.destination');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        module(function ($provide) {
            $provide.value('account', accountMock);
        });
        module(function ($provide) {
            $provide.value('connection', connectionMock);
        });

        inject(function (_$rootScope_, _$state_, $controller, _helper_, _$stateParams_, _account_, _cfg_) {
            account = _account_;
            helper = _helper_;
            cfg = _cfg_;
            $stateParams = _$stateParams_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $state = _$state_;
            scope.initAccounts = function () {
                return true;
            };
            scope.tutorial = {
                account: {
                    source: {
                        id: 5
                    },
                    id: 1,
                    destination: 55
                }
            };
            scope.view = {
                action: {}
            };
            scope.form = {
                info: {
                    context: ''
                },
                config: {
                    field: {
                        type: {
                            options: ''
                        }
                    }
                },
                action: {
                    success: ''
                }
            };
            cnTutorialConnectionDestination = function () {
                return $controller('cnTutorialConnectionDestination', {
                    $scope: scope
                });
            };
        });
    });

    it('cnTutorialConnectionDestination controller is defined', function () {
        expect(cnTutorialConnectionDestination).toBeDefined();
    });

    it('scope.form.action.success function is defined and works correctly', function () {
        spyOn($state, 'go');
        spyOn(scope.form.action, 'success');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        cnTutorialConnectionDestination();
        expect(scope.form.action.success).toBeDefined();
        scope.form.action.success();
        expect($state.go).toHaveBeenCalledWith('tutorial.end');
    });

    it('initAccounts fired when controller run', function () {
        spyOn($state, 'go');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        spyOn(scope, 'initAccounts');
        cnTutorialConnectionDestination();
        expect(scope.initAccounts).toHaveBeenCalled();
    });

    it('change state if account,connection, destination is not defined', function () {
        spyOn($state, 'go');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        expect($state.go).not.toHaveBeenCalled();
        $stateParams.account = '';
        $stateParams.connection = '';
        cfg.connection.config.destination[account.type] = '';
        scope.$digest();
        cnTutorialConnectionDestination();
        expect($state.go).toHaveBeenCalledWith('tutorial.start');
        $stateParams.connection = '';
        scope.$digest();
    });
});