/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $new, $scope, account, action, and, any, api_url, capitalizeString,
    check, config, connection, context, core, css, destination, error, field,
    form, go, id, info, initAccounts, interval, not, notification, options,
    path, returnValue, session, source, success, toBeDefined, toEqual,
    toHaveBeenCalledWith, token, tutorial, type, unauthorized, user, value,
    view
*/


describe('Controller: cnTutorialConnectionSource', function () {
    'use strict';

    var $state, $rootScope, account, connection, $stateParams, scope, cnTutorialConnectionSource, helper, cfg;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var accountMock = {
        id: 2,
        type: 3,
        config: 4
    };
    var connectionMock = {
        id: 1,
        account: 5,
        interval: 6
    };
    var cfgMock = {
        css: {
            notification: ''
        },
        connection: {
            config: {
                destination: 7,
                source: {
                    '3': 'test'
                }
            }
        },
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module('cn.helper');
        module('cn.tutorial.connection.source');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });
        module(function ($provide) {
            $provide.value('account', accountMock);
        });
        module(function ($provide) {
            $provide.value('connection', connectionMock);
        });

        inject(function (_$rootScope_, _$state_, $controller, _helper_, _$stateParams_, _account_, _connection_, _cfg_) {
            connection = _connection_;
            cfg = _cfg_;
            account = _account_;
            helper = _helper_;
            $stateParams = _$stateParams_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $state = _$state_;
            scope.initAccounts = function () {
                return true;
            };
            scope.tutorial = {
                account: {
                    source: {
                        id: 66
                    },
                    id: 1,
                    destination: {
                        id: 2
                    }
                }
            };
            scope.view = {
                action: {}
            };
            scope.form = {
                info: {
                    context: ''
                },
                config: {
                    field: {
                        type: {
                            options: 44
                        }
                    }
                },
                action: {
                    success: ''
                }
            };
            cnTutorialConnectionSource = function () {
                return $controller('cnTutorialConnectionSource', {
                    $scope: scope
                });
            };
        });
    });

    it('cnTutorialConnectionSource controller is defined', function () {
        expect(cnTutorialConnectionSource).toBeDefined();
    });

    it('config scopes are declared', function () {
        expect(scope.form.info.context).toBeDefined();
        expect(scope.view.action).toBeDefined();
    });

    it('scope.form.action.success function is defined and works correctly', function () {
        spyOn($state, 'go');
        spyOn(scope.form.action, 'success');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        cnTutorialConnectionSource();
        scope.form.action.success(connection);
        $stateParams.destination = 'stateParamDestinationMock';
        scope.$apply();
        expect($state.go).toHaveBeenCalledWith('tutorial.connection.destination.add', {connection: connection.id, account: scope.tutorial.account.destination.id});
    });

    it('initAccounts fired when controller run', function () {
        spyOn($state, 'go');
        spyOn(scope.form.action, 'success');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        spyOn(scope, 'initAccounts');
        cnTutorialConnectionSource();
        expect(scope.initAccounts).toHaveBeenCalledWith([account]);
    });

    it('change state to tutorial.account.destination', function () {
        spyOn($state, 'go');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        expect($state.go).not.toHaveBeenCalledWith('tutorial.account.destination');
        scope.tutorial.account.destination = undefined;
        $stateParams.destination = undefined;
        scope.$apply();
        cnTutorialConnectionSource();
        expect($state.go).toHaveBeenCalledWith('tutorial.account.destination');
    });

    it('stateParams.account exist', function () {
        spyOn($state, 'go');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        $stateParams.account = 'stateParamMock';
        cfg.connection.config.source[account.type] = undefined;
        scope.$apply();
        cnTutorialConnectionSource();
        expect($state.go).toHaveBeenCalledWith('tutorial.account.source');
    });

    it('cfg.connection.config.source[account.type] exist', function () {
        spyOn($state, 'go');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        $stateParams.account = undefined;
        cfg.connection.config.source[account.type] = 'someMock';
        scope.$apply();
        cnTutorialConnectionSource();
        expect($state.go).toHaveBeenCalledWith('tutorial.account.source');
    });

    it('stateParams.account and cfg.connection.source doesnt exist', function () {
        spyOn($state, 'go');
        spyOn(helper, 'capitalizeString').and.returnValue('');
        $stateParams.account = undefined;
        cfg.connection.config.source[account.type] = undefined;
        scope.$apply();
        cnTutorialConnectionSource();
        expect($state.go).toHaveBeenCalledWith('tutorial.account.source');
    });
});