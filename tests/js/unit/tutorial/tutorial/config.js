/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $digest, abstract, account, accounts, addPart, api_url, check,
    connection, controller, core, current, data, error, form, get, go, id,
    name, pageTitle, path, put, redirect, resolve, respond, session, template,
    templateUrl, toBe, toBeDefined, toBeFalsy, toBeTruthy, toEqual,
    toHaveBeenCalledWith, token, unauthorized, url, user, value, views,
    whenGET
*/

describe('config: tutorial', function () {
    'use strict';

    var $state, $rootScope, $httpBackend, action;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var cfgMock = {
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {

        module('ui.router');
        module('pascalprecht.translate');
        module(function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('tutorial/tutorial');
        });
        module('cn.tutorial');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });

        inject(function (_$rootScope_, _$state_, _$httpBackend_, $templateCache, _action_) {
            action = _action_;
            $httpBackend = _$httpBackend_;
            $rootScope = _$rootScope_;
            $state = _$state_;
            $templateCache.put('html/tutorial/tutorial.html', '');
            $templateCache.put('html/tutorial/start.html', '');
            $templateCache.put('html/account/item.form.html', '');
            $templateCache.put('html/tutorial/end.html', '');
        });
        $httpBackend.whenGET('/health_check/e909fcd76cc25bb2a4bb19f17ea466e4').respond(200);
        $httpBackend.whenGET('api_url/accounts/1').respond(200);
    });

    it("state tutorial", function () {

        $state.go('tutorial');
        $rootScope.$digest();
        expect($state.current.redirect).toEqual('tutorial.start');
        expect($state.current.templateUrl).toEqual('html/tutorial/tutorial.html');
        expect($state.current.url).toEqual('/tutorial');
        expect($state.current.controller).toEqual('cnTutorial');
        expect($state.current.name).toEqual('tutorial');
    });

    it("state tutorial.start", function () {

        $state.go('tutorial.start');
        $rootScope.$digest();
        expect($state.current.data).toEqual({pageTitle: 'tutorial.nav.start'});
        expect($state.current.name).toEqual('tutorial.start');
        expect($state.current.templateUrl).toEqual('html/tutorial/start.html');
        expect($state.current.url).toEqual('/start');
        expect($state.current.controller).toEqual('cnTutorialStart');
    });

    it('abstract state tutorial.account', function () {

        var state_name = 'tutorial.account';
        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.abstract).toBeTruthy();
        expect(state.url).toBe('/account');
        expect(state.controller).toBe('cnAccountItem');
        expect(state.template).toEqual('<div ui-view autoscroll="false"></div>');
    });

    it("state tutorial.account.source", function () {

        $state.go('tutorial.account.source');
        $rootScope.$digest();
        expect($state.current.data).toEqual({pageTitle: 'tutorial.nav.account.source'});
        expect($state.current.name).toEqual('tutorial.account.source');
        expect($state.current.templateUrl).toEqual('html/account/item.form.html');
        expect($state.current.url).toEqual('/source/:type?notify&account');
        expect($state.current.data).toEqual({pageTitle: 'tutorial.nav.account.source'});
        expect($state.current.controller).toEqual('cnTutorialAccountSource');
    });

    it("state tutorial.account.destination", function () {

        $state.go('tutorial.account.destination');
        $rootScope.$digest();
        expect($state.current.data).toEqual({pageTitle: 'tutorial.nav.account.destination'});
        expect($state.current.name).toEqual('tutorial.account.destination');
        expect($state.current.templateUrl).toEqual('html/account/item.form.html');
        expect($state.current.url).toEqual('/destination/:type?source&notify&account');
        expect($state.current.data).toEqual({pageTitle: 'tutorial.nav.account.destination'});
        expect($state.current.controller).toEqual('cnTutorialAccountDestination');
    });

    it('abstract state tutorial.connection', function () {

        var state_name = 'tutorial.connection';
        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.abstract).toBeTruthy();
        expect(state.resolve.accounts()).toBeFalsy();
        expect(state.url).toEqual('/connection');
        expect(state.controller).toEqual('cnConnectionItem');
        expect(state.template).toEqual('<div ui-view autoscroll="false"></div>');
    });

    it('abstract state tutorial.connection.source', function () {

        var state_name = 'tutorial.connection.source';
        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.abstract).toBeTruthy();
        expect(state.data).toEqual({pageTitle: 'tutorial.nav.connection.source.add'});
        expect(state.url).toEqual('/source');
        expect(state.controller).toEqual('cnSourceItem');
        expect(state.templateUrl).toEqual('html/connection/item.source.html');
    });

    it('abstract state tutorial.connection.source.add', function () {
        spyOn(action.account, 'get');
        var state_name = 'tutorial.connection.source.add';
        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.data).toEqual({pageTitle: 'tutorial.nav.connection.source.add'});
        expect(state.views.form.resolve).toBeDefined();
        expect(state.url).toEqual('/:account?destination');
        expect(state.views.form.resolve.connection()).toBeFalsy();
        state.views.form.resolve.account({account: 1}, action);
        $rootScope.$apply();
        expect(action.account.get).toHaveBeenCalledWith({id: 1});
    });

    it('abstract state tutorial.connection.destination', function () {

        var state_name = 'tutorial.connection.destination';
        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.data).toEqual({pageTitle: 'tutorial.nav.connection.destination.add'});
        expect(state.abstract).toBeTruthy();
        expect(state.url).toBe('/:connection/destination');
        expect(state.controller).toBe('cnDestinationItem');
        expect(state.templateUrl).toEqual('html/connection/item.destination.html');
    });

    it("state tutorial.end", function () {

        $state.go('tutorial.end');
        $rootScope.$apply();
        expect($state.current.name).toEqual('tutorial.end');
        expect($state.current.url).toEqual('/end');
        expect($state.current.templateUrl).toEqual('html/tutorial/end.html');
        expect($state.current.controller).toEqual('cnTutorialEnd');
        expect($state.current.data).toEqual({pageTitle: 'tutorial.nav.end'});
    });

    it('abstract state tutorial.connection.destination.add', function () {
        spyOn(action.account, 'get');
        spyOn(action.connection, 'get');
        var state_name = 'tutorial.connection.destination.add';
        var state = $state.get(state_name);
        expect(state.name).toBe(state_name);
        expect(state.data).toEqual({pageTitle: 'tutorial.nav.connection.destination.add'});
        expect(state.url).toEqual('/:account');
        state.views.form.resolve.connection({connection: 1}, action);
        state.views.form.resolve.account({account: 2}, action);
        $rootScope.$apply();
        expect(action.connection.get).toHaveBeenCalledWith({id: 1});
        expect(action.account.get).toHaveBeenCalledWith({id: 2});
    });
});