/*global spyOn afterEach jasmine angular describe module beforeEach it expect inject*/
/*property
    $apply, $new, $scope, account, addPart, api_url, button, check, class,
    click, core, createSpy, current, end, error, go, indexOf, label, level,
    name, nav, not, notify, objectContaining, path, prefix, replace, session,
    sref, toBeDefined, toEqual, toHaveBeenCalled, toHaveBeenCalledWith, token,
    tutorial, type, unauthorized, user, value, push
*/

describe('Controllers: tutorial', function () {
    'use strict';

    var $state, $rootScope, scope, createCnTutorialStart, cnTutorialEnd, cnTutorial, notification;
    var token = 'e909fcd76cc25bb2a4bb19f17ea466e4';
    var cfgMock = {
        core: {
            api_url: 'api_url/'
        },
        user: {
            core: {
                token: token
            }
        },
        session: {
            check: 60,
            error: {
                unauthorized: {
                    path: "/signin?error=unauthorized&redirect="
                }
            }
        }
    };
    beforeEach(function () {
        module('ui.router');
        module('pascalprecht.translate');
        module(function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('tutorial/tutorial');
        });
        module('cn.tutorial');
        module(function ($provide) {
            $provide.value('cfg', cfgMock);
        });

        inject(function (_$rootScope_, _$state_, $controller, _notification_) {
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            $state = _$state_;
            notification = _notification_;
            createCnTutorialStart = function () {
                return $controller('cnTutorialStart', {
                    $scope: scope
                });
            };
            cnTutorialEnd = function () {
                return $controller('cnTutorialEnd', {
                    $scope: scope
                });
            };

            cnTutorial = function () {
                return $controller('cnTutorial', {
                    $scope: scope
                });
            };
        });
    });

    describe('cnTutorialStart controller', function () {

        it("cnTutorialStart controller is defined", function () {
            expect(createCnTutorialStart).toBeDefined();
        });

        it("scope button is defined", function () {

            createCnTutorialStart();
            $rootScope.$apply();
            expect(scope.button).toEqual(jasmine.objectContaining([{
                label: "continue",
                type: "link",
                sref: "tutorial.account.source",
                class: "btn-primary"
            }]));
        });
    });
    describe('cnTutorialEnd controller', function () {

        it('cnTutorialEnd controller is defined', function () {
            expect(cnTutorialEnd).toBeDefined();
        });

        it("scope button is defined", function () {
            cnTutorialEnd();
            expect(scope.button).toEqual(jasmine.objectContaining([{
                label: "finish",
                click: "end()",
                class: "btn-primary"
            }]));
        });

        it('scope.end function works correctly', function () {
            spyOn($state, 'go');
            spyOn(notification, 'push');
            cnTutorialEnd();
            expect($state.go).not.toHaveBeenCalled();
            expect(notification.push).not.toHaveBeenCalled();
            scope.end();
            $rootScope.$apply();
            expect($state.go).toHaveBeenCalled();
            expect(notification.push).toHaveBeenCalledWith({
                prefix: "tutorial",
                name: "finish",
                level: "success"
            });
        });
    });

    describe('cnTutorial controller', function () {
        it('cnTutorial controller is defined', function () {
            expect(cnTutorial).toBeDefined();

        });
        it('scope.tutorial is defined', function () {
            jasmine.createSpy(cnTutorial, 'current');
            cnTutorial();
            $rootScope.$apply();
            scope.tutorial.current();
            var returnData = scope.tutorial.nav.indexOf($state.current.name.replace("tutorial.", ""));
            expect(scope.tutorial.current()).toEqual(returnData);
            expect(scope.tutorial.account).toEqual({});
            expect(scope.tutorial.nav).toBeDefined();
        });
    });
});
