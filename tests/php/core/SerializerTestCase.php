<?php

namespace Connect\Test\Core;

require_once __DIR__ . '/../../../app/AppKernel.php';

use AppKernel;
use JMS\Serializer\Exception\LogicException;
use JMS\Serializer\SerializerInterface;
use PHPUnit_Framework_TestCase;

/**
 * Class SerializerTestCase
 */
class SerializerTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * @var AppKernel
     */
    private $kernel;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $this->kernel = new AppKernel('test', true);
        $this->kernel->boot();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        $this->kernel->shutdown();

        parent::tearDown();
    }

    /**
     * @param $serviceName
     * @return SerializerInterface
     */
    protected function getSerializer($serviceName)
    {
        $serializer = $this->kernel->getContainer()->get($serviceName);
        if (!$serializer instanceof SerializerInterface) {
            throw new LogicException('Service is not an instance of SerializerInterface');
        }

        return $serializer;
    }
}