<?php

namespace Connect\Test\Core;

use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Contact;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as SymfonyWebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class WebTestCase
 */
class WebTestCase extends SymfonyWebTestCase
{
    /**
     * @var Client
     */
    private $testHttpClient;

    /**
     * @var string
     */
    private $csrfToken;

    /**
     * @var MockHandler[]
     */
    private $guzzleMockHandlers = [];

    /**
     * @var array
     */
    private $guzzleHistoryContainers = [];

    protected function setUp()
    {
        parent::setUp();

        $httpClientsToMock = [
            'ums'       => 'ums.client',
            'core'      => 'core.client',
            'sender'    => 'sender.http.client',
        ];

        foreach ($httpClientsToMock as $name => $service) {
            /** @var HandlerStack $handlerStack */
            $handlerStack = $this->getTestHttpClient()->getContainer()->get($service)->getConfig('handler');
            $handlerStack->push(Middleware::history($this->getGuzzleHistory($name)));
            $handlerStack->setHandler($this->getGuzzleMockHandler($name));
        }
    }

    /**
     * @param $clientName
     * @return array
     */
    protected function &getGuzzleHistory($clientName)
    {
        if (!isset($this->guzzleHistoryContainers[$clientName])) {
            $this->guzzleHistoryContainers[$clientName] = [];
        }

        return $this->guzzleHistoryContainers[$clientName];
    }

    /**
     * @param string $clientName
     * @param int $index
     * @return Request
     */
    protected function getGuzzleHistoryRequest($clientName, $index)
    {
        $history = $this->getGuzzleHistory($clientName);

        $this->assertArrayHasKey($index, $history, "Index $index does not exist in $clientName guzzle history");

        return $history[$index]['request'];
    }

    /**
     * @return Client
     */
    protected function getTestHttpClient()
    {
        if (is_null($this->testHttpClient)) {
            $this->testHttpClient = static::createClient();
        }

        return $this->testHttpClient;
    }

    /**
     * @param $intention
     * @return string
     */
    protected function getCsrfToken($intention)
    {
        if (is_null($this->csrfToken)) {
            return $this->csrfToken = $this
                ->getTestHttpClient()
                ->getContainer()
                ->get('form.csrf_provider')
                ->generateCsrfToken($intention);
        };

        return $this->csrfToken;
    }

    /**
     * @param $clientName
     * @param $statusCode
     * @param string $body
     * @param array $headers
     */
    protected function addMockedResponse($clientName, $statusCode, $body = null, array $headers = [])
    {
        $this
            ->getGuzzleMockHandler($clientName)
            ->append(
                new Response(
                    $statusCode,
                    $headers,
                    $body
                )
            );
    }

    /**
     * @param string $clientName
     * @param ClientException $exception
     */
    protected function addMockedException($clientName, ClientException $exception)
    {
        $this
            ->getGuzzleMockHandler($clientName)
            ->append($exception);
    }

    /**
     * @param string $role
     * @param bool $setUMSSession
     */
    protected function createSession($role = 'user', $setUMSSession = true)
    {
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "' . $role . '",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $user = new UMSUser();
        $contact = (new Contact())
            ->setUser($user)
            ->setValue('john@xf.com')
            ->setType(Contact\Type::EMAIL)
            ->setVerified(true);

        $user->addContact($contact);

        $client = $this->getTestHttpClient();
        $session = $client->getContainer()->get('session');

        $firewall = 'connect';
        $token = new UsernamePasswordToken(
            $user,
            null,
            $firewall,
            ['ROLE_' . strtoupper($role)]
        );

        $client->getContainer()->get('security.token_storage')->setToken($token);

        $session->set('_security_' . $firewall, serialize($token));
        if ($setUMSSession) {
            $session->set('ums_session', 'c41843ba204f057534b358bf838a954c');
        }
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }

    /**
     * @param string $clientName
     * @return MockHandler
     */
    private function getGuzzleMockHandler($clientName)
    {
        if (!isset($this->guzzleMockHandlers[$clientName])) {
            $this->guzzleMockHandlers[$clientName] = new MockHandler();
        }

        return $this->guzzleMockHandlers[$clientName];
    }

    /**
     * @param $expectedMethod
     * @param $expectedPath
     * @param $expectedQuery
     * @param $expectedBody
     * @param RequestInterface $request
     */
    protected function assertRequest(
        $expectedMethod,
        $expectedPath,
        $expectedQuery,
        $expectedBody,
        RequestInterface $request
    ) {
        $this->assertSame($expectedMethod, $request->getMethod());
        $this->assertSame($expectedPath, $request->getUri()->getPath());
        $this->assertSame($expectedQuery, $request->getUri()->getQuery());
        $this->assertSame($expectedBody, $request->getBody()->getContents());
    }
}
