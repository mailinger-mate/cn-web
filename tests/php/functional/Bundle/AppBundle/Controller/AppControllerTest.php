<?php

namespace Connect\Tests\Functional\Bundle\AppBundle\Controller;

use Connect\Test\Core\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AppControllerTest
 */
class AppControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function showAction_with_embedded_valid_js_config()
    {
        $this->createSession();

        $script = $this->getTestHttpClient()
            ->request('GET', '/app')
            ->filterXPath("//script[contains(., 'function (cfgProvider)')]")
            ->html();

        $this->assertSame(1, preg_match('/cfgProvider.set\((.+)\);/', $script, $matches));

        $mockFeedbackToken = 'some_feedback_token';
        $mockStripeToken = 'some_stripe_token';

        $expectedJsConfig = [
            'user'  =>  [
                'id'        =>  1,
                'name'      =>  'John Doe',
                'role'      =>  'user',
                'vendor'    =>  'X-F',
                'email'     =>  [
                    'id'       => 1,
                    'address'  => 'john@xf.com',
                    'verified' => true
                ],
                'core'  =>  [
                    'token'     =>  'c41843ba204f057534b358bf838a954c'
                ],
                'feedback'  =>  [
                    'token'     => $mockFeedbackToken
                ]
            ],
            'core'  =>  [
                'api_url'   => 'http://core.dev:8082/rest/v1/',
                'rpc_url'   => 'http://core.dev:8082/rpc/v1/'
            ],
            'feedback'  =>  [
                'host'      =>  'xformation.userecho.com',
                'forum'     =>  36372,
                'category'  =>  20532
            ],
            'stripe' => [
                'publishable_key' => $mockStripeToken
            ]
        ];

        $decodedResponse = json_decode($matches[1], true);

        $this->assertFalse(empty($decodedResponse['user']['feedback']['token']));
        $this->assertTrue(is_string($decodedResponse['user']['feedback']['token']));
        $decodedResponse['user']['feedback']['token'] = $mockFeedbackToken;

        $this->assertFalse(empty($decodedResponse['stripe']['publishable_key']));
        $this->assertTrue(is_string($decodedResponse['stripe']['publishable_key']));
        $decodedResponse['stripe']['publishable_key'] = $mockStripeToken;

        $this->assertSame($expectedJsConfig, $decodedResponse);
    }

    /**
     * @test
     */
    public function showAction_without_authenticated_user_should_return_homepage()
    {
        $client = $this->getTestHttpClient();
        $client->request('GET', '/');

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertContains('body id="home"', $response->getContent());
        $this->assertContains('register-button', $response->getContent());

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function showAction_with_authenticated_user_should_redirect_to_app()
    {
        $this->createSession();

        $client = $this->getTestHttpClient();
        $client->request('GET', '/');

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertSame('/app', $response->headers->get('location'));

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function showAction_without_supported_user_role_should_return_access_denied_message()
    {
        $this->createSession('some_role');

        $client = $this->getTestHttpClient();
        $client->request('GET', '/app');

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_FORBIDDEN, $response->getStatusCode());

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }
}
