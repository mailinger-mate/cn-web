<?php

namespace Connect\Tests\Functional\Bundle\AppBundle\Controller;

use Connect\Test\Core\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BillingControllerTest
 */
class BillingControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function getInvoiceAction_with_valid_data_should_return_invoice_pdf()
    {
        $this->createSession('admin');

        $pdfContent = 'some_pdf_content';
        $pdfName = 'some_pdf_filename.pdf';

        // core::billings::get()
        $this->addMockedResponse(
            'core',
            Response::HTTP_OK,
            $pdfContent,
            ['Content-Disposition' => "attachment; filename=$pdfName"]
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/billing/invoice/1',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/pdf']
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertSame($pdfContent, $response->getContent());
        $this->assertSame('public', $response->headers->get('cache-control'));
        $this->assertSame('application/pdf', $response->headers->get('content-type'));
        $this->assertSame("inline; filename=\"$pdfName\"", $response->headers->get('content-disposition'));
        $this->assertSame(16, $response->headers->get('content-length'));

        $this->assertCount(1, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function getInvoiceAction_with_user_role_should_return_access_denied()
    {
        $this->createSession();

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/billing/invoice/1',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/pdf']
        );

        $this->assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     * @dataProvider coreResponseProvider
     */
    public function getInvoiceAction_with_error_core_response_role_should_return_404($coreResponseStatus)
    {
        $this->createSession('admin');

        // core::billings::get()
        $this->addMockedResponse('core', $coreResponseStatus);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/billing/invoice/1',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/pdf']
        );

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $this->getGuzzleHistory('core'));
    }

    public static function coreResponseProvider()
    {
        return [
            [Response::HTTP_UNAUTHORIZED],
            [Response::HTTP_NOT_FOUND],
        ];
    }
}