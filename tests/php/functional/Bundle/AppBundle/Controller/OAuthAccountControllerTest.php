<?php

namespace Connect\Tests\Functional\Bundle\AppBundle\Controller;

use Abraham\TwitterOAuth\TwitterOAuth;
use Connect\Domain\Account;
use Connect\Test\Core\WebTestCase;
use Mockery;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OAuthAccountControllerTest
 */
class OAuthAccountControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function ajaxAuthorizeAction_with_valid_data_and_without_account_id_nor_return_url_should_create_account_and_return_auth_url()
    {
        $this->createSession();

        $requestTokens = [
            'oauth_token'        => 'some_oauth_token',
            'oauth_token_secret' => 'some_oauth_secret'
        ];

        $twitter = Mockery::mock(TwitterOAuth::class)
            ->shouldReceive('oauth')
            ->andReturn($requestTokens)
            ->getMock()

            ->shouldReceive('url')
            ->andReturn('some_auth_url')
            ->getMock();

        self::$kernel
            ->getContainer()
            ->set('oauth_app.twitter.connection', $twitter);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/ajaxAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name":"test"}'
        );

        $this->assertJsonStringEqualsJsonString(
            '{"success":true,"auth_url":"some_auth_url"}',
            $client->getResponse()->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));

        $session = $client->getRequest()->getSession();

        /** @var Account $account */
        $account = $session->get('app_oauth_account');

        $this->assertInstanceOf(Account::class, $account);

        $this->assertNull($account->getId());
        $this->assertSame('test', $account->getName());
        $this->assertSame($requestTokens['oauth_token'], $account->getConfig('request_token'));
        $this->assertSame($requestTokens['oauth_token_secret'], $account->getConfig('request_token_secret'));

        $this->assertSame('account/list', $session->get('app_return_url'));
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_with_valid_data_and_account_id_and_return_url_should_find_account_and_return_auth_url()
    {
        $this->createSession();

        $requestTokens = [
            'oauth_token'        => 'some_oauth_token',
            'oauth_token_secret' => 'some_oauth_secret'
        ];

        $twitter = Mockery::mock(TwitterOAuth::class)
            ->shouldReceive('oauth')
            ->andReturn($requestTokens)
            ->getMock()

            ->shouldReceive('url')
            ->andReturn('some_auth_url')
            ->getMock();

        self::$kernel
            ->getContainer()
            ->set('oauth_app.twitter.connection', $twitter);

        // core::accounts::find()
        $this->addMockedResponse(
            'core',
            Response::HTTP_OK,
            '{
              "id": 5,
              "name": "some_account",
              "type": "twitter",
              "config": {
                "access_token": "old_token",
                "access_token_secret": "old_token_secret"
              }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/5/ajaxReAuthorize?return_url=some_url',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name":"test"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":true,"auth_url":"some_auth_url"}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));

        $session = $client->getRequest()->getSession();

        /** @var Account $account */
        $account = $session->get('app_oauth_account');

        $this->assertInstanceOf(Account::class, $account);

        $this->assertSame(5, $account->getId());
        $this->assertSame('test', $account->getName());
        $this->assertSame($requestTokens['oauth_token'], $account->getConfig('request_token'));
        $this->assertSame($requestTokens['oauth_token_secret'], $account->getConfig('request_token_secret'));

        $this->assertSame('some_url', $session->get('app_return_url'));
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     *
     * @param $requestData
     * @param $expectedError
     */
    public function ajaxAuthorizeAction_with_invalid_data_should_return_error_message(
        $requestData,
        $expectedError
    ) {
        $this->createSession();
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/ajaxAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            sprintf('{"success":false,%s}', $expectedError),
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @return array
     */
    public static function invalidDataProvider()
    {
        return [
            // 0)
            // extra fields
            [
                '{"name":"test", "some-random": "field"}',
                '"message": ["This form should not contain extra fields."]'
            ],
            // 1)
            // name: empty
            [
                '{"name": ""}',
                '"message": {"name":["This value should not be blank."]}'
            ],
            // 2)
            // name: too long
            [
                sprintf('{"name": "%s"}', str_repeat('x', 256)),
                '"message": {"name":["This value is too long. It should have 255' .
                ' characters or less."]}'
            ],
        ];
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_with_invalid_application_should_return_404()
    {
        $this->createSession();
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/some_app/ajaxAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name":"test"}'
        );

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_with_invalid_json_should_return_unknown_error_message()
    {
        $this->createSession();
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/ajaxAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            "this is not json"
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Unknown error occurred."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_with_types_mismatch_should_return_unknown_error_message()
    {
        $this->createSession();

        // core::accounts::find()
        $this->addMockedResponse(
            'core',
            Response::HTTP_OK,
            '{
              "id": 5,
              "name": "some_account",
              "type": "some_type",
              "config": {
                "access_token": "old_token",
                "access_token_secret": "old_token_secret"
              }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/5/ajaxReAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name":"test"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Unknown error occurred."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_with_missing_account_id_should_return_unknown_error_message()
    {
        $this->createSession();

        // core::accounts::find()
        $this->addMockedResponse(
            'core',
            Response::HTTP_NOT_FOUND
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/5/ajaxReAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name":"test"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Unknown error occurred."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/ajaxAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            "this is not json"
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Your session has expired. Please sign in again."}',
            $response->getContent()
        );

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAuthorizeAction_without_supported_user_role_should_return_access_denied_message()
    {
        $this->createSession('some_role');
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/ajaxAuthorize',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            "this is not json"
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Page not found."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     * @dataProvider callbackValidDataProvider
     *
     * @param $accountId
     * @param $expectedCoreMethod
     * @param $expectedLocation
     * @param $mockedCoreResponseCode
     * @param $mockedCoreResponseBody
     * @param $mockedAppReturnUrl
     */
    public function callbackAction_with_valid_data_should_replace_account_tokens_and_render_response(
        $accountId,
        $expectedCoreMethod,
        $expectedLocation,
        $mockedCoreResponseCode,
        $mockedCoreResponseBody,
        $mockedAppReturnUrl
    ) {
        $this->createSession();

        $requestTokens = [
            'oauth_token'        => 'some_oauth_token',
            'oauth_token_secret' => 'some_oauth_secret'
        ];

        $accessTokens = [
            'oauth_token'        => 'some_access_token',
            'oauth_token_secret' => 'some_access_secret'
        ];

        $twitter = Mockery::mock(TwitterOAuth::class)
            ->shouldReceive('setOauthToken')
            ->getMock()

            ->shouldReceive('oauth')
            ->andReturn($accessTokens)
            ->getMock();

        self::$kernel
            ->getContainer()
            ->set('oauth_app.twitter.connection', $twitter);

        // core::accounts::update()
        $this->addMockedResponse(
            'core',
            $mockedCoreResponseCode,
            $mockedCoreResponseBody
        );

        $account = (new Account())
            ->setId($accountId)
            ->setName('test')
            ->setType('twitter')
            ->setConfig([
                'request_token'          => $requestTokens['oauth_token'],
                'request_token_secret'   => $requestTokens['oauth_token_secret']
            ]);

        $client = $this->getTestHttpClient();

        $session = $client->getContainer()->get('session');
        $session->set('app_oauth_account', $account);
        if ($mockedAppReturnUrl !== null) {
            $session->set('app_return_url', $mockedAppReturnUrl);
        }

        $session->save();

        $client->request(
            'GET',
            '/oauth/twitter/callback',
            [
                'oauth_token'       => $requestTokens['oauth_token'],
                'oauth_verifier'    => 'some_oauth_verifier',
            ]
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertSame($expectedLocation, $response->headers->get('location'));

        $this->assertFalse($session->has('app_oauth_account'));
        $this->assertFalse($session->has('app_return_url'));

        $this->assertCount(1, $this->getGuzzleHistory('ums'));

        $this->assertCount(1, $this->getGuzzleHistory('core'));
        $this->assertSame($expectedCoreMethod, $this->getGuzzleHistoryRequest('core', 0)->getMethod());
    }

    public static function callbackValidDataProvider()
    {
        return [
            // 0)
            // account id: null
            // expected POST to create new account
            // no return url set
            [
                null,
                SymfonyRequest::METHOD_POST,
                '/app#/account/list?auth=success&account=10',
                Response::HTTP_OK,
                '{"id":10}',
                null
            ],

            // 1)
            // account id: null
            // expected POST to create new account
            // return url set
            [
                null,
                SymfonyRequest::METHOD_POST,
                '/app#/some_url_to_return/test?param=value&auth=success&account=10',
                Response::HTTP_OK,
                '{"id":10}',
                'some_url_to_return/test?param=value'
            ],

            // 2)
            // account id: 5
            // expected PATCH to update an account
            // no return url set
            [
                5,
                SymfonyRequest::METHOD_PATCH,
                '/app#/account/list?auth=success&account=5',
                Response::HTTP_NO_CONTENT,
                null,
                null
            ],

            // 3)
            // account id: 5
            // expected PATCH to update an account
            // return url set
            [
                5,
                SymfonyRequest::METHOD_PATCH,
                '/app#/some_url_to_return/test?param=value&auth=success&account=5',
                Response::HTTP_NO_CONTENT,
                null,
                'some_url_to_return/test?param=value'
            ],
        ];
    }

    /**
     * @test
     * @dataProvider callbackDeniedResponseDataProvider
     *
     * @param $accountId
     * @param $expectedLocation
     */
    public function callbackAction_with_denied_query_token_should_render_error_response($accountId, $expectedLocation)
    {
        $this->createSession();

        $requestTokens = [
            'oauth_token'        => 'some_oauth_token',
            'oauth_token_secret' => 'some_oauth_secret'
        ];

        $twitter = Mockery::mock(TwitterOAuth::class);

        self::$kernel
            ->getContainer()
            ->set('oauth_app.twitter.connection', $twitter);

        $account = (new Account())
            ->setId($accountId)
            ->setName('test')
            ->setType('twitter')
            ->setConfig([
                'access_token'          => $requestTokens['oauth_token'],
                'access_token_secret'   => $requestTokens['oauth_token_secret']
            ]);

        $client = $this->getTestHttpClient();

        $session = $client->getContainer()->get('session');
        $session->set('app_oauth_account', $account);
        $session->save();

        $client->request(
            'GET',
            '/oauth/twitter/callback',
            [
                'denied' => $requestTokens['oauth_token']
            ]
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertSame($expectedLocation, $response->headers->get('location'));

        $this->assertFalse($session->has('app_oauth_account'));

        $this->assertCount(1, $this->getGuzzleHistory('ums'));

        $coreHistory = $this->getGuzzleHistory('core');
        $this->assertCount(0, $coreHistory);
    }

    public static function callbackDeniedResponseDataProvider()
    {
        return [
            // 0)
            // account id: null
            // expected POST to create new account
            [
                null,
                '/app#/account/list?auth=error'
            ],

            // 1)
            // account id: 5
            // expected PATCH to update an account
            [
                5,
                '/app#/account/list?auth=error'
            ],
        ];
    }

    /**
     * @test
     */
    public function callbackAction_without_account_id_in_storage_should_return_500()
    {
        $this->createSession();

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/callback'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        $this->assertContains('Account was not saved in session', $response->getContent());

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function callbackAction_without_account_instance_in_storage_should_return_500()
    {
        $this->createSession();

        $client = $this->getTestHttpClient();

        $session = $client->getContainer()->get('session');
        $session->set('app_oauth_account', 'not account instance');
        $session->save();

        $client->request(
            'GET',
            '/oauth/twitter/callback'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        $this->assertContains('Account in session must be instance of Account', $response->getContent());

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function callbackAction_with_mismatched_types_should_return_500()
    {
        $this->createSession();

        $account = (new Account())
            ->setId(5)
            ->setName('test')
            ->setType('some_type')
            ->setConfig([
                'access_token'          => 'some_token',
                'access_token_secret'   => 'some_secret',
            ]);

        $client = $this->getTestHttpClient();

        $session = $client->getContainer()->get('session');
        $session->set('app_oauth_account', $account);
        $session->save();

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/callback'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        $this->assertContains('Account and app types mismatch', $response->getContent());

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function callbackAction_without_authenticated_user_should_redirect_to_login_page()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/callback'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertContains('Redirecting to http://localhost/signin', $response->getContent());

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }

    /**
     * @test
     */
    public function callbackAction_without_supported_user_role_should_return_access_denied_message()
    {
        $this->createSession('some_role');

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/oauth/twitter/callback'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_FORBIDDEN, $response->getStatusCode());

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
        $this->assertCount(0, $this->getGuzzleHistory('core'));
    }
}
