<?php

namespace Connect\Tests\Functional\Bundle\AppBundle\Controller;

use Connect\Test\Core\WebTestCase;
use Swift_Message;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Class UserControllerTest
 */
class UserControllerTest extends WebTestCase
{
    /**
     * @test
     * @dataProvider validUserEditDataProvider
     *
     * @param string $requestData
     * @param string $expectedUMSRequest
     */
    public function ajaxEditAction_with_valid_data_should_return_success($requestData, $expectedUMSRequest)
    {
        $this->createSession();

        // Facade::editUserProfile()
        $this->addMockedResponse('ums', HttpResponse::HTTP_NO_CONTENT);

        // UMSUserRepository:find()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
              "id": 1,
              "name": "John Doe",
              "login": "johndo",
              "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
              "salt": "b54109793ab030c94c40c5381891fd69",
              "role": "user",
              "activated": true,
              "enabled": false,
              "_links": {
                "self": {
                  "href": "http://localhost/rest/v1/users/1"
                },
                "vendor": {
                  "href": "http://localhost/rest/v1/vendors/1"
                }
              },
              "_embedded": {
                "vendor": {
                  "id": 1,
                  "name": "X-F",
                  "_links": {
                    "self": {
                      "href": "http://localhost/rest/v1/vendors/1"
                    }
                  }
                },
                "contacts": [
                  {
                    "value": "john@xf.com",
                    "type": "email",
                    "verified": true,
                    "id": 1,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/contacts/1"
                      }
                    },
                    "_embedded": {
                      "contact_actions": []
                    }
                  }
                ],
                "user_actions": []
              }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/user/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $this->assertJsonStringEqualsJsonString('{"success":true}', $client->getResponse()->getContent());

        $this->assertCount(3, $this->getGuzzleHistory('ums'));

        $this->assertRequest(
            'GET',
            '/profile/edit',
            'session_id=c41843ba204f057534b358bf838a954c&session_user=1',
            $expectedUMSRequest,
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @return array
     */
    public static function validUserEditDataProvider()
    {
        return [
            // 0)
            // empty request content
            [
                '{}',
                '[]',
            ],
            // 1)
            // new name
            [
                '{"name": "New name"}',
                '{"name":"New name"}',
            ],
            // 2)
            // new password
            [
                '{
                  "current_password": "demo",
                  "password_first": "new-pass",
                  "password_second": "new-pass"
                }',
                '{"password":{"first":"new-pass","second":"new-pass"},"current_password":"demo"}',
            ],
            // 3)
            // new password & name
            [
                '{
                  "name": "New name",
                  "current_password": "demo",
                  "password_first": "new-pass",
                  "password_second": "new-pass"
                }',
                '{"name":"New name","password":{"first":"new-pass","second":"new-pass"},"current_password":"demo"}',
            ],
        ];
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_valid_data_to_change_email_should_return_success()
    {
        $this->createSession();

        // Facade::editUserProfile()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK, '{"action_id": "9a66e06ded843e5848f14fccd6656d41"}');

        // UMSUserRepository:find()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
              "id": 1,
              "name": "John Doe",
              "login": "johndo",
              "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
              "salt": "b54109793ab030c94c40c5381891fd69",
              "role": "user",
              "activated": true,
              "enabled": false,
              "_links": {
                "self": {
                  "href": "http://localhost/rest/v1/users/1"
                },
                "vendor": {
                  "href": "http://localhost/rest/v1/vendors/1"
                }
              },
              "_embedded": {
                "vendor": {
                  "id": 1,
                  "name": "X-F",
                  "_links": {
                    "self": {
                      "href": "http://localhost/rest/v1/vendors/1"
                    }
                  }
                },
                "contacts": [
                  {
                    "value": "new_address@test.com",
                    "type": "email",
                    "verified": true,
                    "id": 1,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/contacts/1"
                      }
                    },
                    "_embedded": {
                      "contact_actions": []
                    }
                  }
                ],
                "user_actions": []
              }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'GET',
            '/user/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{
              "contact": "new_address@test.com",
              "current_password": "demo"
            }'
        );

        $this->assertJsonStringEqualsJsonString('{"success":true}', $client->getResponse()->getContent());

        $this->assertCount(3, $this->getGuzzleHistory('ums'));

        $this->assertRequest(
            Request::METHOD_GET,
            '/profile/edit',
            'session_id=c41843ba204f057534b358bf838a954c&session_user=1',
            '{"contact":"new_address@test.com","current_password":"demo"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message $message */
        $message = $mailCollector->getMessages()[0];
        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('noreply@x-formation.com', key($message->getFrom()));
        $this->assertSame('new_address@test.com', key($message->getTo()));
        $this->assertSame('Please verify your X-Formation Connect email address', $message->getSubject());
        $this->assertContains(
            'http://localhost/action/9a66e06ded843e5848f14fccd6656d41',
            $message->getBody()
        );
        $this->assertNotContains(
            'scheme_and_host',
            $message->getBody()
        );

        $this->assertSame(
            'new_address@test.com',
            $client->getContainer()->get('security.token_storage')->getToken()->getUser()->getPrimaryContact()->getValue()
        );
    }

    /**
     * @test
     * @dataProvider invalidUserEditDataProvider
     *
     * @param $requestData
     * @param $UMSResponseData
     * @param $expectedError
     */
    public function ajaxEditAction_with_invalid_data_should_return_error_message(
        $requestData,
        $UMSResponseData,
        $expectedError
    ) {
        $this->createSession();

        // Facade::editUserProfile()
        $this->addMockedResponse('ums', HttpResponse::HTTP_BAD_REQUEST, $UMSResponseData);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/user/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $this->assertJsonStringEqualsJsonString(
            sprintf('{"success":false,%s}', $expectedError),
            $client->getResponse()->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }

    /**
     * @return array
     */
    public static function invalidUserEditDataProvider()
    {
        return [
            // 0)
            // extra fields
            [
                '{"some-random": "field"}',
                '{"message": ["This form should not contain extra fields."]}',
                '"message": ["This form should not contain extra fields."]',
            ],
            // 1)
            // name: too long
            [
                sprintf('{"name": "%s"}', str_repeat('x', 256)),
                /** @lang JSON */
                '{
                  "message": {"name":["This value is too long. It should have 255 characters or less."]}
                }',
                '"message": {"name":["This value is too long. It should have 255' .
                ' characters or less."]}',
            ],
            // 2)
            // current_password: not matching
            [
                '{
                  "current_password": "this-is-not-my-password",
                  "password_first": "new-pass",
                  "password_second": "new-pass"
                }',
                /** @lang JSON */
                '{
                  "message": {"current_password": ["The value should be your current password."]}
                }',
                '"message": {"current_password":["The value should be your current password."]}',
            ],
            // 3)
            // password: too short
            [
                '{
                  "current_password": "demo",
                  "password_first": "short",
                  "password_second": "short"
                }',
                /** @lang JSON */
                '{
                  "message": {"password": {"first": ["The password must have at least 6 characters."]}}
                }',
                '"message": {"password_first":["The password must have at least 6 characters."]}',
            ],
            // 4)
            // password: to long
            [
                sprintf('{
                  "current_password": "demo",
                  "password_first": "%s",
                  "password_second": "%s"
                }', str_repeat('y', 256), str_repeat('y', 256)),
                /** @lang JSON */
                '{
                  "message": {"password": {"first": ["This value is too long. It should have 255 characters or less."]}}
                }',
                '"message": {"password_first":["This value is too long. It should' .
                ' have 255 characters or less."]}',
            ],
            // 5)
            // password: not match to password confirm
            [
                '{
                  "current_password": "demo",
                  "password_first": "new-pass",
                  "password_second": "new-pass"
                }',
                /** @lang JSON */
                '{
                  "message": {"password": {"first": ["Passwords are not matching."]}}
                }',
                '"message": {"password_first":["Passwords are not matching."]}',
            ],
            // 6)
            // current_password: not match to password confirm
            [
                '{
                  "contact": "new_address@test.com",
                  "current_password": "wrong_password"
                }',
                /** @lang JSON */
                '{
                  "message": {"current_password": ["The value should be your current password."]}
                }',
                '"message": {"current_password":["The value should be your current password."]}',
            ],
            // 7)
            // contact: already taken
            [
                '{
                  "contact": "taken@test.com",
                  "current_password": "wrong_password"
                }',
                /** @lang JSON */
                '{
                  "message": {"contact": ["This value is already used."]}
                }',
                '"message": {"contact":["This value is already used."]}',
            ],
        ];
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_invalid_json_should_return_unknown_error_message()
    {
        $this->createSession();
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/user/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            "this is not json"
        );

        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Unknown error occurred."}',
            $client->getResponse()->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxEditAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/user/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            "this is not json"
        );

        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Your session has expired. Please sign in again."}',
            $client->getResponse()->getContent()
        );

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxEditAction_without_supported_user_role_should_return_access_denied_message()
    {
        $this->createSession('some_role');

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            '/user/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            "this is not json"
        );

        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Page not found."}',
            $client->getResponse()->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }
}
