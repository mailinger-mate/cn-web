<?php

namespace Connect\Test\Functional\Bundle\UMSSecurityBundle\Command;

use Connect\Bundle\UMSSecurityBundle\Command\TwigCompileCommand;
use Mockery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

class TwigCompileCommandTest extends WebTestCase
{
    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid resource
     */
    public function execute_with_invalid_resource_should_throw_exception()
    {
        $container = Mockery::mock(ContainerInterface::class)
            ->shouldReceive('hasParameter')
            ->with('console.command.ids')
            ->andReturn(false)
            ->getMock();

        /** @var KernelInterface $kernel */
        $kernel = Mockery::mock(Kernel::class . '[getContainer,getBundle]', ['test', true])
            ->shouldReceive('registerContainerConfiguration')
            ->shouldReceive('registerBundles')
            ->andReturn([])
            ->shouldReceive('getContainer')
            ->andReturn($container)
            ->getMock();

        $application = new Application($kernel);
        $application->add(new TwigCompileCommand());
        $command = new CommandTester(
            $application->find('twig:compile')
        );
        $command->execute(['resource' => 'invalid']);
    }

    /**
     * @test
     */
    public function execute_with_no_files_should_not_render_anything()
    {
        $files = [];

        $templating = Mockery::mock(EngineInterface::class)
            ->shouldReceive('render')
            ->never()
            ->getMock();

        $filesystem = Mockery::mock(Filesystem::class)
            ->shouldReceive('dumpFile')
            ->never()
            ->getMock();

        $finder = Mockery::mock(Finder::class)
            ->shouldReceive('name')
            ->once()
            ->with('*.twig')
            ->andReturnSelf()
            ->shouldReceive('depth')
            ->once()
            ->with('== 0')
            ->shouldReceive('files')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('in')
            ->once()
            ->with('/fake/bundle/path/Resources/views/Emails')
            ->andReturn($files)
            ->getMock();

        /** @var KernelInterface $kernel */
        $kernel = Mockery::mock(Kernel::class . '[getContainer,getBundle]', ['test', true]);

        $container = Mockery::mock(ContainerInterface::class)
            ->shouldReceive('get')
            ->with('templating')
            ->andReturn($templating)
            ->shouldReceive('get')
            ->with('filesystem')
            ->andReturn($filesystem)
            ->shouldReceive('get')
            ->with('finder')
            ->andReturn($finder)
            ->shouldReceive('get')
            ->with('kernel')
            ->andReturn($kernel)
            ->shouldReceive('getParameter')
            ->never()
            ->shouldReceive('hasParameter')
            ->with('console.command.ids')
            ->andReturn(false)
            ->getMock();

        $bundle = Mockery::mock(BundleInterface::class)
            ->shouldReceive('getPath')
            ->once()
            ->andReturn('/fake/bundle/path')
            ->getMock();

        $kernel
            ->shouldReceive('registerContainerConfiguration')
            ->shouldReceive('registerBundles')
            ->andReturn([])
            ->shouldReceive('getName')
            ->andReturn('app')
            ->shouldReceive('getEnvironment')
            ->andReturn('test')
            ->shouldReceive('isDebug')
            ->andReturn(true)
            ->shouldReceive('getContainer')
            ->andReturn($container)
            ->shouldReceive('getBundle')
            ->once()
            ->with('FooBundle')
            ->andReturn($bundle);

        $application = new Application($kernel);
        $application->add(new TwigCompileCommand());
        $command = new CommandTester(
            $application->find('twig:compile')
        );
        $command->execute(['resource' => 'FooBundle:Emails']);

        $this->assertSame("No templates found\n", $command->getDisplay());
    }

    /**
     * @test
     */
    public function execute_with_one_file_should_render_template()
    {
        $files = [
            Mockery::mock(SplFileInfo::class)
                ->shouldReceive('getFilename')
                ->andReturn('first_template.html.twig')
                ->shouldReceive('getExtension')
                ->andReturn('twig')
                ->shouldReceive('getBasename')
                ->with('.twig')
                ->andReturn('first_template.html')
                ->getMock(),
        ];

        $templating = Mockery::mock(EngineInterface::class)
            ->shouldReceive('render')
            ->once()
            ->with('FooBundle:Emails:first_template.html.twig')
            ->andReturn('first template Lorem ipsum dolor sit amet, consectetur adipiscing elit')
            ->getMock();

        $filesystem = Mockery::mock(Filesystem::class)
            ->shouldReceive('dumpFile')
            ->once()
            ->with(
                '/fake/kernel/path/../build/first_template.html',
                'first template Lorem ipsum dolor sit amet, consectetur adipiscing elit'
            )
            ->getMock();

        $finder = Mockery::mock(Finder::class)
            ->shouldReceive('name')
            ->once()
            ->with('*.twig')
            ->andReturnSelf()
            ->shouldReceive('depth')
            ->once()
            ->with('== 0')
            ->shouldReceive('files')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('in')
            ->once()
            ->with('/fake/bundle/path/Resources/views/Emails')
            ->andReturn($files)
            ->getMock();

        /** @var KernelInterface $kernel */
        $kernel = Mockery::mock(Kernel::class . '[getContainer,getBundle]', ['test', true]);

        $container = Mockery::mock(ContainerInterface::class)
            ->shouldReceive('get')
            ->with('templating')
            ->andReturn($templating)
            ->shouldReceive('get')
            ->with('filesystem')
            ->andReturn($filesystem)
            ->shouldReceive('get')
            ->with('finder')
            ->andReturn($finder)
            ->shouldReceive('get')
            ->with('kernel')
            ->andReturn($kernel)
            ->shouldReceive('getParameter')
            ->once()
            ->with('kernel.root_dir')
            ->andReturn('/fake/kernel/path')
            ->shouldReceive('hasParameter')
            ->with('console.command.ids')
            ->andReturn(false)
            ->getMock();

        $bundle = Mockery::mock(BundleInterface::class)
            ->shouldReceive('getPath')
            ->once()
            ->andReturn('/fake/bundle/path')
            ->getMock();

        $kernel
            ->shouldReceive('registerContainerConfiguration')
            ->shouldReceive('registerBundles')
            ->andReturn([])
            ->shouldReceive('getContainer')
            ->andReturn($container)
            ->shouldReceive('getBundle')
            ->once()
            ->with('FooBundle')
            ->andReturn($bundle);

        $application = new Application($kernel);
        $application->add(new TwigCompileCommand());
        $command = new CommandTester(
            $application->find('twig:compile')
        );
        $command->execute(['resource' => 'FooBundle:Emails']);

        $this->assertSame(
            "Found 1 templates\n\n" .
            "first_template.html.twig........................................................" .
            "\rfirst_template.html.twig......................................................OK\n",
            $command->getDisplay());
    }

    /**
     * @test
     */
    public function execute_with_multiple_files_should_render_templates()
    {
        $files = [
            Mockery::mock(SplFileInfo::class)
                ->shouldReceive('getFilename')
                ->andReturn('first_template.html.twig')
                ->shouldReceive('getExtension')
                ->andReturn('twig')
                ->shouldReceive('getBasename')
                ->with('.twig')
                ->andReturn('first_template.html')
                ->getMock(),
            Mockery::mock(SplFileInfo::class)
                ->shouldReceive('getFilename')
                ->andReturn('second_template.html.twig')
                ->shouldReceive('getExtension')
                ->andReturn('twig')
                ->shouldReceive('getBasename')
                ->with('.twig')
                ->andReturn('second_template.html')
                ->getMock(),
            Mockery::mock(SplFileInfo::class)
                ->shouldReceive('getFilename')
                ->andReturn('third_template.html.twig')
                ->shouldReceive('getExtension')
                ->andReturn('twig')
                ->shouldReceive('getBasename')
                ->with('.twig')
                ->andReturn('third_template.html')
                ->getMock(),
        ];

        $templating = Mockery::mock(EngineInterface::class)
            ->shouldReceive('render')
            ->once()
            ->with('FooBundle:Emails:first_template.html.twig')
            ->andReturn('first template Lorem ipsum dolor sit amet, consectetur adipiscing elit')
            ->shouldReceive('render')
            ->once()
            ->with('FooBundle:Emails:second_template.html.twig')
            ->andReturn('second template Lorem ipsum dolor sit amet, consectetur adipiscing elit')
            ->shouldReceive('render')
            ->once()
            ->with('FooBundle:Emails:third_template.html.twig')
            ->andReturn('third template Lorem ipsum dolor sit amet, consectetur adipiscing elit')
            ->getMock();

        $filesystem = Mockery::mock(Filesystem::class)
            ->shouldReceive('dumpFile')
            ->once()
            ->with(
                '/fake/kernel/path/../build/first_template.html',
                'first template Lorem ipsum dolor sit amet, consectetur adipiscing elit'
            )
            ->shouldReceive('dumpFile')
            ->once()
            ->with(
                '/fake/kernel/path/../build/second_template.html',
                'second template Lorem ipsum dolor sit amet, consectetur adipiscing elit'
            )
            ->shouldReceive('dumpFile')
            ->once()
            ->with(
                '/fake/kernel/path/../build/third_template.html',
                'third template Lorem ipsum dolor sit amet, consectetur adipiscing elit'
            )
            ->getMock();

        $finder = Mockery::mock(Finder::class)
            ->shouldReceive('name')
            ->once()
            ->with('*.twig')
            ->andReturnSelf()
            ->shouldReceive('depth')
            ->once()
            ->with('== 0')
            ->shouldReceive('files')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('in')
            ->once()
            ->with('/fake/bundle/path/Resources/views/Emails')
            ->andReturn($files)
            ->getMock();

        /** @var KernelInterface $kernel */
        $kernel = Mockery::mock(Kernel::class . '[getContainer,getBundle]', ['test', true]);

        $container = Mockery::mock(ContainerInterface::class)
            ->shouldReceive('get')
            ->with('templating')
            ->andReturn($templating)
            ->shouldReceive('get')
            ->with('filesystem')
            ->andReturn($filesystem)
            ->shouldReceive('get')
            ->with('finder')
            ->andReturn($finder)
            ->shouldReceive('get')
            ->with('kernel')
            ->andReturn($kernel)
            ->shouldReceive('getParameter')
            ->times(3)
            ->with('kernel.root_dir')
            ->andReturn('/fake/kernel/path')
            ->shouldReceive('hasParameter')
            ->with('console.command.ids')
            ->andReturn(false)
            ->getMock();

        $bundle = Mockery::mock(BundleInterface::class)
            ->shouldReceive('getPath')
            ->once()
            ->andReturn('/fake/bundle/path')
            ->getMock();

        $kernel
            ->shouldReceive('registerContainerConfiguration')
            ->shouldReceive('registerBundles')
            ->andReturn([])
            ->shouldReceive('getName')
            ->andReturn('app')
            ->shouldReceive('getEnvironment')
            ->andReturn('test')
            ->shouldReceive('isDebug')
            ->andReturn(true)
            ->shouldReceive('getContainer')
            ->andReturn($container)
            ->shouldReceive('getBundle')
            ->once()
            ->with('FooBundle')
            ->andReturn($bundle);

        $application = new Application($kernel);
        $application->add(new TwigCompileCommand());
        $command = new CommandTester(
            $application->find('twig:compile')
        );
        $command->execute(['resource' => 'FooBundle:Emails']);

        $this->assertSame(
            "Found 3 templates\n\n" .
            "first_template.html.twig........................................................" .
            "\rfirst_template.html.twig......................................................OK\n" .
            "second_template.html.twig......................................................." .
            "\rsecond_template.html.twig.....................................................OK\n" .
            "third_template.html.twig........................................................" .
            "\rthird_template.html.twig......................................................OK\n",
            $command->getDisplay());
    }
}
