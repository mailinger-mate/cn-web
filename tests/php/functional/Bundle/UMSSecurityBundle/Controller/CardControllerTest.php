<?php

namespace Connect\Test\Functional\Bundle\UMSSecurityBundle\Controller;


use Connect\Bundle\UMSSecurityBundle\Stripe\Client;
use Connect\Test\Core\WebTestCase;
use Mockery;
use Stripe\Customer;
use Stripe\Error\InvalidRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CardControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function ajaxGetAction_with_existing_card_should_return_card_info()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "card_token": "card.token",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession();
        $client = $this->getTestHttpClient();

        $resultCustomer = Mockery::mock(Customer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [
                            [
                                'brand'     => 'Visa',
                                'exp_month' => 7,
                                'exp_year'  => 2019,
                                'last4'     => '0125',
                            ],
                        ],
                    ],
                ]
            )
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('card.token')
            ->andReturn($resultCustomer)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $client->request(
            'GET',
            'vendor/card',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": true,
              "card": {
                "brand": "Visa",
                "expiration_month": 7,
                "expiration_year": 2019,
                "last_four_digits": "0125"
              }
            }',
            $response->getContent()
        );
    }

    /**
     * @test
     */
    public function ajaxGetAction_without_card_token_should_return_404()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession();
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'vendor/card',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": false,
              "message": "Page not found."
            }',
            $response->getContent()
        );
    }


    /**
     * @test
     */
    public function ajaxGetAction_with_missing_card_should_return_404()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "card_token": "card.token",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession();
        $client = $this->getTestHttpClient();

        $exception = Mockery::mock(InvalidRequest::class)
            ->shouldReceive('getHttpStatus')
            ->andReturn(404)
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('card.token')
            ->andThrow($exception)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $client->request(
            'GET',
            'vendor/card',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": false,
              "message": "Page not found."
            }',
            $response->getContent()
        );
    }


    /**
     * @test
     */
    public function ajaxGetAction_with_error_should_return_500()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "card_token": "card.token",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession();
        $client = $this->getTestHttpClient();

        $exception = Mockery::mock(InvalidRequest::class)
            ->shouldReceive('getHttpStatus')
            ->andReturn(500)
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('card.token')
            ->andThrow($exception)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $client->request(
            'GET',
            'vendor/card',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": false,
              "message": "Unknown error occurred."
            }',
            $response->getContent()
        );
    }

    /**
     * @test
     */
    public function ajaxGetAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();

        $client->request(
            'GET',
            'vendor/card',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertSame('{"success":false,"message":"Your session has expired. Please sign in again."}',
            $response->getContent());
    }

    /**
     * @test
     */
    public function ajaxGetAction_without_supported_user_role_should_return_access_denied_message()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "some_role",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "card_token": "card.token",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession();
        $client = $this->getTestHttpClient();

        $resultCustomer = Mockery::mock(Customer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [
                            [
                                'brand'     => 'Visa',
                                'exp_month' => 7,
                                'exp_year'  => 2019,
                                'last4'     => '0125',
                            ],
                        ],
                    ],
                ]
            )
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('card.token')
            ->andReturn($resultCustomer)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $client->request(
            'GET',
            'vendor/card',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertSame('{"success":false,"message":"Page not found."}', $response->getContent());
    }

    /**
     * @test
     */
    public function ajaxAddAction_with_new_card_should_create_customer_and_save_id_to_ums()
    {
        $this->mockVendorWithBillingData();
        
        $this->createSession('admin');
        $client = $this->getTestHttpClient();

        $resultCustomer = Mockery::mock(Customer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(['id' => 'cust.id'])
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('createCustomer')
            ->with('token.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $this->addMockedResponse(
            'ums',
            Response::HTTP_NO_CONTENT
        );

        $this->addMockedResponse(
            'sender',
            Response::HTTP_NO_CONTENT
        );

        $client->request(
            'GET',
            'vendor/card/add',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{"card_token": "token.id"}'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            /** @lang JSON */
            '{
              "success": true
            }',
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            Request::METHOD_PUT,
            '/rest/v1/stripe_tokens/cust.id',
            '',
            '{"vendor":1}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );

        $this->assertCount(1, $this->getGuzzleHistory('sender'));
        $senderRequestContent = json_decode($this->getGuzzleHistoryRequest('sender', 0)->getBody()->getContents());

        $this->assertCount(1, $senderRequestContent);
        $this->assertSame('internal_connect_message', $senderRequestContent[0]->con_token);
        $this->assertSame('<strong>New Connect Credit Card Registration</strong><br/><br/>Vendor: <i>X-F</i><br>' .
            'User: <i>John Doe</i><br>Email: <i><a href="mailto:john@xf.com">john@xf.com</a></i>',
            $senderRequestContent[0]->message_config->content->values[0]);
        $this->assertSame('hipchat', $senderRequestContent[0]->type);
    }

    /**
     * @test
     */
    public function ajaxAddAction_with_new_card_without_billing_data_should_return_error()
    {
        $this->createSession('admin');

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'vendor/card/add',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{"card_token": "token.id"}'
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": false,
              "message": {"card_token": ["Cannot add card details for vendor without client details."]}
            }',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAddAction_with_existing_card_should_replace_current_customer_in_stripe_and_ums()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "billing_name": "X-Formation\\nPoland Sp. z o.o.",
                    "street": "Radzikowskiego 47A",
                    "city": "31-315 Kraków",
                    "state": "Małopolska",
                    "country": "PL",
                    "vat_number": "PL11223344",
                    "note": "New license",
                    "po_number": "#order-no: 112233",
                    "card_token": "card.token",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession('admin');
        $client = $this->getTestHttpClient();

        $currentCustomer = Mockery::mock(Customer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [
                            [
                                'brand'     => 'Visa',
                                'exp_month' => 7,
                                'exp_year'  => 2019,
                                'last4'     => '0125',
                            ],
                        ],
                    ],
                ]
            )
            ->shouldReceive('delete')
            ->once()
            ->getMock();

        $resultCustomer = Mockery::mock(Customer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(['id' => 'new.cust.id'])
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('card.token')
            ->andReturn($currentCustomer)
            ->shouldReceive('createCustomer')
            ->with('token.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $this->addMockedResponse(
            'ums',
            Response::HTTP_NO_CONTENT
        );

        $this->addMockedResponse(
            'sender',
            Response::HTTP_NO_CONTENT
        );

        $client->request(
            'GET',
            'vendor/card/add',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{"card_token": "token.id"}'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            /** @lang JSON */
            '{
              "success": true
            }',
            $response->getContent()
        );

        $this->assertCount(3, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            Request::METHOD_DELETE,
            '/rest/v1/stripe_tokens/card.token',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
        $this->assertRequest(
            Request::METHOD_PUT,
            '/rest/v1/stripe_tokens/new.cust.id',
            '',
            '{"vendor":1}',
            $this->getGuzzleHistoryRequest('ums', 2)
        );

        $this->assertCount(0, $this->getGuzzleHistory('sender'));
    }

    /**
     * @test
     */
    public function ajaxAddAction_invalid_stripe_token_should_return_error()
    {
        $this->mockVendorWithBillingData();
        $this->createSession('admin');

        $client = $this->getTestHttpClient();

        $exception = Mockery::mock(InvalidRequest::class)
            ->shouldReceive('getHttpStatus')
            ->andReturn(400)
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('createCustomer')
            ->with('token.id')
            ->andThrow($exception)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $this->addMockedResponse(
            'ums',
            Response::HTTP_NO_CONTENT
        );

        $client->request(
            'GET',
            'vendor/card/add',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{"card_token": "token.id"}'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            /** @lang JSON */
            '{
              "success": false,
              "message": {"card_token": ["This is not a valid value."]}
            }',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAddAction_with_missing_stripe_token_should_return_error()
    {
        $this->mockVendorWithBillingData();
        $this->createSession('admin');

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'vendor/card/add',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{}'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            /** @lang JSON */
            '{
              "success": false,
              "message": {"card_token": ["This value should not be blank."]}
            }',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxAddAction_with_empty_stripe_token_should_return_error()
    {
        $this->mockVendorWithBillingData();

        $this->createSession('admin');
        $client = $this->getTestHttpClient();

        $client->request(
            'GET',
            'vendor/card/add',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            /** @lang JSON */
            '{"card_token":""}'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            /** @lang JSON */
            '{
              "success": false,
              "message": {"card_token": ["This value should not be blank."]}
            }',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxDelete_with_existing_card_should_remove_card()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "card_token": "card.token",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );

        $this->createSession('admin');
        $client = $this->getTestHttpClient();

        $currentCustomer = Mockery::mock(Customer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [
                            [
                                'brand'     => 'Visa',
                                'exp_month' => 7,
                                'exp_year'  => 2019,
                                'last4'     => '0125',
                            ],
                        ],
                    ],
                ]
            )
            ->shouldReceive('delete')
            ->once()
            ->getMock();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('card.token')
            ->andReturn($currentCustomer)
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $this->addMockedResponse(
            'ums',
            Response::HTTP_NO_CONTENT
        );

        $client->request(
            'GET',
            'vendor/card/delete',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": true
            }',
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            Request::METHOD_DELETE,
            '/rest/v1/stripe_tokens/card.token',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     */
    public function ajaxDelete_without_card_should_return_success()
    {
        $this->createSession('admin');
        $client = $this->getTestHttpClient();

        $stripe = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->never()
            ->getMock();

        $container = $client->getContainer();
        $container->set('stripe.client', $stripe);

        $this->addMockedResponse(
            'ums',
            Response::HTTP_NO_CONTENT
        );

        $client->request(
            'GET',
            'vendor/card/delete',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": true
            }',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    private function mockVendorWithBillingData()
    {
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "admin",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http://localhost/rest/v1/users/1"},
                  "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "billing_name": "X-Formation\\nPoland Sp. z o.o.",
                    "street": "Radzikowskiego 47A",
                    "city": "31-315 Kraków",
                    "state": "Małopolska",
                    "country": "PL",
                    "vat_number": "PL11223344",
                    "note": "New license",
                    "po_number": "#order-no: 112233",
                    "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
              }
            ]'
        );
    }
}
