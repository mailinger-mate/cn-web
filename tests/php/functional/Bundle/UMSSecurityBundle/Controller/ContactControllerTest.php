<?php
namespace Connect\Tests\Functional\Bundle\UMSSecurityBundle\Controller;

use Connect\Test\Core\WebTestCase;
use Swift_Message;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ContactControllerTest
 */
class ContactControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function ajaxVerifyAction_with_valid_data_should_return_success()
    {
        $this->createSession();

        // ums facade ContactVerify
        $this->addMockedResponse('ums',
            Response::HTTP_OK,
            '{
              "contact": "john@xf.com",
              "action_id": "9a66e06ded843e5848f14fccd6656d41"
            }'
        );

        $client = $this->getTestHttpClient();
        $client->enableProfiler();

        $client->request(
            'GET',
            'contact/verify/1',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $content = $client->getResponse()->getContent();
        $this->assertJsonStringEqualsJsonString('{"success": true}', $content);
        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            Request::METHOD_GET,
            '/verify/1',
            'user=1',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message $message */
        $message = $mailCollector->getMessages()[0];
        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('noreply@x-formation.com', key($message->getFrom()));
        $this->assertSame('john@xf.com', key($message->getTo()));
        $this->assertSame('Please verify your X-Formation Connect email address', $message->getSubject());
        $this->assertContains(
            'http://localhost/action/9a66e06ded843e5848f14fccd6656d41',
            $message->getBody()
        );
        $this->assertNotContains(
            'scheme_and_host',
            $message->getBody()
        );
    }

    /**
     * @test
     */
    public function ajaxVerifyAction_with_not_existing_contact_should_return_404()
    {
        $this->createSession();

        // ums facade ContactVerify
        $this->addMockedResponse('ums', Response::HTTP_NOT_FOUND);

        $client = $this->getTestHttpClient();
        $client->enableProfiler();

        $client->request(
            'GET',
            'contact/verify/1',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            Request::METHOD_GET,
            '/verify/1',
            'user=1',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(0, $mailCollector->getMessageCount());
    }

    /**
     * @test
     */
    public function ajaxVerifyAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();
        $client->enableProfiler();

        $client->request(
            'GET',
            'contact/verify/1',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertSame('{"success":false,"message":"Your session has expired. Please sign in again."}', $response->getContent());

        $history = $this->getGuzzleHistory('ums');
        $this->assertCount(0, $history);

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(0, $mailCollector->getMessageCount());
    }

    /**
     * @test
     */
    public function ajaxVerifyAction_without_supported_user_role_should_return_access_denied_message()
    {
        $this->createSession('some_role');

        $client = $this->getTestHttpClient();
        $client->enableProfiler();

        $client->request(
            'GET',
            'contact/verify/1',
            [],
            [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertSame('{"success":false,"message":"Page not found."}', $response->getContent());

        $history = $this->getGuzzleHistory('ums');
        $this->assertCount(1, $history);

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(0, $mailCollector->getMessageCount());
    }
}
