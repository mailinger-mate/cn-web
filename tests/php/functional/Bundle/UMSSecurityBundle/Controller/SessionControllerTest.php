<?php

namespace Connect\Tests\Functional\Bundle\UMSSecurityBundle\Controller;

use Connect\Test\Core\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SessionControllerTest
 */
class SessionControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function ajaxHealthCheckAction_invalid_token_format_should_respond_with_404()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'health_check/invalid_token_format'
        );

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function ajaxHealthCheckAction_without_session_should_return_false()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'health_check/6ad6c7d6116407022a7a65cb6c8f4c3f'
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"success": false}', $response->getContent());
    }

    /**
     * @test
     */
    public function ajaxHealthCheckAction_without_token_in_ums_should_return_false()
    {
        $this->createSession();

        $this->addMockedResponse('ums', Response::HTTP_NOT_FOUND);

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'GET',
            'health_check/c41843ba204f057534b358bf838a954c'
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"success": false}', $response->getContent());
        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxHealthCheckAction_with_token_not_match_session_token_should_return_false()
    {
        $this->createSession();

        $this->addMockedResponse('ums', Response::HTTP_NOT_FOUND);

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'GET',
            'health_check/6ad6c7d6116407022a7a65cb6c8f4c3f'
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"success": false}', $response->getContent());
        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxHealthCheckAction_with_valid_data_should_return_true()
    {
        $this->createSession();

        $this->addMockedResponse('ums', Response::HTTP_OK, '{"id":"c41843ba204f057534b358bf838a954c"}');

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'GET',
            'health_check/c41843ba204f057534b358bf838a954c'
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"success": true}', $response->getContent());
        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }
}
