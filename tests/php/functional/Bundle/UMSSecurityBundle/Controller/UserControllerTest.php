<?php

namespace Connect\Tests\Functional\Bundle\UMSSecurityBundle\Controller;

use Connect\Test\Core\WebTestCase;
use GuzzleHttp\HandlerStack;
use Mockery;
use Swift_Message;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class UserControllerTest
 */
class UserControllerTest extends WebTestCase
{
    /**
     * @test
     * @dataProvider newsletterAgreementProvider
     *
     * @param $newsletterAgreement
     */
    public function addAction_with_valid_data_should_add_user_and_send_verification_email_and_notify_sender($newsletterAgreement)
    {
        // Facade::RegisterUser()
        $this->addMockedResponse('ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{"user_id": 8}'
        );

        // UMSUserRepository::find()
        $this->addMockedResponse('ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
              "id": 8,
              "name": "John Doe",
              "login": "johndo",
              "password": "secret",
              "salt": "aa3c554286c46a74e08caf6e2111520c",
              "role": "admin",
              "activated": true,
              "enabled": true,
              "_links": {
                "self": {
                  "href": "http://localhost/rest/v1/users/8"
                },
                "vendor": {
                  "href": "http://localhost/rest/v1/vendors/2"
                }
              },
              "_embedded": {
                "vendor": {
                  "id": 2,
                  "name": "X-F2",
                  "_links": {
                    "self": {
                      "href": "http://localhost/rest/v1/vendors/2"
                    }
                  }
                },
                "contacts": [
                  {
                    "value": "john@x-f.com",
                    "type": "email",
                    "verified": false,
                    "id": 2,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/contacts/2"
                      }
                    },
                    "_embedded": {
                      "contact_actions": [
                        {
                          "id": "14fe64b7e51dd33a656c8162e409684b",
                          "type": "contact_verify",
                          "expiration_time": "2020-02-04T06:05:15+0000",
                          "_links": {
                            "self": {
                              "href": "rest/v1/contact_actions/14fe64b7e51dd33a656c8162e409684b"
                            },
                            "contact": {
                              "href": "rest/v1/contacts/1"
                            }
                          }
                        }
                      ]
                    }
                  }
                ],
                "user_actions": []
              }
            }'
        );

        // UMSSessionRepository::add()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_CREATED,
            /** @lang JSON */
            '{"id": "9bd4c0e862ee28a24ced1da505b7af62","expiration_time":"2020-02-04T06:05:15+0000"}'
        );

        // Sender\Client::send
        $this->addMockedResponse(
            'sender',
            HttpResponse::HTTP_NO_CONTENT
        );

        $form = [
            'user' => [
                'name'                  => 'John Doe',
                'contact'               => 'john@x-f.com',
                'vendor'                => 'X-F2',
                'password_first'        => 'secret',
                'password_second'       => 'secret',
                '_token'                => $this->getCsrfToken('user'),
                'submit'                => '',
            ],
        ];

        if ($newsletterAgreement) {
            $form['user']['newsletter_agreement'] = 1;
        }

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'POST',
            '/register',
            $form
        );

        $this->assertTrue($client->getResponse()->isRedirect('/app'));

        $history = $this->getGuzzleHistory('ums');
        $this->assertCount(3, $history);

        $this->assertRequest(
            Request::METHOD_GET,
            '/register',
            '',
            '{"name":"John Doe","vendor":{"name":"X-F2"},"contacts":[{"value":"john@x-f.com"}]' .
            ',"password":{"first":"secret","second":"secret"}}',
            $this->getGuzzleHistoryRequest('ums', 0)
        );
        $this->assertRequest(
            Request::METHOD_GET,
            '/rest/v1/users/8',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
        $this->assertRequest(
            Request::METHOD_POST,
            '/rest/v1/sessions',
            '',
            '{"user":8,"product":"connect"}',
            $this->getGuzzleHistoryRequest('ums', 2)
        );

        $this->assertCount(1, $this->getGuzzleHistory('sender'));
        $senderRequestContent = json_decode($this->getGuzzleHistoryRequest('sender', 0)->getBody()->getContents());

        $this->assertCount($newsletterAgreement ? 2 : 1, $senderRequestContent);
        $this->assertSame('internal_connect_message', $senderRequestContent[0]->con_token);
        $this->assertSame('<strong>New Connect Registration</strong><br/><br/>Vendor: <i>X-F2 (id:2)</i><br>' .
            'User: <i>John Doe</i><br>Email: <i><a href="mailto:john@x-f.com">john@x-f.com</a></i>',
            $senderRequestContent[0]->message_config->content->values[0]);
        $this->assertSame('hipchat', $senderRequestContent[0]->type);

        if ($newsletterAgreement) {
            $this->assertSame('internal_connect_message', $senderRequestContent[1]->con_token);
            $this->assertSame('mailchimp', $senderRequestContent[1]->type);
            $this->assertNotEquals($senderRequestContent[0]->msg_token, $senderRequestContent[1]->msg_token);
        }

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message $message */
        $message = $mailCollector->getMessages()[0];
        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('noreply@x-formation.com', key($message->getFrom()));
        $this->assertSame('john@x-f.com', key($message->getTo()));
        $this->assertSame('Welcome to Connect: Your 30-day free trial begins today', $message->getSubject());
        $this->assertContains(
            'http://localhost/action/14fe64b7e51dd33a656c8162e409684b',
            $message->getBody()
        );
        $this->assertNotContains(
            'scheme_and_host',
            $message->getBody()
        );

        /** @var AttributeBag $sessionAttributeBag */
        $sessionAttributeBag = $client->getContainer()->get('session')->getBag('attributes');
        $this->assertTrue($sessionAttributeBag->has('_security_connect'));
        $this->assertTrue($sessionAttributeBag->has('ums_session'));

        $this->assertSame('9bd4c0e862ee28a24ced1da505b7af62', $sessionAttributeBag->get('ums_session'));

        /** @var UsernamePasswordToken $token */
        $token = unserialize($sessionAttributeBag->get('_security_connect'));
        $this->assertTrue($token->isAuthenticated());
        $this->assertSame('john@x-f.com', $token->getUsername());
    }

    public static function newsletterAgreementProvider()
    {
        return [
            [true],
            [false]
        ];
    }

    /**
     * @test
     * @dataProvider invalidFormDataProvider
     *
     * @param string $UMSResponse
     * @param array $requestData
     * @param array $expectedErrors
     */
    public function addAction_with_invalid_data(
        $UMSResponse,
        array $requestData,
        array $expectedErrors
    ) {
        // Facade::RegisterUser()
        $this->addMockedResponse('ums', HttpResponse::HTTP_BAD_REQUEST, $UMSResponse);

        $formData = [
            'user' => [
                'name'            => $requestData['name'],
                'contact'         => $requestData['email'],
                'vendor'          => $requestData['vendor'],
                'password_first'  => $requestData['password'],
                'password_second' => $requestData['password_confirm'],
                '_token'          => $this->getCsrfToken('user'),
                'submit'          => '',
            ],
        ];

        $client = $this->getTestHttpClient();
        $crawler = $client->request(
            'POST',
            '/register',
            $formData
        );

        $formElement = $crawler->filterXPath('//form[@name=\'user\']');

        $this->assertFormErrors($formElement, $expectedErrors);
    }

    /**
     * @return array
     */
    public static function invalidFormDataProvider()
    {
        $emptyFieldError = 'This value should not be blank.';
        $tooLongError = 'This value is too long. It should have 255 characters or less.';

        return [
            // 0)
            // name: empty
            [
                /** @lang JSON */
                '{
                  "message": {
                    "name": ["This value should not be blank."]
                  }
                }',
                [
                    'name'             => '',
                    'email'            => 'john@x-f.com',
                    'vendor'           => 'X-F',
                    'password'         => 'secret',
                    'password_confirm' => 'secret',
                ],
                [
                    'name'             => $emptyFieldError,
                    'email'            => null,
                    'vendor'           => null,
                    'password'         => null,
                    'password_confirm' => null,
                ],
            ],
            // 1)
            // name: too long
            [
                /** @lang JSON */
                '{
                  "message": {
                    "name": ["This value is too long. It should have 255 characters or less."]
                  }
                }',
                [
                    'name'             => str_repeat('a', 256),
                    'email'            => 'john@x-f.com',
                    'vendor'           => 'X-F',
                    'password'         => 'secret',
                    'password_confirm' => 'secret',
                ],
                [
                    'name'             => $tooLongError,
                    'email'            => null,
                    'vendor'           => null,
                    'password'         => null,
                    'password_confirm' => null,
                ],
            ],
            // 2)
            // email: empty
            [
                /** @lang JSON */
                '{
                  "message": {
                    "contacts": [{"value": ["This value should not be blank."]}]
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => '',
                    'vendor'           => 'X-F',
                    'password'         => 'secret',
                    'password_confirm' => 'secret',
                ],
                [
                    'name'             => null,
                    'email'            => $emptyFieldError,
                    'vendor'           => null,
                    'password'         => null,
                    'password_confirm' => null,
                ],
            ],
            // 3)
            // email: invalid email
            [
                /** @lang JSON */
                '{
                  "message": {
                    "contacts": [{"value": ["This value is not a valid email address."]}]
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => '@this@is@not$email',
                    'vendor'           => 'X-F',
                    'password'         => 'secret',
                    'password_confirm' => 'secret',
                ],
                [
                    'name'             => null,
                    'email'            => 'This value is not a valid email address.',
                    'vendor'           => null,
                    'password'         => null,
                    'password_confirm' => null,
                ],
            ],
            // 4)
            // email: to long
            [
                /** @lang JSON */
                '{
                  "message": {
                    "contacts": [{"value": ["This value is too long. It should have 255 characters or less."]}]
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => str_repeat('a', 249) . '@xf.com', // 256 chars
                    'vendor'           => 'X-F',
                    'password'         => 'secret',
                    'password_confirm' => 'secret',
                ],
                [
                    'name'             => null,
                    'email'            => $tooLongError,
                    'vendor'           => null,
                    'password'         => null,
                    'password_confirm' => null,
                ],
            ],
            // 5)
            // vendor: empty
            [
                /** @lang JSON */
                '{
                  "message": {
                    "vendor": {"name": ["This value should not be blank."]}
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => 'john@x-f.com',
                    'vendor'           => '',
                    'password'         => 'secret',
                    'password_confirm' => 'secret',
                ],
                [
                    'name'             => null,
                    'email'            => null,
                    'vendor'           => $emptyFieldError,
                    'password'         => null,
                    'password_confirm' => null,
                ],
            ],
            // 6)
            // password: empty
            [
                /** @lang JSON */
                '{
                  "message": {
                    "password": {"first": ["This value should not be blank."]}
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => 'john@x-f.com',
                    'vendor'           => 'X-F',
                    'password'         => '',
                    'password_confirm' => '',
                ],
                [
                    'name'             => null,
                    'email'            => null,
                    'vendor'           => null,
                    'password'         => $emptyFieldError,
                    'password_confirm' => null,
                ],
            ],
            // 7)
            // password: too short
            [
                /** @lang JSON */
                '{
                  "message": {
                    "password": {"first": ["The password must have at least 6 characters."]}
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => 'john@x-f.com',
                    'vendor'           => 'X-F',
                    'password'         => 'short',
                    'password_confirm' => 'short',
                ],
                [
                    'name'             => null,
                    'email'            => null,
                    'vendor'           => null,
                    'password'         => 'The password must have at least 6 characters.',
                    'password_confirm' => null,
                ],
            ],
            // 8)
            // password: too long
            [
                /** @lang JSON */
                '{
                  "message": {
                    "password": {"first": ["This value is too long. It should have 255 characters or less."]}
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => 'john@x-f.com',
                    'vendor'           => 'X-F',
                    'password'         => str_repeat('a', 256),
                    'password_confirm' => str_repeat('a', 256),
                ],
                [
                    'name'             => null,
                    'email'            => null,
                    'vendor'           => null,
                    'password'         => $tooLongError,
                    'password_confirm' => null,
                ],
            ],
            // 9)
            // password: not match to password_confirm
            [
                /** @lang JSON */
                '{
                  "message": {
                    "password": {"first": ["Passwords are not matching."]}
                  }
                }',
                [
                    'name'             => 'John Doe',
                    'email'            => 'john@x-f.com',
                    'vendor'           => 'X-F',
                    'password'         => 'secret',
                    'password_confirm' => 'different',
                ],
                [
                    'name'             => null,
                    'email'            => null,
                    'vendor'           => null,
                    'password'         => 'Passwords are not matching.',
                    'password_confirm' => null,
                ],
            ],
            // 10)
            // multiple errors:
            // name: too long
            // vendor: empty
            // password: not match to password_confirm
            [
                /** @lang JSON */
                '{
                  "message": {
                  "name": ["This value is too long. It should have 255 characters or less."],
                  "vendor": {"name": ["This value should not be blank."]},
                  "password": {"first": ["Passwords are not matching."]}
                  }
                }',
                [
                    'name'             => str_repeat('a', 256),
                    'email'            => 'john@x-f.com',
                    'vendor'           => '',
                    'password'         => 'secret',
                    'password_confirm' => 'different',
                ],
                [
                    'name'             => $tooLongError,
                    'email'            => null,
                    'vendor'           => $emptyFieldError,
                    'password'         => 'Passwords are not matching.',
                    'password_confirm' => null,
                ],
            ],
        ];
    }

    /**
     * @test
     */
    public function addAction_without_submitted_form_with_email_in_get_should_render_form_with_contact_value() {
        $requestData = ['email' => 'test@test.com'];
        $client = $this->getTestHttpClient();
        $crawler = $client->request(
            'GET',
            '/register',
            $requestData
        );

        $formElement = $crawler->filterXPath('//input[@id=\'user_contact\']//@value');

        $this->assertSame('test@test.com', $formElement->text());
    }

    /**
     * @test
     */
    public function addAction_without_submitted_form_without_email_in_get_should_render_form_without_contact_value() {
        $client = $this->getTestHttpClient();
        $crawler = $client->request(
            'GET',
            '/register'
        );

        $formElement = $crawler->filterXPath('//input[@id=\'user_contact\']//@value');

        $this->assertCount(0, $formElement);
    }

    /**
     * @test
     */
    public function addAction_with_submitted_form_with_email_in_get_with_error_render_form_with_post_contact_value() {
        $requestData = [
                'user' => [
                    'name'    => '',
                    'contact' => 'john@x-f.com'
                ],
                'email' => 'test@test.com'
            ];
        $client = $this->getTestHttpClient();
        $crawler = $client->request(
            'POST',
            '/register',
            $requestData
        );

        $formElement = $crawler->filterXPath('//input[@id=\'user_contact\']//@value');

        $this->assertSame('john@x-f.com', $formElement->text());
    }

    /**
     * @test
     */
    public function remindPasswordAction_with_valid_data_should_send_email()
    {
        // Facade::remindUserPassword()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK, '{"user_id": "1"}');
        // UMSUserRepository::find()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
              "id": 1,
              "name": "John Doe",
              "login": "johndo",
              "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
              "salt": "b54109793ab030c94c40c5381891fd69",
              "role": "user",
              "activated": false,
              "enabled": false,
              "_links": {
                "self": {
                  "href": "http://localhost/rest/v1/users/1"
                },
                "vendor": {
                  "href": "http://localhost/rest/v1/vendors/1"
                }
              },
              "_embedded": {
                "vendor": {
                  "id": 1,
                  "name": "X-F",
                  "_links": {
                    "self": {
                      "href": "http://localhost/rest/v1/vendors/1"
                    }
                  }
                },
                "contacts": [
                  {
                    "value": "john@xf.com",
                    "type": "email",
                    "verified": true,
                    "id": 1,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/contacts/1"
                      }
                    },
                    "_embedded": {
                      "contact_actions": []
                    }
                  }
                ],
                "user_actions": [
                  {
                    "type": "password_reset",
                    "expiration_time": "2030-05-25T22:06:59+0000",
                    "id": "ad9d2afb6195b3cc377855ae315c6291",
                    "_links": {
                      "self": {
                        "href": "rest/v1/user_actions/ad9d2afb6195b3cc377855ae315c6291"
                      },
                      "user": {
                        "href": "rest/v1/user/1"
                      }
                    }
                  }
                ]
              }
            }'
        );

        $form = [
            'forgot_password' => [
                'email'  => 'john@xf.com',
                '_token' => $this->getCsrfToken('forgot_password'),
            ],
        ];

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'POST',
            '/remind_password',
            $form
        );
        $response = $client->getResponse();
        $this->assertTrue($response->isRedirect('/confirm?action=remind_password'));

        $history = $this->getGuzzleHistory('ums');
        $this->assertCount(2, $history);

        $this->assertRequest(
            Request::METHOD_GET,
            '/remind',
            '',
            '{"email":"john@xf.com"}',
            $this->getGuzzleHistoryRequest('ums', 0)
        );

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message $message */
        $message = $mailCollector->getMessages()[0];
        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('noreply@x-formation.com', key($message->getFrom()));
        $this->assertSame('john@xf.com', key($message->getTo()));
        $this->assertSame('Here\'s the link to reset your Connect password', $message->getSubject());
        $this->assertContains(
            'http://localhost/action/ad9d2afb6195b3cc377855ae315c6291',
            $message->getBody()
        );
        $this->assertNotContains(
            'scheme_and_host',
            $message->getBody()
        );
    }

    /**
     * @test
     */
    public function accountActivateAction_with_valid_data()
    {
        // UMSContactRepository::findOneByAction()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK, '[]');

        // UMSUserRepository::findUserByAction()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK,
            /** @lang JSON */
            '[
              {
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": false,
                "enabled": false,
                "_links": {
                  "self": {
                    "href": "http://localhost/rest/v1/users/1"
                  },
                  "vendor": {
                    "href": "http://localhost/rest/v1/vendors/1"
                  }
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/vendors/1"
                      }
                    }
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {
                        "self": {
                          "href": "http://localhost/rest/v1/contacts/1"
                        }
                      },
                      "_embedded": {
                        "contact_actions": []
                      }
                    }
                  ],
                  "user_actions": [
                    {
                      "type": "account_activate",
                      "expiration_time": "2030-05-25T22:06:59+0000",
                      "id": "6ad6c7d6116407022a7a65cb6c8f4c3f",
                      "_links": {
                        "self": {"href": "rest/v1/user_actions/6ad6c7d6116407022a7a65cb6c8f4c3f"},
                        "user": {"href": "rest/v1/user/1"}
                      }
                    }
                  ]
                }
              }
            ]'
        );

        // Facade::ActivateUserAccount()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK, '{"user_id": 1}');

        // UMSUserRepository::find()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
              "id": 1,
              "name": "John Doe",
              "login": "johndo",
              "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
              "salt": "b54109793ab030c94c40c5381891fd69",
              "role": "user",
              "activated": true,
              "enabled": true,
              "_links": {
                "self": {
                  "href": "http://localhost/rest/v1/users/1"
                },
                "vendor": {
                  "href": "http://localhost/rest/v1/vendors/1"
                }
              },
              "_embedded": {
                "vendor": {
                  "id": 1,
                  "name": "X-F",
                  "_links": {
                    "self": {
                      "href": "http://localhost/rest/v1/vendors/1"
                    }
                  }
                },
                "contacts": [
                  {
                    "value": "john@xf.com",
                    "type": "email",
                    "verified": true,
                    "id": 1,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/contacts/1"
                      }
                    },
                    "_embedded": {
                      "contact_actions": []
                    }
                  }
                ],
                "user_actions": []
              }
            }'
        );

        $form = [
            'user_set_password' => [
                'password_first'  => 'secret',
                'password_second' => 'secret',
                '_token'          => $this->getCsrfToken('user_set_password'),
            ],
        ];

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'POST',
            '/action/6ad6c7d6116407022a7a65cb6c8f4c3f',
            $form
        );

        $this->assertTrue($client->getResponse()->isRedirect('/app'));

        $history = $this->getGuzzleHistory('ums');
        $this->assertCount(4, $history);

        $this->assertJsonStringEqualsJsonString(
            '{
              "password": {"first": "secret", "second": "secret"}
            }',
            $this->getGuzzleHistoryRequest('ums', 2)->getBody()->getContents()
        );

        /** @var AttributeBag $sessionAttributeBag */
        $sessionAttributeBag = $client->getContainer()->get('session')->getBag('attributes');
        $this->assertTrue($sessionAttributeBag->has('_security_connect'));

        /** @var UsernamePasswordToken $token */
        $token = unserialize($sessionAttributeBag->get('_security_connect'));
        $this->assertTrue($token->isAuthenticated());
        $this->assertSame('john@xf.com', $token->getUsername());
    }

    /**
     * @test
     */
    public function ajaxGetUsersAction_should_return_users_list()
    {
        $this->createSession('admin');

        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            json_encode(
                [
                    [
                        "id"        => 1,
                        "name"      => "John Doe",
                        "activated" => false,
                        "password"  => "secret",
                        "salt"      => "salt",
                        "role"      => "admin",
                        "enabled"   => true,
                        "_links"    => [
                            "self" => ["href" => "/rest/v1/users/1"],
                        ],
                        "_embedded" => [
                            "vendor"       => [
                                "id"     => 1,
                                "name"   => "Cool Co.",
                                "_links" => [
                                    "self" => ["href" => "/rest/v1/vendors/1"],
                                ],
                            ],
                            "contacts"     => [
                                [
                                    "id"        => 5,
                                    "value"     => "john.doe@x-formation.com",
                                    "type"      => "email",
                                    "verified"  => true,
                                    "_links"    => [
                                        "self" => ["href" => "rest/v1/contacts/1"],
                                    ],
                                    '_embedded' => [
                                        'contact_actions' => [],
                                    ],
                                ],
                            ],
                            'user_actions' => [],
                        ],
                    ],
                    [
                        "id"        => 2,
                        "name"      => "John Foo",
                        "activated" => true,
                        "password"  => "secret",
                        "salt"      => "salt",
                        "role"      => "user",
                        "enabled"   => true,
                        "_links"    => [
                            "self" => ["href" => "/rest/v1/users/2"],
                        ],
                        "_embedded" => [
                            "vendor"       => [
                                "id"     => 1,
                                "name"   => "Cool Co.",
                                "_links" => [
                                    "self" => ["href" => "/rest/v1/vendors/1"],
                                ],
                            ],
                            "contacts"     => [
                                [
                                    "id"        => 15,
                                    "value"     => "john.foo@x-formation.com",
                                    "type"      => "email",
                                    "verified"  => true,
                                    "_links"    => [
                                        "self" => ["href" => "rest/v1/contacts/15"],
                                    ],
                                    '_embedded' => [
                                        'contact_actions' => [],
                                    ],
                                ],
                            ],
                            'user_actions' => [],
                        ],
                    ],
                ]
            ));

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/list',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
        /** @lang JSON */
            '{
              "success": true,
              "users": [
                {
                  "id": 1,
                  "name": "John Doe",
                  "activated": false,
                  "contact": "john.doe@x-formation.com",
                  "verified": true,
                  "role": "admin"
                },
                {
                  "id": 2,
                  "name": "John Foo",
                  "activated": true,
                  "contact": "john.foo@x-formation.com",
                  "verified": true,
                  "role": "user"
                }
              ]
            }',
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            Request::METHOD_GET,
            '/rest/v1/users',
            'vendor=1',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     */
    public function ajaxGetUsersAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/list',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Your session has expired. Please sign in again."}',
            $response->getContent()
        );

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     * @dataProvider invalidRolesDataProvider
     *
     * @param $role
     */
    public function ajaxGetUsersAction_without_supported_user_role_should_return_not_found_message($role)
    {
        $this->createSession($role);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/list',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Page not found."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxDeleteAction_with_same_as_logged_user_should_return_error()
    {
        $this->createSession('admin');

        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
                "id": 1,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http:\/\/localhost\/rest\/v1\/users\/1"},
                  "vendor": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/contacts\/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/delete/1',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success": false, "message": "You can not delete yourself"}',
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            'GET',
            '/rest/v1/users/1',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     */
    public function ajaxDeleteAction_with_user_for_different_vendor_should_return_404()
    {
        $this->createSession('admin');

        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
                "id": 5,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http:\/\/localhost\/rest\/v1\/users\/1"},
                  "vendor": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 2,
                    "name": "X-F",
                    "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/contacts\/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/delete/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            'GET',
            '/rest/v1/users/5',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     */
    public function ajaxDeleteAction_with_valid_id_should_return_success()
    {
        $this->createSession('admin');

        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
                "id": 5,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http:\/\/localhost\/rest\/v1\/users\/1"},
                  "vendor": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/contacts\/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
            }'
        );

        $this->addMockedResponse('ums', HttpResponse::HTTP_NO_CONTENT);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/delete/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success": true}',
            $response->getContent()
        );

        $this->assertCount(3, $this->getGuzzleHistory('ums'));
        $this->assertRequest('GET',
            '/rest/v1/users/5',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
        $this->assertRequest('DELETE',
            '/rest/v1/users/5',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 2)
        );
    }

    /**
     * @test
     */
    public function ajaxDeleteAction_without_authenticated_user_should_return_expired_session_message()
    {
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
                "id": 5,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http:\/\/localhost\/rest\/v1\/users\/1"},
                  "vendor": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/contacts\/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/delete/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Your session has expired. Please sign in again."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     * @dataProvider invalidRolesDataProvider
     *
     * @param string $role
     */
    public function ajaxDeleteAction_without_supported_user_role_should_return_not_found_message($role)
    {
        $this->createSession($role);

        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
                "id": 5,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http:\/\/localhost\/rest\/v1\/users\/1"},
                  "vendor": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}}
                  },
                  "contacts": [
                    {
                      "value": "john@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/contacts\/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/delete/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Page not found."}',
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_same_as_logged_user_should_return_error()
    {
        $this->createSession('admin');

        // Facade::editUser()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_BAD_REQUEST,
            '{"message": "You can not edit yourself"}'
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/edit/1',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name": "Foo", "role": "user"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success": false, "message": "You can not edit yourself"}',
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            'GET',
            '/edit/1',
            'session_user=1',
            '{"name":"Foo","contacts":[{"value":null}],"role":"user"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_user_for_different_vendor_should_return_404()
    {

        $this->createSession('admin');

        // Facade::editUser()
        $this->addMockedResponse('ums', Response::HTTP_NOT_FOUND);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/edit/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name": "Foo", "role": "user"}'
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest('GET',
            '/edit/5',
            'session_user=1',
            '{"name":"Foo","contacts":[{"value":null}],"role":"user"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     * @dataProvider editInvalidDataProvider
     *
     * @param $UMSResponse
     * @param $requestData
     * @param $expectedError
     */
    public function ajaxEditAction_with_invalid_data_should_return_error(
        $UMSResponse,
        $requestData,
        $expectedError
    ) {
        $this->createSession('admin');

        // Facade::editUser()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_BAD_REQUEST,
            $UMSResponse
        );

        $this->addMockedResponse('ums', HttpResponse::HTTP_NO_CONTENT);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/edit/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            sprintf('{"success": false, "message": {%s}}', $expectedError),
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }

    public static function editInvalidDataProvider()
    {
        return [
            // 0)
            // name: empty
            [
                /** @lang JSON */
                '{
                  "message":{
                    "name": ["This value should not be blank."]
                  }
                }',
                /** @lang JSON */
                '{"name": "", "contact": "foo@.bar.com", "role": "user"}',
                '"name": ["This value should not be blank."]',
            ],
            // 1)
            // role: empty
            [
                /** @lang JSON */
                '{
                  "message":{
                    "role": ["This value should not be blank."]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "foo@.bar.com", "role": ""}',
                '"role": ["This value should not be blank."]',
            ],
            // 2)
            // name: too long
            [
                /** @lang JSON */
                '{
                  "message":{
                    "name": ["This value is too long. It should have 255 characters or less."]
                  }
                }',
                sprintf('{"name": "%s", "contact": "foo@.bar.com", "role": "user"}',
                    str_repeat('r', 256)),
                '"name": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 3)
            // role: not supported
            [
                /** @lang JSON */
                '{
                  "message":{
                    "role": ["This value is not valid."]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "foo@.bar.com", "role": "not_supported"}',
                '"role": ["This value is not valid."]',
            ],
            // 4)
            // contacts: empty value
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value should not be blank."]}]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "", "role": "user"}',
                '"contact": ["This value should not be blank."]',
            ],
            // 5)
            // contacts: invalid value
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value is not a valid email address."]}]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "invalid@mail@", "role": "user"}',
                '"contact": ["This value is not a valid email address."]',
            ],
            // 6)
            // contact: already taken
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value is already used."]}]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "foo@.bar.com", "role": "user"}',
                '"contact": ["This value is already used."]',
            ],
        ];
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_valid_data_without_changed_email_should_return_success()
    {
        $this->createSession('admin');

        // Facade::editUser()
        $this->addMockedResponse('ums', HttpResponse::HTTP_NO_CONTENT);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/edit/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name": "Foo", "role": "admin", "contact": "john@xf.com"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString('{"success": true}', $response->getContent());

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            'GET',
            '/edit/5',
            'session_user=1',
            '{"name":"Foo","contacts":[{"value":"john@xf.com"}],"role":"admin"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_valid_data_with_changed_email_should_return_success()
    {
        $this->createSession('admin');

        // Facade::editUser()
        $this->addMockedResponse(
            'ums',
            Response::HTTP_OK,
            '{"action_id": "9a66e06ded843e5848f14fccd6656d41"}'
        );

        // UMSUserRepository::find()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
                "id": 5,
                "name": "John Doe",
                "login": "johndo",
                "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                "salt": "b54109793ab030c94c40c5381891fd69",
                "role": "user",
                "activated": true,
                "enabled": false,
                "_links": {
                  "self": {"href": "http:\/\/localhost\/rest\/v1\/users\/1"},
                  "vendor": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}
                },
                "_embedded": {
                  "vendor": {
                    "id": 1,
                    "name": "X-F",
                    "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/vendors\/1"}}
                  },
                  "contacts": [
                    {
                      "value": "new_email@xf.com",
                      "type": "email",
                      "verified": true,
                      "id": 1,
                      "_links": {"self": {"href": "http:\/\/localhost\/rest\/v1\/contacts\/1"}},
                      "_embedded": {"contact_actions": []}
                    }
                  ],
                  "user_actions": []
                }
            }'
        );

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'GET',
            'user/edit/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name": "Foo", "role": "admin", "contact": "new_email@xf.com"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString('{"success": true}', $response->getContent());

        $this->assertCount(3, $this->getGuzzleHistory('ums'));
        $this->assertRequest(
            'GET',
            '/edit/5',
            'session_user=1',
            '{"name":"Foo","contacts":[{"value":"new_email@xf.com"}],"role":"admin"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
        $this->assertRequest(
            'GET',
            '/rest/v1/users/5',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 2)
        );

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message $message */
        $message = $mailCollector->getMessages()[0];
        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('noreply@x-formation.com', key($message->getFrom()));
        $this->assertSame('new_email@xf.com', key($message->getTo()));
        $this->assertSame('Please verify your X-Formation Connect email address', $message->getSubject());
        $this->assertContains(
            'http://localhost/action/9a66e06ded843e5848f14fccd6656d41',
            $message->getBody()
        );
        $this->assertNotContains(
            'scheme_and_host',
            $message->getBody()
        );
    }

    /**
     * @test
     */
    public function ajaxEditAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/edit/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Your session has expired. Please sign in again."}',
            $response->getContent()
        );

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     * @dataProvider invalidRolesDataProvider
     *
     * @param $role
     */
    public function ajaxEditAction_without_supported_user_role_should_return_not_found_message($role)
    {
        $this->createSession($role);;

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/edit/5',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Page not found."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     * @dataProvider invalidAddUserDataProvider
     *
     * @param $UMSResponse
     * @param $requestData
     * @param $expectedError
     */
    public function ajaxCreateAction_with_invalid_data_should_return_error(
        $UMSResponse,
        $requestData,
        $expectedError
    ) {
        $this->createSession('admin');

        // Facade::addUser()
        $this->addMockedResponse('ums', HttpResponse::HTTP_BAD_REQUEST, $UMSResponse);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/add',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            sprintf('{"success": false, "message": {%s}}', $expectedError),
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }

    /**
     * @return array
     */
    public static function invalidAddUserDataProvider()
    {
        return [
            // 0)
            // name: empty
            [
                /** @lang JSON */
                '{
                  "message":{
                    "name": ["This value should not be blank."]
                  }
                }',
                /** @lang JSON */
                '{"name": "", "contact": "foo@.bar.com", "role": "user"}',
                '"name": ["This value should not be blank."]',
            ],
            // 1)
            // name: too long
            [
                /** @lang JSON */
                '{
                  "message":{
                    "name": ["This value is too long. It should have 255 characters or less."]
                  }
                }',
                sprintf('{"name": "%s", "contact": "foo@.bar.com", "role": "user"}',
                    str_repeat('r', 256)),
                '"name": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 3)
            // contact: empty
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value should not be blank."]}]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "", "role": "user"}',
                '"contact": ["This value should not be blank."]',
            ],
            // 3)
            // contact: invalid email
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value is not a valid email address."]}]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "invalid@mail@", "role": "user"}',
                '"contact": ["This value is not a valid email address."]',
            ],
            // 4)
            // contact: too long (256 chars)
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value is too long. It should have 255 characters or less."]}]
                  }
                }',
                /** @lang JSON */
                sprintf('{"name": "Foo", "contact": "%s@foo.com", "role": "user"}',
                    str_repeat('b', 248)),
                '"contact": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 5)
            // role: empty
            [
                /** @lang JSON */
                '{
                  "message":{
                    "role": ["This value should not be blank."]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "foo@.bar.com", "role": ""}',
                '"role": ["This value should not be blank."]',
            ],
            // 6)
            // role: not supported
            [
                /** @lang JSON */
                '{
                  "message":{
                    "role": ["This value is not valid."]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "foo@.bar.com", "role": "not_supported"}',
                '"role": ["This value is not valid."]',
            ],
            // 7)
            // contact: already taken
            [
                /** @lang JSON */
                '{
                  "message":{
                    "contacts": [{"value": ["This value is already used."]}]
                  }
                }',
                /** @lang JSON */
                '{"name": "Foo", "contact": "foo@.bar.com", "role": "user"}',
                '"contact": ["This value is already used."]',
            ],
        ];
    }

    /**
     * @test
     */
    public function ajaxCreateAction_with_valid_data_should_return_success()
    {
        $this->createSession('admin');

        // Facade::addUser()
        $this->addMockedResponse('ums', HttpResponse::HTTP_OK, '{"user_id": 8}');

        // UMSUserRepository::find()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_OK,
            /** @lang JSON */
            '{
              "id": 8,
              "name": "John Doe",
              "login": "johndo",
              "password": "secret",
              "salt": "aa3c554286c46a74e08caf6e2111520c",
              "role": "admin",
              "activated": true,
              "enabled": true,
              "_links": {
                "self": {
                  "href": "http://localhost/rest/v1/users/8"
                },
                "vendor": {
                  "href": "http://localhost/rest/v1/vendors/2"
                }
              },
              "_embedded": {
                "vendor": {
                  "id": 2,
                  "name": "X-F2",
                  "_links": {
                    "self": {
                      "href": "http://localhost/rest/v1/vendors/2"
                    }
                  }
                },
                "contacts": [
                  {
                    "value": "john@xf.com",
                    "type": "email",
                    "verified": false,
                    "id": 2,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/contacts/2"
                      }
                    },
                    "_embedded": {
                      "contact_actions": [
                        {
                          "id": "14fe64b7e51dd33a656c8162e409684b",
                          "type": "contact_verify",
                          "expiration_time": "2020-02-04T06:05:15+0000",
                          "_links": {
                            "self": {
                              "href": "rest/v1/contact_actions/14fe64b7e51dd33a656c8162e409684b"
                            },
                            "contact": {
                              "href": "rest/v1/contacts/1"
                            }
                          }
                        }
                      ]
                    }
                  }
                ],
                "user_actions": [
                  {
                    "type": "account_activate",
                    "expiration_time": "2030-05-25T22:06:59+0000",
                    "id": "c6d4b0eee17378e401c363985f7d3eee",
                    "_links": {
                      "self": {"href": "rest/v1/user_actions/c6d4b0eee17378e401c363985f7d3eee"},
                      "user": {"href": "rest/v1/user/8"}
                    }
                  }
                ]
              }
            }'
        );

        // UMSSessionRepository::add()
        $this->addMockedResponse(
            'ums',
            HttpResponse::HTTP_CREATED,
            /** @lang JSON */
            '{"id": "9bd4c0e862ee28a24ced1da505b7af62","expiration_time":"2020-02-04T06:05:15+0000"}'
        );

        $client = $this->getTestHttpClient();
        $client->enableProfiler();
        $client->request(
            'GET',
            'user/add',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"name": "Foo", "contact": "john@xf.com", "role": "user"}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString('{"success": true}', $response->getContent());

        $this->assertCount(4, $this->getGuzzleHistory('ums'));

        $this->assertRequest(
            Request::METHOD_GET,
            '/add',
            'session_user=1',
            '{"name":"Foo","contacts":[{"value":"john@xf.com"}],"role":"user"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );

        $this->assertRequest(Request::METHOD_GET,
            '/rest/v1/users/8',
            '',
            '',
            $this->getGuzzleHistoryRequest('ums', 2)
        );
        $this->assertRequest(
            Request::METHOD_POST,
            '/rest/v1/sessions',
            '',
            '{"user":8,"product":"connect"}',
            $this->getGuzzleHistoryRequest('ums', 3)
        );

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message $message */
        $message = $mailCollector->getMessages()[0];
        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('noreply@x-formation.com', key($message->getFrom()));
        $this->assertSame('john@xf.com', key($message->getTo()));
        $this->assertSame('A new Connect account has been created for you', $message->getSubject());
        $this->assertContains(
            'http://localhost/action/c6d4b0eee17378e401c363985f7d3eee',
            $message->getBody()
        );
        $this->assertNotContains(
            'scheme_and_host',
            $message->getBody()
        );
    }

    /**
     * @test
     */
    public function ajaxCreateAction_without_authenticated_user_should_return_expired_session_message()
    {
        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/add',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"value":"john@xf.com","type":"email","user":6}'
        );

        $response = $client->getResponse();
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Your session has expired. Please sign in again."}',
            $response->getContent()
        );

        $this->assertCount(0, $this->getGuzzleHistory('ums'));
    }

    /**
     * @test
     * @dataProvider invalidRolesDataProvider
     *
     * @param $role
     */
    public function ajaxCreateAction_without_supported_user_role_should_return_not_found_message($role)
    {
        $this->createSession($role);

        $client = $this->getTestHttpClient();
        $client->request(
            'GET',
            'user/add',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            '{"value":"john@xf.com","type":"email","user":6}'
        );

        $response = $client->getResponse();

        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            '{"success":false,"message": "Page not found."}',
            $response->getContent()
        );

        $this->assertCount(1, $this->getGuzzleHistory('ums'));
    }

    public static function invalidRolesDataProvider()
    {
        return [
            ['some_role'],
            ['user'],
        ];
    }

    /**
     * @param Crawler $form
     * @param array $expectedErrors
     */
    private function assertFormErrors(Crawler $form, array $expectedErrors)
    {
        $fieldMap = [
            'name'             => 'user_name',
            'email'            => 'user_contact',
            'vendor'           => 'user_vendor',
            'password'         => 'user_password_first',
            'password_confirm' => 'user_password_second',
        ];

        foreach ($expectedErrors as $field => $error) {
            $errorElement = $form->filterXPath(
                sprintf('//div/input[@id=\'%s\']/following-sibling::p', $fieldMap[$field])
            );

            if (is_null($error)) {
                $this->assertCount(
                    0,
                    $errorElement,
                    sprintf('Failed assertion that field \'%s\' has no error.', $field)
                );
                continue;
            }

            $this->assertCount(
                1,
                $errorElement,
                sprintf('Failed assertion that field \'%s\' has error.', $field)
            );
            $this->assertSame(
                $error,
                $errorElement->text(),
                sprintf('Failed assertion that field \'%s\' has error: %s', $field, $error)
            );
        }
    }
}
