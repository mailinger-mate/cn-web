<?php

namespace Connect\Test\Functional\Bundle\UMSSecurityBundle\Controller;

use Connect\Test\Core\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VendorControllerTest extends WebTestCase
{
    /**
     * @test
     * @dataProvider validDataProvider
     *
     * @param $sessionUser
     * @param $expectedResponse
     */
    public function ajaxGetAction($sessionUser, $expectedResponse)
    {
        $this->addMockedResponse('ums', Response::HTTP_OK, $sessionUser);

        $this->createSession();

        $client = $this->getTestHttpClient();
        $client->request('GET', 'vendor');

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedResponse, $response->getContent());
    }

    /**
     * @return array
     */
    public static function validDataProvider()
    {
        return [
            // vendor without billing data
            [
                /** @lang JSON */
                '[
                  {
                    "id": 1,
                    "name": "John Doe",
                    "login": "johndo",
                    "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                    "salt": "b54109793ab030c94c40c5381891fd69",
                    "role": "admin",
                    "activated": true,
                    "enabled": false,
                    "_links": {
                      "self": {"href": "http://localhost/rest/v1/users/1"},
                      "vendor": {"href": "http://localhost/rest/v1/vendors/1"}
                    },
                    "_embedded": {
                      "vendor": {
                        "id": 1,
                        "name": "X-F",
                        "_links": {"self": {"href": "http://localhost/rest/v1/vendors/1"}}
                      },
                      "contacts": [
                        {
                          "value": "john@xf.com",
                          "type": "email",
                          "verified": true,
                          "id": 1,
                          "_links": {"self": {"href": "http://localhost/rest/v1/contacts/1"}},
                          "_embedded": {"contact_actions": []}
                        }
                      ],
                      "user_actions": []
                    }
                  }
                ]',
                /** @lang JSON */
                '{
                  "success": true,
                  "vendor": {
                        "name": "X-F"
                  }
                }',
            ],
            // vendor with billing data
            [
                /** @lang JSON */
                '[
                  {
                    "id": 1,
                    "name": "John Doe",
                    "login": "johndo",
                    "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                    "salt": "b54109793ab030c94c40c5381891fd69",
                    "role": "admin",
                    "activated": true,
                    "enabled": false,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/users/1"
                      },
                      "vendor": {
                        "href": "http://localhost/rest/v1/vendors/1"
                      }
                    },
                    "_embedded": {
                      "vendor": {
                        "id": 1,
                        "name": "X-F",
                        "billing_name": "X-Formation\\nPoland Sp. z o.o.",
                        "street": "Radzikowskiego 47A",
                        "city": "31-315 Kraków",
                        "state": "Małopolska",
                        "country": "PL",
                        "vat_number": "PL11223344",
                        "note": "New license",
                        "po_number": "#order-no: 112233",
                        "_links": {
                          "self": {
                            "href": "http://localhost/rest/v1/vendors/1"
                          }
                        }
                      },
                      "contacts": [
                        {
                          "value": "john@xf.com",
                          "type": "email",
                          "verified": true,
                          "id": 1,
                          "_links": {
                            "self": {
                              "href": "http://localhost/rest/v1/contacts/1"
                            }
                          },
                          "_embedded": {
                            "contact_actions": []
                          }
                        }
                      ],
                      "user_actions": []
                    }
                  }
                ]',
                /** @lang JSON */
                '{
                  "success": true,
                  "vendor": {
                    "name": "X-F",
                    "billing_name": "X-Formation\\nPoland Sp. z o.o.",
                    "street": "Radzikowskiego 47A",
                    "city": "31-315 Kraków",
                    "state": "Małopolska",
                    "country": "PL",
                    "vat_number": "PL11223344",
                    "note": "New license",
                    "po_number": "#order-no: 112233"
                  }
                }',
            ],
            // vendor with partial billing data
            [
                /** @lang JSON */
                '[
                  {
                    "id": 1,
                    "name": "John Doe",
                    "login": "johndo",
                    "password": "Rt+4KjFXDmjDQbE2Cub0Ok0LLqLORXdfA3XKrYKqHbxJHlXbbffnGVWnpoq0lOZBMjlSY6RwQom1MP9cbJzYBw==",
                    "salt": "b54109793ab030c94c40c5381891fd69",
                    "role": "admin",
                    "activated": true,
                    "enabled": false,
                    "_links": {
                      "self": {
                        "href": "http://localhost/rest/v1/users/1"
                      },
                      "vendor": {
                        "href": "http://localhost/rest/v1/vendors/1"
                      }
                    },
                    "_embedded": {
                      "vendor": {
                        "id": 1,
                        "name": "X-F",
                        "billing_name": "X-Formation\\nPoland Sp. z o.o.",
                        "street": "Radzikowskiego 47A",
                        "city": "31-315 Kraków",
                        "country": "PL",
                        "vat_number": "PL11223344",
                        "_links": {
                          "self": {
                            "href": "http://localhost/rest/v1/vendors/1"
                          }
                        }
                      },
                      "contacts": [
                        {
                          "value": "john@xf.com",
                          "type": "email",
                          "verified": true,
                          "id": 1,
                          "_links": {
                            "self": {
                              "href": "http://localhost/rest/v1/contacts/1"
                            }
                          },
                          "_embedded": {
                            "contact_actions": []
                          }
                        }
                      ],
                      "user_actions": []
                    }
                  }
                ]',
                /** @lang JSON */
                '{
                  "success": true,
                  "vendor": {
                    "name": "X-F",
                    "billing_name": "X-Formation\\nPoland Sp. z o.o.",
                    "street": "Radzikowskiego 47A",
                    "city": "31-315 Kraków",
                    "country": "PL",
                    "vat_number": "PL11223344"
                  }
                }',
            ],
        ];
    }

    /**
     * @test
     */
    public function ajaxEditAction_with_valid_data_should_return_success()
    {
        $this->createSession('admin');

        // Facade::editVendor()
        $this->addMockedResponse('ums', Response::HTTP_NO_CONTENT);

        $requestData = /** @lang JSON */
            '{
              "billing_name": "X-Formation\\nPoland Sp. z o.o.",
              "street": "Radzikowskiego 47A",
              "city": "31-315 Kraków",
              "state": "Małopolska",
              "country": "PL",
              "vat_number": "PL00112233",
              "note": "New license",
              "po_number": "#order-no: 112233"
            }';

        $client = $this->getTestHttpClient();
        $client->request(
            'POST',
            '/vendor/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"success": true}', $response->getContent());

        $this->assertCount(2, $this->getGuzzleHistory('ums'));

        $this->assertRequest(
            Request::METHOD_GET,
            '/vendor/edit/1',
            '',
            '{"billingName":"X-Formation\nPoland Sp. z o.o.","street":"Radzikowskiego 47A",' .
            '"city":"31-315 Krak\u00f3w","state":"Ma\u0142opolska","country":"PL",' .
            '"vatNumber":"PL00112233","note":"New license","poNumber":"#order-no: 112233"}',
            $this->getGuzzleHistoryRequest('ums', 1)
        );
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     *
     * @param $UMSResponse
     * @param $requestData
     * @param $expectedError
     */
    public function ajaxEditAction_with_invalid_data_should_return_error(
        $UMSResponse,
        array $requestData,
        $expectedError
    ) {
        $this->createSession('admin');

        // Facade::editVendor()
        $this->addMockedResponse(
            'ums',
            Response::HTTP_BAD_REQUEST,
            $UMSResponse
        );

        $client = $this->getTestHttpClient();
        $client->request(
            'POST',
            '/vendor/edit',
            [], [],
            ['HTTP_CONTENT_TYPE' => 'application/json'],
            json_encode($requestData)
        );

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
            sprintf('{"success": false, "message": {%s}}', $expectedError),
            $response->getContent()
        );

        $this->assertCount(2, $this->getGuzzleHistory('ums'));
    }

    /**
     * @return array
     */
    public static function invalidDataProvider()
    {
        $data = [
            'billing_name' => "X-Formation\nPoland Sp. z o.o.",
            'street'      => 'Radzikowskiego 47A',
            'city'        => '31-315 Kraków',
            'state'       => 'Małopolska',
            'country'     => 'PL',
            'vat_number'   => 'PL00112233',
            'note'        => 'New license',
            'po_number'    => '#order-no: 112233',
        ];

        return [
            // 0)
            // billing_name - empty
            [
                /** @lang JSON */
                '{"message": {"billingName": ["This value should not be blank."]}}',
                array_merge($data, ['billing_name' => '']),
                '"billing_name": ["This value should not be blank."]',
            ],
            // 1)
            // billing_name - missing
            [
                /** @lang JSON */
                '{"message": {"billingName": ["This value should not be blank."]}}',
                [
                    'street'    => 'Radzikowskiego 47A',
                    'city'      => '31-315 Kraków',
                    'state'     => 'Małopolska',
                    'country'   => 'PL',
                    'vat_number' => 'PL00112233',
                    'note'      => 'New license',
                    'po_number'  => '#order-no: 112233',
                ],
                '"billing_name": ["This value should not be blank."]',
            ],
            // 2)
            // name - too long
            [
                /** @lang JSON */
                '{"message": {"billingName": ["This value is too long. It should have 255 characters or less."]}}',
                array_merge($data, ['billing_name' => str_repeat('y', 256)]),
                '"billing_name": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 3)
            // street - empty
            [
                /** @lang JSON */
                '{"message": {"street": ["This value should not be blank."]}}',
                array_merge($data, ['street' => '']),
                '"street": ["This value should not be blank."]',
            ],
            // 4)
            // street - missing
            [
                /** @lang JSON */
                '{"message": {"street": ["This value should not be blank."]}}',
                [
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'city'        => '31-315 Kraków',
                    'state'       => 'Małopolska',
                    'country'     => 'PL',
                    'vat_number'   => 'PL00112233',
                    'note'        => 'New license',
                    'po_number'    => '#order-no: 112233',
                ],
                '"street": ["This value should not be blank."]',
            ],
            // 5)
            // street - too long
            [
                /** @lang JSON */
                '{"message": {"street": ["This value is too long. It should have 255 characters or less."]}}',
                array_merge($data, ['street' => str_repeat('y', 256)]),
                '"street": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 6)
            // city - empty
            [
                /** @lang JSON */
                '{"message": {"city": ["This value should not be blank."]}}',
                array_merge($data, ['city' => '']),
                '"city": ["This value should not be blank."]',
            ],
            // 7)
            // city - missing
            [
                /** @lang JSON */
                '{"message": {"city": ["This value should not be blank."]}}',
                [
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'street'      => 'Radzikowskiego 47A',
                    'state'       => 'Małopolska',
                    'country'     => 'PL',
                    'vat_number'   => 'PL00112233',
                    'note'        => 'New license',
                    'po_number'    => '#order-no: 112233',
                ],
                '"city": ["This value should not be blank."]',
            ],
            // 8)
            // city - too long
            [
                /** @lang JSON */
                '{"message": {"city": ["This value is too long. It should have 255 characters or less."]}}',
                array_merge($data, ['city' => str_repeat('y', 256)]),
                '"city": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 9)
            // state - too long
            [
                /** @lang JSON */
                '{"message": {"state": ["This value is too long. It should have 255 characters or less."]}}',
                array_merge($data, ['state' => str_repeat('y', 256)]),
                '"state": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 10)
            // country - empty
            [
                /** @lang JSON */
                '{"message": {"country": ["This value should not be blank."]}}',
                array_merge($data, ['country' => '']),
                '"country": ["This value should not be blank."]',
            ],
            // 11)
            // country - missing
            [
                /** @lang JSON */
                '{"message": {"country": ["This value should not be blank."]}}',
                [
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'street'      => 'Radzikowskiego 47A',
                    'city'        => '31-315 Kraków',
                    'state'       => 'Małopolska',
                    'vat_number'   => 'PL00112233',
                    'note'        => 'New license',
                    'po_number'    => '#order-no: 112233',
                ],
                '"country": ["This value should not be blank."]',
            ],
            // 12)
            // country - too long
            [
                /** @lang JSON */
                '{"message": {"country": ["This value is too long. It should have 255 characters or less.", "This value is not a valid country."]}}',
                array_merge($data, ['country' => str_repeat('y', 256)]),
                '"country": ["This value is too long. It should have 255 characters or less.", "This value is not a valid country."]',
            ],
            // 13)
            // country - not existing code
            [
                /** @lang JSON */
                '{"message": {"country": ["This value is not a valid country."]}}',
                array_merge($data, ['country' => 'XX']),
                '"country": ["This value is not a valid country."]',
            ],
            // 14)
            // vat_number - empty
            [
                /** @lang JSON */
                '{"message": {"vatNumber": ["This value should not be blank.", "This value is not a valid VAT number."]}}',
                array_merge($data, ['vat_number' => '']),
                '"vat_number": ["This value should not be blank.", "This value is not a valid VAT number."]',
            ],
            // 15)
            // vat_number - missing
            [
                /** @lang JSON */
                '{"message": {"vatNumber": ["This value should not be blank.", "This value is not a valid VAT number."]}}',
                [
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'street'      => 'Radzikowskiego 47A',
                    'city'        => '31-315 Kraków',
                    'state'       => 'Małopolska',
                    'country'     => 'PL',
                    'note'        => 'New license',
                    'po_number'    => '#order-no: 112233',
                ],
                '"vat_number": ["This value should not be blank.", "This value is not a valid VAT number."]',
            ],
            // 16)
            // vat_number - invalid
            [
                /** @lang JSON */
                '{"message": {"vatNumber": ["This value is not a valid VAT number."]}}',
                [
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'street'      => 'Radzikowskiego 47A',
                    'city'        => '31-315 Kraków',
                    'state'       => 'Małopolska',
                    'country'     => 'PL',
                    'vat_number'   => 'invalid-number',
                    'note'        => 'New license',
                    'po_number'    => '#order-no: 112233',
                ],
                '"vat_number": ["This value is not a valid VAT number."]',
            ],
            // 17)
            // vat_number - too long
            [
                /** @lang JSON */
                '{"message": {"vatNumber": ["This value is too long. It should have 255 characters or less.", "This value is not a valid VAT number."]}}',
                array_merge($data, ['vat_number' => str_repeat('y', 256)]),
                '"vat_number": ["This value is too long. It should have 255 characters or less.", "This value is not a valid VAT number."]',
            ],
            // 18)
            // note - too long
            [
                /** @lang JSON */
                '{"message": {"note": ["This value is too long. It should have 255 characters or less."]}}',
                array_merge($data, ['note' => str_repeat('y', 256)]),
                '"note": ["This value is too long. It should have 255 characters or less."]',
            ],
            // 19)
            // po_number - too long
            [
                /** @lang JSON */
                '{"message": {"poNumber": ["This value is too long. It should have 255 characters or less."]}}',
                array_merge($data, ['po_number' => str_repeat('y', 256)]),
                '"po_number": ["This value is too long. It should have 255 characters or less."]',
            ],
        ];
    }
}
