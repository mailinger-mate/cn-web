<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Core\Client;

use Connect\Bundle\AppBundle\Core\Client\CredentialsProvider;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class CredentialsProviderTest
 */
class CredentialsProviderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getCoreTokenValue_should_return_ums_session_value()
    {
        $userTokenValue = 'some_user_token';

        /** @var SessionInterface $session */
        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->andReturn(true)
            ->shouldReceive('get')
            ->with('ums_session')
            ->andReturn($userTokenValue)
            ->getMock();

        /** @var UMSUser $user */

        $this->assertSame($userTokenValue, (new CredentialsProvider($session))->getCoreTokenValue());
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @expectedExceptionMessage Missing UMS session id
     */
    public function getCoreTokenValue_without_ums_session_should_throw_exception()
    {
        /** @var SessionInterface $session */
        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->andReturn(false)
            ->getMock();

        (new CredentialsProvider($session))->getCoreTokenValue();
    }
}