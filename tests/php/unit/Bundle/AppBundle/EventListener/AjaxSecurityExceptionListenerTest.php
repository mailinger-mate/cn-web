<?php

namespace Connect\Test\Unit\Bundle\AppBundle\EventListener;

use Connect\Bundle\AppBundle\EventListener\AjaxSecurityExceptionListener;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SecurityControllerListenerTest
 */
class AjaxSecurityExceptionListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onKernelException_without_ajax_request_should_not_do_anything()
    {
        $jsonEncoder = Mockery::mock(JsonEncoder::class);
        $translator = Mockery::mock(TranslatorInterface::class);
        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class);

        $request = Mockery::mock(Request::class)
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn(false)
            ->getMock()

            ->shouldReceive('getContentType')
            ->andReturn('not_json')
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();

        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var AuthorizationChecker $authChecker */
        /** @var GetResponseForExceptionEvent $event */
        (new AjaxSecurityExceptionListener($jsonEncoder, $translator, $authChecker))->onKernelException($event);
    }

    /**
     * @test
     * @dataProvider ajaxRequestParamsProvider
     *
     * @param $isXmlHttpRequest
     * @param $isJsonContent
     */
    public function onKernelException_with_ajax_request_and_without_proper_exception_should_not_do_anything($isXmlHttpRequest, $isJsonContent)
    {
        $jsonEncoder = Mockery::mock(JsonEncoder::class);
        $translator = Mockery::mock(TranslatorInterface::class);
        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class);

        $request = Mockery::mock(Request::class)
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn($isXmlHttpRequest)
            ->getMock()

            ->shouldReceive('getContentType')
            ->andReturn($isJsonContent ? 'json' : 'not_json')
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock()

            ->shouldReceive('getException')
            ->andReturn(Mockery::mock(\Exception::class))
            ->getMock();

        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var AuthorizationChecker $authChecker */
        /** @var GetResponseForExceptionEvent $event */
        (new AjaxSecurityExceptionListener($jsonEncoder, $translator, $authChecker))->onKernelException($event);
    }

    /**
     * @test
     * @dataProvider ajaxRequestParamsProvider
     *
     * @param $isXmlHttpRequest
     * @param $isJsonContent
     */
    public function onKernelException_with_ajax_request_and_access_denied_exception_and_not_authenticated_user_should_set_response($isXmlHttpRequest, $isJsonContent)
    {
        $message = 'some_message';
        $responseContent = ['success' => false, 'message' => $message];

        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with('message.error.session_expired')
            ->andReturn($message)
            ->getMock();

        $jsonEncoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($responseContent, JsonEncoder::FORMAT)
            ->andReturn($isJsonContent ? 'json' : 'not_json')
            ->getMock();

        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(false)
            ->getMock();

        $request = Mockery::mock(Request::class)
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn($isXmlHttpRequest)
            ->getMock()

            ->shouldReceive('getContentType')
            ->andReturn($isJsonContent ? 'json' : 'not_json')
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock()

            ->shouldReceive('getException')
            ->andReturn(Mockery::mock(AccessDeniedException::class))
            ->getMock()

            ->shouldReceive('setResponse')
            ->with(Mockery::on(function (Response $response) use ($isJsonContent) {
                $this->assertSame($isJsonContent ? 'json' : 'not_json', $response->getContent());
                $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
                $this->assertSame(Response::HTTP_UNAUTHORIZED, $response->headers->get('X-Status-Code'));

                return true;
            }))
            ->getMock();

        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var GetResponseForExceptionEvent $event */
        (new AjaxSecurityExceptionListener($jsonEncoder, $translator, $authChecker))->onKernelException($event);
    }

    /**
     * @test
     * @dataProvider ajaxRequestParamsProvider
     *
     * @param $isXmlHttpRequest
     * @param $isJsonContent
     */
    public function onKernelException_with_ajax_request_and_access_denied_exception_and_authenticated_user_should_set_response($isXmlHttpRequest, $isJsonContent)
    {
        $message = 'some_message';
        $responseContent = ['success' => false, 'message' => $message];

        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with('message.error.not_found')
            ->andReturn($message)
            ->getMock();

        $jsonEncoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($responseContent, JsonEncoder::FORMAT)
            ->andReturn($isJsonContent ? 'json' : 'not_json')
            ->getMock();

        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(true)
            ->getMock();

        $request = Mockery::mock(Request::class)
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn($isXmlHttpRequest)
            ->getMock()

            ->shouldReceive('getContentType')
            ->andReturn($isJsonContent ? 'json' : 'not_json')
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock()

            ->shouldReceive('getException')
            ->andReturn(Mockery::mock(AccessDeniedException::class))
            ->getMock()

            ->shouldReceive('setResponse')
            ->with(Mockery::on(function (Response $response) use ($isJsonContent) {
                $this->assertSame($isJsonContent ? 'json' : 'not_json', $response->getContent());
                $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
                $this->assertSame(Response::HTTP_NOT_FOUND, $response->headers->get('X-Status-Code'));

                return true;
            }))
            ->getMock();

        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var AuthorizationChecker $authChecker */
        /** @var GetResponseForExceptionEvent $event */
        (new AjaxSecurityExceptionListener($jsonEncoder, $translator, $authChecker))->onKernelException($event);
    }

    public static function ajaxRequestParamsProvider()
    {
        return [
            [true, true],
            [true, false],
            [false, true],
        ];
    }
}
