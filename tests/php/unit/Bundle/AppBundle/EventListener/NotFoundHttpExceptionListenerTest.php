<?php

namespace Connect\Test\Unit\Bundle\AppBundle\EventListener;

use Connect\Bundle\AppBundle\EventListener\NotFoundHttpExceptionListener;
use Exception;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SecurityControllerListenerTest
 */
class NotFoundHttpExceptionListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onKernelException_without_supported_exception_should_not_do_anything()
    {
        $logger = Mockery::mock(LoggerInterface::class);
        $kernel = Mockery::mock(KernelInterface::class);
        $twig = Mockery::mock(EngineInterface::class);
        $jsonEncoder = Mockery::mock(JsonEncoder::class);
        $translator = Mockery::mock(TranslatorInterface::class);

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getException')
            ->andReturn(new Exception('some exception'))
            ->getMock();

        /** @var LoggerInterface $logger */
        /** @var KernelInterface $kernel */
        /** @var EngineInterface $twig */
        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var GetResponseForExceptionEvent $event */
        (new NotFoundHttpExceptionListener($logger, $kernel, $twig, $jsonEncoder, $translator))->onKernelException($event);
    }

    /**
     * @test
     */
    public function onKernelException_with_supported_exception_without_prod_env_should_not_do_anything()
    {
        $logger = Mockery::mock(LoggerInterface::class);
        $twig = Mockery::mock(EngineInterface::class);
        $jsonEncoder = Mockery::mock(JsonEncoder::class);
        $translator = Mockery::mock(TranslatorInterface::class);

        $kernel = Mockery::mock(KernelInterface::class)
            ->shouldReceive('getEnvironment')
            ->andReturn('dev')
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getException')
            ->andReturn(new NotFoundHttpException('some exception'))
            ->getMock();

        /** @var LoggerInterface $logger */
        /** @var KernelInterface $kernel */
        /** @var EngineInterface $twig */
        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var GetResponseForExceptionEvent $event */
        (new NotFoundHttpExceptionListener($logger, $kernel, $twig, $jsonEncoder, $translator))->onKernelException($event);
    }

    /**
     * @test
     */
    public function onKernelException_with_supported_exception_and_prod_env_should_log_info_and_set_response_for_non_json_content_type()
    {
        $exceptionMessage = 'some exception';
        $content = 'rendered string';

        $logger = Mockery::mock(LoggerInterface::class)
            ->shouldReceive('info')
            ->with($exceptionMessage)
            ->getMock();

        $twig = Mockery::mock(EngineInterface::class)
            ->shouldReceive('render')
            ->with('@Twig/Exception/error404.html.twig')
            ->andReturn($content)
            ->getMock();

        $kernel = Mockery::mock(KernelInterface::class)
            ->shouldReceive('getEnvironment')
            ->andReturn('prod')
            ->getMock();

        $request = Mockery::mock(Request::class)
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn(false)
            ->getMock()

            ->shouldReceive('getContentType')
            ->andReturn(false)
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getException')
            ->andReturn(new NotFoundHttpException($exceptionMessage))
            ->getMock()

            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock()

            ->shouldReceive('setResponse')
            ->with(Mockery::on(function (Response $response) use ($content) {
                $this->assertSame($content, $response->getContent());
                $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
                $this->assertSame(Response::HTTP_NOT_FOUND, $response->headers->get('X-Status-Code'));

                return true;
            }))
            ->getMock();

        $jsonEncoder = Mockery::mock(JsonEncoder::class);
        $translator = Mockery::mock(TranslatorInterface::class);

        /** @var LoggerInterface $logger */
        /** @var KernelInterface $kernel */
        /** @var EngineInterface $twig */
        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var GetResponseForExceptionEvent $event */
        (new NotFoundHttpExceptionListener($logger, $kernel, $twig, $jsonEncoder, $translator))->onKernelException($event);
    }

    /**
     * @test
     * @dataProvider ajaxContentTypeDataProvider
     *
     * @param $isXmlHttp
     * @param $isJson
     */
    public function onKernelException_with_supported_exception_and_prod_env_should_log_info_and_set_response_for_json_content_type($isXmlHttp, $isJson)
    {
        $exceptionMessage = 'some exception';
        $translatedMessage = 'some not found message';
        $content = '{"success":false,"message":"' . $translatedMessage . '"}';

        $logger = Mockery::mock(LoggerInterface::class)
            ->shouldReceive('info')
            ->with($exceptionMessage)
            ->getMock();

        $twig = Mockery::mock(EngineInterface::class)
            ->shouldReceive('render')
            ->with('@Twig/Exception/error404.html.twig')
            ->andReturn($content)
            ->getMock();

        $kernel = Mockery::mock(KernelInterface::class)
            ->shouldReceive('getEnvironment')
            ->andReturn('prod')
            ->getMock();

        $request = Mockery::mock(Request::class)
            ->shouldReceive('isXmlHttpRequest')
            ->andReturn($isXmlHttp)
            ->getMock()

            ->shouldReceive('getContentType')
            ->andReturn($isJson ? 'json' : 'non-json')
            ->getMock();

        $event = Mockery::mock(GetResponseForExceptionEvent::class)
            ->shouldReceive('getException')
            ->andReturn(new NotFoundHttpException($exceptionMessage))
            ->getMock()

            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock()

            ->shouldReceive('setResponse')
            ->with(Mockery::on(function (Response $response) use ($content) {
                $this->assertSame($content, $response->getContent());
                $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
                $this->assertSame(Response::HTTP_NOT_FOUND, $response->headers->get('X-Status-Code'));

                return true;
            }))
            ->getMock();

        $jsonEncoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->once()
            ->with(['success' => false, 'message' => $translatedMessage], JsonEncoder::FORMAT)
            ->andReturn($content)
            ->getMock();

        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->once()
            ->with('message.error.not_found')
            ->andReturn($translatedMessage)
            ->getMock();

        /** @var LoggerInterface $logger */
        /** @var KernelInterface $kernel */
        /** @var EngineInterface $twig */
        /** @var JsonEncoder $jsonEncoder */
        /** @var TranslatorInterface $translator */
        /** @var GetResponseForExceptionEvent $event */
        (new NotFoundHttpExceptionListener($logger, $kernel, $twig, $jsonEncoder, $translator))->onKernelException($event);
    }

    public static function ajaxContentTypeDataProvider()
    {
        return [
            [true, true],
            [false, true],
            [true, false]
        ];
    }
}
