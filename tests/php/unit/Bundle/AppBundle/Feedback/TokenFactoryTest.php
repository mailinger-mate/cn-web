<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Feedback;

use Connect\Bundle\AppBundle\Feedback\TokenFactory;
use Connect\Domain\Contact;
use Connect\Domain\Token;
use Connect\Domain\User;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class TokenFactoryTest
 */
class TokenFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function create()
    {
        /** @var Contact $contact */
        $contact = Mockery::mock(Contact::class)
            ->shouldReceive('getValue')
            ->once()
            ->andReturn('email@test.com')
            ->getMock()

            ->shouldReceive('isVerified')
            ->once()
            ->andReturn(true)
            ->getMock();

        /** @var User $user */
        $user = Mockery::mock(User::class)
            ->shouldReceive('getPrimaryContact')
            ->once()
            ->andReturn($contact)
            ->getMock()

            ->shouldReceive('getId')
            ->once()
            ->andReturn(1002)
            ->getMock()

            ->shouldReceive('getName')
            ->once()
            ->andReturn('user name')
            ->getMock();

        $token = (new TokenFactory('api_key', 'project_key'))->create($user);

        $this->assertTrue(is_string($token));
        $this->assertFalse(empty($token));
    }
}