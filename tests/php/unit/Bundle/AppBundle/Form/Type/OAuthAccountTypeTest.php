<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Form\Type;

use Connect\Bundle\AppBundle\Form\Type\OAuthAccountType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class OAuthAccountTypeTest
 */
class OAuthAccountTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_set_fields_and_constraints()
    {
        /** @var FormBuilderInterface $builder */
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('name', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new Length(array('max' => 255))
                ]
            ])
            ->andReturnSelf()
            ->getMock();

        (new OAuthAccountType())->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function setDefaultOptions_should_configure_proper_values()
    {
        $resolver = Mockery::mock(OptionsResolverInterface::class)
            ->shouldReceive('setDefaults')
            ->with([
                'csrf_protection'   => false,
            ])
            ->getMock();


        /** @var OptionsResolverInterface $resolver */
        (new OAuthAccountType())->setDefaultOptions($resolver);
    }

    /**
     * @test
     */
    public function getName_should_return_type_name()
    {
        $this->assertSame('account', (new OAuthAccountType())->getName());
    }
}
