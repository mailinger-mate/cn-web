<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Form\Type;

use Connect\Bundle\AppBundle\Form\Type\SalesforceType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

/**
 * Class SalesforceTypeTest
 */
class SalesforceTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_set_fields_and_constraints()
    {
        /** @var FormBuilderInterface $builder */
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('name', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new Length(array('max' => 255))
                ]
            ])
            ->andReturnSelf()
            ->getMock()

            ->shouldReceive('add')
            ->with('login_endpoint', 'text', [
                'constraints' => [
                    new NotBlank(),
                    new Url(),
                    new Length(['max' => 255])
                ]
            ])
            ->andReturnSelf()
            ->getMock();

        (new SalesforceType())->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function setDefaultOptions_should_configure_proper_values()
    {
        $resolver = Mockery::mock(OptionsResolverInterface::class)
            ->shouldReceive('setDefaults')
            ->with([
                'csrf_protection'   => false,
            ])
            ->getMock();


        /** @var OptionsResolverInterface $resolver */
        (new SalesforceType())->setDefaultOptions($resolver);
    }

    /**
     * @test
     */
    public function getName_should_return_type_name()
    {
        $this->assertSame('account', (new SalesforceType())->getName());
    }
}
