<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Form\Type;

use Connect\Bundle\AppBundle\Form\Type\OAuthAccountType;
use Connect\Bundle\AppBundle\Form\Type\SalesforceType;
use Connect\Bundle\AppBundle\Form\Type\TypeFactory;
use Connect\Bundle\AppBundle\OAuth\App\Type;
use InvalidArgumentException;
use PHPUnit_Framework_TestCase;
use ReflectionClass;

/**
 * Class SalesforceTypeTest
 */
class TypeFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Not supported app type: some_type
     */
    public function create_with_not_supported_app_type()
    {
        TypeFactory::create('some_type');
    }

    /**
     * @test
     * @dataProvider appTypesDataProvider
     *
     * @param string $appType
     * @param string $formTypeClass
     */
    public function create_with_supported_app_type($appType, $formTypeClass)
    {
        $this->assertInstanceOf($formTypeClass, TypeFactory::create($appType));
    }

    public static function appTypesDataProvider()
    {
        $appTypes = [
            [Type::SALESFORCE, SalesforceType::class],
            [Type::SLACK, OAuthAccountType::class],
            [Type::TWITTER, OAuthAccountType::class],
        ];

        self::assertCount(count($appTypes), (new ReflectionClass(Type::class))->getConstants());

        return $appTypes;
    }
}
