<?php

namespace Connect\Test\Unit\Bundle\AppBundle\OAuth\App;

use Connect\Bundle\AppBundle\OAuth\App\AppAbstract;
use Connect\Domain\Account;
use InvalidArgumentException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Routing\Router;

/**
 * Class AppAbstractTest
 */
class AppAbstractTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Missing required field in account data: name
     */
    public function populateAccount_without_name_in_data_should_throw_exception()
    {
        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var AppAbstract $appAbstract */
        $appAbstract = Mockery::mock(
            AppAbstract::class . '[getAuthorizationUrl,getType,validateCallback,handleCallback]',
            [Mockery::mock(Router::class)]
        );

        $appAbstract->populateAccount($account, ['missing_name_key' => 'test']);
    }

    /**
     * @test
     */
    public function populateAccount_with_name_in_data_should_set_name_in_account()
    {
        $accountName = 'some_name';

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('setName')
            ->once()
            ->with($accountName)
            ->andReturnSelf()
            ->getMock();

        /** @var AppAbstract $appAbstract */
        $appAbstract = Mockery::mock(
            AppAbstract::class . '[getAuthorizationUrl,getType,validateCallback,handleCallback]',
            [Mockery::mock(Router::class)]
        );

        $appAbstract->populateAccount($account, ['name' => $accountName]);
    }

    /**
     * @test
     */
    public function initializeCallback_should_not_modify_account()
    {
        /** @var Account $account */
        $account = Mockery::mock(Account::class);
        $expectedAccount = clone $account;

        /** @var AppAbstract $appAbstract */
        $appAbstract = Mockery::mock(
            AppAbstract::class . '[getAuthorizationUrl,getType,validateCallback,handleCallback]',
            [Mockery::mock(Router::class)]
        );

        $appAbstract->initializeCallback($account);

        $this->assertEquals($expectedAccount, $account);
    }
}