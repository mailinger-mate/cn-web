<?php

namespace Connect\Test\Unit\Bundle\AppBundle\OAuth\App;

use Connect\Bundle\AppBundle\OAuth\App\Salesforce;
use Connect\Bundle\AppBundle\OAuth\App\Type;
use Connect\Domain\Account;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use LogicException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class TwitterTest
 */
class SalesforceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getType_should_return_salesforce()
    {
        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = (new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret'));

        $this->assertSame(Type::SALESFORCE, $salesforce->getType());
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Login endpoint cannot be empty
     */
    public function getAuthorizationUrl_with_empty_endpoint_should_throw_exception()
    {
        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('login_endpoint')
            ->andReturn('')
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->getAuthorizationUrl($account);
    }

    /**
     * @test
     */
    public function getAuthorizationUrl_with_non_empty_endpoint_should_return_url()
    {
        $loginEndpoint = 'http://loginEndpoint';
        $clientId = 'client_id';
        $clientSecret = 'client_secret';
        $callbackUrl = 'some_callback_url';

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('login_endpoint')
            ->andReturn($loginEndpoint)
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->andReturn($callbackUrl)
            ->getMock();

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);
        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, $clientId, $clientSecret);

        $this->assertSame(
            $loginEndpoint . "/services/oauth2/authorize"
                . "?response_type=code"
                . "&client_id=$clientId"
                . "&redirect_uri=$callbackUrl"
                . "&prompt=consent",
            $salesforce->getAuthorizationUrl($account)
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Missing required field in account data: name
     */
    public function populateAccount_without_required_name_field_should_throw_exception()
    {
        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->populateAccount($account, []);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Missing required field in account data: login_endpoint
     */
    public function populateAccount_without_required_login_endpoint_field_should_throw_exception()
    {
        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('setName')
            ->once()
            ->with('some_name')
            ->andReturnSelf()
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->populateAccount($account, ['name' => 'some_name']);
    }

    /**
     * @test
     */
    public function populateAccount_with_required_fields_should_set_values_in_account()
    {
        $name = 'some_name';
        $loginEndpoint = 'some_endpoint';

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('setName')
            ->once()
            ->with($name)
            ->andReturnSelf()
            ->getMock()

            ->shouldReceive('setConfig')
            ->once()
            ->with(['login_endpoint' => $loginEndpoint])
            ->andReturnSelf()
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->populateAccount($account, [
            'name' => $name,
            'login_endpoint' => $loginEndpoint
        ]);
    }

    /**
     * @test
     */
    public function initializeCallback_should_not_modify_account()
    {
        /** @var Account $account */
        $account = Mockery::mock(Account::class);
        $expectedAccount = clone $account;

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');

        $salesforce->initializeCallback($account);

        $this->assertEquals($expectedAccount, $account);
    }

    /**
     * @test
     * @expectedException \Connect\Bundle\AppBundle\OAuth\App\AppException
     * @expectedExceptionMessage Salesforce denied access in callback request
     */
    public function validateCallback_with_access_denied_error_param_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturn('access_denied')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException \Connect\Bundle\AppBundle\OAuth\App\AppException
     * @expectedExceptionMessage Salesforce error returned in callback request: some_error
     */
    public function validateCallback_with_not_null_error_param_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturn('some_error')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Code callback param cannot be empty
     */
    public function validateCallback_with_empty_code_param_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturn(null)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturn(null)
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     */
    public function validateCallback_with_valid_data_should_not_throw_any_exceptions()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturn(null)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturn('some_code')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class);

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class);

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, 'client_id', 'client_secret');
        $salesforce->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     */
    public function handleCallback_should_set_proper_access_tokens_in_account_config()
    {
        $loginEndpoint = 'http://loginEndpoint';
        $refreshToken = 'some_refresh_token';
        $code = 'some_code';
        $callbackUrl = 'some_callback_url';
        $clientId = 'client_id';
        $clientSecret = 'client_secret';
        $responseBodyContents = 'some_response_body_contents';

        $expectedConfigArray = [
            'login_endpoint'    =>  $loginEndpoint,
            'refresh_token'     =>  $refreshToken
        ];

        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturn($code)
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->twice()
            ->with('login_endpoint')
            ->andReturn($loginEndpoint)
            ->getMock()

            ->shouldReceive('setConfig')
            ->once()
            ->with($expectedConfigArray)
            ->andReturnSelf()
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->andReturn($callbackUrl)
            ->getMock();

        /** @var StreamInterface $body */
        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->once()
            ->andReturn($responseBodyContents)
            ->getMock();

        /** @var Response $authResponse */
        $authResponse = Mockery::mock(Response::class)
            ->shouldReceive('getBody')
            ->once()
            ->andReturn($body)
            ->getMock();

        /** @var JsonEncoder $jsonEncoder */
        $jsonEncoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->once()
            ->with($responseBodyContents, JsonEncoder::FORMAT)
            ->andReturn(['refresh_token' => $refreshToken])
            ->getMock();

        /** @var Client $httpClient */
        $httpClient = Mockery::mock(Client::class)
            ->shouldReceive('post')
            ->once()
            ->with(
                $loginEndpoint . '/services/oauth2/token',
                [
                    'form_params'   => [
                        'grant_type'    => 'authorization_code',
                        'client_id'     => $clientId,
                        'client_secret' => $clientSecret,
                        'redirect_uri'  => $callbackUrl,
                        'code'          => $code,
                    ]
                ]
            )
            ->andReturn($authResponse)
            ->getMock();

        $salesforce = new Salesforce($router, $jsonEncoder, $httpClient, $clientId, $clientSecret);
        $salesforce->handleCallback($callbackRequest, $account);
    }
}
