<?php

namespace Connect\Test\Unit\Bundle\AppBundle\OAuth\App;

use Bramdevries\Oauth\Client\Provider\Slack as SlackClient;
use Connect\Bundle\AppBundle\OAuth\App\Slack;
use Connect\Bundle\AppBundle\OAuth\App\Type;
use Connect\Domain\Account;
use League\OAuth2\Client\Token\AccessToken;
use LogicException;
use Mockery;
use PHPUnit_Framework_TestCase;
use RandomLib\Factory;
use RandomLib\Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SlackTest
 */
class SlackTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getType_should_return_slack()
    {
        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        $this->assertSame(Type::SLACK, (new Slack($router, $slackClient))->getType());
    }

    /**
     * @test
     */
    public function initializeCallback_should_set_proper_state_value_in_account_config()
    {
        $stateTokenValueTokenValue = 'some_random_string_with_32_chars';

        $expectedConfigArray = [
            'state' => $stateTokenValueTokenValue
        ];

        $randomGenerator = Mockery::mock(Generator::class)
            ->shouldReceive('generateString')
            ->once()
            ->with(32)
            ->andReturn($stateTokenValueTokenValue)
            ->getMock();

        $randomFactory = Mockery::mock(Factory::class)
            ->shouldReceive('getMediumStrengthGenerator')
            ->once()
            ->andReturn($randomGenerator)
            ->getMock();

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class)
            ->shouldReceive('getRandomFactory')
            ->once()
            ->andReturn($randomFactory)
            ->getMock();

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('setConfig')
            ->once()
            ->with($expectedConfigArray)
            ->andReturnSelf()
            ->getMock();

        (new Slack($router, $slackClient))->initializeCallback($account);
    }

    /**
     * @test
     */
    public function getAuthorizationUrl_should_return_url()
    {
        $expectedUrl = 'some_url';
        $stateTokenValue = 'some_state';

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class)
            ->shouldReceive('getAuthorizationUrl')
            ->once()
            ->with([
                    'state' => $stateTokenValue,
                    'scope' => [
                        'channels:read',
                        'users:read',
                        'im:write',
                        'chat:write:user',
                        'chat:write:bot',
                        'files:write:user',
                    ]
            ])
            ->andReturn($expectedUrl)
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('state')
            ->andReturn($stateTokenValue)
            ->getMock();

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        $this->assertSame($expectedUrl, (new Slack($router, $slackClient))->getAuthorizationUrl($account));
    }

    /**
     * @test
     * @expectedException \Connect\Bundle\AppBundle\OAuth\App\AppException
     * @expectedExceptionMessage Slack denied access in callback request
     */
    public function validateCallback_with_error_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturn('some_error')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Callback OAuth state cannot be empty
     */
    public function validateCallback_with_empty_state_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('state')
            ->andReturnNull()
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Request states mismatch
     */
    public function validateCallback_with_not_matching_states_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('state')
            ->andReturn('some_state_1')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('state')
            ->andReturn('some_state_2')
            ->getMock();

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage OAuth authorization code cannot be empty
     */
    public function validateCallback_with_empty_authorization_code_should_throw_exception()
    {
        $stateTokenValue = 'some_state';

        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('state')
            ->andReturn($stateTokenValue)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturnNull()
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('state')
            ->andReturn($stateTokenValue)
            ->getMock();

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     */
    public function validateCallback_with_valid_data_should_not_throw_any_exceptions()
    {
        $stateTokenValue = 'some_state';

        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('error')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('state')
            ->andReturn($stateTokenValue)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturn('some_code')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('state')
            ->andReturn($stateTokenValue)
            ->getMock();

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage OAuth authorization code cannot be empty
     */
    public function handleCallback_with_empty_authorization_code_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturnNull()
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class);

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->handleCallback($callbackRequest, $account);
    }

    /**
     * @test
     */
    public function handleCallback_should_set_proper_access_token_in_account_config()
    {
        $codeTokenValue = 'some_code_token';
        $accessTokenValue = 'some_access_token';

        $expectedConfigArray = [
            'access_token'        =>  $accessTokenValue
        ];

        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('code')
            ->andReturn($codeTokenValue)
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('setConfig')
            ->once()
            ->with($expectedConfigArray)
            ->andReturnSelf()
            ->getMock();

        /** @var SlackClient $slackClient */
        $accessToken = Mockery::mock(AccessToken::class)
            ->shouldReceive('getToken')
            ->once()
            ->andReturn($accessTokenValue)
            ->getMock();

        /** @var SlackClient $slackClient */
        $slackClient = Mockery::mock(SlackClient::class)
            ->shouldReceive('getAccessToken')
            ->once()
            ->with('authorization_code', ['code' => $codeTokenValue])
            ->andReturn($accessToken)
            ->getMock();

        /** @var RouterInterface $router */
        $router = $this->getRouterMock();

        (new Slack($router, $slackClient))->handleCallback($callbackRequest, $account);
    }

    /**
     * @return RouterInterface
     */
    private function getRouterMock()
    {
        return Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->with(
                'app_oauth_callback',
                ['app' => Type::SLACK],
                RouterInterface::ABSOLUTE_URL
            )
            ->andReturn('some_callback_url')
            ->getMock();
    }
}
