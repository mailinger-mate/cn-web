<?php

namespace Connect\Test\Unit\Bundle\AppBundle\OAuth\App;

use Abraham\TwitterOAuth\TwitterOAuth;
use Connect\Bundle\AppBundle\OAuth\App\Twitter;
use Connect\Bundle\AppBundle\OAuth\App\Type;
use Connect\Domain\Account;
use LogicException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class TwitterTest
 */
class TwitterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getType_should_return_twitter()
    {
        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        $twitter = (new Twitter($router, $twitterConnection));

        $this->assertSame(Type::TWITTER, $twitter->getType());
    }

    /**
     * @test
     */
    public function getAuthorizationUrl_should_return_url()
    {
        $expectedUrl = 'some_url';
        $requestToken = 'some_request_token';

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class)
            ->shouldReceive('url')
            ->once()
            ->with(
                'oauth/authorize',
                ['oauth_token' => $requestToken]
            )
            ->andReturn($expectedUrl)
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('request_token')
            ->andReturn($requestToken)
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        $twitter = new Twitter($router, $twitterConnection);

        $this->assertSame($expectedUrl, $twitter->getAuthorizationUrl($account));
    }

    /**
     * @test
     */
    public function initializeCallback_should_set_proper_request_tokens_in_account_config()
    {
        $requestTokenArray = [
            'oauth_token'           =>  'some_request_token',
            'oauth_token_secret'    =>  'some_request_secret',
        ];

        $expectedConfigArray = [
            'request_token'        =>  $requestTokenArray['oauth_token'],
            'request_token_secret' =>  $requestTokenArray['oauth_token_secret']
        ];

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class)
            ->shouldReceive('oauth')
            ->once()
            ->with(
                'oauth/request_token',
                ['oauth_callback' => 'some_callback_url']
            )
            ->andReturn($requestTokenArray)
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->with(
                'app_oauth_callback',
                ['app' => Type::TWITTER],
                RouterInterface::ABSOLUTE_URL
            )
            ->andReturn('some_callback_url')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('setConfig')
            ->once()
            ->with($expectedConfigArray)
            ->andReturnSelf()
            ->getMock();

        $twitter = new Twitter($router, $twitterConnection);

        $twitter->initializeCallback($account);
    }

    /**
     * @test
     * @expectedException \Connect\Bundle\AppBundle\OAuth\App\AppException
     * @expectedExceptionMessage Twitter denied access in callback request
     */
    public function validateCallback_with_denied_token_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('denied')
            ->andReturn('some_deny_token')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        (new Twitter($router, $twitterConnection))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Callback OAuth request token cannot be empty
     */
    public function validateCallback_with_empty_oauth_token_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('denied')
            ->andReturn(null)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('oauth_token')
            ->andReturnNull()
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        (new Twitter($router, $twitterConnection))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Request tokens mismatch
     */
    public function validateCallback_with_not_matching_request_tokens_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('denied')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('oauth_token')
            ->andReturn('some_request_token_1')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('request_token')
            ->andReturn('some_request_token_2')
            ->getMock();

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        (new Twitter($router, $twitterConnection))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage OAuth verifier token cannot be empty
     */
    public function validateCallback_with_empty_verifier_token_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('denied')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('oauth_token')
            ->andReturn('some_request_token')
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('oauth_verifier')
            ->andReturnNull()
            ->getMock();


        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('request_token')
            ->andReturn('some_request_token')
            ->getMock();

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        (new Twitter($router, $twitterConnection))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     */
    public function validateCallback_with_valid_data_should_not_throw_any_exceptions()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('denied')
            ->andReturnNull()
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('oauth_token')
            ->andReturn('some_request_token')
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with('oauth_verifier')
            ->andReturn('some_verifier')
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('request_token')
            ->andReturn('some_request_token')
            ->getMock();

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        (new Twitter($router, $twitterConnection))->validateCallback($callbackRequest, $account);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage OAuth verifier token cannot be empty
     */
    public function handleCallback_with_empty_oauth_verifier_should_throw_exception()
    {
        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('oauth_verifier')
            ->andReturnNull()
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class);

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class);

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        (new Twitter($router, $twitterConnection))->handleCallback($callbackRequest, $account);
    }

    /**
     * @test
     */
    public function handleCallback_should_set_proper_access_tokens_in_account_config()
    {
        $accessTokenArray = [
            'oauth_token'           =>  'some_access_token',
            'oauth_token_secret'    =>  'some_access_secret',
        ];

        $expectedConfigArray = [
            'access_token'        =>  $accessTokenArray['oauth_token'],
            'access_token_secret' =>  $accessTokenArray['oauth_token_secret']
        ];

        $requestTokenValue = 'some_request_token';
        $requestSecretValue = 'some_request_secret';
        $verifierToken = 'some_verify_token';

        /** @var Request $callbackRequest */
        $callbackRequest = Mockery::mock(Request::class)
            ->shouldReceive('get')
            ->once()
            ->with('oauth_verifier')
            ->andReturn($verifierToken)
            ->getMock();

        /** @var Account $account */
        $account = Mockery::mock(Account::class)
            ->shouldReceive('getConfig')
            ->once()
            ->with('request_token')
            ->andReturn($requestTokenValue)
            ->getMock()

            ->shouldReceive('getConfig')
            ->once()
            ->with('request_token_secret')
            ->andReturn($requestSecretValue)
            ->getMock()

            ->shouldReceive('setConfig')
            ->once()
            ->with($expectedConfigArray)
            ->andReturnSelf()
            ->getMock();

        /** @var TwitterOAuth $twitterConnection */
        $twitterConnection = Mockery::mock(TwitterOAuth::class)
            ->shouldReceive('setOauthToken')
            ->once()
            ->with($requestTokenValue, $requestSecretValue)
            ->getMock()

            ->shouldReceive('oauth')
            ->once()
            ->with(
                'oauth/access_token',
                ['oauth_verifier' => $verifierToken]
            )
            ->andReturn($accessTokenArray)
            ->getMock();

        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class);

        $twitter = new Twitter($router, $twitterConnection);

        $twitter->handleCallback($callbackRequest, $account);
    }
}
