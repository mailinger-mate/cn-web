<?php

namespace Connect\Test\Unit\Bundle\AppBundle\OAuth\App;

use Connect\Bundle\AppBundle\OAuth\App\Type;
use PHPUnit_Framework_TestCase;
use ReflectionClass;

/**
 * Class TypeTest
 */
class TypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function isSupported_without_supported_type_should_return_false()
    {
        $this->assertFalse(Type::isSupported('some_type'));
    }

    /**
     * @test
     * @dataProvider supportedTypesProvider
     *
     * @param string $type
     */
    public function isSupported_with_supported_type_should_return_true($type)
    {
        $this->assertTrue(Type::isSupported($type));
    }

    public static function supportedTypesProvider()
    {
        return array_map(
            function ($type) {
                return [$type];
            },
            (new ReflectionClass(Type::class))->getConstants()
        );
    }
}
