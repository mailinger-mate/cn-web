<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Repository;

use Connect\Bundle\AppBundle\Core\Client\CredentialsProvider;
use Connect\Bundle\AppBundle\Repository\CoreAccountRepository;
use Connect\Domain\Account;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class CoreAccountRepositoryTest
 */
class CoreAccountRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function add_with_http_200_should_return_true()
    {
        $coreBearerToken = 'some_core_token';
        $requestBody = '{"name":"some_name","type":"some_type","config":{"some_key":"some_value"}}';
        $expectedId = 52;

        /** @var Account $account */
        $account = (new Account())
            ->setName('some_name')
            ->setType('some_type')
            ->setConfig(['some_key' => 'some_value']);

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('getStatusCode')
            ->once()
            ->andReturn(HttpResponse::HTTP_OK)
            ->getMock()

            ->shouldReceive('getBody')
            ->andReturn($requestBody)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'POST',
                'accounts',
                [
                    'body'    => $requestBody,
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                        'Content-Type'  => 'application/json'
                    ],
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('serialize')
            ->with(
                $account,
                JsonEncoder::FORMAT,
                Mockery::type(SerializationContext::class)
            )
            ->andReturn($requestBody)
            ->getMock()

            ->shouldReceive('deserialize')
            ->with(
                $requestBody,
                'array',
                JsonEncoder::FORMAT
            )
            ->andReturn(['id' => $expectedId])
            ->getMock();

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $this->assertTrue((new CoreAccountRepository($client, $credProvider, $serializer))->add($account));
        $this->assertSame($expectedId, $account->getId());
    }

    /**
     * @test
     */
    public function add_without_http_200_should_return_false()
    {
        $coreBearerToken = 'some_core_token';
        $requestBody = '{"name":"some_name","type":"some_type","config":{"some_key":"some_value"}}';

        /** @var Account $account */
        $account = (new Account())
            ->setName('some_name')
            ->setType('some_type')
            ->setConfig(['some_key' => 'some_value']);

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('getStatusCode')
            ->once()
            ->andReturn(HttpResponse::HTTP_FORBIDDEN)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'POST',
                'accounts',
                [
                    'body'    => $requestBody,
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                        'Content-Type'  => 'application/json'
                    ],
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('serialize')
            ->with(
                $account,
                JsonEncoder::FORMAT,
                Mockery::type(SerializationContext::class)
            )
            ->andReturn($requestBody)
            ->getMock();

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $this->assertFalse((new CoreAccountRepository($client, $credProvider, $serializer))->add($account));
        $this->assertNull($account->getId());
    }

    /**
     * @test
     */
    public function update_with_http_204_should_return_true()
    {
        $coreBearerToken = 'some_core_token';
        $accountId = 5;
        $requestBody = '{"id":5,"name":"some_name","type":"some_type","config":{"some_key":"some_value"}}';

        /** @var Account $account */
        $account = (new Account())
            ->setId($accountId)
            ->setName('some_name')
            ->setType('some_type')
            ->setConfig(['some_key' => 'some_value']);

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('getStatusCode')
            ->once()
            ->andReturn(HttpResponse::HTTP_NO_CONTENT)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'PATCH',
                "accounts/$accountId",
                [
                    'body'    => $requestBody,
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                        'Content-Type'  => 'application/json'
                    ],
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('serialize')
            ->with(
                $account,
                JsonEncoder::FORMAT,
                Mockery::type(SerializationContext::class)
            )
            ->andReturn($requestBody)
            ->getMock();

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $this->assertTrue((new CoreAccountRepository($client, $credProvider, $serializer))->update($account));
    }

    /**
     * @test
     */
    public function update_without_http_204_should_return_true()
    {
        $coreBearerToken = 'some_core_token';
        $accountId = 5;
        $requestBody = '{"id":5,"name":"some_name","type":"some_type","config":{"some_key":"some_value"}}';

        /** @var Account $account */
        $account = (new Account())
            ->setId($accountId)
            ->setName('some_name')
            ->setType('some_type')
            ->setConfig(['some_key' => 'some_value']);

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('getStatusCode')
            ->once()
            ->andReturn(HttpResponse::HTTP_FORBIDDEN)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'PATCH',
                "accounts/$accountId",
                [
                    'body'    => $requestBody,
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                        'Content-Type'  => 'application/json'
                    ],
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('serialize')
            ->with(
                $account,
                JsonEncoder::FORMAT,
                Mockery::type(SerializationContext::class)
            )
            ->andReturn($requestBody)
            ->getMock();


        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $this->assertFalse((new CoreAccountRepository($client, $credProvider, $serializer))->update($account));
    }

    /**
     * @test
     */
    public function find_should_return_account()
    {
        $coreBearerToken = 'some_core_token';
        $accountArray = [
            'id'     =>  5,
            'name'   =>  'some_name',
            'type'   =>  'some_type',
            'config' =>  ['some_key' => 'some_value'],
        ];
        $accountJson = json_encode($accountArray);

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('getBody')
            ->once()
            ->andReturn($accountJson)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'GET',
                'accounts/' . $accountArray['id'],
                [
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                        'Content-Type'  => 'application/json'
                    ],
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->with(
                $accountJson,
                'array',
                JsonEncoder::FORMAT
            )
            ->andReturn($accountArray)
            ->getMock();

        /** @var Account $account */
        $expectedAccount = (new Account())
            ->setId($accountArray['id'])
            ->setName($accountArray['name'])
            ->setType($accountArray['type'])
            ->setConfig($accountArray['config']);

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $coreAccountRepository = (new CoreAccountRepository($client, $credProvider, $serializer));

        $this->assertEquals($expectedAccount, $coreAccountRepository->find($accountArray['id']));
    }
}
