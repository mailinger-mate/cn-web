<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Repository;

use Connect\Bundle\AppBundle\Core\Client\CredentialsProvider;
use Connect\Bundle\AppBundle\Repository\CoreAccountRepository;
use Connect\Bundle\AppBundle\Repository\CoreBillingRepository;
use Connect\Domain\Account;
use Connect\Domain\Invoice;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use LogicException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class CoreAccountRepositoryTest
 */
class CoreBillingRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider validContentDispositionHeaderProvider
     *
     * @param $contentDispositionHeader
     */
    public function find_with_valid_response_should_return_invoice($contentDispositionHeader)
    {
        $coreBearerToken = 'some_core_token';

        $invoiceArray = [
            'id'         =>  5,
            'filename'   =>  'some_filename',
            'content'    =>  'some_content',
        ];

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('hasHeader')
            ->with('Content-Disposition')
            ->once()
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('getHeader')
            ->with('Content-Disposition')
            ->once()
            ->andReturn([sprintf($contentDispositionHeader, $invoiceArray['filename'])])
            ->getMock()

            ->shouldReceive('getBody')
            ->once()
            ->andReturn($invoiceArray['content'])
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'GET',
                'billings/' . $invoiceArray['id'],
                [
                    'headers' => [
                        'Accept'        => 'application/pdf',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                    ]
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var Invoice $expected */
        $expected = (new Invoice())
            ->setId($invoiceArray['id'])
            ->setPdfName($invoiceArray['filename'])
            ->setPdfContent($invoiceArray['content']);

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $coreAccountRepository = (new CoreBillingRepository($client, $credProvider, $serializer));

        $this->assertEquals($expected, $coreAccountRepository->find($invoiceArray['id']));
    }

    public static function validContentDispositionHeaderProvider()
    {
        return [
            ['filename=%s'],
            ['filename="%s"'],
            ['some_param; filename=%s; some_param'],
            ['some_param; filename="%s"; some_param'],
        ];
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Missing required Content-Disposition header
     */
    public function find_with_missing_header_should_throw_exception()
    {
        $coreBearerToken = 'some_core_token';
        $invoiceId = 10;

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('hasHeader')
            ->with('Content-Disposition')
            ->once()
            ->andReturn(false)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'GET',
                'billings/' . $invoiceId,
                [
                    'headers' => [
                        'Accept'        => 'application/pdf',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                    ]
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $coreAccountRepository = (new CoreBillingRepository($client, $credProvider, $serializer));

        $coreAccountRepository->find($invoiceId);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Not supported Content-Disposition received:
     */
    public function find_with_not_supported_header_should_throw_exception()
    {
        $coreBearerToken = 'some_core_token';
        $invoiceId = 10;

        /** @var Response $response */
        $response = Mockery::mock(Response::class)
            ->shouldReceive('hasHeader')
            ->with('Content-Disposition')
            ->once()
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('getHeader')
            ->with('Content-Disposition')
            ->once()
            ->andReturn('invalid value')
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->with(
                'GET',
                'billings/' . $invoiceId,
                [
                    'headers' => [
                        'Accept'        => 'application/pdf',
                        'Authorization' => 'Bearer ' . $coreBearerToken,
                    ]
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var CredentialsProvider $credProvider */
        $credProvider = Mockery::mock(CredentialsProvider::class)
            ->shouldReceive('getCoreTokenValue')
            ->andReturn($coreBearerToken)
            ->getMock();

        $coreAccountRepository = (new CoreBillingRepository($client, $credProvider, $serializer));

        $coreAccountRepository->find($invoiceId);
    }
}
