<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Request;

use Connect\Bundle\AppBundle\OAuth\App\AppInterface;
use Connect\Bundle\AppBundle\OAuth\App\Type;
use Connect\Bundle\AppBundle\Request\OAuthAppParamConverter;
use Mockery;
use PHPUnit_Framework_TestCase;
use ReflectionClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OAuthAppParamConverterTest
 */
class OAuthAppParamConverterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider appTypesDataProvider
     *
     * @param string $appType
     * @param string $expectedService
     */
    public function apply_with_valid_param_should_apply_app_and_return_true($appType, $expectedService)
    {
        $paramName = 'some_param';

        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->once()
            ->andReturn($paramName)
            ->getMock();

        /** @var AppInterface $app */
        $app = Mockery::mock(AppInterface::class);

        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->once()
            ->with($paramName)
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with($paramName)
            ->andReturn($appType)
            ->getMock()

            ->shouldReceive('set')
            ->once()
            ->with($paramName, $app)
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;

        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class)
            ->shouldReceive('get')
            ->once()
            ->with($expectedService)
            ->andReturn($app)
            ->getMock();

        $converter = new OAuthAppParamConverter($serviceContainer);

        $this->assertTrue($converter->apply($request, $configuration));
    }

    public static function appTypesDataProvider()
    {
        $appTypes = [
            [Type::SALESFORCE, 'oauth_app.salesforce'],
            [Type::SLACK, 'oauth_app.slack'],
            [Type::TWITTER, 'oauth_app.twitter'],
        ];

        self::assertCount(count($appTypes), (new ReflectionClass(Type::class))->getConstants());

        return $appTypes;
    }

    /**
     * @test
     */
    public function apply_without_required_param_should_return_false()
    {
        $paramName = 'some_param_name';

        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->once()
            ->andReturn($paramName)
            ->getMock();

        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->once()
            ->with($paramName)
            ->andReturn(false)
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;

        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class);

        $converter = new OAuthAppParamConverter($serviceContainer);

        $this->assertFalse($converter->apply($request, $configuration));
    }

    /**
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Not supported app type: some_type
     */
    public function apply_with_not_supported_type_should_throw_exception()
    {
        $paramName = 'some_param_name';

        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->once()
            ->andReturn($paramName)
            ->getMock();

        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->once()
            ->with($paramName)
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('get')
            ->once()
            ->with($paramName)
            ->andReturn('some_type')
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;

        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class);

        $converter = new OAuthAppParamConverter($serviceContainer);

        $this->assertFalse($converter->apply($request, $configuration));
    }

    /**
     * @test
     */
    public function supports_with_supported_class_should_return_true()
    {
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getClass')
            ->once()
            ->andReturn(AppInterface::class)
            ->getMock();

        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class);

        $converter = new OAuthAppParamConverter($serviceContainer);

        $this->assertTrue($converter->supports($configuration));
    }

    /**
     * @test
     */
    public function supports_without_supported_class_should_return_false()
    {
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getClass')
            ->once()
            ->andReturn('some_class')
            ->getMock();

        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class);

        $converter = new OAuthAppParamConverter($serviceContainer);

        $this->assertFalse($converter->supports($configuration));
    }
}