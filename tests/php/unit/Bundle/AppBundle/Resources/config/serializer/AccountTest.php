<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Resources\config\serializer;

use Connect\Domain\Account;
use Connect\Domain\Contact;
use Connect\Domain\Token;
use Connect\Test\Core\SerializerTestCase;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class ContactTest
 */
class AccountTest extends SerializerTestCase
{
    /**
     * @test
     * @dataProvider validDataProvider
     *
     * @param $context
     * @param $expectedJson
     */
    public function serialize($context, $expectedJson)
    {
        $account = new Account();
        $account->setId(5);
        $account->setName('some_account');
        $account->setType('some_type');
        $account->setConfig([
            'config_key' => 'config_value'
        ]);

        $this->assertJsonStringEqualsJsonString(
            $expectedJson,
            $this->getSerializer('app.serializer.jms')->serialize($account, JsonEncoder::FORMAT, $context)
        );
    }

    /**
     * @return array
     */
    public static function validDataProvider()
    {
        return [
            // 0)
            // without context
            [
                null,
                '{
                  "id": 5,
                  "name": "some_account",
                  "type": "some_type",
                  "config": {
                    "config_key": "config_value"
                  }
                }'
            ],
            // 1)
            // context: group add
            [
                SerializationContext::create()->setGroups(['add']),
                '{
                  "name": "some_account",
                  "type": "some_type",
                  "config": {
                    "config_key": "config_value"
                  }
                }'
            ],
            // 2)
            // context: group edit
            [
                SerializationContext::create()->setGroups(['edit']),
                '{
                  "name": "some_account",
                  "config": {
                    "config_key": "config_value"
                  }
                }'
            ],
        ];
    }
}