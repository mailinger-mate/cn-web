<?php

namespace Connect\Test\Unit\Bundle\AppBundle\Routing;

use Connect\Bundle\AppBundle\Routing\AppUrlGenerator;
use InvalidArgumentException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AppUrlGeneratorTest
 */
class AppUrlGeneratorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider generateValidDataProvider
     *
     * @param $expected
     * @param $appRoute
     * @param array $parameters
     */
    public function generate_with_valid_data_should_return_url($expected, $appRoute, array $parameters)
    {
        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->with('app')
            ->andReturn('url/')
            ->getMock();

        $this->assertSame($expected, (new AppUrlGenerator($router))->generate($appRoute, $parameters));
    }

    public static function generateValidDataProvider()
    {
        return [
            // 0)
            // empty app route
            // no parameters
            [
                'url/#/',
                '',
                []
            ],

            // 1)
            // not empty app route
            // no parameters
            [
                'url/#/some_res1/some_res1',
                'some_res1/some_res1',
                []
            ],

            // 2)
            // empty app route
            // one parameter
            [
                'url/#/?param1=value1',
                '',
                ['param1' => 'value1']
            ],

            // 3)
            // not empty app route
            // one string parameter
            [
                'url/#/some_res1/some_res1?param1=value1',
                'some_res1/some_res1',
                ['param1' => 'value1']
            ],

            // 4)
            // not empty app route
            // one int parameter
            [
                'url/#/some_res1/some_res1?param1=5',
                'some_res1/some_res1',
                ['param1' => 5]
            ],

            // 5)
            // not empty app route
            // one float parameter
            [
                'url/#/some_res1/some_res1?param1=5.574',
                'some_res1/some_res1',
                ['param1' => 5.574]
            ],

            // 6)
            // not empty app route
            // more parameters
            [
                'url/#/some_res1/some_res1?param1=value1&param2=5',
                'some_res1/some_res1',
                ['param1' => 'value1', 'param2' => 5]
            ],

            // 7)
            // not empty app route
            // one parameter with null value
            [
                'url/#/some_res1/some_res1?param1',
                'some_res1/some_res1',
                ['param1' => null]
            ],

            // 8)
            // not empty app route
            // more parameters with null values
            [
                'url/#/some_res1/some_res1?param1&param2',
                'some_res1/some_res1',
                ['param1' => null, 'param2' => null]
            ],

            // 9)
            // not empty app route
            // more parameters with mixed values
            [
                'url/#/some_res1/some_res1?param1&param2=value2&param3=52.6',
                'some_res1/some_res1',
                ['param1' => null, 'param2' => 'value2', 'param3' => 52.6]
            ],

            // 10)
            // not empty app route
            // more parameters with already provided param in appRoute
            [
                'url/#/some_res1/some_res1?some_param=123&param1&param2=value2',
                'some_res1/some_res1?some_param=123',
                ['param1' => null, 'param2' => 'value2']
            ],
        ];
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Key in parameters array must by a non-empty string.
     * @dataProvider generateInvalidKeyProvider
     *
     * @param mixed $key
     */
    public function generate_with_invalid_key_should_throw_exception($key)
    {
        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->with('app')
            ->andReturn('url/')
            ->getMock();

        (new AppUrlGenerator($router))->generate('some_app_route', [$key => 'value']);
    }

    public static function generateInvalidKeyProvider()
    {
        return [
            // not string
            [0],
            [1.2],
            [true],

            // empty
            [''],
            [null]
        ];
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Value must be a numeric or non-empty string or null:
     * @dataProvider generateInvalidValueProvider
     *
     * @param mixed $value
     */
    public function generate_with_invalid_value_should_throw_exception($value)
    {
        /** @var RouterInterface $router */
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->once()
            ->with('app')
            ->andReturn('url/')
            ->getMock();

        (new AppUrlGenerator($router))->generate('some_app_route', ['param' => $value]);
    }

    public static function generateInvalidValueProvider()
    {
        return [
            // not string nor numeric
            [new \stdClass()],
            [array()],
            [true],

            // empty string
            [''],
        ];
    }
}