<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Client;

use Connect\Bundle\UMSSecurityBundle\Client\Facade;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use Connect\Domain\User;
use Connect\Domain\UserAction;
use Connect\Domain\Vendor;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class FacadeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function ContactVerify_with_valid_data_should_return_action_id()
    {
        $dataArray = [
            'action_id' => 'action_token',
            'contact'   => 'john@doe.foo',
        ];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'verify/4', ['query' => ['user' => 3]])
            ->andReturn($response)
            ->getMock();

        $user = (new User())->setId(3);
        $contact = (new Contact())->setId(4)->setUser($user);

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->getMock();
        $actionId = (new Facade($client, $repository, $encoder))->contactVerify($contact);

        $this->assertSame('action_token', $actionId);
        $this->assertSame('john@doe.foo', $contact->getValue());
    }

    /**
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function ContactVerify_with_404_from_ums_should_throw_not_found()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(404)
            ->getMock();
        $exception = Mockery::mock(ClientException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'verify/4', ['query' => ['user' => 3]])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        $user = (new User())->setId(3);
        $contact = (new Contact())->setId(4)->setUser($user);
        (new Facade($client, $repository, $encoder))->contactVerify($contact);
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function ContactVerify_with_not_404_from_ums_should_rethrow()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(403)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'verify/4', ['query' => ['user' => 3]])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        $user = (new User())->setId(3);
        $contact = (new Contact())->setId(4)->setUser($user);

        (new Facade($client, $repository, $encoder))->contactVerify($contact);
    }

    /**
     * @test
     */
    public function ContactConfirm_with_valid_data_should_return_true()
    {
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'confirm/aabbcc')
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        $contactAction = (new ContactAction())->setId('aabbcc')->setType('contact_verify');
        $contact = (new Contact())->addAction($contactAction);

        $this->assertTrue((new Facade($client, $repository, $encoder))->contactConfirm($contact));
    }

    /**
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function ContactConfirm_with_404_from_ums_should_throw_not_found()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(404)
            ->getMock();
        $exception = Mockery::mock(ClientException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'confirm/aabbcc')
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        $contactAction = (new ContactAction())->setId('aabbcc')->setType('contact_verify');
        $contact = (new Contact())->addAction($contactAction);

        (new Facade($client, $repository, $encoder))->contactConfirm($contact);
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function ContactConfirm_with_not_404_from_ums_should_rethrow()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(403)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'confirm/aabbcc')
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        $contactAction = (new ContactAction())->setId('aabbcc')->setType('contact_verify');
        $contact = (new Contact())->addAction($contactAction);

        (new Facade($client, $repository, $encoder))->contactConfirm($contact);
    }

    /**
     * @test
     * @expectedException \LogicException
     * @expectedExceptionMessage Contact does not have action with "contact_verify" type.
     */
    public function ContactConfirm_without_action_should_throw_logic_exception()
    {
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class);

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        $contactAction = (new ContactAction())->setId('aabbcc')->setType('not_contact_verify');
        $contact = (new Contact())->addAction($contactAction);

        (new Facade($client, $repository, $encoder))->contactConfirm($contact);
    }

    /**
     * @test
     */
    public function RegisterUser_with_valid_data_should_return_user()
    {
        $requestData = [
            'name'            => 'Foo',
            'vendor'          => 'Foo Co',
            'contact'         => 'foo@bar.dev',
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'vendor'   => ['name' => $requestData['vendor']],
            'contacts' => [['value' => $requestData['contact']]],
            'password' => ['first' => $requestData['password_first'], 'second' => $requestData['password_second']],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();

        $dataArray = ['user_id' => 12];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'register', ['body' => $requestDataJson])
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertSame(
            $user,
            (new Facade($client, $repository, $encoder))->registerUser($requestData)
        );
    }

    /**
     * @test
     */
    public function RegisterUser_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = [
            'name'            => 'Foo',
            'vendor'          => 'Foo Co',
            'contact'         => 'foo@bar.dev',
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'vendor'   => ['name' => $requestData['vendor']],
            'contacts' => [['value' => $requestData['contact']]],
            'password' => ['first' => $requestData['password_first'], 'second' => $requestData['password_second']],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'register', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->registerUser($requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function RegisterUser_with_not_400_from_ums_should_rethrow()
    {
        $requestData = [
            'name'            => 'Foo',
            'vendor'          => 'Foo Co',
            'contact'         => 'foo@bar.dev',
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'vendor'   => ['name' => $requestData['vendor']],
            'contacts' => [['value' => $requestData['contact']]],
            'password' => ['first' => $requestData['password_first'], 'second' => $requestData['password_second']],
        ];
        $requestDataJson = json_encode($requestData);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'register', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->registerUser($requestData);
    }

    /**
     * @test
     */
    public function RemindPassword_with_valid_data_should_return_user()
    {
        $user = new User();

        $dataArray = ['user_id' => 12];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'remind', ['body' => '{"email":"foo@bar.dev"}'])
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->getMock();

        $this->assertSame(
            $user,
            (new Facade($client, $repository, $encoder))->remindUserPassword('foo@bar.dev')
        );
    }

    /**
     * @test
     */
    public function RemindPassword_with_400_form_ums_should_return_array_with_error()
    {
        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'remind', ['body' => '{"email":"foo@bar.dev"}'])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->remindUserPassword('foo@bar.dev')
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function RemindPassword_with_not_400_from_ums_should_rethrow()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'remind', ['body' => '{"email":"foo@bar.dev"}'])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class);

        (new Facade($client, $repository, $encoder))->remindUserPassword('foo@bar.dev');
    }

    /**
     * @test
     */
    public function ResetUserPassword_with_valid_data_should_return_user()
    {
        $requestData = [
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'password' => [
                'first'  => $requestData['password_first'],
                'second' => $requestData['password_second'],
            ],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $action = (new UserAction())->setId('62e919b5766b39d74698e24fc251632f');

        $dataArray = ['user_id' => 12];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'resetpass/62e919b5766b39d74698e24fc251632f', ['body' => $requestDataJson])
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertSame(
            $user,
            (new Facade($client, $repository, $encoder))->resetUserPassword($action, $requestData)
        );
    }

    /**
     * @test
     */
    public function ResetUserPassword_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = [
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'password' => [
                'first'  => $requestData['password_first'],
                'second' => $requestData['password_second'],
            ],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $action = (new UserAction())->setId('62e919b5766b39d74698e24fc251632f');

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'resetpass/62e919b5766b39d74698e24fc251632f', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->resetUserPassword($action, $requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function ResetUserPassword_with_not_400_from_ums_should_rethrow()
    {
        $requestData = [
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'password' => [
                'first'  => $requestData['password_first'],
                'second' => $requestData['password_second'],
            ],
        ];
        $requestDataJson = json_encode($requestData);
        $action = (new UserAction())->setId('62e919b5766b39d74698e24fc251632f');

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'resetpass/62e919b5766b39d74698e24fc251632f', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->resetUserPassword($action, $requestData);
    }

    /**
     * @test
     */
    public function ActivateUserAccount_with_valid_data_should_return_user()
    {
        $requestData = [
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'password' => [
                'first'  => $requestData['password_first'],
                'second' => $requestData['password_second'],
            ],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $action = (new UserAction())->setId('62e919b5766b39d74698e24fc251632f');

        $dataArray = ['user_id' => 12];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'activate/62e919b5766b39d74698e24fc251632f', ['body' => $requestDataJson])
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertSame(
            $user,
            (new Facade($client, $repository, $encoder))->activateUserAccount($action, $requestData)
        );
    }

    /**
     * @test
     */
    public function ActivateUserAccount_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = [
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'password' => [
                'first'  => $requestData['password_first'],
                'second' => $requestData['password_second'],
            ],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $action = (new UserAction())->setId('62e919b5766b39d74698e24fc251632f');

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'activate/62e919b5766b39d74698e24fc251632f', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->activateUserAccount($action, $requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function ActivateUserAccount_with_not_400_from_ums_should_rethrow()
    {
        $requestData = [
            'password_first'  => 'secret.pass',
            'password_second' => 'secret.pass',
        ];
        $formattedData = [
            'password' => [
                'first'  => $requestData['password_first'],
                'second' => $requestData['password_second'],
            ],
        ];
        $requestDataJson = json_encode($requestData);
        $action = (new UserAction())->setId('62e919b5766b39d74698e24fc251632f');

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'activate/62e919b5766b39d74698e24fc251632f', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->activateUserAccount($action, $requestData);
    }

    /**
     * @test
     */
    public function addUser_with_valid_data_should_return_user()
    {
        $requestData = [
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $sessionUser = (new User())->setId(7);

        $dataArray = ['user_id' => 12];
        $dataJson = json_encode($dataArray);
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'add', ['query' => ['session_user' => 7], 'body' => $requestDataJson])
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertSame(
            $user,
            (new Facade($client, $repository, $encoder))->addUser($sessionUser, $requestData)
        );
    }

    /**
     * @test
     */
    public function addUser_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = [
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $sessionUser = (new User())->setId(7);

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'add', ['query' => ['session_user' => 7], 'body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->addUser($sessionUser, $requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function addUser_with_not_400_from_ums_should_rethrow()
    {
        $requestData = [
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $sessionUser = (new User())->setId(7);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'add', ['query' => ['session_user' => 7], 'body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->addUser($sessionUser, $requestData);
    }

    /**
     * @test
     */
    public function editUser_with_valid_data_should_return_true()
    {
        $requestData = [
            'id'      => 12,
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $sessionUser = (new User())->setId(7);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(204)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET',
                'edit/12', ['query' => ['session_user' => 7], 'body' => $requestDataJson]
            )
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertTrue(
            (new Facade($client, $repository, $encoder))
                ->editUser($sessionUser, $requestData)
        );
    }

    /**
     * @test
     */
    public function editUser_with_valid_data_and_new_contact_should_return_action_id()
    {
        $requestData = [
            'id'      => 12,
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $sessionUser = (new User())->setId(7);

        $dataArray = ['action_id' => 'action_token'];
        $dataJson = json_encode($dataArray);
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(200)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET',
                'edit/12', ['query' => ['session_user' => 7], 'body' => $requestDataJson]
            )
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertSame(
            'action_token',
            (new Facade($client, $repository, $encoder))
                ->editUser($sessionUser, $requestData)
        );
    }

    /**
     * @test
     */
    public function editUser_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = [
            'id'      => 12,
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $user = new User();
        $sessionUser = (new User())->setId(7);

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'edit/12', ['query' => ['session_user' => 7], 'body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(12)
            ->andReturn($user)
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->editUser($sessionUser, $requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function editUser_with_not_400_from_ums_should_rethrow()
    {
        $requestData = [
            'id'      => 12,
            'name'    => 'Foo',
            'contact' => 'foo@bar.dev',
            'role'    => 'user',
        ];
        $formattedData = [
            'name'     => $requestData['name'],
            'contacts' => [['value' => $requestData['contact']]],
            'role'     => $requestData['role'],
        ];
        $requestDataJson = json_encode($requestData);
        $sessionUser = (new User())->setId(7);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'edit/12', ['query' => ['session_user' => 7], 'body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->editUser($sessionUser, $requestData);
    }

    /**
     * @test
     * @dataProvider editProfileValidDataProvider
     * @param array $requestData
     * @param array $formattedData
     */
    public function editUserProfile_with_valid_data_should_return_true(
        array $requestData,
        array $formattedData
    ) {
        $requestDataJson = json_encode($requestData);
        $sessionUser = (new User())->setId(7);
        $sessionId = 'abc';

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(204)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET',
                'profile/edit', [
                    'query' => [
                        'session_id'    => $sessionId,
                        'session_user'  => 7
                    ],
                    'body' => $requestDataJson
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->never()
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertTrue(
            (new Facade($client, $repository, $encoder))
                ->editUserProfile($sessionUser, $sessionId, $requestData)
        );
    }

    /**
     * @return array
     */
    public static function editProfileValidDataProvider()
    {
        return [
            // 0) new name
            [
                ['name' => 'Foo'],
                ['name' => 'Foo'],
            ],
            // 1) new password
            [
                [
                    'password_first'   => 'new.pass',
                    'password_second'  => 'new.pass',
                    'current_password' => 'secret.pass',
                ],
                [
                    'password'         => [
                        'first'  => 'new.pass',
                        'second' => 'new.pass',
                    ],
                    'current_password' => 'secret.pass',
                ],
            ],
        ];
    }

    /**
     * @test
     */
    public function editUserProfile_with_valid_data_and_new_contact_should_return_action_id()
    {
        $requestData = [
            'contact'          => 'foo@bar.dev',
            'current_password' => 'secret.pass',
        ];
        $formattedData = $requestData;

        $requestDataJson = json_encode($requestData);
        $sessionUser = (new User())->setId(7);
        $sessionId = 'abc';

        $dataArray = ['action_id' => 'action_token'];
        $dataJson = json_encode($dataArray);
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(200)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET',
                'profile/edit', [
                    'query' => [
                        'session_id'    => $sessionId,
                        'session_user'  => 7
                    ],
                    'body' => $requestDataJson
                ]
            )
            ->andReturn($response)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->never()
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('decode')
            ->with($dataJson, 'json')
            ->andReturn($dataArray)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertSame(
            'action_token',
            (new Facade($client, $repository, $encoder))
                ->editUserProfile($sessionUser, $sessionId, $requestData)
        );
    }

    /**
     * @test
     */
    public function editUserProfile_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = ['name' => 'Foo'];
        $formattedData = $requestData;
        $requestDataJson = json_encode($requestData);
        $sessionUser = (new User())->setId(7);
        $sessionId = 'abc';

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET',
                'profile/edit', [
                    'query' => [
                        'session_id'    => $sessionId,
                        'session_user'  => 7
                    ],
                    'body' => $requestDataJson
                ]
            )
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->never()
            ->getMock();

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))
                ->editUserProfile($sessionUser, $sessionId, $requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function editUserProfile_with_not_400_from_ums_should_rethrow()
    {
        $requestData = ['name' => 'Foo'];
        $formattedData = $requestData;
        $requestDataJson = json_encode($requestData);
        $sessionUser = (new User())->setId(7);
        $sessionId = 'abc';

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET',
                'profile/edit', [
                    'query' => [
                        'session_id'    => $sessionId,
                        'session_user'  => 7
                    ],
                    'body' => $requestDataJson
                ]
            )
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($formattedData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->editUserProfile($sessionUser, $sessionId, $requestData);
    }

    /**
     * @test
     */
    public function editVendor_with_valid_data_should_return_true()
    {
        $requestData = ['Foo' => 'Bar'];
        $requestDataJson = json_encode($requestData);
        $vendor = (new Vendor())->setId(23);

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'vendor/edit/23', ['body' => $requestDataJson])
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($requestData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        $this->assertTrue(
            (new Facade($client, $repository, $encoder))
                ->editVendor($vendor, $requestData)
        );
    }

    /**
     * @test
     */
    public function editVendor_with_400_form_ums_should_return_array_with_error()
    {
        $requestData = ['Foo' => 'Bar'];
        $requestDataJson = json_encode($requestData);
        $vendor = (new Vendor())->setId(23);

        $body = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('{"some":"error"}')
            ->getMock();
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->shouldReceive('getBody')
            ->andReturn($body)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'vendor/edit/23', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($requestData, 'json')
            ->andReturn($requestDataJson)
            ->shouldReceive('decode')
            ->with('{"some":"error"}', 'json')
            ->andReturn(['some' => 'error'])
            ->getMock();

        $this->assertSame(
            ['some' => 'error'],
            (new Facade($client, $repository, $encoder))->editVendor($vendor, $requestData)
        );
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function editVendor_with_not_400_from_ums_should_rethrow()
    {
        $requestData = ['Foo' => 'Bar'];
        $requestDataJson = json_encode($requestData);
        $vendor = (new Vendor())->setId(23);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(401)
            ->getMock();
        /** @var RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);
        $exception = new ClientException(
            'something bad',
            $request,
            $response
        );
        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with('GET', 'vendor/edit/23', ['body' => $requestDataJson])
            ->andThrow($exception)
            ->getMock();

        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);

        /** @var JsonEncoder $encoder */
        $encoder = Mockery::mock(JsonEncoder::class)
            ->shouldReceive('encode')
            ->with($requestData, 'json')
            ->andReturn($requestDataJson)
            ->getMock();

        (new Facade($client, $repository, $encoder))->editVendor($vendor, $requestData);
    }
}
