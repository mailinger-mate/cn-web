<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\EventListener;

use Connect\Bundle\UMSSecurityBundle\Controller\ContactController;
use Connect\Bundle\UMSSecurityBundle\Controller\DummyController;
use Connect\Bundle\UMSSecurityBundle\EventListener\ContactActionControllerListener;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSContactRepository;
use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Message\Response;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class ContactActionControllerListenerTest
 */
class ContactActionControllerListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onKernelController_with_not_closure_should_not_do_anything()
    {
        $request = Mockery::mock(Request::class);
        $controllerClosure = 'not-closure';

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $contactController = Mockery::mock(ContactController::class);
        $contactRepository = Mockery::mock(UMSContactRepository::class);

        /** @var FilterControllerEvent $event */
        /** @var ContactController $contactController */
        /** @var UMSContactRepository $contactRepository */
        (new ContactActionControllerListener($contactController, $contactRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_not_DummyController_should_not_do_anything()
    {
        $request = Mockery::mock(Request::class);
        $controllerClosure = [new \stdClass(), 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $contactController = Mockery::mock(ContactController::class);
        $contactRepository = Mockery::mock(UMSContactRepository::class);

        /** @var FilterControllerEvent $event */
        /** @var ContactController $contactController */
        /** @var UMSContactRepository $contactRepository */
        (new ContactActionControllerListener($contactController, $contactRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\RequestException
     */
    public function onKernelController_with_ums_request_exception_400_on_token_repository_should_rethrow()
    {
        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $response = Mockery::mock(Response::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->getMock();

        $exception = Mockery::mock(RequestException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();

        $contactRepository = Mockery::mock(UMSContactRepository::class)
            ->shouldReceive('findOneByAction')
            ->with('some-id')
            ->andThrow($exception)
            ->getMock();

        $contactController = Mockery::mock(ContactController::class);

        /** @var FilterControllerEvent $event */
        /** @var ContactController $contactController */
        /** @var UMSContactRepository $contactRepository */
        (new ContactActionControllerListener($contactController, $contactRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_ums_request_exception_404_on_token_repository_should_not_do_anything()
    {
        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $response = Mockery::mock(Response::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(404)
            ->getMock();

        $exception = Mockery::mock(RequestException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();

        $contactRepository = Mockery::mock(UMSContactRepository::class)
            ->shouldReceive('findOneByAction')
            ->with('some-id')
            ->andThrow($exception)
            ->getMock();

        $contactController = Mockery::mock(ContactController::class);

        /** @var FilterControllerEvent $event */
        /** @var ContactController $contactController */
        /** @var UMSContactRepository $contactRepository */
        (new ContactActionControllerListener($contactController, $contactRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_valid_action_should_set_contact_controller()
    {
        $contact = new Contact();

        $contactController = Mockery::mock(ContactController::class);

        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $parameterBag->shouldReceive('set')
            ->withArgs(['contact', $contact]);
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->once()
            ->with([$contactController, 'confirmAction']);

        $contactRepository = Mockery::mock(UMSContactRepository::class)
            ->shouldReceive('findOneByAction')
            ->with('some-id')
            ->andReturn($contact)
            ->getMock();

        /** @var FilterControllerEvent $event */
        /** @var ContactController $contactController */
        /** @var UMSContactRepository $contactRepository */
        (new ContactActionControllerListener($contactController, $contactRepository))
            ->onKernelController($event);
    }
}
