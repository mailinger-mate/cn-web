<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\EventListener;

use Connect\Bundle\UMSSecurityBundle\Controller\DummyController;
use Connect\Bundle\UMSSecurityBundle\Controller\UserController;
use Connect\Bundle\UMSSecurityBundle\EventListener\UserActionControllerListener;
use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Domain\User;
use Connect\Domain\UserAction;
use GuzzleHttp\Exception\RequestException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class UserActionControllerListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onKernelController_with_not_closure_should_not_do_anything()
    {
        $request = Mockery::mock(Request::class);
        $controllerClosure = 'not-closure';

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $userController = Mockery::mock(UserController::class);
        $userRepository = Mockery::mock(UMSUserRepository::class);

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_not_DummyController_should_not_do_anything()
    {
        $request = Mockery::mock(Request::class);
        $controllerClosure = [new \stdClass(), 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $userController = Mockery::mock(UserController::class);
        $userRepository = Mockery::mock(UMSUserRepository::class);

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\RequestException
     */
    public function onKernelController_with_ums_request_exception_400_on_user_repository_should_rethrow()
    {
        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->getMock();

        $exception = Mockery::mock(RequestException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();

        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByAction')
            ->with('some-id')
            ->andThrow($exception)
            ->getMock();

        $userController = Mockery::mock(UserController::class);

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_ums_request_exception_404_on_token_repository_should_not_do_anything()
    {
        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(404)
            ->getMock();

        $exception = Mockery::mock(RequestException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();

        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByAction')
            ->with('some-id')
            ->andThrow($exception)
            ->getMock();

        $userController = Mockery::mock(UserController::class);

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_action_type_not_supported_should_not_do_anything()
    {
        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->never();

        $user = (new User());
        $user->addAction(
            (new UserAction())
                ->setId('some-id')
                ->setType('this-is-not-supported')
                ->setUser($user)
        );

        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByAction')
            ->with('some-id')
            ->andReturn($user)
            ->getMock();

        $userController = Mockery::mock(UserController::class);

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_action_type_password_reset_should_set_user_controller()
    {
        $user = new User();
        $user->addAction(
            (new UserAction())
                ->setId('some-id')
                ->setType('password_reset')
                ->setUser($user)
        );
        $userController = Mockery::mock(UserController::class);

        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $parameterBag->shouldReceive('set')
            ->withArgs(['user', $user]);
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->once()
            ->with([$userController, 'resetPasswordAction']);

        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByAction')
            ->with('some-id')
            ->andReturn($user)
            ->getMock();

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_action_type_account_activate_should_set_user_controller()
    {
        $user = new User();
        $user->addAction(
            (new UserAction())
                ->setId('some-id')
                ->setType('account_activate')
                ->setUser($user)
        );
        $userController = Mockery::mock(UserController::class);

        $parameterBag = Mockery::mock(ParameterBag::class)
            ->shouldReceive('get')
            ->with('action_id')
            ->andReturn('some-id')
            ->getMock();
        $parameterBag->shouldReceive('set')
            ->withArgs(['user', $user]);
        $request = Mockery::mock(Request::class);
        $request->attributes = $parameterBag;
        $controller = Mockery::mock(DummyController::class);
        $controllerClosure = [$controller, 'action'];

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getRequest')
            ->andReturn($request)
            ->getMock();
        $event->shouldReceive('getController')
            ->andReturn($controllerClosure);
        $event->shouldReceive('setController')
            ->once()
            ->with([$userController, 'accountActivateAction']);

        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByAction')
            ->with('some-id')
            ->andReturn($user)
            ->getMock();

        /** @var FilterControllerEvent $event */
        /** @var UserController $userController */
        /** @var UMSUserRepository $userRepository */
        (new UserActionControllerListener($userController, $userRepository))
            ->onKernelController($event);
    }
}
