<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;

use Connect\Bundle\UMSSecurityBundle\Form\AdminUserFormFieldMapper;
use PHPUnit_Framework_TestCase;

class AdminUserFormFieldMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getMap_should_return_array()
    {
        $this->assertSame(
            [
                'name' => 'name',
                'contacts.0.value' => 'contact',
                'role' => 'role',
            ],
            (new AdminUserFormFieldMapper())->getMap()
        );
    }
}
