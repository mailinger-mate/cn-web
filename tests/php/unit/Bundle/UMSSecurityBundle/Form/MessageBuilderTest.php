<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;

use Connect\Bundle\UMSSecurityBundle\Form\MessageBuilder;
use Mockery;
use Mockery\MockInterface;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MessageBuilderTest
 */
class MessageBuilderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function build_with_child_error_should_return_array_with_child_one_message()
    {
        $errorMessageLabel  = 'message';
        $errorMessage       = 'some error message';
        $childName          = 'child_name';

        $childForm = Mockery::mock(FormInterface::class)
            ->shouldReceive('getName')
            ->once()
            ->andReturn($childName)
            ->getMock();

        $formError = Mockery::mock(FormError::class)
            ->shouldReceive('getMessage')
            ->once()
            ->andReturn($errorMessage)
            ->getMock();

        $formChildErrorIterator = Mockery::mock(FormErrorIterator::class)
            ->shouldReceive('getForm')
            ->once()
            ->andReturn($childForm)
            ->getMock();

        $this->setupMockIteratorValues($formChildErrorIterator, [$formError]);

        $formErrorIterator = Mockery::mock(FormErrorIterator::class);
        $this->setupMockIteratorValues($formErrorIterator, [$formChildErrorIterator]);

        /** @var FormInterface $form */
        $form = Mockery::mock(FormInterface::class)
            ->shouldReceive('getErrors')
            ->once()
            ->with(true, false)
            ->andReturn($formErrorIterator)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = $this->getTranslatorMock('label.message', $errorMessageLabel);

        $messageBuilder = new MessageBuilder($translator);
        $expectedErrors = [$errorMessageLabel => [$childName => [$errorMessage]]];

        $this->assertSame($expectedErrors, $messageBuilder->build($form));
    }

    /**
     * @test
     */
    public function build_with_child_errors_should_return_array_with_child_multiple_messages()
    {
        $errorMessageLabel  = 'message';
        $errorMessage       = 'some error message';
        $childName          = 'child_name';

        $childForm = Mockery::mock(FormInterface::class)
            ->shouldReceive('getName')
            ->once()
            ->andReturn($childName)
            ->getMock();

        $formError = Mockery::mock(FormError::class)
            ->shouldReceive('getMessage')
            ->times(3)
            ->andReturn($errorMessage)
            ->getMock();

        $formChildErrorIterator = Mockery::mock(FormErrorIterator::class)
            ->shouldReceive('getForm')
            ->once()
            ->andReturn($childForm)
            ->getMock();

        $this->setupMockIteratorValues($formChildErrorIterator, [$formError, $formError, $formError]);

        $formErrorIterator = Mockery::mock(FormErrorIterator::class);
        $this->setupMockIteratorValues($formErrorIterator, [$formChildErrorIterator]);

        /** @var FormInterface $form */
        $form = Mockery::mock(FormInterface::class)
            ->shouldReceive('getErrors')
            ->once()
            ->with(true, false)
            ->andReturn($formErrorIterator)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = $this->getTranslatorMock('label.message', $errorMessageLabel);

        $messageBuilder = new MessageBuilder($translator);
        $expectedErrors = [
            $errorMessageLabel => [
                $childName => [$errorMessage, $errorMessage, $errorMessage]
            ]
        ];

        $this->assertSame($expectedErrors, $messageBuilder->build($form));
    }

    /**
     * @test
     */
    public function build_with_global_errors_should_return_array_with_global_messages()
    {
        $errorMessageLabel  = 'message';
        $errorMessage       = 'some error message';

        $formError = Mockery::mock(FormError::class)
            ->shouldReceive('getMessage')
            ->times(2)
            ->andReturn($errorMessage)
            ->getMock();

        $formErrorIterator = Mockery::mock(FormErrorIterator::class);
        $this->setupMockIteratorValues($formErrorIterator, [$formError, $formError]);

        /** @var FormInterface $form */
        $form = Mockery::mock(FormInterface::class)
            ->shouldReceive('getErrors')
            ->once()
            ->with(true, false)
            ->andReturn($formErrorIterator)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = $this->getTranslatorMock('label.message', $errorMessageLabel);

        $messageBuilder = new MessageBuilder($translator);
        $expectedErrors = [
            $errorMessageLabel => [$errorMessage, $errorMessage]
        ];

        $this->assertSame($expectedErrors, $messageBuilder->build($form));
    }

    /**
     * @param MockInterface $iteratorMock
     * @param array $values
     * @return MockInterface
     */
    private function setupMockIteratorValues(MockInterface $iteratorMock, array $values)
    {
        $iteratorMock
            ->shouldReceive('rewind')
            ->once()
            ->getMock();

        foreach ($values as $value) {
            $iteratorMock
                ->shouldReceive('valid')
                ->once()
                ->andReturn(true)
                ->getMock()

                ->shouldReceive('current')
                ->once()
                ->andReturn($value)
                ->getMock()

                ->shouldReceive('next')
                ->once()
                ->getMock();
        }

        $iteratorMock
            ->shouldReceive('valid')
            ->once()
            ->andReturn(false)
            ->getMock();

        return $iteratorMock;
    }

    /**
     * @param $messageKey
     * @param $messageText
     * @return MockInterface
     */
    private function getTranslatorMock($messageKey, $messageText)
    {
        return Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with($messageKey)
            ->andReturn($messageText)
            ->getMock();
    }
}
