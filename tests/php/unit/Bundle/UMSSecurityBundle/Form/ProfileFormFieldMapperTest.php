<?php
namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;


use Connect\Bundle\UMSSecurityBundle\Form\ProfileFormFieldMapper;

class ProfileFormFieldMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getMap_should_return_array()
    {
        $this->assertSame(
            [
                'name' => 'name',
                'contact' => 'contact',
                'password.first' => 'password_first',
                'current_password' => 'current_password',
            ],
            (new ProfileFormFieldMapper())->getMap()
        );
    }

}
