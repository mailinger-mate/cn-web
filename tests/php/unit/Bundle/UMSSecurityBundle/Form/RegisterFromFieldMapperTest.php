<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;

use Connect\Bundle\UMSSecurityBundle\Form\RegisterFromFieldMapper;
use PHPUnit_Framework_TestCase;

class RegisterFromFieldMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getMap_should_return_array()
    {
        $this->assertSame(
            [
                'name' => 'name',
                'vendor.name' => 'vendor',
                'contacts.0.value' => 'contact',
                'password.first' => 'password_first',
            ],
            (new RegisterFromFieldMapper())->getMap()
        );
    }
}
