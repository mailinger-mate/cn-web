<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;

use Connect\Bundle\UMSSecurityBundle\Form\RemindPassFromFieldMapper;
use PHPUnit_Framework_TestCase;

class RemindPassFromFieldMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getMap_should_return_array()
    {
        $this->assertSame(['email' => 'email'], (new RemindPassFromFieldMapper())->getMap());
    }
}
