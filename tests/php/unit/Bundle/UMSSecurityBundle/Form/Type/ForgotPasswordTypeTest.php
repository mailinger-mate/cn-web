<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\ForgotPasswordType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ForgotPasswordTypeTest
 */
class ForgotPasswordTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_set_form_fields()
    {
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->withArgs(['email', 'email'])
            ->andReturnSelf()
            ->getMock();
        $builder->shouldReceive('add')
            ->withArgs(['reset', 'submit']);

        /** @var FormBuilderInterface $builder */
        (new ForgotPasswordType())->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function getName_should_return_string()
    {
        $this->assertSame('forgot_password', (new ForgotPasswordType())->getName());
    }
}
