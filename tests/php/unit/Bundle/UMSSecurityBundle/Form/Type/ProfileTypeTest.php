<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\ProfileType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProfileTypeTest
 */
class ProfileTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_add_event_subscriber()
    {
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('name', 'text')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('contact', 'email')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('password_first', 'password')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('password_second', 'password')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('current_password', 'password')
            ->andReturnSelf()
            ->getMock();

        /** @var FormBuilderInterface $builder */
        (new ProfileType())->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function setDefaultOptions_should_configure_proper_values()
    {
        $resolver = Mockery::mock(OptionsResolverInterface::class)
            ->shouldReceive('setDefaults')
            ->with([
                'csrf_protection'   => false,
            ])
            ->getMock();

        /** @var OptionsResolverInterface $resolver */
        (new ProfileType())->setDefaultOptions($resolver);
    }

    /**
     * @test
     */
    public function getName_should_return_string_user_edit()
    {
        $userType = new ProfileType();
        $this->assertSame('profile', $userType->getName());
    }
}
