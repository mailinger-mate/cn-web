<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\TokenCardType;
use Connect\Domain\Card\TokenCard;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TokenCardTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_add_event_subscriber()
    {
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('card_token', 'text', ['property_path' => 'cardToken'])
            ->getMock();

        /** @var FormBuilderInterface $builder */
        (new TokenCardType())->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function setDefaultOptions_should_configure_proper_values()
    {
        $resolver = Mockery::mock(OptionsResolverInterface::class)
            ->shouldReceive('setDefaults')
            ->with([
                'data_class'      => TokenCard::class,
                'csrf_protection' => false,
            ])
            ->getMock();

        /** @var OptionsResolverInterface $resolver */
        (new TokenCardType())->setDefaultOptions($resolver);
    }

    /**
     * @test
     */
    public function getName_should_return_string()
    {
        $this->assertSame('customer', (new TokenCardType())->getName());
    }
}
