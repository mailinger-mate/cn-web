<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\UserAdminType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserAdminTypeTest
 */
class UserAdminTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getName_should_return_string_name()
    {
        $this->assertSame('user_admin', (new UserAdminType())->getName());
    }

    /**
     * @test
     */
    public function setDefaultOptions_should_configure_proper_values()
    {
        /** @var OptionsResolverInterface $resolver */
        $resolver = Mockery::mock(OptionsResolverInterface::class)
            ->shouldReceive('setDefaults')
            ->with([
                'csrf_protection' => false,
            ])
            ->getMock();

        (new UserAdminType())->setDefaultOptions($resolver);
    }

    /**
     * @test
     */
    public function buildForm_should_set_fields()
    {
        /** @var FormBuilderInterface $formBuilder */
        $formBuilder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('name', 'text')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('contact', 'email')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('role', 'text')
            ->getMock();

        (new UserAdminType())->buildForm($formBuilder, []);
    }
}
