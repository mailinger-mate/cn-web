<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\UserSetPasswordType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UserSetPasswordTypeTest
 */
class UserSetPasswordTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_set_fields_and_constraints()
    {
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('contact', 'email', ['disabled' => true])
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('password_first', 'password')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('password_second', 'password')
            ->andReturnSelf()
            ->shouldReceive('add')
            ->with('save', 'submit')
            ->andReturnSelf()
            ->getMock();

        $userType = new UserSetPasswordType();
        /** @var FormBuilderInterface $builder */
        $userType->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function getName_should_return_user_string()
    {
        $this->assertSame('user_set_password', (new UserSetPasswordType())->getName());
    }
}
