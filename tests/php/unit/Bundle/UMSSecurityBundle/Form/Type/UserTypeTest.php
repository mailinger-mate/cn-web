<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\UserType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UserTypeTest
 */
class UserTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_set_fields_and_constraints()
    {
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')
            ->with('name', 'text')
            ->andReturnSelf()
            ->getMock();

        $builder
            ->shouldReceive('add')
            ->with('contact', 'email')
            ->andReturnSelf();

        $builder
            ->shouldReceive('add')
            ->with('vendor', 'text')
            ->andReturnSelf();

        $builder
            ->shouldReceive('add')
            ->with('password_first', 'password')
            ->andReturnSelf();

        $builder
            ->shouldReceive('add')
            ->with('password_second', 'password')
            ->andReturnSelf();

        $builder
            ->shouldReceive('add')
            ->with('newsletter_agreement', 'checkbox')
            ->andReturnSelf();

        $builder
            ->shouldReceive('add')
            ->with('submit', 'submit')
            ->andReturnSelf();

        $userType = new UserType();
        /** @var FormBuilderInterface $builder */
        $userType->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function getName_should_return_user_string()
    {
        $userType = new UserType();
        $this->assertSame('user', $userType->getName());
    }
}
