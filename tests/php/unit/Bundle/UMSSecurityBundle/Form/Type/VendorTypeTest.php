<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form\Type;

use Connect\Bundle\UMSSecurityBundle\Form\Type\VendorType;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VendorTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function buildForm_should_add_fields()
    {
        $builder = Mockery::mock(FormBuilderInterface::class)
            ->shouldReceive('add')->with('billing_name', 'text', ['property_path' => '[billingName]'])->andReturnSelf()
            ->shouldReceive('add')->with('street', 'text')->andReturnSelf()
            ->shouldReceive('add')->with('city', 'text')->andReturnSelf()
            ->shouldReceive('add')->with('state', 'text')->andReturnSelf()
            ->shouldReceive('add')->with('country', 'text')->andReturnSelf()
            ->shouldReceive('add')->with('vat_number', 'text', ['property_path' => '[vatNumber]'])->andReturnSelf()
            ->shouldReceive('add')->with('note', 'text')->andReturnSelf()
            ->shouldReceive('add')->with('po_number', 'text', ['property_path' => '[poNumber]'])->andReturnSelf()
            ->getMock();

        /** @var FormBuilderInterface $builder */
        (new VendorType())->buildForm($builder, []);
    }

    /**
     * @test
     */
    public function setDefaultOptions_should_configure_proper_values()
    {
        $resolver = Mockery::mock(OptionsResolverInterface::class)
            ->shouldReceive('setDefaults')
            ->with([
                'csrf_protection'   => false,
            ])
            ->getMock();

        /** @var OptionsResolverInterface $resolver */
        (new VendorType())->setDefaultOptions($resolver);
    }

    /**
     * @test
     */
    public function getName_should_return_string_user_edit()
    {
        $userType = new VendorType();
        $this->assertSame('vendor', $userType->getName());
    }
}
