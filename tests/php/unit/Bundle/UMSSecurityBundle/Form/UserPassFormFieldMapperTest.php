<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;

use Connect\Bundle\UMSSecurityBundle\Form\UserPassFormFieldMapper;
use PHPUnit_Framework_TestCase;

class UserPassFormFieldMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getMap_should_return_array()
    {
        $this->assertSame(
            ['password.first' => 'password_first'],
            (new UserPassFormFieldMapper())->getMap()
        );
    }
}
