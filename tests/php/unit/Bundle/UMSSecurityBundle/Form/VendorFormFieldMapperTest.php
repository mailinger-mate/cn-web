<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Form;

use Connect\Bundle\UMSSecurityBundle\Form\VendorFormFieldMapper;
use PHPUnit_Framework_TestCase;

class VendorFormFieldMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getMap_should_return_array()
    {
        $this->assertSame(
            [
                'billingName' => 'billing_name',
                'street'      => 'street',
                'city'        => 'city',
                'state'       => 'state',
                'country'     => 'country',
                'vatNumber'   => 'vat_number',
                'note'        => 'note',
                'poNumber'    => 'po_number',
            ],
            (new VendorFormFieldMapper())->getMap()
        );
    }

}
