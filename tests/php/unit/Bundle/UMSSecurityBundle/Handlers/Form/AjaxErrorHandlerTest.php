<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Handlers\Form;

use Connect\Bundle\UMSSecurityBundle\Form\FieldMapperInterface;
use Connect\Bundle\UMSSecurityBundle\Handlers\Form\AjaxErrorHandler;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class AjaxErrorHandlerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException \LogicException
     * @expectedExceptionMessage Response body does not contain `message` key
     */
    public function handle_without_message_key_in_response_throws_exception()
    {
        $mapper = Mockery::mock(FieldMapperInterface::class);
        $mapper->shouldNotHaveReceived('getMap');

        /** @var Form $form */
        $form = Mockery::mock(Form::class);

        /** @var FieldMapperInterface $mapper */
        (new AjaxErrorHandler($mapper))->handle([], $form);
    }
    /**
     * @test
     * @expectedException \Connect\Bundle\UMSSecurityBundle\Handlers\Form\Exception\InvalidFormatException
     * @expectedExceptionMessage "message" expected to be array, "string" given
     */
    public function handle_with_invalid_format_throws_exception()
    {
        $mapper = Mockery::mock(FieldMapperInterface::class);
        $mapper->shouldNotHaveReceived('getMap');

        /** @var Form $form */
        $form = Mockery::mock(Form::class);

        /** @var FieldMapperInterface $mapper */
        (new AjaxErrorHandler($mapper))
            ->handle(['message'=> 'some error string'], $form);
    }

    /**
     * @test
     */
    public function handle_without_messages_does_not_modify_form()
    {
        $mapper = Mockery::mock(FieldMapperInterface::class);
        $mapper->shouldReceive('getMap')
            ->andReturn(['some' => 'field']);

        $form = Mockery::mock(Form::class);
        $form->shouldNotHaveReceived('get');

        /** @var FieldMapperInterface $mapper */
        /** @var Form $form */
        (new AjaxErrorHandler($mapper))->handle(['message' => []], $form);
    }

    /**
     * @test
     */
    public function handle_with_messages_sets_error()
    {
        /** @var FieldMapperInterface $mapper */
        $mapper = Mockery::mock(FieldMapperInterface::class)
            ->shouldReceive('getMap')
            ->once()
            ->andReturn(
                [
                    'foo'        => 'foo',
                    'bar.baz'    => 'baz',
                    'qux.0.norf' => 'norf',
                ]
            )
            ->getMock();

        /** @var FormInterface $formChild */
        $formChild = Mockery::mock(FormInterface::class)
            ->shouldReceive('addError')
            ->once()
            ->with(Mockery::type(FormError::class))
            ->andReturnSelf()
            ->getMock();

        /** @var FormInterface $formChild */
        $formChildNested = Mockery::mock(FormInterface::class)
            ->shouldReceive('addError')
            ->once()
            ->with(Mockery::type(FormError::class))
            ->andReturnSelf()
            ->getMock();

        /** @var FormInterface $formChild */
        $formChildCollection = Mockery::mock(FormInterface::class)
            ->shouldReceive('addError')
            ->once()
            ->with(Mockery::type(FormError::class))
            ->andReturnSelf()
            ->getMock();

        /** @var Form $formParent */
        $formParent = Mockery::mock(Form::class)
            ->shouldReceive('get')
            ->with('foo')
            ->andReturn($formChild)
            ->shouldReceive('get')
            ->with('baz')
            ->andReturn($formChildNested)
            ->shouldReceive('get')
            ->with('norf')
            ->andReturn($formChildCollection)
            ->getMock();

        (new AjaxErrorHandler($mapper))
            ->handle(
                [
                    'message' => [
                        'foo' => ['some error message'],
                        'bar' => ['baz' => ['some error 2']],
                        'qux' => [['norf' => ['some error 3']]],
                    ],
                ],
                $formParent
            );
    }
}
