<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Mail\Message;

use Connect\Bundle\UMSSecurityBundle\Mail\Message\Factory;
use Connect\Domain\Contact;
use Connect\Domain\User;
use Connect\Domain\UserAction;
use Connect\Domain\Vendor;
use Mockery;
use PHPUnit_Framework_TestCase;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class FactoryTest
 */
class FactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getResetPasswordMessage_should_return_mail_to_reset_password()
    {
        $to = 'test@x-formation.com';
        $subject = 'Reset password subject';
        $body = 'some body';
        $userName = 'user name';
        $link = 'some_url';
        $tokenId = 'some_token_id';
        $vendorName = 'XF';

        $vendor = (new Vendor())->setName($vendorName);
        $user = (new User())
            ->setName($userName)
            ->setVendor($vendor);
        $action = (new UserAction())->setId($tokenId)->setType('password_reset')->setUser($user);
        $contact = (new Contact())
            ->setValue($to)
            ->setUser($user);
        $user
            ->addAction($action)
            ->addContact($contact);

        /** @var Swift_Message $message */
        $message = Swift_Message::newInstance();

        /** @var Swift_Mailer $mailer */
        $mailer = Mockery::mock(Swift_Mailer::class)
            ->shouldReceive('createMessage')
            ->andReturn($message)
            ->getMock();

        $context = Mockery::mock(RequestContext::class)
            ->shouldReceive('getScheme')
            ->andReturn('https')
            ->shouldReceive('getHost')
            ->andReturn('connect.dev')
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('getContext')
            ->andReturn($context)
            ->shouldReceive('generate')
            ->with(
                'ums_action',
                ['action_id' => $tokenId],
                Router::ABSOLUTE_URL
            )
            ->andReturn($link)
            ->getMock();

        /** @var TwigEngine $twig */
        $twig = Mockery::mock(TwigEngine::class)
            ->shouldReceive('render')
            ->with(
                'ConnectUMSSecurityBundle:Email/Content:passwordReset.html.twig',
                [
                    'name'            => $userName,
                    'vendor'          => $vendorName,
                    'email'           => $to,
                    'link'            => $link,
                    'scheme_and_host' => 'https://connect.dev',
                ]
            )
            ->andReturn($body)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with('email.password_reset.subject')
            ->andReturn($subject)
            ->getMock();

        $resultMessage = (new Factory(
            $mailer, $translator, $router, $twig
        ))
            ->getResetPasswordMessage($user);

        $this->assertInstanceOf(Swift_Message::class, $resultMessage);
        $this->assertSame($body, $resultMessage->getBody());
        $this->assertSame([$to => null], $resultMessage->getTo());
        $this->assertSame(['noreply@x-formation.com' => 'Connect'], $resultMessage->getFrom());
        $this->assertSame($subject, $resultMessage->getSubject());
    }

    /**
     * @test
     */
    public function getActivateAccountMessage_should_return_mail_to_activate_account()
    {
        $to = 'test@x-formation.com';
        $subject = 'Activate account subject';
        $body = 'some body';
        $userName = 'user name';
        $link = 'some_url';
        $actionId = 'some_token_id';
        $vendorName = 'XF';

        $vendor = (new Vendor())->setName($vendorName);
        $user = (new User())
            ->setName($userName)
            ->setVendor($vendor);
        $user
            ->addContact(
                (new Contact())
                    ->setValue($to)
                    ->setUser($user)
            )
            ->addAction(
                (new UserAction())
                    ->setId($actionId)
                    ->setType('account_activate')
                    ->setUser($user)
            );

        /** @var Swift_Message $message */
        $message = Swift_Message::newInstance();

        /** @var Swift_Mailer $mailer */
        $mailer = Mockery::mock(Swift_Mailer::class)
            ->shouldReceive('createMessage')
            ->andReturn($message)
            ->getMock();

        $context = Mockery::mock(RequestContext::class)
            ->shouldReceive('getScheme')
            ->andReturn('https')
            ->shouldReceive('getHost')
            ->andReturn('connect.dev')
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('getContext')
            ->andReturn($context)
            ->shouldReceive('generate')
            ->with(
                'ums_action',
                ['action_id' => $actionId],
                Router::ABSOLUTE_URL
            )
            ->andReturn($link)
            ->getMock();

        /** @var TwigEngine $twig */
        $twig = Mockery::mock(TwigEngine::class)
            ->shouldReceive('render')
            ->with(
                'ConnectUMSSecurityBundle:Email/Content:activateAccount.html.twig',
                [
                    'name'   => $userName,
                    'vendor' => $vendorName,
                    'email'  => $to,
                    'link'   => $link,
                    'scheme_and_host' => 'https://connect.dev',
                ]
            )
            ->andReturn($body)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with('email.activate_account.subject')
            ->andReturn($subject)
            ->getMock();

        $resultMessage = (new Factory(
            $mailer, $translator, $router, $twig
        ))
            ->getActivateAccountMessage($user);

        $this->assertInstanceOf(Swift_Message::class, $resultMessage);
        $this->assertSame($body, $resultMessage->getBody());
        $this->assertSame([$to => null], $resultMessage->getTo());
        $this->assertSame(['noreply@x-formation.com' => 'Connect'], $resultMessage->getFrom());
        $this->assertSame($subject, $resultMessage->getSubject());
    }

    /**
     * @test
     */
    public function getContactConfirmMessage_should_return_mail_to_confirm_contact()
    {
        $to = 'test@x-formation.com';
        $subject = 'Contact Confirm subject';
        $body = 'some body';
        $userName = 'user name';
        $link = 'some_url';
        $tokenId = 'some_token_id';

        $user = (new User())
            ->setName($userName);
        $contact = (new Contact())
            ->setValue($to)
            ->setUser($user);

        /** @var Swift_Message $message */
        $message = Swift_Message::newInstance();

        /** @var Swift_Mailer $mailer */
        $mailer = Mockery::mock(Swift_Mailer::class)
            ->shouldReceive('createMessage')
            ->andReturn($message)
            ->getMock();

        $context = Mockery::mock(RequestContext::class)
            ->shouldReceive('getScheme')
            ->andReturn('https')
            ->shouldReceive('getHost')
            ->andReturn('connect.dev')
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('getContext')
            ->andReturn($context)
            ->shouldReceive('generate')
            ->with(
                'ums_action',
                ['action_id' => $tokenId],
                Router::ABSOLUTE_URL
            )
            ->andReturn($link)
            ->getMock();

        /** @var TwigEngine $twig */
        $twig = Mockery::mock(TwigEngine::class)
            ->shouldReceive('render')
            ->with(
                'ConnectUMSSecurityBundle:Email/Content:contactVerification.html.twig',
                [
                    'name' => $userName,
                    'link' => $link,
                    'scheme_and_host' => 'https://connect.dev',
                ]
            )
            ->andReturn($body)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with('email.contact_verification.subject')
            ->andReturn($subject)
            ->getMock();

        $resultMessage = (new Factory(
            $mailer, $translator, $router, $twig
        ))
            ->getContactConfirmMessage($tokenId, $contact);

        $this->assertInstanceOf(Swift_Message::class, $resultMessage);
        $this->assertSame($body, $resultMessage->getBody());
        $this->assertSame([$to => null], $resultMessage->getTo());
        $this->assertSame(['noreply@x-formation.com' => 'Connect'], $resultMessage->getFrom());
        $this->assertSame($subject, $resultMessage->getSubject());
    }

    /**
     * @test
     */
    public function getVendorRegistrationMessage_should_return_mail_with_welcome_message()
    {
        $to = 'test@x-formation.com';
        $subject = 'Vendor Registration subject';
        $body = 'some body';
        $userName = 'user name';
        $link = 'some_url';
        $tokenId = 'some_token_id';

        $user = (new User())
            ->setName($userName);
        $contact = (new Contact())
            ->setValue($to)
            ->setUser($user);

        /** @var Swift_Message $message */
        $message = Swift_Message::newInstance();

        /** @var Swift_Mailer $mailer */
        $mailer = Mockery::mock(Swift_Mailer::class)
            ->shouldReceive('createMessage')
            ->andReturn($message)
            ->getMock();

        $context = Mockery::mock(RequestContext::class)
            ->shouldReceive('getScheme')
            ->andReturn('https')
            ->shouldReceive('getHost')
            ->andReturn('connect.dev')
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('getContext')
            ->andReturn($context)
            ->shouldReceive('generate')
            ->with(
                'ums_action',
                ['action_id' => $tokenId],
                Router::ABSOLUTE_URL
            )
            ->andReturn($link)
            ->getMock();

        /** @var TwigEngine $twig */
        $twig = Mockery::mock(TwigEngine::class)
            ->shouldReceive('render')
            ->with(
                'ConnectUMSSecurityBundle:Email/Content:vendorRegistration.html.twig',
                [
                    'name' => $userName,
                    'link' => $link,
                    'scheme_and_host' => 'https://connect.dev',
                ]
            )
            ->andReturn($body)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->with('email.vendor_registration.subject')
            ->andReturn($subject)
            ->getMock();

        $resultMessage = (new Factory(
            $mailer, $translator, $router, $twig
        ))
            ->getVendorRegistrationMessage($tokenId, $contact);

        $this->assertInstanceOf(Swift_Message::class, $resultMessage);
        $this->assertSame($body, $resultMessage->getBody());
        $this->assertSame([$to => null], $resultMessage->getTo());
        $this->assertSame(['noreply@x-formation.com' => 'Connect'], $resultMessage->getFrom());
        $this->assertSame($subject, $resultMessage->getSubject());
    }
}
