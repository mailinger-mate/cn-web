<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Repository\StripeCustomerRepository;
use Connect\Bundle\UMSSecurityBundle\Stripe\Client;
use Connect\Domain\Card\SimpleCard;
use Connect\Domain\Card\TokenCard;
use Connect\Domain\Customer;
use Mockery;
use PHPUnit_Framework_TestCase;
use Stripe\Customer as StripeCustomer;
use Stripe\Error\InvalidRequest;

class StripeCustomerRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function find_with_valid_data_should_return_card()
    {
        $expectedCustomer = (new Customer())
            ->setId('cust.id')
            ->setCard(
                (new SimpleCard())
                    ->setBrand('Visa')
                    ->setExpirationMonth(7)
                    ->setExpirationYear(2018)
                    ->setLastFourDigits('0574'));

        $resultCustomer = Mockery::mock(StripeCustomer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [
                            [
                                'brand'     => 'Visa',
                                'exp_month' => 7,
                                'exp_year'  => 2018,
                                'last4'     => '0574',
                            ],
                        ],
                    ],
                ]
            )
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('cust.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $customer = (new Customer())->setId('cust.id');

        (new StripeCustomerRepository($client))->refresh($customer);

        $this->assertEquals($expectedCustomer, $customer);
    }

    /**
     * @test
     */
    public function find_with_404_from_stripe_should_clear_out_customer()
    {
        $exception = Mockery::mock(InvalidRequest::class)
            ->shouldReceive('getHttpStatus')
            ->andReturn(404)
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('cust.id')
            ->andThrow($exception)
            ->getMock();

        $customer = (new Customer())
            ->setId('cust.id')
            ->setCard(new SimpleCard());

        (new StripeCustomerRepository($client))->refresh($customer);

        $this->assertEquals(new Customer(), $customer);
    }

    /**
     * @test
     */
    public function find_with_deleted_customer_should_clear_out_customer()
    {
        $resultCustomer = Mockery::mock(StripeCustomer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'deleted' => true,
                    'id'      => 'cust.id',
                ]
            )
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('cust.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $customer = (new Customer())
            ->setId('cust.id')
            ->setCard(new SimpleCard());

        (new StripeCustomerRepository($client))->refresh($customer);

        $this->assertEquals(new Customer(), $customer);
    }

    /**
     * @test
     */
    public function find_with_customer_without_card_clear_out_customer()
    {
        $resultCustomer = Mockery::mock(StripeCustomer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [],
                    ],
                ]
            )
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('cust.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $customer = (new Customer())
            ->setId('cust.id')
            ->setCard(new SimpleCard());

        (new StripeCustomerRepository($client))->refresh($customer);

        $this->assertEquals(new Customer(), $customer);
    }

    /**
     * @test
     */
    public function find_with_customer_with_multiple_cards_clear_out_customer()
    {
        $resultCustomer = Mockery::mock(StripeCustomer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(
                [
                    'sources' => [
                        'data' => [['card1'], ['card2']],
                    ],
                ]
            )
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('cust.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $customer = (new Customer())
            ->setId('cust.id')
            ->setCard(new SimpleCard());

        (new StripeCustomerRepository($client))->refresh($customer);

        $this->assertEquals(new Customer(), $customer);
    }


    /**
     * @test
     * @expectedException \Stripe\Error\InvalidRequest
     */
    public function find_with_stripe_exception_should_rethrow_exception()
    {
        $exception = Mockery::mock(InvalidRequest::class)
            ->shouldReceive('getHttpStatus')
            ->andReturn(500)
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->with('cust.id')
            ->andThrow($exception)
            ->getMock();

        $customer = (new Customer())
            ->setId('cust.id');

        (new StripeCustomerRepository($client))->refresh($customer);
    }

    /**
     * @test
     */
    public function add_with_valid_data_should_create_customer_in_stripe()
    {
        $resultCustomer = Mockery::mock(StripeCustomer::class)
            ->shouldReceive('jsonSerialize')
            ->andReturn(['id' => 'cust.id'])
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('createCustomer')
            ->once()
            ->with('token.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $customer = (new Customer())->setCard((new TokenCard())->setCardToken('token.id'));

        (new StripeCustomerRepository($client))->add($customer);

        $this->assertSame('cust.id', $customer->getId());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Add customer is supported only using TokenCard
     */
    public function add_with_invalid_data_should_throw_exception()
    {
        /** @var Client $client */
        $client = Mockery::mock(Client::class);

        $customer = (new Customer())->setCard(new SimpleCard());

        (new StripeCustomerRepository($client))->add($customer);
    }

    /**
     * @test
     */
    public function remove_should_remove_customer_from_stripe()
    {
        $resultCustomer = Mockery::mock(StripeCustomer::class)
            ->shouldReceive('delete')
            ->once()
            ->getMock();

        /** @var Client $client */
        $client = Mockery::mock(Client::class)
            ->shouldReceive('retrieveCustomer')
            ->once()
            ->with('cust.id')
            ->andReturn($resultCustomer)
            ->getMock();

        $customer = (new Customer())->setId('cust.id');

        (new StripeCustomerRepository($client))->remove($customer);
    }
}
