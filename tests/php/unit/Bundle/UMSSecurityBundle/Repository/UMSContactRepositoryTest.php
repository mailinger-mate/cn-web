<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSContactRepository;
use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use DateTime;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * Class UMSContactRepositoryTest
 */
class UMSContactRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function findOneByAction_with_existing_action_should_return_contact()
    {
        $contact = (new Contact())
            ->setId(1)
            ->setType('email')
            ->setValue('john@xf.com')
            ->setVerified(false);
        $action = (new ContactAction())
            ->setId('9a66e06ded843e5848f14fccd6656d41')
            ->setType('contact_verify')
            ->setExpirationTime(new DateTime('2030-05-25T22:06:59+0000'))
            ->setContact($contact);
        $contact->addAction($action);

        $dataArray = [
            [
                "value"     => "john@xf.com",
                "type"      => "email",
                "verified"  => false,
                "id"        => 1,
                "_links"    => [
                    "self" => [
                        "href" => "http://localhost/rest/v1/contacts/1",
                    ],
                    "user" => [
                        "href" => "http://localhost/rest/v1/users/1",
                    ],
                ],
                "_embedded" => [
                    "contact_actions" => [
                        [
                            "type"            => "contact_verify",
                            "expiration_time" => "2030-05-25T22:06:59+0000",
                            "id"              => "9a66e06ded843e5848f14fccd6656d41",
                            "_links"          => [
                                "self" => [
                                    "href" => "http://localhost/rest/v1/tokens/9a66e06ded843e5848f14fccd6656d41",
                                ],
                                "contact" => [
                                    "href" => "http://localhost/rest/v1/contacts/1",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/contacts?action=9a66e06ded843e5848f14fccd6656d41',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$dataJson, 'array', 'json'])
            ->andReturn($dataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $result = (new UMSContactRepository($client, $serializer))->findOneByAction('9a66e06ded843e5848f14fccd6656d41');

        $this->assertEquals($contact, $result);
    }

    /**
     * @test
     */
    public function findOneByAction_several_results_should_return_false()
    {
        $dataArray = [
            ['contact_1'],
            ['contact_2'],
        ];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/contacts?action=9a66e06ded843e5848f14fccd6656d41',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ]
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$dataJson, 'array', 'json'])
            ->andReturn($dataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $result = (new UMSContactRepository($client, $serializer))->findOneByAction('9a66e06ded843e5848f14fccd6656d41');

        $this->assertFalse($result);
    }

    /**
     * @test
     */
    public function findOneByAction_not_existing_action_should_return_false()
    {
        $dataArray = [];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/contacts?action=9a66e06ded843e5848f14fccd6656d41',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ]
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$dataJson, 'array', 'json'])
            ->andReturn($dataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $result = (new UMSContactRepository($client, $serializer))->findOneByAction('9a66e06ded843e5848f14fccd6656d41');

        $this->assertFalse($result);
    }
}
