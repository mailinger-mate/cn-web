<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Session;
use Connect\Domain\Vendor;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class UMSSessionRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function add_with_valid_data_returns_true()
    {
        $user = (new UMSUser())
            ->setId(8)
            ->setName("John Doe")
            ->setPassword("secret")
            ->setSalt("salt")
            ->setRoles(["user"])
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor((new Vendor())->setId(3));
        $session = (new Session())
            ->setUser($user);

        $dataArray = [
            'id' => 'session-id',
        ];
        $dataJson = json_encode($dataArray);
        $productName = 'product1';
        $expectedRequestData = ['user' => $user->getId(), 'product' => $productName];
        $expectedRequestDataJson = json_encode($expectedRequestData);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->shouldReceive('getStatusCode')
            ->andReturn(201)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'POST',
                'rest/v1/sessions',
                [
                    'body'    => $expectedRequestDataJson,
                    'headers' => [
                        'Accept'       => 'application/json',
                        'Content-Type' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$dataJson, 'array', 'json'])
            ->andReturn($dataArray)
            ->shouldReceive('serialize')
            ->withArgs([$expectedRequestData, 'json', null])
            ->andReturn($expectedRequestDataJson)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $this->assertTrue((new UMSSessionRepository($client, $serializer, $productName))->add($session));

        $this->assertSame('session-id', $session->getId());
    }

    /**
     * @test
     */
    public function add_with_with_error_should_return_false()
    {
        $user = (new UMSUser())
            ->setId(8)
            ->setName("John Doe")
            ->setPassword("secret")
            ->setSalt("salt")
            ->setRoles(["user"])
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor((new Vendor())->setId(3));
        $session = (new Session())
            ->setUser($user);

        $productName = 'product1';
        $expectedRequestData = ['user' => $user->getId(), 'product' => $productName];
        $expectedRequestDataJson = json_encode($expectedRequestData);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'POST',
                'rest/v1/sessions',
                [
                    'body'    => $expectedRequestDataJson,
                    'headers' => [
                        'Accept'       => 'application/json',
                        'Content-Type' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('serialize')
            ->withArgs([$expectedRequestData, 'json', null])
            ->andReturn($expectedRequestDataJson)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $this->assertFalse((new UMSSessionRepository($client, $serializer, $productName))->add($session));

        $this->assertNull($session->getId());
    }

    /**
     * @test
     */
    public function remove_with_not_existing_id_should_return_false()
    {
        $session = (new Session())
            ->setId('session_id');

        $productName = 'product1';

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(Response::HTTP_NOT_FOUND)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'DELETE',
                'rest/v1/sessions/session_id',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        /** @var UMSUser $user */
        $this->assertFalse((new UMSSessionRepository($client, $serializer, $productName))->remove($session));
    }

    /**
     * @test
     */
    public function remove_with_existing_id_should_return_true()
    {
        $session = (new Session())
            ->setId('session_id');

        $productName = 'product1';

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(Response::HTTP_NO_CONTENT)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'DELETE',
                'rest/v1/sessions/session_id',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        /** @var UMSUser $user */
        $this->assertTrue((new UMSSessionRepository($client, $serializer, $productName))->remove($session));
    }

    /**
     * @test
     */
    public function find_with_existing_id_should_return_session()
    {
        $session = (new Session())
            ->setId('session_id');

        $productName = 'product1';
        $dataArray = [
            'id' => 'session_id',
        ];
        $dataJson = json_encode($dataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($dataJson)
            ->shouldReceive('getStatusCode')
            ->andReturn(Response::HTTP_OK)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'GET',
                'rest/v1/sessions/session_id',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$dataJson, 'array', 'json'])
            ->andReturn($dataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        /** @var UMSUser $user */
        $this->assertEquals(
            $session,
            (new UMSSessionRepository($client, $serializer, $productName))->find('session_id')
        );
    }
}
