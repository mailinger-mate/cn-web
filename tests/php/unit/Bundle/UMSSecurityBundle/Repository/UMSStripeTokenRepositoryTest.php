<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSStripeTokenRepository;
use Connect\Domain\Customer;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Mockery;
use PHPUnit_Framework_TestCase;
use Zend\Stdlib\ResponseInterface;

class UMSStripeTokenRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function add_with_valid_data_should_returns_true()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(204)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'PUT',
                'rest/v1/stripe_tokens/cust.id',
                [
                    'body'    => '{"vendor": 1}',
                    'headers' => [
                        'Accept'       => 'application/json',
                        'Content-Type' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $customer = (new Customer())->setId('cust.id');

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('serialize')
            ->withArgs([$customer, 'json', null])
            ->andReturn('{"vendor": 1}')
            ->getMock();;


        $this->assertTrue((new UMSStripeTokenRepository($client, $serializer))->add($customer));
    }

    /**
     * @test
     */
    public function remove_with_valid_data_should_returns_true()
    {
        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(204)
            ->getMock();

        /** @var ClientInterface $client */
        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'DELETE',
                'rest/v1/stripe_tokens/cust.id',
                [
                    'headers' => [
                        'Accept'       => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $customer = (new Customer())->setId('cust.id');

        /** @var SerializerInterface $serializer */
        $serializer = Mockery::mock(SerializerInterface::class);

        $this->assertTrue((new UMSStripeTokenRepository($client, $serializer))->remove($customer));
    }
}
