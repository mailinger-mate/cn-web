<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Repository;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use Connect\Domain\Customer;
use Connect\Domain\UserAction;
use Connect\Domain\Vendor;
use DateTime;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class UMSUserRepositoryTest
 */
class UMSUserRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function findUserByEmailWithOneUserWithBothTokenAndContact()
    {
        $email = 'john@gmail.com';
        $responseDataArray = [
            [
                "id"        => 1,
                "name"      => "John Doe",
                "activated" => "false",
                "password"  => "secret",
                "salt"      => "salt",
                "role"      => "admin,user",
                "enabled"   => true,
                "_links"    => [
                    "self" => ["href" => "/rest/v1/users/1"],
                ],
                "_embedded" => [
                    "vendor"       => [
                        "id"         => 1,
                        "name"       => "Cool Co.",
                        "card_token" => "cust.id",
                        "_links"     => [
                            "self" => ["href" => "/rest/v1/vendors/1"],
                        ],
                    ],
                    "contacts"     => [
                        [
                            "id"        => 5,
                            "value"     => "john.doe@x-formation.com",
                            "type"      => "email",
                            "verified"  => true,
                            "_links"    => [
                                "self" => ["href" => "rest/v1/contacts/1"],
                            ],
                            '_embedded' => [
                                'contact_actions' => [],
                            ],
                        ],
                    ],
                    'user_actions' => [],
                ],
            ],
        ];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['email' => $email],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->once()
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->findUserByEmail($email);

        $vendor = (new Vendor())
            ->setId(1)
            ->setName('Cool Co.');
        $expectedUser = (new UMSUser())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor(
                $vendor->setCustomer((new Customer())->setId('cust.id')->setVendor($vendor))
            )
            ->setPassword('secret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSalt('salt');
        $expectedUser->addContact(
            (new Contact())
                ->setId(5)
                ->setValue('john.doe@x-formation.com')
                ->setType('email')
                ->setVerified(true)
                ->setUser($expectedUser)
        );

        $this->assertEquals($expectedUser, $user);
    }

    /**
     * @test
     */
    public function findUserByEmailWithNoUser()
    {

        $email = 'john@gmail.com';
        $responseDataArray = [];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['email' => $email],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->once()
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->findUserByEmail($email);

        $this->assertFalse($user);
    }

    /**
     * @test
     * @expectedException \LogicException
     * @expectedExceptionMessage Multiple users received from UMS for email: john@gmail.com
     */
    public function findUserByEmailWithMultipleUsers()
    {

        $email = 'john@gmail.com';
        $responseDataArray = [
            ['first user'],
            ['second user'],
        ];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['email' => $email],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->once()
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->findUserByEmail($email);

        $this->assertFalse($user);
    }

    /**
     * @test
     */
    public function findUserByActionWithOneUser()
    {
        $responseDataArray = [
            [
                "id"        => 1,
                "name"      => "John Doe",
                "activated" => "false",
                "password"  => "secret",
                "salt"      => "salt",
                "role"      => "admin,user",
                "enabled"   => true,
                "_links"    => [
                    "self" => ["href" => "/rest/v1/users/1"],
                ],
                "_embedded" => [
                    "vendor"       => [
                        "id"     => 1,
                        "name"   => "Cool Co.",
                        "_links" => [
                            "self" => ["href" => "/rest/v1/vendors/1"],
                        ],
                    ],
                    "contacts"     => [
                        [
                            "id"        => 5,
                            "value"     => "john.doe@x-formation.com",
                            "type"      => "email",
                            "verified"  => true,
                            "_links"    => [
                                "self" => ["href" => "rest/v1/contacts/1"],
                            ],
                            '_embedded' => [
                                'contact_actions' => [],
                            ],
                        ],
                    ],
                    'user_actions' => [
                        [
                            "type"            => "password_reset",
                            "expiration_time" => "2020-02-04T06:05:15+0000",
                            "id"              => "1b04a6240a61c2b5f92e5dcef748d350",
                            "_links"          => [
                                "self"    => ["href" => "rest/v1/user_actions/1b04a6240a61c2b5f92e5dcef748d350"],
                                "contact" => ["href" => "rest/v1/users/1"],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['action' => '1b04a6240a61c2b5f92e5dcef748d350'],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->findUserByAction('1b04a6240a61c2b5f92e5dcef748d350');

        $vendor = (new Vendor())->setId(1)->setName('Cool Co.');
        $expectedUser = (new UMSUser())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor($vendor->setCustomer((new Customer())->setVendor($vendor)))
            ->setPassword('secret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSalt('salt');
        $expectedUser->addContact(
            (new Contact())
                ->setId(5)
                ->setValue('john.doe@x-formation.com')
                ->setType('email')
                ->setVerified(true)
                ->setUser($expectedUser)
        );
        $expectedUser->addAction(
            (new UserAction())
                ->setId('1b04a6240a61c2b5f92e5dcef748d350')
                ->setType('password_reset')
                ->setExpirationTime(new DateTime('2020-02-04T06:05:15+0000'))
                ->setUser($expectedUser)
        );

        $this->assertEquals($expectedUser, $user);
    }

    /**
     * @test
     */
    public function findUserByActionWithNoUser()
    {
        $responseDataArray = [];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['action' => '1b04a6240a61c2b5f92e5dcef748d350'],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->findUserByAction('1b04a6240a61c2b5f92e5dcef748d350');

        $this->assertFalse($user);
    }

    /**
     * @test
     * @expectedException \LogicException
     * @expectedExceptionMessage Multiple users received from UMS for action: 1b04a6240a61c2b5f92e5dcef748d350
     */
    public function findUserByActionWithMultipleUsers()
    {
        $responseDataArray = [
            ['first user'],
            ['second user'],
        ];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['action' => '1b04a6240a61c2b5f92e5dcef748d350'],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->findUserByAction('1b04a6240a61c2b5f92e5dcef748d350');

        $this->assertFalse($user);
    }

    /**
     * @test
     */
    public function remove_with_not_existing_id_should_return_false()
    {
        $user = Mockery::mock(UMSUser::class)
            ->shouldReceive('getId')
            ->andReturn(5)
            ->getMock();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(HttpResponse::HTTP_NOT_FOUND)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'DELETE',
                'rest/v1/users/5',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        /** @var UMSUser $user */
        $this->assertFalse((new UMSUserRepository($client, $serializer))->remove($user));
    }

    /**
     * @test
     */
    public function remove_with_existing_id_should_return_true()
    {
        $user = Mockery::mock(UMSUser::class)
            ->shouldReceive('getId')
            ->andReturn(5)
            ->getMock();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(HttpResponse::HTTP_NO_CONTENT)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->once()
            ->withArgs([
                'DELETE',
                'rest/v1/users/5',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class);

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        /** @var UMSUser $user */
        $this->assertTrue((new UMSUserRepository($client, $serializer))->remove($user));
    }

    /**
     * @test
     */
    public function findByVendor_without_user_should_return_empty_array()
    {
        $vendorId = 1;
        $vendor = (new Vendor())->setId($vendorId)->setName('Cool Co.');

        $responseDataArray = [];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['vendor' => $vendorId],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $userList = $repository->findByVendor($vendor);

        $this->assertEquals([], $userList);
    }

    /**
     * @test
     */
    public function findByVendor_with_one_user_should_return_list_with_one_user()
    {
        $vendorId = 1;
        $vendor = (new Vendor())->setId($vendorId)->setName('Cool Co.');

        $responseDataArray = [
            [
                "id"        => 1,
                "name"      => "John Doe",
                "activated" => "false",
                "password"  => "secret",
                "salt"      => "salt",
                "role"      => "admin,user",
                "enabled"   => true,
                "_links"    => [
                    "self" => ["href" => "/rest/v1/users/1"],
                ],
                "_embedded" => [
                    "vendor"       => [
                        "id"     => 1,
                        "name"   => "Cool Co.",
                        "_links" => [
                            "self" => ["href" => "/rest/v1/vendors/1"],
                        ],
                    ],
                    "contacts"     => [
                        [
                            "id"        => 5,
                            "value"     => "john.doe@x-formation.com",
                            "type"      => "email",
                            "verified"  => true,
                            "_links"    => [
                                "self" => ["href" => "rest/v1/contacts/1"],
                            ],
                            '_embedded' => [
                                'contact_actions' => [],
                            ],
                        ],
                    ],
                    'user_actions' => [],
                ],
            ],
        ];

        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['vendor' => $vendorId],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $userList = $repository->findByVendor($vendor);

        $vendor = (new Vendor())->setId(1)->setName('Cool Co.');
        $expectedUser = (new UMSUser())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor($vendor->setCustomer((new Customer())->setVendor($vendor)))
            ->setPassword('secret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSalt('salt');
        $expectedUser->addContact(
            (new Contact())
                ->setId(5)
                ->setValue('john.doe@x-formation.com')
                ->setType('email')
                ->setVerified(true)
                ->setUser($expectedUser)
        );

        $this->assertEquals([$expectedUser], $userList);
    }

    /**
     * @test
     */
    public function findByVendor_with_multiple_users_should_return_list()
    {
        $vendorId = 1;
        $vendor = (new Vendor())->setId($vendorId)->setName('Cool Co.');

        $responseDataArray = [
            [
                "id"        => 1,
                "name"      => "John Doe",
                "activated" => "false",
                "password"  => "secret",
                "salt"      => "salt",
                "role"      => "admin,user",
                "enabled"   => true,
                "_links"    => [
                    "self" => ["href" => "/rest/v1/users/1"],
                ],
                "_embedded" => [
                    "vendor"       => [
                        "id"     => 1,
                        "name"   => "Cool Co.",
                        "_links" => [
                            "self" => ["href" => "/rest/v1/vendors/1"],
                        ],
                    ],
                    "contacts"     => [
                        [
                            "id"        => 5,
                            "value"     => "john.doe@x-formation.com",
                            "type"      => "email",
                            "verified"  => true,
                            "_links"    => [
                                "self" => ["href" => "rest/v1/contacts/1"],
                            ],
                            '_embedded' => [
                                'contact_actions' => [],
                            ],
                        ],
                    ],
                    'user_actions' => [],
                ],
            ],
            [
                "id"        => 2,
                "name"      => "John Foo",
                "activated" => "false",
                "password"  => "secret",
                "salt"      => "salt",
                "role"      => "user",
                "enabled"   => true,
                "_links"    => [
                    "self" => ["href" => "/rest/v1/users/2"],
                ],
                "_embedded" => [
                    "vendor"       => [
                        "id"     => 1,
                        "name"   => "Cool Co.",
                        "_links" => [
                            "self" => ["href" => "/rest/v1/vendors/1"],
                        ],
                    ],
                    "contacts"     => [
                        [
                            "id"        => 15,
                            "value"     => "john.foo@x-formation.com",
                            "type"      => "email",
                            "verified"  => true,
                            "_links"    => [
                                "self" => ["href" => "rest/v1/contacts/15"],
                            ],
                            '_embedded' => [
                                'contact_actions' => [],
                            ],
                        ],
                    ],
                    'user_actions' => [],
                ],
            ],
        ];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users',
                [
                    'query'   => ['vendor' => $vendorId],
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $userList = $repository->findByVendor($vendor);

        $vendor1 = (new Vendor())->setId(1)->setName('Cool Co.');
        $expectedUser1 = (new UMSUser())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor($vendor1->setCustomer((new Customer())->setVendor($vendor1)))
            ->setPassword('secret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSalt('salt');
        $expectedUser1->addContact(
            (new Contact())
                ->setId(5)
                ->setValue('john.doe@x-formation.com')
                ->setType('email')
                ->setVerified(true)
                ->setUser($expectedUser1)
        );

        $vendor2 = (new Vendor())->setId(1)->setName('Cool Co.');
        $expectedUser2 = (new UMSUser())
            ->setId(2)
            ->setName('John Foo')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor($vendor2->setCustomer((new Customer())->setVendor($vendor2)))
            ->setPassword('secret')
            ->setRoles(['ROLE_USER'])
            ->setSalt('salt');
        $expectedUser2->addContact(
            (new Contact())
                ->setId(15)
                ->setValue('john.foo@x-formation.com')
                ->setType('email')
                ->setVerified(true)
                ->setUser($expectedUser2)
        );

        $this->assertEquals([$expectedUser1, $expectedUser2], $userList);
    }

    /**
     * @test
     */
    public function find_should_return_user()
    {
        $responseDataArray = [
            "id"        => 1,
            "name"      => "John Doe",
            "activated" => "false",
            "password"  => "secret",
            "salt"      => "salt",
            "role"      => "admin,user",
            "enabled"   => true,
            "_links"    => [
                "self" => ["href" => "/rest/v1/users/1"],
            ],
            "_embedded" => [
                "vendor"       => [
                    "id"     => 1,
                    "name"   => "Cool Co.",
                    "_links" => [
                        "self" => ["href" => "/rest/v1/vendors/1"],
                    ],
                ],
                "contacts"     => [
                    [
                        "id"        => 5,
                        "value"     => "john.doe@x-formation.com",
                        "type"      => "email",
                        "verified"  => true,
                        "_links"    => [
                            "self" => ["href" => "rest/v1/contacts/1"],
                        ],
                        '_embedded' => [
                            'contact_actions' => [
                                [
                                    "type"            => "contact_verify",
                                    "expiration_time" => "2020-02-04T06:05:15+0000",
                                    "id"              => "6ced9d7e8a95b9919d1e8a145adab770",
                                    "_links"          => [
                                        "self"    => ["href" => "rest/v1/contact_actions/6ced9d7e8a95b9919d1e8a145adab770"],
                                        "contact" => ["href" => "rest/v1/contacts/1"],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'user_actions' => [
                    [
                        "type"            => "password_reset",
                        "expiration_time" => "2020-02-04T06:05:15+0000",
                        "id"              => "59949b8668231edc2e79a8a321f048b4",
                        "_links"          => [
                            "self"    => ["href" => "rest/v1/user_actions/59949b8668231edc2e79a8a321f048b4"],
                            "contact" => ["href" => "rest/v1/users/1"],
                        ],
                    ],
                ],
            ],

        ];
        $responseDataJson = json_encode($responseDataArray);

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getBody')
            ->andReturn($responseDataJson)
            ->getMock();

        $client = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->withArgs([
                'GET',
                'rest/v1/users/1',
                [
                    'headers' => ['Accept' => 'application/json'],
                ],
            ])
            ->andReturn($response)
            ->getMock();

        $serializer = Mockery::mock(SerializerInterface::class)
            ->shouldReceive('deserialize')
            ->withArgs([$responseDataJson, 'array', JsonEncoder::FORMAT])
            ->andReturn($responseDataArray)
            ->getMock();

        /** @var ClientInterface $client */
        /** @var SerializerInterface $serializer */
        $repository = new UMSUserRepository($client, $serializer);
        $user = $repository->find(1);

        $vendor = (new Vendor())->setId(1)->setName('Cool Co.');
        $expectedUser = (new UMSUser())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor($vendor->setCustomer((new Customer())->setVendor($vendor)))
            ->setPassword('secret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSalt('salt');
        $expectedUser->addAction(
            (new UserAction())
                ->setId('59949b8668231edc2e79a8a321f048b4')
                ->setType('password_reset')
                ->setExpirationTime(new DateTime('2020-02-04T06:05:15+0000'))
                ->setUser($expectedUser)
        );
        $expectedContact = (new Contact())
            ->setId(5)
            ->setValue('john.doe@x-formation.com')
            ->setType('email')
            ->setVerified(true)
            ->setUser($expectedUser);
        $expectedUser->addContact(
            $expectedContact
                ->addAction(
                    (new ContactAction())
                        ->setId('6ced9d7e8a95b9919d1e8a145adab770')
                        ->setType('contact_verify')
                        ->setExpirationTime(new DateTime('2020-02-04T06:05:15+0000'))
                        ->setContact($expectedContact)
                )
        );

        $this->assertEquals($expectedUser, $user);
    }
}
