<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Request;

use Connect\Bundle\UMSSecurityBundle\Request\ParamConverterAbstract;
use GuzzleHttp\Exception\RequestException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use stdClass;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ParamConverterAbstractTest
 */
class ParamConverterAbstractTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function apply_without_attribute_in_request_should_return_false()
    {
        $paramName = 'some_param';

        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->andReturn($paramName)
            ->getMock();

        /** @var ParameterBag $attributes */
        $attributes = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->with($paramName)
            ->andReturn(false)
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $attributes;

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);

        /** @var ParamConverterAbstract $abstractConverter */
        $abstractConverter = Mockery::mock(ParamConverterAbstract::class . '[findObject]', [$translator]);

        $this->assertFalse(
            $abstractConverter->apply($request, $configuration)
        );
    }

    /**
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage some error
     */
    public function apply_with_invalid_id_should_throw_exception()
    {
        $paramId = 4;
        $paramName = 'some_param';

        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->andReturn($paramName)
            ->getMock();

        /** @var ParameterBag $attributes */
        $attributes = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->with($paramName)
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('get')
            ->with($paramName)
            ->andReturn($paramId)
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $attributes;

        /** @var RequestException $exception */
        $exception = Mockery::mock(RequestException::class);

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->andReturn('some error')
            ->getMock();

        /** @var ParamConverterAbstract $abstractConverter */
        $abstractConverter = Mockery::mock(ParamConverterAbstract::class . '[findObject]', [$translator])
            ->shouldAllowMockingProtectedMethods()
            ->shouldReceive('findObject')
            ->once()
            ->with($paramId)
            ->andThrowExceptions([$exception])
            ->getMock();

        $abstractConverter->apply($request, $configuration);
    }

    /**
     * @test
     */
    public function apply_with_valid_id_should_return_true()
    {
        $paramId = 4;
        $paramName = 'some_param';
        $paramObject = new stdClass();

        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->andReturn($paramName)
            ->getMock();
        
        $attributes = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->with($paramName)
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('get')
            ->with($paramName)
            ->andReturn($paramId)
            ->getMock()

            ->shouldReceive('set')
            ->withArgs([$paramName, $paramObject])
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $attributes;

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);

        /** @var ParamConverterAbstract $abstractConverter */
        $abstractConverter = Mockery::mock(ParamConverterAbstract::class . '[findObject]', [$translator])
            ->shouldAllowMockingProtectedMethods()
            ->shouldReceive('findObject')
            ->once()
            ->with($paramId)
            ->andReturn($paramObject)
            ->getMock();

        $this->assertTrue(
            $abstractConverter->apply($request, $configuration)
        );
    }
}
