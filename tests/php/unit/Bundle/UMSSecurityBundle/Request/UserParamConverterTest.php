<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Request;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Request\UserParamConverter;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use GuzzleHttp\Exception\RequestException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use stdClass;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserParamConverterTest
 */
class UserParamConverterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function supports_with_supported_class_should_return_true()
    {
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getClass')
            ->andReturn(UMSUser::class)
            ->getMock();
        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);

        $this->assertTrue((new UserParamConverter($repository, $translator))->supports($configuration));
    }

    /**
     * @test
     */
    public function supports_without_supported_class_should_return_false()
    {
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getClass')
            ->andReturn(stdClass::class)
            ->getMock();
        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);


        $this->assertFalse((new UserParamConverter($repository, $translator))->supports($configuration));
    }

    /**
     * @test
     */
    public function apply_without_attribute_in_request_should_return_false()
    {
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->andReturn('user')
            ->getMock();
        $attributes = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->with('user')
            ->andReturn(false)
            ->getMock();
        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $attributes;
        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class);
        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);

        $this->assertFalse(
            (new UserParamConverter($repository, $translator))->apply($request, $configuration)
        );
    }

    /**
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage some error
     */
    public function apply_with_invalid_id_should_throw_exception()
    {
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->andReturn('user')
            ->getMock();
        $attributes = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->with('user')
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('get')
            ->with('user')
            ->andReturn(4)
            ->getMock();
        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $attributes;
        $exception = Mockery::mock(RequestException::class);
        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(4)
            ->andThrowExceptions([$exception])
            ->getMock();
        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
            ->shouldReceive('trans')
            ->andReturn('some error')
            ->getMock();

        $this->assertFalse(
            (new UserParamConverter($repository, $translator))->apply($request, $configuration)
        );
    }

    /**
     * @test
     */
    public function apply_with_valid_id_should_return_true()
    {
        $user = new UMSUser();
        /** @var ParamConverter $configuration */
        $configuration = Mockery::mock(ParamConverter::class)
            ->shouldReceive('getName')
            ->andReturn('user')
            ->getMock();
        $attributes = Mockery::mock(ParameterBag::class)
            ->shouldReceive('has')
            ->with('user')
            ->andReturn(true)
            ->getMock()

            ->shouldReceive('get')
            ->with('user')
            ->andReturn(4)
            ->getMock()

            ->shouldReceive('set')
            ->withArgs(['user', $user])
            ->getMock();
        /** @var Request $request */
        $request = Mockery::mock(Request::class);
        $request->attributes = $attributes;
        /** @var UMSUserRepository $repository */
        $repository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('find')
            ->with(4)
            ->andReturn($user)
            ->getMock();
        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);

        $this->assertTrue(
            (new UserParamConverter($repository, $translator))->apply($request, $configuration)
        );
    }
}
