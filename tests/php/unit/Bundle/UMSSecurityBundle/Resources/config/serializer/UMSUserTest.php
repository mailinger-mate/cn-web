<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Resources\config\serializer;

use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Contact;
use Connect\Domain\Vendor;
use Connect\Test\Core\SerializerTestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class UMSUserTest
 */
class UMSUserTest extends SerializerTestCase
{
    /**
     * @test
     */
    public function serialize()
    {
        $vendor = (new Vendor())
            ->setName('Bar')
            ->setId(2);
        $contact = (new Contact())
            ->setId(3)
            ->setType('email')
            ->setValue('foo@bar.bazz')
            ->setVerified(true);
        $user = (new UMSUser())
            ->setPassword('secret')
            ->setRoles(['user'])
            ->setSalt('salt')
            ->setId(4)
            ->setVendor($vendor)
            ->setName('foo')
            ->setActivated(true)
            ->setEnabled(false);
        $user->addContact($contact);
        $contact->setUser($user);

        $this->assertJsonStringEqualsJsonString(
            '{
              "role": "user",
              "id": 4,
              "name": "foo",
              "activated": true,
              "contact": "foo@bar.bazz",
              "verified": true
            }',
            $this->getSerializer('ums.serializer.jms')->serialize($user, JsonEncoder::FORMAT)
        );
    }
}
