<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Resources\config\serializer;

use Connect\Domain\Contact;
use Connect\Domain\User;
use Connect\Domain\Vendor;
use Connect\Test\Core\SerializerTestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class UserTest
 */
class UserTest extends SerializerTestCase
{
    /**
     * @test
     */
    public function serialize()
    {
        $vendor = (new Vendor())
            ->setName('Bar')
            ->setId(2);
        $contact = (new Contact())
            ->setId(3)
            ->setType('email')
            ->setValue('foo@bar.bazz')
            ->setVerified(true);
        $user = (new User())
            ->setId(4)
            ->setVendor($vendor)
            ->setName('foo')
            ->setActivated(true)
            ->setEnabled(false);
        $user->addContact($contact);
        $contact->setUser($user);

        $this->assertJsonStringEqualsJsonString(
            '{
              "id": 4,
              "name": "foo",
              "activated": true,
              "contact": "foo@bar.bazz",
              "verified": true
            }',
            $this->getSerializer('ums.serializer.jms')->serialize($user, JsonEncoder::FORMAT)
        );
    }
}
