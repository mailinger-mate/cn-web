<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Resources\config\serializer;

use Connect\Domain\Vendor;
use Connect\Test\Core\SerializerTestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class VendorTest
 */
class VendorTest extends SerializerTestCase
{
    /**
     * @test
     */
    public function serialize_without_context()
    {
        $vendor = (new Vendor())
            ->setName('Bar')
            ->setId(2);

        $this->assertJsonStringEqualsJsonString(
            '{"name": "Bar"}',
            $this->getSerializer('ums.serializer.jms')->serialize($vendor, JsonEncoder::FORMAT)
        );
    }
}
