<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Security\Handler;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use Connect\Bundle\UMSSecurityBundle\Security\Handler\AuthenticationSuccess;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Session;
use GuzzleHttp\Exception\ClientException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\HttpUtils;

class AuthenticationSuccessTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onAuthenticationSuccess_without_session_should_create_new()
    {
        $user = new UMSUser();
        $targetPath = 'some_target_path';
        $expectedResponse = new RedirectResponse('/path');

        /** @var SessionInterface $session */
        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->with('ums_session')
            ->andReturn(false)

            ->shouldReceive('set')
            ->once()
            ->with('ums_session', 'foo-bar')
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class)
            ->shouldReceive('getSession')
            ->andReturn($session)

            ->shouldReceive('get')
            ->with('_target_path')
            ->andReturn($targetPath)
            ->getMock();

        /** @var TokenInterface $token */
        $token = Mockery::mock(TokenInterface::class)
            ->shouldReceive('getUser')
            ->andReturn($user)
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn('/path')
            ->getMock();

        /** @var UMSSessionRepository $sessionRepository */
        $sessionRepository = Mockery::mock(UMSSessionRepository::class)
            ->shouldReceive('add')
            ->with(Mockery::on(function ($session) {
                if (!$session instanceof Session) {
                    return false;
                }
                $session->setId('foo-bar');

                return true;
            }))
            ->getMock();

        /** @var HttpUtils $httpUtils */
        $httpUtils = Mockery::mock(HttpUtils::class)
            ->shouldReceive('createRedirectResponse')
            ->with($request, $targetPath)
            ->andReturn($expectedResponse)
            ->getMock();

        $response = (new AuthenticationSuccess($router, $sessionRepository, $httpUtils))
            ->onAuthenticationSuccess($request, $token);

        $this->assertSame($response, $expectedResponse);
    }

    /**
     * @test
     */
    public function onAuthenticationSuccess_with_session_id_but_missing_in_ums_should_create_new()
    {
        $user = new UMSUser();
        $targetPath = 'some_target_path';
        $expectedResponse = new RedirectResponse('/path');

        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->with('ums_session')
            ->andReturn(true)

            ->shouldReceive('get')
            ->with('ums_session')
            ->andReturn('missing-id')

            ->shouldReceive('set')
            ->once()
            ->with('ums_session', 'foo-bar')
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class)
            ->shouldReceive('getSession')
            ->andReturn($session)

            ->shouldReceive('get')
            ->with('_target_path')
            ->andReturn($targetPath)
            ->getMock();

        /** @var TokenInterface $token */
        $token = Mockery::mock(TokenInterface::class)
            ->shouldReceive('getUser')
            ->andReturn($user)
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn('/path')
            ->getMock();

        $guzzleResponse = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(404)
            ->getMock();

        $exception = Mockery::mock(ClientException::class)
            ->shouldReceive('getResponse')
            ->andReturn($guzzleResponse)
            ->getMock();

        /** @var UMSSessionRepository $sessionRepository */
        $sessionRepository = Mockery::mock(UMSSessionRepository::class)
            ->shouldReceive('find')
            ->with('missing-id')
            ->andThrow($exception)
            ->shouldReceive('add')
            ->with(Mockery::on(function ($session) {
                if (!$session instanceof Session) {
                    return false;
                }
                $session->setId('foo-bar');

                return true;
            }))
            ->getMock();

        /** @var HttpUtils $httpUtils */
        $httpUtils = Mockery::mock(HttpUtils::class)
            ->shouldReceive('createRedirectResponse')
            ->with($request, $targetPath)
            ->andReturn($expectedResponse)
            ->getMock();

        $response = (new AuthenticationSuccess($router, $sessionRepository, $httpUtils))
            ->onAuthenticationSuccess($request, $token);

        $this->assertSame($response, $expectedResponse);
    }

    /**
     * @test
     */
    public function onAuthenticationSuccess_with_session_id_should_return_redirect()
    {
        $targetPath = 'some_target_path';
        $expectedResponse = new RedirectResponse('/path');

        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->with('ums_session')
            ->andReturn(true)
            ->shouldReceive('get')
            ->with('ums_session')
            ->andReturn('session_id')
            ->shouldReceive('set')
            ->never()
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class)
            ->shouldReceive('getSession')
            ->andReturn($session)

            ->shouldReceive('get')
            ->with('_target_path')
            ->andReturn($targetPath)
            ->getMock();

        /** @var TokenInterface $token */
        $token = Mockery::mock(TokenInterface::class);

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn('/path')
            ->getMock();

        /** @var UMSSessionRepository $sessionRepository */
        $sessionRepository = Mockery::mock(UMSSessionRepository::class)
            ->shouldReceive('find')
            ->with('session_id')

            ->shouldReceive('add')
            ->never()
            ->getMock();

        /** @var HttpUtils $httpUtils */
        $httpUtils = Mockery::mock(HttpUtils::class)
            ->shouldReceive('createRedirectResponse')
            ->with($request, $targetPath)
            ->andReturn($expectedResponse)
            ->getMock();

        $response = (new AuthenticationSuccess($router, $sessionRepository, $httpUtils))
            ->onAuthenticationSuccess($request, $token);

        $this->assertEquals($response, new RedirectResponse('/path'));
    }

    /**
     * @test
     * @expectedException \GuzzleHttp\Exception\ClientException
     */
    public function onAuthenticationSuccess_with_ums_exception_should_throw_exception()
    {
        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->with('ums_session')
            ->andReturn(true)

            ->shouldReceive('get')
            ->with('ums_session')
            ->andReturn('session-id')

            ->shouldReceive('set')
            ->never()
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class)
            ->shouldReceive('getSession')
            ->andReturn($session)
            ->getMock();

        /** @var TokenInterface $token */
        $token = Mockery::mock(TokenInterface::class);

        /** @var Router $router */
        $router = Mockery::mock(Router::class);

        $guzzleResponse = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(500)
            ->getMock();

        $exception = Mockery::mock(ClientException::class)
            ->shouldReceive('getResponse')
            ->andReturn($guzzleResponse)
            ->getMock();

        /** @var UMSSessionRepository $sessionRepository */
        $sessionRepository = Mockery::mock(UMSSessionRepository::class)
            ->shouldReceive('find')
            ->with('session-id')
            ->andThrow($exception)

            ->shouldReceive('add')
            ->never()
            ->getMock();

        /** @var HttpUtils $httpUtils */
        $httpUtils = Mockery::mock(HttpUtils::class);

        (new AuthenticationSuccess($router, $sessionRepository, $httpUtils))
            ->onAuthenticationSuccess($request, $token);
    }
}
