<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Security\Handler;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSSessionRepository;
use Connect\Bundle\UMSSecurityBundle\Security\Handler\LogoutSuccess;
use Connect\Domain\Session;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LogoutSuccessTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onLogoutSuccess_with_session_id_should_remove_session()
    {
        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->with('ums_session')
            ->andReturn(true)
            ->shouldReceive('get')
            ->with('ums_session')
            ->andReturn('some-id')
            ->shouldReceive('remove')
            ->with('ums_session')
            ->once()
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class)
            ->shouldReceive('getSession')
            ->andReturn($session)
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn('/path')
            ->getMock();
        /** @var UMSSessionRepository $sessionRepository */
        $sessionRepository = Mockery::mock(UMSSessionRepository::class)
            ->shouldReceive('remove')
            ->with(Mockery::on(function ($session) {
                if (!$session instanceof Session || $session->getId() !== 'some-id') {
                    return false;
                }
                return true;
            }))
            ->getMock();

        $this->assertEquals(
            new RedirectResponse('/path'),
            (new LogoutSuccess($router, $sessionRepository))->onLogoutSuccess($request)
        );
    }
    /**
     * @test
     */
    public function onLogoutSuccess_without_session_id_should_return_redirect()
    {
        $session = Mockery::mock(SessionInterface::class)
            ->shouldReceive('has')
            ->with('ums_session')
            ->andReturn(false)
            ->shouldReceive('remove')
            ->never()
            ->getMock();

        /** @var Request $request */
        $request = Mockery::mock(Request::class)
            ->shouldReceive('getSession')
            ->andReturn($session)
            ->getMock();

        /** @var Router $router */
        $router = Mockery::mock(Router::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn('/path')
            ->getMock();
        /** @var UMSSessionRepository $sessionRepository */
        $sessionRepository = Mockery::mock(UMSSessionRepository::class)
            ->shouldReceive('remove')
            ->never()
            ->getMock();

        $this->assertEquals(
            new RedirectResponse('/path'),
            (new LogoutSuccess($router, $sessionRepository))->onLogoutSuccess($request)
        );
    }
}
