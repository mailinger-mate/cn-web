<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Security\RequestAccessEvaluator;

use Connect\Bundle\UMSSecurityBundle\Security\RequestAccessEvaluator\BelongsEvaluator;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Domain\Vendor;
use Mockery;
use PHPUnit_Framework_TestCase;
use stdClass;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class BelongsEvaluatorTest
 */
class BelongsEvaluatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function belongsToVendor_with_anonymous_user_should_return_false()
    {
        $token = Mockery::mock(TokenInterface::class)
            ->shouldReceive('getUser')
            ->once()
            ->andReturn('anon.')
            ->getMock();

        $tokenStorage = Mockery::mock(TokenStorageInterface::class)
            ->shouldReceive('getToken')
            ->once()
            ->andReturn($token)
            ->getMock();

        $user = Mockery::mock(UMSUser::class);

        /** @var TokenStorageInterface $tokenStorage */
        /** @var UMSUser $user */
        $this->assertFalse((new BelongsEvaluator($tokenStorage))->belongsToVendor($user));
    }

    /**
     * @test
     */
    public function belongsToVendor_with_user_belong_to_different_vendor_should_return_false()
    {
        $authVendor = Mockery::mock(Vendor::class)
            ->shouldReceive('getId')
            ->once()
            ->andReturn(10)
            ->getMock();

        $authUser = Mockery::mock(UMSUser::class)
            ->shouldReceive('getVendor')
            ->once()
            ->andReturn($authVendor)
            ->getMock();

        $token = Mockery::mock(TokenInterface::class)
            ->shouldReceive('getUser')
            ->once()
            ->andReturn($authUser)
            ->getMock();

        $tokenStorage = Mockery::mock(TokenStorageInterface::class)
            ->shouldReceive('getToken')
            ->once()
            ->andReturn($token)
            ->getMock();

        $checkVendor = Mockery::mock(Vendor::class)
            ->shouldReceive('getId')
            ->once()
            ->andReturn(8)
            ->getMock();

        $checkUser = Mockery::mock(UMSUser::class)
            ->shouldReceive('getVendor')
            ->once()
            ->andReturn($checkVendor)
            ->getMock();

        /** @var TokenStorageInterface $tokenStorage */
        /** @var UMSUser $checkUser */
        $this->assertFalse((new BelongsEvaluator($tokenStorage))->belongsToVendor($checkUser));
    }

    /**
     * @test
     */
    public function belongsToVendor_with_user_belong_to_the_same_vendor_should_return_true()
    {
        $vendor = Mockery::mock(Vendor::class)
            ->shouldReceive('getId')
            ->twice()
            ->andReturn(10)
            ->getMock();

        $authUser = Mockery::mock(UMSUser::class)
            ->shouldReceive('getVendor')
            ->once()
            ->andReturn($vendor)
            ->getMock();

        $token = Mockery::mock(TokenInterface::class)
            ->shouldReceive('getUser')
            ->once()
            ->andReturn($authUser)
            ->getMock();

        $tokenStorage = Mockery::mock(TokenStorageInterface::class)
            ->shouldReceive('getToken')
            ->once()
            ->andReturn($token)
            ->getMock();

        $checkUser = Mockery::mock(UMSUser::class)
            ->shouldReceive('getVendor')
            ->once()
            ->andReturn($vendor)
            ->getMock();

        /** @var TokenStorageInterface $tokenStorage */
        /** @var UMSUser $checkUser */
        $this->assertTrue((new BelongsEvaluator($tokenStorage))->belongsToVendor($checkUser));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Not supported object to get related vendor: stdClass
     */
    public function belongsToVendor_without_ums_user_should_throw_exception()
    {
        $vendor = Mockery::mock(Vendor::class)
            ->shouldReceive('getId')
            ->once()
            ->andReturn(10)
            ->getMock();

        $authUser = Mockery::mock(UMSUser::class)
            ->shouldReceive('getVendor')
            ->once()
            ->andReturn($vendor)
            ->getMock();

        $token = Mockery::mock(TokenInterface::class)
            ->shouldReceive('getUser')
            ->once()
            ->andReturn($authUser)
            ->getMock();

        $tokenStorage = Mockery::mock(TokenStorageInterface::class)
            ->shouldReceive('getToken')
            ->once()
            ->andReturn($token)
            ->getMock();

        /** @var TokenStorageInterface $tokenStorage */
        /** @var UMSUser $checkUser */
        $this->assertTrue((new BelongsEvaluator($tokenStorage))->belongsToVendor(new stdClass()));
    }
}