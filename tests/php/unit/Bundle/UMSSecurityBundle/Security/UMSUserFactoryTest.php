<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Security;

use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUserFactory;
use Connect\Domain\Vendor;
use PHPUnit_Framework_TestCase;

/**
 * Class UMSUserFactoryTest
 */
class UMSUserFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $vendorObject = (new Vendor())->setId(1)->setName('John Doe');
        $data = [
            "id"        => 1,
            "name"      => "John Doe",
            "activated" => false,
            "password"  => "secret",
            "salt"      => "salt",
            "role"      => "admin,user",
            "enabled"   => true,
        ];

        $expectedUserObject = (new UMSUser())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(false)
            ->setEnabled(true)
            ->setVendor($vendorObject)
            ->setPassword('secret')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSalt('salt');

        $userObject = (new UMSUserFactory())->createFromArray($data, $vendorObject);

        $this->assertEquals($expectedUserObject, $userObject);
    }
}
