<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Security;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUserProvider;
use Connect\Domain\Contact;
use Connect\Domain\Vendor;
use Mockery;
use PHPUnit_Framework_TestCase;
use stdClass;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UMSUserProviderTest
 */
class UMSUserProviderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function loadUserByUsername()
    {
        $user = new UMSUser(
            1,
            'John',
            true,
            true,
            (new Vendor())->setId(1)->setName('XF'),
            'secret-password',
            ['admin'],
            'salt'
        );

        $repositoryMock = $this->getMockBuilder(UMSUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('findUserByEmail')
            ->with($this->equalTo('john@xf.com'))
            ->will($this->returnValue($user));

        /** @var UMSUserRepository $repositoryMock */
        /** @var UserInterface $userMock */
        $resultUser = (new UMSUserProvider($repositoryMock))->loadUserByUsername('john@xf.com');

        $this->assertEquals($user, $resultUser);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     * @expectedExceptionMessage Username "john@xf.com" does not exist.
     */
    public function loadUserByUsernameWithMissingUser()
    {
        $repositoryMock = $this->getMockBuilder(UMSUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('findUserByEmail')
            ->with($this->equalTo('john@xf.com'))
            ->will($this->returnValue(false));

        /** @var UMSUserRepository $repositoryMock */
        /** @var UserInterface $userMock */
        (new UMSUserProvider($repositoryMock))->loadUserByUsername('john@xf.com');
    }

    /**
     * @test
     */
    public function refreshUser()
    {
        $user = Mockery::mock(UMSUser::class)
            ->shouldReceive('getUsername')
            ->andReturn('john@xf.com')
            ->getMock();

        $newUser = new UMSUser(
            1,
            'John Doe',
            true,
            false,
            (new Vendor())->setId(1)->setName('XF'),
            'top-secret-password',
            ['admin', 'user'],
            'salt'
        );

        $repositoryMock = $this->getMockBuilder(UMSUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('findUserByEmail')
            ->with($this->equalTo('john@xf.com'))
            ->will($this->returnValue($newUser));

        /** @var UMSUserRepository $repositoryMock */
        /** @var UserInterface $userMock */
        $resultUser = (new UMSUserProvider($repositoryMock))->refreshUser($user);

        $this->assertEquals($newUser, $resultUser);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Security\Core\Exception\UnsupportedUserException
     */
    public function refreshUserWithNotSupportedClass()
    {
        $repositoryMock = $this->getMockBuilder(UMSUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userMock = $this->getMock(UserInterface::class);

        /** @var UMSUserRepository $repositoryMock */
        /** @var UserInterface $userMock */
        (new UMSUserProvider($repositoryMock))->refreshUser($userMock);
    }

    /**
     * @test
     */
    public function supportedClass()
    {
        $repositoryMock = $this->getMockBuilder(UMSUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var UMSUserRepository $repositoryMock */
        $this->assertTrue(
            (new UMSUserProvider($repositoryMock))->supportsClass(UMSUser::class)
        );
    }

    /**
     * @test
     */
    public function supportedClassWithNotSupportedClass()
    {
        $repositoryMock = $this->getMockBuilder(UMSUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var UMSUserRepository $repositoryMock */
        $this->assertFalse(
            (new UMSUserProvider($repositoryMock))->supportsClass(stdClass::class)
        );
    }
}
