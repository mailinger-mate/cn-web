<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender;

use Connect\Bundle\UMSSecurityBundle\Sender\Client;
use Connect\Bundle\UMSSecurityBundle\Sender\ClientNull;
use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ClientNullTest
 */
class ClientNullTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function send_with_empty_array_should_return_false()
    {
        $this->assertFalse((new ClientNull())->send([]));
    }

    /**
     * @test
     */
    public function send_with_non_empty_array_should_return_true()
    {
        $this->assertTrue((new ClientNull())->send(['some data']));
    }
}