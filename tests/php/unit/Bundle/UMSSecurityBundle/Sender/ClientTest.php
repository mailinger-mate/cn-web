<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender;

use Connect\Bundle\UMSSecurityBundle\Sender\Client;
use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ClientTest
 */
class ClientTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function send_with_empty_array_should_return_false()
    {
        $httpClient = Mockery::mock(ClientInterface::class);
        $logger = Mockery::mock(LoggerInterface::class);

        /** @var ClientInterface $httpClient */
        /** @var LoggerInterface $logger */
        $client = new Client($httpClient, $logger);

        $this->assertFalse($client->send([]));
    }

    /**
     * @test
     */
    public function send_with_single_message_and_success_response_should_return_true()
    {
        $messageArray = ['key' => 'value'];
        $message = Mockery::mock(Message::class)
            ->shouldReceive('getAsArray')
            ->andReturn($messageArray)
            ->getMock();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(204)
            ->getMock();

        $httpClient = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with(
                'POST',
                'messages',
                ['json' => [$messageArray]]
            )
            ->andReturn($response)
            ->getMock();

        $logger = Mockery::mock(LoggerInterface::class);

        /** @var ClientInterface $httpClient */
        /** @var LoggerInterface $logger */
        $client = new Client($httpClient, $logger);

        $this->assertTrue($client->send([$message]));
    }

    /**
     * @test
     */
    public function send_with_multiple_messages_and_success_response_should_return_true()
    {
        $messageArray = ['key' => 'value'];
        $message = Mockery::mock(Message::class)
            ->shouldReceive('getAsArray')
            ->times(3)
            ->andReturn($messageArray)
            ->getMock();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(204)
            ->getMock();

        $httpClient = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with(
                'POST',
                'messages',
                ['json' => [$messageArray, $messageArray, $messageArray]]
            )
            ->andReturn($response)
            ->getMock();

        $logger = Mockery::mock(LoggerInterface::class);

        /** @var ClientInterface $httpClient */
        /** @var LoggerInterface $logger */
        $client = new Client($httpClient, $logger);

        $this->assertTrue($client->send([$message, $message, $message]));
    }

    /**
     * @test
     */
    public function send_with_single_message_and_non_204_response_should_log_error_and_return_false()
    {
        $messageArray = ['key' => 'value'];
        $message = Mockery::mock(Message::class)
            ->shouldReceive('getAsArray')
            ->andReturn($messageArray)
            ->getMock();

        $bodyStream = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('some body response')
            ->getMock();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(300)
            ->getMock()

            ->shouldReceive('getBody')
            ->andReturn($bodyStream)
            ->getMock();

        $httpClient = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with(
                'POST',
                'messages',
                ['json' => [$messageArray]]
            )
            ->andReturn($response)
            ->getMock();

        $logger = Mockery::mock(LoggerInterface::class)
            ->shouldReceive('error')
            ->with('Error on sending message to connect sender with response code: 300, body: some body response')
            ->andReturnNull()
            ->getMock();

        /** @var ClientInterface $httpClient */
        /** @var LoggerInterface $logger */
        $client = new Client($httpClient, $logger);

        $this->assertFalse($client->send([$message]));
    }

    /**
     * @test
     */
    public function send_with_single_message_and_request_exception_should_log_error_and_return_false()
    {
        $messageArray = ['key' => 'value'];
        $message = Mockery::mock(Message::class)
            ->shouldReceive('getAsArray')
            ->andReturn($messageArray)
            ->getMock();

        $bodyStream = Mockery::mock(StreamInterface::class)
            ->shouldReceive('getContents')
            ->andReturn('some body response')
            ->getMock();

        $response = Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')
            ->andReturn(400)
            ->getMock()

            ->shouldReceive('getBody')
            ->andReturn($bodyStream)
            ->getMock();

        $exception = Mockery::mock(RequestException::class)
            ->shouldReceive('getResponse')
            ->andReturn($response)
            ->getMock();
        
        $httpClient = Mockery::mock(ClientInterface::class)
            ->shouldReceive('request')
            ->with(
                'POST',
                'messages',
                ['json' => [$messageArray]]
            )
            ->andThrowExceptions([$exception])
            ->getMock();

        $logger = Mockery::mock(LoggerInterface::class)
            ->shouldReceive('error')
            ->with('Error on sending message to connect sender with response code: 400, body: some body response')
            ->andReturnNull()
            ->getMock();

        /** @var ClientInterface $httpClient */
        /** @var LoggerInterface $logger */
        $client = new Client($httpClient, $logger);

        $this->assertFalse($client->send([$message]));
    }
}