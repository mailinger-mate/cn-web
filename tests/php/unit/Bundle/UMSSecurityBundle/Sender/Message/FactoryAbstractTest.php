<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\FactoryAbstract;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Domain\User;
use Mockery;
use PHPUnit_Framework_TestCase;
use Ramsey\Uuid\UuidFactoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class FactoryAbstractTest
 */
class FactoryAbstractTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createForUser_should_return_message()
    {
        /** @var User $user */
        $user = Mockery::mock(User::class);

        /** @var UuidInterface $uuid */
        $uuid = Mockery::mock(UuidInterface::class)
            ->shouldReceive('toString')
            ->andReturn('some_uuid')
            ->getMock();

        /** @var UuidFactoryInterface $uuidFactory */
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class)
            ->shouldReceive('uuid4')
            ->andReturn($uuid)
            ->getMock();

        /** @var FactoryAbstract $factory */
        $factory = Mockery::mock(FactoryAbstract::class, [$uuidFactory])
            ->makePartial()
            ->shouldAllowMockingProtectedMethods()

            ->shouldReceive('getType')
            ->andReturn('some_type')
            ->getMock()

            ->shouldReceive('getAccountConfig')
            ->andReturn(['some_acc_config'])
            ->getMock()

            ->shouldReceive('getMessageConfig')
            ->with($user, 'some_trigger')
            ->andReturn(['some_msg_config'])
            ->getMock();

        $message = $factory->createForUser($user, 'some_trigger');

        $expectedMessage = new Message(
            'internal_connect_message',
            'some_uuid',
            0,
            'some_type',
            ['some_acc_config'],
            ['some_msg_config'],
            []
        );

        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals($expectedMessage, $message);
    }
}