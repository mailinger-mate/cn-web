<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\FactoryAbstract;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\FactoryProvider;
use Connect\Bundle\UMSSecurityBundle\Sender\Type;
use InvalidArgumentException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FactoryProviderTest
 */
class FactoryProviderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Not supported sender type: some_type
     */
    public function getForType_with_unsupported_type_throws_exception()
    {
        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class);

        (new FactoryProvider($serviceContainer))->getForType('some_type');
    }

    /**
     * @test
     * @dataProvider supportedSenderTypesProvider
     *
     * @param $type
     * @param $serviceName
     */
    public function getForType_with_supported_type_returns_factory_service($type, $serviceName)
    {
        $factory = Mockery::mock(FactoryAbstract::class);

        /** @var ContainerInterface $serviceContainer */
        $serviceContainer = Mockery::mock(ContainerInterface::class)
            ->shouldReceive('get')
            ->with($serviceName)
            ->andReturn($factory)
            ->getMock();

        $factoryProvider = (new FactoryProvider($serviceContainer));

        $this->assertSame($factory, $factoryProvider->getForType($type));
    }

    public static function supportedSenderTypesProvider()
    {
        $types = [
            [Type::MAILCHIMP, 'sender.message.factory.mailchimp'],
            [Type::HIPCHAT, 'sender.message.factory.hipchat'],
        ];

        self::assertCount(count($types), (new \ReflectionClass(Type::class))->getConstants());

        return $types;
    }
}