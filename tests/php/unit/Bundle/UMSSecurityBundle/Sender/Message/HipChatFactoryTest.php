<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\HipChatFactory;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Domain\User;
use Connect\Domain\Vendor;
use Mockery;
use LogicException;
use PHPUnit_Framework_TestCase;
use Ramsey\Uuid\UuidFactoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class HipChatFactoryTest
 */
class HipChatFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createForUser_with_user_registration_should_return_message()
    {
        /** @var Vendor $vendor */
        $vendor = Mockery::mock(Vendor::class)
            ->shouldReceive('getId')
            ->andReturn('some_id')
            ->getMock()

            ->shouldReceive('getName')
            ->andReturn('some_vendor')
            ->getMock();

        /** @var User $user */
        $user = Mockery::mock(User::class)
            ->shouldReceive('getVendor')
            ->andReturn($vendor)
            ->getMock()

            ->shouldReceive('getPrimaryContactValue')
            ->andReturn('some_email')
            ->getMock()

            ->shouldReceive('getName')
            ->andReturn('some_name')
            ->getMock();

        /** @var UuidInterface $uuid */
        $uuid = Mockery::mock(UuidInterface::class)
            ->shouldReceive('toString')
            ->andReturn('some_uuid')
            ->getMock();

        /** @var UuidFactoryInterface $uuidFactory */
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class)
            ->shouldReceive('uuid4')
            ->andReturn($uuid)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
        ->shouldReceive('trans')
        ->with(
            'sender.hipchat.user_registered',
            [
                '%vendor_id%'   => 'some_id',
                '%vendor_name%' => 'some_vendor',
                '%user_name%'   => 'some_name',
                '%user_email%'  => 'some_email',
            ]
        )
        ->andReturn('some_message')
        ->getMock();

        $expectedMessage = new Message(
            'internal_connect_message',
            'some_uuid',
            0,
            'hipchat',
            [
                'token' => ['some_token'],
            ],
            [
                'room_id' => [
                    'values' => ['some_sales_room_id']
                ],
                'content' => [
                    'values' => ['some_message'],
                    'meta' => [
                        'message_format'    => ['html'],
                        'color'             => ['green'],
                        'notify'            => ['true'],
                    ]
                ],
            ],
            []
        );

        $factory = new HipChatFactory(
            $uuidFactory,
            $translator,
            'some_token',
            'some_sales_room_id',
            'some_news_room_id'
        );

        $message = $factory->createForUser($user, TriggerType::USER_REGISTRATION);

        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals($expectedMessage, $message);
    }

    /**
     * @test
     */
    public function createForUser_with_card_registration_should_return_message()
    {
        /** @var Vendor $vendor */
        $vendor = Mockery::mock(Vendor::class)
            ->shouldReceive('getName')
            ->andReturn('some_vendor')
            ->getMock();

        /** @var User $user */
        $user = Mockery::mock(User::class)
            ->shouldReceive('getVendor')
            ->andReturn($vendor)
            ->getMock()

            ->shouldReceive('getPrimaryContactValue')
            ->andReturn('some_email')
            ->getMock()

            ->shouldReceive('getName')
            ->andReturn('some_name')
            ->getMock();

        /** @var UuidInterface $uuid */
        $uuid = Mockery::mock(UuidInterface::class)
            ->shouldReceive('toString')
            ->andReturn('some_uuid')
            ->getMock();

        /** @var UuidFactoryInterface $uuidFactory */
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class)
            ->shouldReceive('uuid4')
            ->andReturn($uuid)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class)
        ->shouldReceive('trans')
        ->with(
            'sender.hipchat.card_registered',
            [
                '%vendor_name%' => 'some_vendor',
                '%user_name%'   => 'some_name',
                '%user_email%'  => 'some_email',
            ]
        )
        ->andReturn('some_message')
        ->getMock();

        $expectedMessage = new Message(
            'internal_connect_message',
            'some_uuid',
            0,
            'hipchat',
            [
                'token' => ['some_token'],
            ],
            [
                'room_id' => [
                    'values' => ['some_news_room_id']
                ],
                'content' => [
                    'values' => ['some_message'],
                    'meta' => [
                        'message_format'    => ['html'],
                        'color'             => ['green'],
                        'notify'            => ['true'],
                    ]
                ],
            ],
            []
        );

        $factory = new HipChatFactory(
            $uuidFactory,
            $translator,
            'some_token',
            'some_sales_room_id',
            'some_news_room_id'
        );

        $message = $factory->createForUser($user, TriggerType::CARD_REGISTRATION);

        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals($expectedMessage, $message);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Not supported trigger type: not_supported_trigger
     */
    public function createForUser_with_unsupported_trigger_should_throw_exception()
    {
        /** @var User $user */
        $user = Mockery::mock(User::class);

        /** @var UuidInterface $uuid */
        $uuid = Mockery::mock(UuidInterface::class)
            ->shouldReceive('toString')
            ->andReturn('some_uuid')
            ->getMock();

        /** @var UuidFactoryInterface $uuidFactory */
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class)
            ->shouldReceive('uuid4')
            ->andReturn($uuid)
            ->getMock();

        /** @var TranslatorInterface $translator */
        $translator = Mockery::mock(TranslatorInterface::class);

        $factory = new HipChatFactory(
            $uuidFactory,
            $translator,
            'some_token',
            'some_sales_room_id',
            'some_news_room_id'
        );

        $factory->createForUser($user, 'not_supported_trigger');
    }
}