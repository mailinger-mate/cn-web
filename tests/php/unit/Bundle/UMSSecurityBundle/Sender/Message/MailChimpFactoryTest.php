<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender\Message;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use Connect\Bundle\UMSSecurityBundle\Sender\Message\MailChimpFactory;
use Connect\Bundle\UMSSecurityBundle\Sender\TriggerType;
use Connect\Domain\User;
use LogicException;
use Mockery;
use PHPUnit_Framework_TestCase;
use Ramsey\Uuid\UuidFactoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class MailChimpFactoryTest
 */
class MailChimpFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createForUser_with_user_registration_should_return_message()
    {
        /** @var User $user */
        $user = Mockery::mock(User::class)
            ->shouldReceive('getPrimaryContactValue')
            ->andReturn('some_email')
            ->getMock()

            ->shouldReceive('getName')
            ->andReturn('some_name')
            ->getMock();

        /** @var UuidInterface $uuid */
        $uuid = Mockery::mock(UuidInterface::class)
            ->shouldReceive('toString')
            ->andReturn('some_uuid')
            ->getMock();

        /** @var UuidFactoryInterface $uuidFactory */
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class)
            ->shouldReceive('uuid4')
            ->andReturn($uuid)
            ->getMock();

        $expectedMessage = new Message(
            'internal_connect_message',
            'some_uuid',
            0,
            'mailchimp',
            [
                'access_token' => ['some_token'],
                'api_endpoint' => ['some_api_endpoint']
            ],
            [
                'action' => [
                    'values' => ['subscribe']
                ],
                'list_id' => [
                    'values' => ['some_list_id']
                ],
                'subscriber_email' => [
                    'values' => ['some_email']
                ],
                'manage_fields_keys' => [
                    'values' => ['LNAME']
                ],
                'manage_fields_values' => [
                    'values' => ['some_name']
                ],
                'interests' => [
                    'values' => ['interest1', 'interest2'],
                    'meta'   => ['append' => ['true']]
                ],
            ],
            []
        );

        $factory = new MailChimpFactory(
            $uuidFactory,
            'some_token',
            'some_api_endpoint',
            'some_list_id',
            ['interest1', 'interest2']
        );

        $message = $factory->createForUser($user, TriggerType::USER_REGISTRATION);

        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals($expectedMessage, $message);
    }

    /**
     * @test
     * @expectedException LogicException
     * @expectedExceptionMessage Not supported trigger type:
     * @dataProvider triggerTypeProvider
     *
     * @param $triggerType
     */
    public function createForUser_with_unsupported_trigger_should_throw_exception($triggerType)
    {
        /** @var User $user */
        $user = Mockery::mock(User::class);

        /** @var UuidInterface $uuid */
        $uuid = Mockery::mock(UuidInterface::class)
            ->shouldReceive('toString')
            ->andReturn('some_uuid')
            ->getMock();

        /** @var UuidFactoryInterface $uuidFactory */
        $uuidFactory = Mockery::mock(UuidFactoryInterface::class)
            ->shouldReceive('uuid4')
            ->andReturn($uuid)
            ->getMock();

        $factory = new MailChimpFactory(
            $uuidFactory,
            'some_token',
            'some_api_endpoint',
            'some_list_id',
            ['interest1', 'interest2']
        );

        $factory->createForUser($user, $triggerType);
    }

    public static function triggerTypeProvider()
    {
        return [
            [TriggerType::CARD_REGISTRATION],
            ['not_supported_trigger'],
        ];
    }
}