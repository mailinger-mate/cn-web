<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Sender;

use Connect\Bundle\UMSSecurityBundle\Sender\Message;
use PHPUnit_Framework_TestCase;

/**
 * Class MessageTest
 */
class MessageTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getAsArray_should_return_formatted_array()
    {
        $message = new Message(
            'some_con_token',
            'some_con_token',
            'some_dst_id',
            'some_type',
            ['some_acc_cfg'],
            ['some_msg_cfg'],
            ['some_attachments']
        );

        $expectedArray = [
            "con_token"      => 'some_con_token',
            "msg_token"      => 'some_con_token',
            "dst_id"         => 'some_dst_id',
            "type"           => 'some_type',
            "account_config" => ['some_acc_cfg'],
            "message_config" => ['some_msg_cfg'],
            "attachments"    => ['some_attachments']
        ];

        $this->assertSame($expectedArray, $message->getAsArray());
    }
}