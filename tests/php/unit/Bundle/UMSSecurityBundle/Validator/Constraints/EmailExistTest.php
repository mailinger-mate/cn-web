<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Validator\Constraints;

use Connect\Bundle\UMSSecurityBundle\Validator\Constraints\EmailExist;
use PHPUnit_Framework_TestCase;

/**
 * Class EmailExistsTest
 */
class EmailExistTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function validateBy_should_return_string()
    {
        $constraint = new EmailExist();
        $this->assertSame('email_exist_validator', $constraint->validatedBy());
    }
}
