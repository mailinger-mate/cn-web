<?php

namespace Connect\Test\Unit\Bundle\UMSSecurityBundle\Validator\Constraints;

use Connect\Bundle\UMSSecurityBundle\Repository\UMSUserRepository;
use Connect\Bundle\UMSSecurityBundle\Security\UMSUser;
use Connect\Bundle\UMSSecurityBundle\Validator\Constraints\EmailExist;
use Connect\Bundle\UMSSecurityBundle\Validator\Constraints\EmailExistValidator;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

/**
 * Class EmailExistValidatorTest
 */
class EmailExistValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function validate_with_existing_email_should_not_report_violation()
    {
        $user = new UMSUser();
        $context = Mockery::mock(ExecutionContextInterface::class)
            ->shouldReceive('buildViolation')
            ->never()
            ->getMock();

        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByEmail')
            ->once()
            ->with('some@user.com')
            ->andReturn($user)
            ->getMock();
        $constraint = Mockery::mock(Constraint::class);

        /** @var UMSUserRepository $userRepository */
        /** @var ExecutionContextInterface $context */
        $validator = new EmailExistValidator($userRepository);
        $validator->initialize($context);
        /** @var Constraint $constraint */
        $validator->validate('some@user.com', $constraint);
    }

    /**
     * @test
     */
    public function validate_with_not_existing_email_should_report_violation()
    {
        $violationBuilder = Mockery::mock(ConstraintViolationBuilderInterface::class)
            ->shouldReceive('addViolation')
            ->once()
            ->getMock();
        $context = Mockery::mock(ExecutionContextInterface::class)
            ->shouldReceive('buildViolation')
            ->once()
            ->andReturn($violationBuilder)
            ->getMock();
        $userRepository = Mockery::mock(UMSUserRepository::class)
            ->shouldReceive('findUserByEmail')
            ->once()
            ->with('some@user.com')
            ->andReturn(false)
            ->getMock();
        $constraint = new EmailExist();

        /** @var UMSUserRepository $userRepository */
        /** @var ExecutionContextInterface $context */
        $validator = new EmailExistValidator($userRepository);
        $validator->initialize($context);
        /** @var Constraint $constraint */
        $validator->validate('some@user.com', $constraint);
    }
}
