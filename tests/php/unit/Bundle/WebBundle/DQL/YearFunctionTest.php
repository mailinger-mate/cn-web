<?php

namespace Connect\Test\Unit\Bundle\WebBundle\DQL;

use Connect\Bundle\WebBundle\DQL\YearFunction;
use Doctrine\ORM\Query\AST\AggregateExpression;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Mockery;
use PHPUnit_Framework_TestCase;

class YearFunctionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function parse_should_parse_sql()
    {
        /** @var Parser $parser */
        $parser = Mockery::mock(Parser::class)
            ->shouldReceive('match')
            ->once()
            ->with(100)
            ->shouldReceive('match')
            ->once()
            ->with(7)
            ->shouldReceive('ArithmeticPrimary')
            ->once()
            ->shouldReceive('match')
            ->once()
            ->with(6)
            ->getMock();

        (new YearFunction('name'))->parse($parser);
    }

    /**
     * @test
     */
    public function getSql_should_return_string()
    {
        /** @var SqlWalker $sqlWalker */
        $sqlWalker = Mockery::mock(SqlWalker::class);

        $yearFunction = new YearFunction('name');
        $yearFunction->yearExpression = Mockery::mock(AggregateExpression::class)
            ->shouldReceive('dispatch')
            ->once()
            ->with($sqlWalker)
            ->andReturn('foo')
            ->getMock();

        $this->assertSame('YEAR(foo)', $yearFunction->getSql($sqlWalker));
    }
}
