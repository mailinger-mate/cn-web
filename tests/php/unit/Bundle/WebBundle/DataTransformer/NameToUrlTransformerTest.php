<?php

namespace Connect\Test\Unit\Bundle\WebBundle\DataTransformer;

use Connect\Bundle\WebBundle\DataTransformer\NameToUrlTransformer;
use Connect\Bundle\WebBundle\Entity\PostImage;
use Connect\Bundle\WebBundle\Generator\S3Url;
use Mockery;
use PHPUnit_Framework_TestCase;

class NameToUrlTransformerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException \Symfony\Component\Form\Exception\TransformationFailedException
     * @expectedExceptionMessage Reverse transform not supported
     */
    public function reverseTransform_should_throw_exception()
    {
        /** @var S3Url $generator */
        $generator = Mockery::mock(S3Url::class);
        (new NameToUrlTransformer($generator, []))->reverseTransform('foo');
    }

    /**
     * @test
     */
    public function transform_with_empty_images_should_return_name()
    {
        /** @var S3Url $generator */
        $generator = Mockery::mock(S3Url::class)
            ->shouldReceive('generate')
            ->never()
            ->getMock();

        $this->assertSame(
            'foo',
            (new NameToUrlTransformer($generator, []))->transform('foo')
        );
    }

    /**
     * @test
     */
    public function transform_with_missing_image_should_return_name()
    {
        $images = [
            (new PostImage())->setName('Bar')
        ];

        /** @var S3Url $generator */
        $generator = Mockery::mock(S3Url::class)
            ->shouldReceive('generate')
            ->never()
            ->getMock();

        $this->assertSame(
            'foo',
            (new NameToUrlTransformer($generator, $images))->transform('foo')
        );
    }

    /**
     * @test
     */
    public function transform_with_image_should_return_url()
    {
        $images = [
            (new PostImage())->setName('foo')->setPath('fake/path')
        ];

        /** @var S3Url $generator */
        $generator = Mockery::mock(S3Url::class)
            ->shouldReceive('generate')
            ->once()
            ->with('fake/path')
            ->andReturn('http://whatever.dev/fake/path')
            ->getMock();

        $this->assertSame(
            'http://whatever.dev/fake/path',
            (new NameToUrlTransformer($generator, $images))->transform('foo')
        );
    }
}
