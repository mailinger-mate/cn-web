<?php

namespace Connect\Test\Unit\Bundle\WebBundle\Entity;

use Connect\Bundle\WebBundle\Entity\Post;
use PHPUnit_Framework_TestCase;

class PostTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getIntro_with_intro_set_should_return_intro()
    {
        $post = (new Post())
            ->setContent('post content')
            ->setIntro('this is intro');

        $this->assertSame('this is intro', $post->getIntro());
    }

    /**
     * @test
     * @dataProvider validDataProvider
     * @param $content
     * @param $expectedIntro
     */
    public function getIntro_without_intro_set_should_return_beginning_of_content($content, $expectedIntro)
    {
        $post = (new Post())
            ->setContent($content);

        $this->assertSame($expectedIntro, $post->getIntro());
    }

    /**
     * @return array
     */
    public static function validDataProvider()
    {
        return [
            // content 29 words, intro = content
            [
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo felis, consequat' .
                ' et commodo in, porta ut ipsum. Nunc tellus metus, efficitur non augue at, venenatis' .
                ' ultrices nisi. Maecenas.',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo felis, consequat' .
                ' et commodo in, porta ut ipsum. Nunc tellus metus, efficitur non augue at, venenatis' .
                ' ultrices nisi. Maecenas.',
            ],
            // content 30 words, intro = content
            [
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id nibh libero. Nam' .
                ' vel neque sit amet est iaculis interdum vel quis erat. Class aptent taciti sociosqu' .
                ' ad litora torquent.',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id nibh libero. Nam' .
                ' vel neque sit amet est iaculis interdum vel quis erat. Class aptent taciti sociosqu' .
                ' ad litora torquent.',
            ],
            // content 31 words, intro = first 30 words
            [
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vitae nisi in nunc' .
                ' pellentesque egestas eget in felis. Donec tristique nec tortor et cursus. In posuere' .
                ' ante in risus scelerisque condimentum.',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vitae nisi in nunc' .
                ' pellentesque egestas eget in felis. Donec tristique nec tortor et cursus. In posuere' .
                ' ante in risus scelerisque',
            ],
            // special chars
            [
                'L/.,o\';l\][r=-em i?><p":Ls|}{u+_m d)(*&o^%$l#@!or 1234567890',
                'L/.,o\';l\][r=-em i?><p":Ls|}{u+_m d)(*&o^%$l#@!or 1234567890',
            ]
        ];
    }
}
