<?php

namespace Connect\Test\Unit\Bundle\WebBundle\EventListener;

use Connect\Bundle\AppBundle\Controller\AppController;
use Connect\Bundle\WebBundle\Controller\HomeController;
use Connect\Bundle\WebBundle\EventListener\RedirectControllerListener;
use Mockery;
use PHPUnit_Framework_TestCase;
use stdClass;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class RedirectControllerListenerTest
 */
class RedirectControllerListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function onKernelController_with_controller_not_array_should_not_do_anything()
    {
        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class);
        $homeController = Mockery::mock(HomeController::class);
        $router = Mockery::mock(RouterInterface::class);

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getController')
            ->andReturnNull()
            ->getMock();

        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var FilterControllerEvent $event */
        /** @var HomeController $homeController */
        /** @var RouterInterface $router */
        (new RedirectControllerListener(
            $authChecker,
            $homeController,
            $router
        ))->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_not_home_controller_should_not_do_anything()
    {
        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(false)
            ->getMock();;
        $homeController = Mockery::mock(HomeController::class);
        $router = Mockery::mock(RouterInterface::class);

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getController')
            ->andReturn([new stdClass()])
            ->getMock();

        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var FilterControllerEvent $event */
        /** @var HomeController $homeController */
        /** @var RouterInterface $router */
        (new RedirectControllerListener(
            $authChecker,
            $homeController,
            $router
        ))->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_index_action_and_user_not_authenticated_should_set_landing()
    {
        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(false)
            ->getMock();

        $homeController = Mockery::mock(HomeController::class);
        $router = Mockery::mock(RouterInterface::class);

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getController')
            ->andReturn([$homeController, 'indexAction'])
            ->getMock();
        $event->shouldReceive('setController')
            ->with([$homeController, 'landingPageAction']);

        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var FilterControllerEvent $event */
        /** @var HomeController $homeController */
        /** @var RouterInterface $router */
        (new RedirectControllerListener(
            $authChecker,
            $homeController,
            $router
        ))->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_index_action_and_user_authenticated_should_set_closure_redirect_controller()
    {
        $redirectUrl = 'some_url';

        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(true)
            ->getMock();

        $homeController = Mockery::mock(HomeController::class);
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn($redirectUrl)
            ->getMock();

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getController')
            ->andReturn([$homeController, 'indexAction'])
            ->getMock()

            ->shouldReceive('setController')
            ->with(Mockery::on(function ($closure) use ($redirectUrl) {
                /** @var RedirectResponse $redirect */
                $redirect = $closure();
                $this->assertInstanceOf(RedirectResponse::class, $redirect);
                $this->assertSame(302, $redirect->getStatusCode());
                $this->assertSame($redirectUrl, $redirect->getTargetUrl());

                return true;
            }))
            ->getMock();

        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var FilterControllerEvent $event */
        /** @var HomeController $homeController */
        /** @var RouterInterface $router */
        (new RedirectControllerListener(
            $authChecker,
            $homeController,
            $router
        ))->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_not_index_action_and_user_authenticated_should_set_closure_redirect_controller()
    {
        $redirectUrl = 'some_url';

        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(true)
            ->getMock();

        $homeController = Mockery::mock(HomeController::class);
        $router = Mockery::mock(RouterInterface::class)
            ->shouldReceive('generate')
            ->with('app')
            ->andReturn($redirectUrl)
            ->getMock();

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getController')
            ->andReturn([$homeController, 'notIndexAction'])
            ->getMock()

            ->shouldReceive('setController')
            ->with(Mockery::on(function ($closure) use ($redirectUrl) {
                /** @var RedirectResponse $redirect */
                $redirect = $closure();
                $this->assertInstanceOf(RedirectResponse::class, $redirect);
                $this->assertSame(302, $redirect->getStatusCode());
                $this->assertSame($redirectUrl, $redirect->getTargetUrl());

                return true;
            }))
            ->getMock();

        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var FilterControllerEvent $event */
        /** @var HomeController $homeController */
        /** @var RouterInterface $router */
        (new RedirectControllerListener(
            $authChecker,
            $homeController,
            $router
        ))->onKernelController($event);
    }

    /**
     * @test
     */
    public function onKernelController_with_home_controller_and_user_not_authenticated_should_not_do_anything()
    {
        $authChecker = Mockery::mock(AuthorizationCheckerInterface::class)
            ->shouldReceive('isGranted')
            ->with('IS_AUTHENTICATED_FULLY')
            ->andReturn(false)
            ->getMock();

        $homeController = Mockery::mock(HomeController::class);
        $router = Mockery::mock(RouterInterface::class);

        $event = Mockery::mock(FilterControllerEvent::class)
            ->shouldReceive('getController')
            ->andReturn([$homeController, 'notIndexAction'])
            ->getMock();

        /** @var AuthorizationCheckerInterface $authChecker */
        /** @var FilterControllerEvent $event */
        /** @var HomeController $homeController */
        /** @var RouterInterface $router */
        (new RedirectControllerListener(
            $authChecker,
            $homeController,
            $router
        ))->onKernelController($event);
    }
}
