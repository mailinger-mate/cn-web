<?php

namespace Connect\Test\Unit\Bundle\WebBundle\Generator;

use Connect\Bundle\WebBundle\Generator\S3Url;

class S3UrlTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider validDataProvider
     *
     * @param $domain
     * @param $path
     */
    public function generate_with_valid_data_should_return_url($domain, $path)
    {
        $generator = new S3Url($domain);

        $this->assertSame(
            'http://s3.dev/fake/path',
            $generator->generate($path)
        );
    }

    /**
     * @return array
     */
    public static function validDataProvider()
    {
        return [
            ['http://s3.dev/', '/fake/path'],
            ['http://s3.dev', '/fake/path'],
            ['http://s3.dev/', 'fake/path'],
            ['http://s3.dev', 'fake/path'],
        ];
    }
}
