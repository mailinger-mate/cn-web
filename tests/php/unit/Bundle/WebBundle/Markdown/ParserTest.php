<?php

namespace Connect\Test\Unit\Bundle\WebBundle\Markdown;

use Connect\Bundle\WebBundle\Markdown\Parser;
use Mockery;
use Symfony\Component\Form\DataTransformerInterface;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider validDataProvider
     *
     * @param string $inputMarkdown
     * @param string $expectedHtml
     * @param bool $urlTransformer
     */
    public function transformMarkdown_should_inject_image_url(
        $inputMarkdown,
        $expectedHtml,
        $urlTransformer = false
    ) {
        $parser = new Parser();

        if ($urlTransformer) {
            $transformer = Mockery::mock(DataTransformerInterface::class)
                ->shouldReceive('transform')
                ->with('foo.png')
                ->andReturn('http://s3.dev/images/foo.png')
            ->getMock();
        } else {
            $transformer = null;
        }

        $this->assertSame(
            $expectedHtml,
            $parser->transformMarkdown($inputMarkdown, $transformer)
        );
    }

    public static function validDataProvider()
    {
        return [
            // without transformer should leave url untouched
            [
                '![example image](foo.png)',
                "<p><img src=\"foo.png\" alt=\"example image\" /></p>\n",
            ],
            // with transformer should change url
            [
                '![example image](foo.png)',
                "<p><img src=\"http://s3.dev/images/foo.png\" alt=\"example image\" /></p>\n",
                true,
            ],
            // with transformer should change url (with title)
            [
                '![example image](foo.png "some title")',
                "<p><img src=\"http://s3.dev/images/foo.png\" alt=\"example image\" title=\"some title\" /></p>\n",
                true,
            ],
        ];
    }
}
