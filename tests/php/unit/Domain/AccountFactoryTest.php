<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\Account;
use Connect\Domain\AccountFactory;
use PHPUnit_Framework_TestCase;

/**
 * Class AccountFactoryTest
 */
class AccountFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $name = 'some account name';
        $type = 'some account type';
        $config = ['some_key' => 'some_value'];

        $data = [
            'id'        => '10',
            'name'      => $name,
            'type'      => $type,
            'config'    => $config,
        ];

        $expectedAccountObject = (new Account())
            ->setId(10)
            ->setName($name)
            ->setType($type)
            ->setConfig($config);

        $accountObject = (new AccountFactory())->createFromArray($data);

        $this->assertEquals($expectedAccountObject, $accountObject);
    }
}
