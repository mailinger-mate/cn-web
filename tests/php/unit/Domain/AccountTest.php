<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\Account;
use Connect\Domain\Contact;
use Connect\Domain\Token;
use InvalidArgumentException;
use PHPUnit_Framework_TestCase;

/**
 * Class AccountTest
 */
class AccountTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getConfig_without_field_should_return_config_array()
    {
        $config = [
            'key1' => 'value1',
            'key2' => 'value2',
        ];

        $account = new Account();
        $account->setConfig($config);

        $this->assertSame($config, $account->getConfig());
    }

    /**
     * @test
     */
    public function getConfig_with_existing_field_should_return_config_field_value()
    {
        $config = [
            'key1' => 'value1',
            'key2' => 'value2',
        ];

        $account = new Account();
        $account->setConfig($config);

        $this->assertSame($config['key1'], $account->getConfig('key1'));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Config does not contain field: missing_key
     */
    public function getConfig_without_existing_field_should_throw_exception()
    {
        $account = new Account();
        $account->setConfig([
            'key1' => 'value1',
            'key2' => 'value2',
        ]);

        $account->getConfig('missing_key');
    }
}