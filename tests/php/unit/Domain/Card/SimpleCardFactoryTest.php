<?php

namespace Connect\Test\Unit\Domain\Card;

use Connect\Domain\Card\SimpleCard;
use Connect\Domain\Card\SimpleCardFactory;
use PHPUnit_Framework_TestCase;

class SimpleCardFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $data = [
            'brand'     => 'Visa',
            'exp_month' => 2,
            'exp_year'  => 2022,
            'last4'     => '0334',
        ];
        $card = (new SimpleCard())
            ->setBrand('Visa')
            ->setExpirationMonth(2)
            ->setExpirationYear(2022)
            ->setLastFourDigits('0334');

        $resultCard = ((new SimpleCardFactory())->createFromArray($data));
        $this->assertEquals($card, $resultCard);
    }
}
