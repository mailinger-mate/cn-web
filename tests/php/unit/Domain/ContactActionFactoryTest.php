<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\Contact;
use Connect\Domain\ContactAction;
use Connect\Domain\ContactActionFactory;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class ContactActionFactoryTest
 * @package Connect\Test\Unit\Domain
 */
class ContactActionFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $data = [
            'id' => 22,
            'type' => 'verify',
            'expiration_time'=>'2015-03-23T09:28:15+0000'
        ];
        $contact = (new Contact())->setId(1)->setValue('whatever');
        $expectedAction = (new ContactAction())
            ->setId(22)
            ->setType('verify')
            ->setExpirationTime(new DateTime('2015-03-23T09:28:15+0000'))
            ->setContact($contact);

        $resultAction = ((new ContactActionFactory())->createFromArray($data, $contact));
        $this->assertEquals($expectedAction, $resultAction);
    }
}
