<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\Session;
use Connect\Domain\SessionFactory;
use Connect\Domain\User;
use PHPUnit_Framework_TestCase;

class SessionFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $data = [
            'id'              => 'some-id',
        ];

        $user = (new User())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(true)
            ->setEnabled(false);

        $expectedObject = (new Session())
            ->setId('some-id')
            ->setUser($user);

        $result = (new SessionFactory())->createFromArray($data, $user);

        $this->assertEquals($expectedObject, $result);
    }
    /**
     * @test
     */
    public function createFromArray_without_user()
    {
        $data = [
            'id'              => 'some-id',
        ];

        $expectedObject = (new Session())
            ->setId('some-id');

        $result = (new SessionFactory())->createFromArray($data);

        $this->assertEquals($expectedObject, $result);
    }
}
