<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\User;
use Connect\Domain\UserAction;
use Connect\Domain\UserActionFactory;
use DateTime;
use PHPUnit_Framework_TestCase;

class UserActionFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $data = [
            'id' => 22,
            'type' => 'pass_reset',
            'expiration_time'=>'2015-03-23T09:28:15+0000'
        ];
        $user = (new User())->setId(1)->setName('whatever');
        $expectedAction = (new UserAction())
            ->setId(22)
            ->setType('pass_reset')
            ->setExpirationTime(new DateTime('2015-03-23T09:28:15+0000'))
            ->setUser($user);

        $resultAction = ((new UserActionFactory())->createFromArray($data, $user));
        $this->assertEquals($expectedAction, $resultAction);
    }
}
