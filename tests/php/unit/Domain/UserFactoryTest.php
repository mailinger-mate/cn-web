<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\User;
use Connect\Domain\UserFactory;
use Connect\Domain\Vendor;
use PHPUnit_Framework_TestCase;

/**
 * Class UserFactoryTest
 */
class UserFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createFromArray()
    {
        $data = [
            'id'        => '1',
            'name'      => 'John Doe',
            'activated' => true,
            'enabled'   => false
        ];

        $vendor = (new Vendor());
        $expectedObject = (new User())
            ->setId(1)
            ->setName('John Doe')
            ->setActivated(true)
            ->setEnabled(false)
            ->setVendor($vendor);

        $result = (new UserFactory())->createFromArray($data, $vendor);

        $this->assertEquals($expectedObject, $result);
    }
}
