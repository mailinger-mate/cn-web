<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\Contact;
use Connect\Domain\User;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class UserTest
 */
class UserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getPrimaryContact_should_return_first_contact_value()
    {
        $user = new User();
        $contact = (new Contact())
            ->setValue('some-email')
            ->setType('email')
            ->setVerified(true);

        $user->addContact($contact);

        $this->assertSame($contact, $user->getPrimaryContact());
    }

    /**
     * @test
     * @dataProvider contactsDataProvider
     * @expectedException \LogicException
     * @expectedExceptionMessage User do not have any contacts
     *
     * @param array $contacts
     */
    public function getPrimaryContact_should_throw_exception_if_number_of_contacts_is_not_one(array $contacts)
    {
        $user = new User();

        foreach ($contacts as $contact) {
            $user->addContact($contact);
        }

        $user->getPrimaryContact();
    }

    /**
     * @return array
     */
    public static function contactsDataProvider()
    {
        return [
            //0)
            // zero contacts
            [[]],
            // 1)
            // more then 1
            [
                [
                    (new Contact())->setValue('some-email')->setType('email')->setVerified(true),
                    (new Contact())->setValue('some-email-2')->setType('email')->setVerified(true),
                ]
            ]
        ];
    }
}
