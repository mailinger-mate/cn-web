<?php

namespace Connect\Test\Unit\Domain;

use Connect\Domain\Customer;
use Connect\Domain\Vendor;
use Connect\Domain\VendorFactory;
use PHPUnit_Framework_TestCase;

/**
 * Class VendorFactoryTest
 */
class VendorFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider validDataProvider
     * @param array $data
     * @param Vendor $expectedVendorObject
     */
    public function createFromArray(array $data, Vendor $expectedVendorObject)
    {
        $data = [
            'id'   => '1',
            'name' => 'John Doe',
        ];
        $customer = (new Customer())->setId('cust.id');

        $expectedVendorObject = (new Vendor())
            ->setId(1)
            ->setName('John Doe')
            ->setCustomer($customer);

        $vendorObject = (new VendorFactory())->createFromArray($data, $customer);

        $this->assertEquals($expectedVendorObject, $vendorObject);
    }

    /**
     * @return array
     */
    public static function validDataProvider()
    {
        return [
            // vendor without billing data
            [
                [
                    'id'   => '1',
                    'name' => 'John Doe',
                ],
                (new Vendor())->setId(1)->setName('John Doe'),
            ],
            // vendor with billing data
            [
                [
                    'id'           => '1',
                    'name'         => 'John Doe',
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'street'       => 'Radzikowskiego 47A',
                    'city'         => '31-315 Kraków',
                    'state'        => 'Małopolska',
                    'country'      => 'PL',
                    'vat_number'   => 'PL11223344',
                    'note'         => 'New license',
                    'po_number'    => '#order-no: 112233',
                ],
                (new Vendor())
                    ->setId(1)
                    ->setName('John Doe')
                    ->setBillingName("X-Formation\nPoland Sp. z o.o.")
                    ->setStreet('Radzikowskiego 47A')
                    ->setCity('31-315 Kraków')
                    ->setState('Małopolska')
                    ->setCountry('PL')
                    ->setVatNumber('PL11223344')
                    ->setNote('New license')
                    ->setPoNumber('#order-no: 112233')
            ],
            // vendor with partial billing data
            [
                [
                    'id'           => '1',
                    'name'         => 'John Doe',
                    'billing_name' => "X-Formation\nPoland Sp. z o.o.",
                    'street'       => 'Radzikowskiego 47A',
                    'city'         => '31-315 Kraków',
                    'country'      => 'PL',
                    'vat_number'   => 'PL11223344',
                ],
                (new Vendor())
                    ->setId(1)
                    ->setName('John Doe')
                    ->setBillingName("X-Formation\nPoland Sp. z o.o.")
                    ->setStreet('Radzikowskiego 47A')
                    ->setCity('31-315 Kraków')
                    ->setCountry('PL')
                    ->setVatNumber('PL11223344')
            ],
        ];
    }
}
